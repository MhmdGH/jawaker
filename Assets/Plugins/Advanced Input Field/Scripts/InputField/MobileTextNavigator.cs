﻿//-----------------------------------------
//			Advanced Input Field
// Copyright (c) 2017 Jeroen van Pienbroek
//------------------------------------------

#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS || UNITY_WSA)
using UnityEngine;
using UnityEngine.UI;

namespace AdvancedInputFieldPlugin
{
	/// <summary>The TextNavigator for mobile platforms</summary>
	public class MobileTextNavigator: TextNavigator
	{
		/// <summary>The thumb size multiplier used for selection cursors size calculations</summary>
		private const float THUMB_SIZE_SCALE = 0.55f;

		/// <summary>The TouchScreenKeyboard</summary>
		private NativeKeyboard keyboard;

		/// <summary>The Transform of the selection cursors</summary>
		private Transform selectionCursorsTransform;

		/// <summary>The start cursor renderer</summary>
		private Image startCursor;

		/// <summary>The end cursor renderer</summary>
		private Image endCursor;

		/// <summary>The current cursor renderer</summary>
		private Image currentCursor;

		/// <summary>The size of the cursors</summary>
		private float cursorSize;

		/// <summary>Indicates whether at least one character has been inserted (or deleted) afer last click</summary>
		public bool hasInsertedCharAfterClick;

		/// <summary>Indicates whether at least one character has been inserted (or deleted) afer last click</summary>
		public bool HasInsertedCharAfterClick
		{
			get { return hasInsertedCharAfterClick; }
			set
			{
				hasInsertedCharAfterClick = value;
				currentCursor.enabled = (!hasInsertedCharAfterClick && !HasSelection);
			}
		}

		/// <summary>The TouchScreenKeyboard</summary>
		private NativeKeyboard Keyboard
		{
			get
			{
				if(keyboard == null)
				{
					keyboard = NativeKeyboardManager.Keyboard;
				}

				return keyboard;
			}
		}

		public override int ProcessedCaretPosition
		{
			get { return base.ProcessedCaretPosition; }
			set
			{
				base.ProcessedCaretPosition = value;

				if(Keyboard.State == KeyboardState.VISIBLE && !InputField.ReadOnly && !BlockNativeSelectionChange)
				{
					UpdateNativeSelection();
				}
			}
		}

		public override int SelectionStartPosition
		{
			get { return base.SelectionStartPosition; }
			protected set
			{
				int originalValue = base.SelectionStartPosition;
				base.SelectionStartPosition = value;

				if(InputField.SelectionCursorsEnabled)
				{
					if(Canvas != null)
					{
						UpdateSelectionCursors(Canvas.scaleFactor);
					}
				}

				if(InputField.ActionBarEnabled && base.SelectionStartPosition != originalValue)
				{
					UpdateSelectionCursorsActionBar();
				}

				if(Keyboard.State == KeyboardState.VISIBLE && !InputField.ReadOnly && !BlockNativeSelectionChange)
				{
					UpdateNativeSelection();
				}
			}
		}

		public override int ProcessedSelectionStartPosition
		{
			get { return base.ProcessedSelectionStartPosition; }
			protected set
			{
				int originalValue = base.ProcessedSelectionStartPosition;
				base.ProcessedSelectionStartPosition = value;

				if(InputField.SelectionCursorsEnabled)
				{
					if(Canvas != null)
					{
						UpdateSelectionCursors(Canvas.scaleFactor);
					}
				}

				if(InputField.ActionBarEnabled && base.ProcessedSelectionStartPosition != originalValue)
				{
					UpdateSelectionCursorsActionBar();
				}

				if(Keyboard.State == KeyboardState.VISIBLE && !InputField.ReadOnly && !BlockNativeSelectionChange)
				{
					UpdateNativeSelection();
				}
			}
		}

		public override int SelectionEndPosition
		{
			get { return base.SelectionEndPosition; }
			protected set
			{
				int originalValue = base.selectionEndPosition;
				base.SelectionEndPosition = value;

				if(InputField.SelectionCursorsEnabled)
				{
					if(Canvas != null)
					{
						UpdateSelectionCursors(Canvas.scaleFactor);
					}
				}

				if(InputField.ActionBarEnabled && base.SelectionEndPosition != originalValue)
				{
					UpdateSelectionCursorsActionBar();
				}

				if(Keyboard.State == KeyboardState.VISIBLE && !InputField.ReadOnly && !BlockNativeSelectionChange)
				{
					UpdateNativeSelection();
				}
			}
		}

		public override int ProcessedSelectionEndPosition
		{
			get { return base.ProcessedSelectionEndPosition; }
			protected set
			{
				int originalValue = base.ProcessedSelectionEndPosition;
				base.ProcessedSelectionEndPosition = value;

				if(InputField.SelectionCursorsEnabled)
				{
					if(Canvas != null)
					{
						UpdateSelectionCursors(Canvas.scaleFactor);
					}
				}

				if(InputField.ActionBarEnabled && base.ProcessedSelectionEndPosition != originalValue)
				{
					UpdateSelectionCursorsActionBar();
				}

				if(Keyboard.State == KeyboardState.VISIBLE && !InputField.ReadOnly && !BlockNativeSelectionChange)
				{
					UpdateNativeSelection();
				}
			}
		}

		/// <summary>The ActionBar</summary>
		public ActionBar ActionBar { get; set; }

		internal override void Initialize(AdvancedInputField inputField, Image caretRenderer, TextSelectionRenderer selectionRenderer)
		{
			base.Initialize(inputField, caretRenderer, selectionRenderer);

			selectionCursorsTransform = GameObject.Instantiate(Settings.MobileSelectionCursorsPrefab);
			selectionCursorsTransform.SetParent(TextRenderer.transform);
			selectionCursorsTransform.localScale = Vector3.one;
			selectionCursorsTransform.localPosition = Vector3.zero;
			selectionCursorsTransform.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;

			startCursor = selectionCursorsTransform.Find("StartCursor").GetComponent<Image>();
			endCursor = selectionCursorsTransform.Find("EndCursor").GetComponent<Image>();
			currentCursor = selectionCursorsTransform.Find("CurrentCursor").GetComponent<Image>();

			if(Canvas != null)
			{
				UpdateCursorSize(Canvas.scaleFactor);
			}

			startCursor.enabled = false;
			endCursor.enabled = false;
			currentCursor.enabled = false;

			startCursor.GetComponent<PointerListener>().RegisterCallbacks(OnStartCursorDown, OnStartCursorUp);
			endCursor.GetComponent<PointerListener>().RegisterCallbacks(OnEndCursorDown, OnEndCursorUp);
			currentCursor.GetComponent<PointerListener>().RegisterCallbacks(OnCurrentCursorDown, OnCurrentCursorUp);
		}

		internal override void SetCaretPosition(int caretPosition, bool invokeCaretPositonChangeEvent = false)
		{
			base.SetCaretPosition(caretPosition, invokeCaretPositonChangeEvent);

			if(Keyboard.State == KeyboardState.VISIBLE && !InputField.ReadOnly && !BlockNativeSelectionChange)
			{
				UpdateNativeSelection();
			}
		}

		internal override void OnCanvasScaleChanged(float canvasScaleFactor)
		{
			base.OnCanvasScaleChanged(canvasScaleFactor);

			UpdateCursorSize(canvasScaleFactor);
		}

		internal void UpdateCursorSize(float canvasScaleFactor)
		{
			int thumbSize = Util.DetermineThumbSize(GetCamera());
			if(thumbSize <= 0)
			{
				Debug.LogWarning("Unknown DPI");
				if(InputField.TextRenderer.resizeTextForBestFit)
				{
					cursorSize = InputField.TextRenderer.cachedTextGenerator.fontSizeUsedForBestFit / canvasScaleFactor;
				}
				else
				{
					cursorSize = InputField.TextRenderer.fontSize / canvasScaleFactor;
				}
			}
			else
			{
				cursorSize = (thumbSize * THUMB_SIZE_SCALE) / canvasScaleFactor;
			}

			cursorSize *= Settings.MobileSelectionCursorsScale;

			startCursor.rectTransform.sizeDelta = new Vector2(cursorSize, cursorSize);
			endCursor.rectTransform.sizeDelta = new Vector2(cursorSize, cursorSize);
			currentCursor.rectTransform.sizeDelta = new Vector2(cursorSize, cursorSize);
		}

		internal Camera GetCamera()
		{
			if(Canvas != null && Canvas.worldCamera != null) { return Canvas.worldCamera; }
			if(Camera.main != null) { return Camera.main; }

			return GameObject.FindObjectOfType<Camera>();
		}

		internal override void BeginEditMode()
		{
			base.BeginEditMode();

			if(Canvas != null)
			{
				UpdateCursorSize(Canvas.scaleFactor);
			}
		}

		internal override void EndEditMode()
		{
			EditMode = false;
			caretBlinkTime = InputField.CaretBlinkRate;
			CaretRenderer.enabled = false;
			UpdateSelection(0, 0);

			TextOffset = 0;

			startCursor.enabled = false;
			endCursor.enabled = false;
			currentCursor.enabled = false;

			if(ActionBar != null)
			{
				ActionBar.Hide();
			}
		}

		internal void HideCurrentMobileCursor()
		{
			currentCursor.enabled = false;
		}

		internal void UpdateNativeSelection()
		{
			if(HasSelection)
			{
				Keyboard.ChangeSelection(selectionStartPosition, selectionEndPosition);
			}
			else
			{
				Keyboard.ChangeSelection(caretPosition, caretPosition);
			}
		}

		/// <summary>Event callback when start cursor is pressed</summary>
		internal void OnStartCursorDown()
		{
			InputField.CurrentDragMode = DragMode.FROM_SELECTION_END; //We're going to move the start cursor, so start from end cursor
			InputField.DragOffset = new Vector2(startCursor.rectTransform.rect.width * 0.5f, startCursor.rectTransform.rect.height);
		}

		/// <summary>Event callback when start cursor is released</summary>
		internal void OnStartCursorUp()
		{
		}

		/// <summary>Event callback when end cursor is pressed</summary>
		internal void OnEndCursorDown()
		{
			InputField.CurrentDragMode = DragMode.FROM_SELECTION_START; //We're going to move the end cursor, so start from start cursor
			InputField.DragOffset = new Vector2(-startCursor.rectTransform.rect.width * 0.5f, startCursor.rectTransform.rect.height);
		}

		/// <summary>Event callback when end cursor is released</summary>
		internal void OnEndCursorUp()
		{
		}

		/// <summary>Event callback when current cursor is pressed</summary>
		internal void OnCurrentCursorDown()
		{
			if(!InputField.ActionBarEnabled)
			{
				return;
			}

			if(ActionBar.Visible)
			{
				ActionBar.Hide();
			}
			else
			{
				bool paste = !InputField.ReadOnly && InputField.ActionBarPaste;
				bool selectAll = InputField.ActionBarSelectAll;
				ActionBar.Show(false, false, paste, selectAll);
			}
		}

		/// <summary>Event callback when current cursor is released</summary>
		internal void OnCurrentCursorUp()
		{
		}

		internal void ShowActionBar()
		{
			bool paste = !InputField.ReadOnly && InputField.ActionBarPaste;
			bool selectAll = InputField.ActionBarSelectAll && InputField.Text.Length > 0;
			ActionBar.Show(false, false, paste, selectAll);
		}

		/// <summary>Updates the selection cursors</summary>
		internal void UpdateSelectionCursors(float canvasScaleFactor)
		{
			if(SelectionEndPosition > SelectionStartPosition)
			{
				TextGenerator textGenerator = TextRenderer.cachedTextGenerator;
				int lineHeight = textGenerator.lines[0].height;

				if(VisibleSelectionStartPosition >= 0)
				{
					int charIndex = Mathf.Clamp(VisibleSelectionStartPosition, 0, textGenerator.characterCount - 1);
					UICharInfo charInfo = textGenerator.characters[charIndex];
					int lineIndex = DetermineCharacterLine(textGenerator, charIndex);
					UILineInfo lineInfo = textGenerator.lines[lineIndex];

					Vector2 startCursorPosition = new Vector2(charInfo.cursorPos.x, lineInfo.topY - lineInfo.height);
					startCursor.rectTransform.anchoredPosition = startCursorPosition / canvasScaleFactor;
					startCursor.enabled = true;
				}
				else
				{
					startCursor.enabled = false;
				}

				if(VisibleSelectionEndPosition >= 0)
				{
					int charIndex = Mathf.Clamp(VisibleSelectionEndPosition, 0, textGenerator.characterCount - 1);
					UICharInfo charInfo = textGenerator.characters[charIndex];
					int lineIndex = DetermineCharacterLine(textGenerator, charIndex);
					UILineInfo lineInfo = textGenerator.lines[lineIndex];

					Vector2 endCursorPosition = new Vector2(charInfo.cursorPos.x, lineInfo.topY - lineInfo.height);
					endCursor.rectTransform.anchoredPosition = endCursorPosition / canvasScaleFactor;
					endCursor.enabled = true;
				}
				else
				{
					endCursor.enabled = false;
				}
			}
			else
			{
				startCursor.enabled = false;
				endCursor.enabled = false;
			}
		}

		internal void UpdateSelectionCursorsActionBar()
		{
			if(SelectionEndPosition > SelectionStartPosition)
			{
				TextGenerator textGenerator = TextRenderer.cachedTextGenerator;
				int startIndex = 0;

				if(VisibleSelectionStartPosition >= 0)
				{
					startIndex = Mathf.Clamp(VisibleSelectionStartPosition, 0, textGenerator.characterCount - 1);
				}

				ActionBar.transform.SetParent(currentCursor.transform.parent);
				ActionBar.transform.localScale = Vector3.one;
				bool cut = !InputField.Secure && !InputField.ReadOnly && InputField.ActionBarCut;
				bool copy = !InputField.Secure && InputField.ActionBarCopy;
				bool paste = !InputField.ReadOnly && InputField.ActionBarPaste;
				bool selectAll = InputField.ActionBarSelectAll;
				ActionBar.Show(cut, copy, paste, selectAll);
				Vector2 actionBarPosition = textGenerator.characters[startIndex].cursorPos;
				ActionBar.UpdatePosition(actionBarPosition);
			}
			else
			{
				ActionBar.Hide();
			}
		}

		/// <summary>Updates the current cursor</summary>
		internal void UpdateCurrentCursor(float canvasScaleFactor)
		{
			TextGenerator textGenerator = TextRenderer.cachedTextGenerator;
			if(textGenerator.lineCount > 0)
			{
				int charIndex = Mathf.Clamp(VisibleCaretPosition, 0, textGenerator.characterCount - 1);
				UICharInfo charInfo = textGenerator.characters[charIndex];
				int lineIndex = DetermineCharacterLine(textGenerator, charIndex);
				UILineInfo lineInfo = textGenerator.lines[lineIndex];

				Vector2 currentCursorPosition = new Vector2(charInfo.cursorPos.x, lineInfo.topY - lineInfo.height);
				if(VisibleCaretPosition >= textGenerator.characterCountVisible)
				{
					currentCursorPosition.x += charInfo.charWidth;
				}

				currentCursor.rectTransform.anchoredPosition = currentCursorPosition / canvasScaleFactor;
				if(!HasInsertedCharAfterClick)
				{
					currentCursor.enabled = true;
				}
				else if(HasSelection)
				{
					currentCursor.enabled = false;
				}

				if(InputField.ActionBarEnabled)
				{
					ActionBar.transform.SetParent(currentCursor.transform.parent);
					ActionBar.transform.localScale = Vector3.one;
					Vector2 actionBarPosition = textGenerator.characters[charIndex].cursorPos;
					ActionBar.UpdatePosition(actionBarPosition);
				}
			}
			else
			{
				currentCursor.enabled = false;
			}
		}

		internal override void UpdateCaret()
		{
			base.UpdateCaret();

			if(EditMode && (InputField.ActionBarEnabled || InputField.SelectionCursorsEnabled))
			{
				if(Canvas != null)
				{
					UpdateCurrentCursor(Canvas.scaleFactor);
				}
			}
		}

		internal override void UpdateSelected()
		{
			base.UpdateSelected();

			if(EditMode && (InputField.ActionBarEnabled || InputField.SelectionCursorsEnabled))
			{
				currentCursor.enabled = !HasSelection && !HasInsertedCharAfterClick;
			}
		}

		internal override void OnDestroy()
		{
			base.OnDestroy();

			if(selectionCursorsTransform != null)
			{
				GameObject.Destroy(selectionCursorsTransform.gameObject);
				selectionCursorsTransform = null;
			}
		}

		/// <summary>Updates the selection without updating selection in native text editor</summary>
		/// <param name="position">The new caret position</param>
		internal void UpdateSelection(int startPosition, int endPosition)
		{
			if(startPosition + 1 <= selectionStartPosition)
			{
				base.SelectionStartPosition = startPosition;
				base.SelectionEndPosition = endPosition;
				base.CaretPosition = startPosition;
			}
			else
			{
				base.SelectionStartPosition = startPosition;
				base.SelectionEndPosition = endPosition;
				base.CaretPosition = endPosition;
			}

			if(InputField.SelectionCursorsEnabled)
			{
				if(Canvas != null)
				{
					UpdateSelectionCursors(Canvas.scaleFactor);
				}
			}

			if(InputField.ActionBarEnabled)
			{
				UpdateSelectionCursorsActionBar();
			}
		}
	}
}
#endif