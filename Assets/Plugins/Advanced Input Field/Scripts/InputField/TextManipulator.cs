﻿//-----------------------------------------
//			Advanced Input Field
// Copyright (c) 2017 Jeroen van Pienbroek
//------------------------------------------

using UnityEngine;
using UnityEngine.UI;

namespace AdvancedInputFieldPlugin
{
	/// <summary>Base class for text string manipulation</summary>
	public class TextManipulator
	{
		/// <summary>The TextValidator</summary>
		private TextValidator textValidator;

		/// <summary>The InputField</summary>
		public AdvancedInputField InputField { get; private set; }

		/// <summary>The TextNavigator/summary>
		public virtual TextNavigator TextNavigator { get; protected set; }

		/// <summary>The main renderer for text</summary>
		public Text TextRenderer { get; private set; }

		/// <summary>The renderer for processed text</summary>
		public Text ProcessedTextRenderer { get; private set; }

		/// <summary>The main text string</summary>
		public virtual string Text
		{
			get { return InputField.Text; }
			set
			{
				InputField.SetText(value, true);
			}
		}

		/// <summary>The text in the clipboard</summary>
		public static string Clipboard
		{
			get
			{
				return GUIUtility.systemCopyBuffer;
			}
			set
			{
				GUIUtility.systemCopyBuffer = value;
			}
		}

		/// <summary>Indicates whether to currrently block text change events from being send to the native bindings</summary>
		public bool BlockNativeTextChange { get; set; }

		/// <summary>Initializes the class</summary>
		internal virtual void Initialize(AdvancedInputField inputField, TextNavigator textNavigator, Text textRenderer, Text processedTextRenderer)
		{
			InputField = inputField;
			TextNavigator = textNavigator;
			TextRenderer = textRenderer;
			ProcessedTextRenderer = processedTextRenderer;
			textValidator = new TextValidator(InputField.CharacterValidation, InputField.LineType);
		}

		/// <summary>Begins the edit mode</summary>
		internal void BeginEditMode()
		{
			TextNavigator.RefreshRenderedText();
			if(InputField.LiveProcessing)
			{
				TextRenderer.enabled = false;
				ProcessedTextRenderer.enabled = true;

				LiveProcessingFilter liveProcessingFilter = InputField.LiveProcessingFilter;
				string text = InputField.Text;
				int caretPosition = TextNavigator.CaretPosition;
				string processedText = liveProcessingFilter.ProcessText(text, caretPosition);
				if(processedText != null)
				{
					InputField.ProcessedText = processedText;

					int processedCaretPosition = liveProcessingFilter.DetermineProcessedCaret(text, caretPosition, processedText);
					TextNavigator.ProcessedCaretPosition = processedCaretPosition;
				}
			}
			else
			{
				TextRenderer.enabled = true;
				ProcessedTextRenderer.enabled = false;
			}
		}

		/// <summary>Ends the edit mode</summary>
		internal void EndEditMode()
		{
			if(Text.Length == 0)
			{
				TextRenderer.enabled = false;
				ProcessedTextRenderer.text = InputField.PlaceHolderText;
				ProcessedTextRenderer.enabled = true;
				InputField.ProcessedText = InputField.PlaceHolderText;
			}
			else if(InputField.Filter != null)
			{
				string processedText = null;
				if(InputField.Filter.ProcessText(Text, out processedText))
				{
					TextRenderer.enabled = false;
					ProcessedTextRenderer.text = processedText;
					ProcessedTextRenderer.enabled = true;
				}
			}
		}

		/// <summary>Checks if character is valid</summary>
		/// <param name="c">The character to check</param>
		internal bool IsValidChar(char c)
		{
			if((int)c == 127) //Delete key on mac
			{
				return false;
			}

			if(c == '\t' || c == '\n') // Accept newline and tab
			{
				return true;
			}

			return TextRenderer.font.HasCharacter(c);
		}

		/// <summary>Insert a string at caret position</summary>
		/// <param name="input">the string to insert</param>
		internal void Insert(string input)
		{
			if(InputField.ReadOnly)
			{
				return;
			}

			if(TextNavigator.HasSelection)
			{
				DeleteSelection();
			}

			int length = input.Length;
			for(int i = 0; i < length; i++)
			{
				TryInsertChar(input[i]);
			}
		}

		/// <summary>Tries to insert a character</summary>
		/// <param name="c">The character to insert</param>
		internal void TryInsertChar(char c)
		{
			if(!IsValidChar(c))
			{
				return;
			}

			string currentText = Text;
			int selectionStartPosition = -1;
			if(TextNavigator.HasSelection)
			{
				selectionStartPosition = TextNavigator.SelectionStartPosition;
			}

			char result = textValidator.Validate(currentText, TextNavigator.CaretPosition, c, selectionStartPosition);
			if(result != 0)
			{
				Insert(result);
			}
		}

		/// <summary>Insert a character at caret position</summary>
		/// <param name="c">The character to insert</param>
		internal virtual void Insert(char c)
		{
			if(InputField.ReadOnly)
			{
				return;
			}

			if(TextNavigator.HasSelection)
			{
				DeleteSelection();
			}
			else if(InputField.CharacterLimit > 0 && Text.Length >= InputField.CharacterLimit)
			{
				return;
			}

			Text = Text.Insert(TextNavigator.CaretPosition, c.ToString());

			TextNavigator.MoveCaret(1);
		}

		/// <summary>Deletes previous character</summary>
		internal void DeletePreviousChar()
		{
			if(InputField.ReadOnly)
			{
				return;
			}

			int originalLineCount = TextNavigator.LineCount;

			if(TextNavigator.HasSelection)
			{
				TextNavigator.MoveLineUpTill(TextNavigator.SelectionStartPosition);
				DeleteSelection();
			}
			else if(TextNavigator.CaretPosition > 0)
			{
				TextNavigator.MoveCaret(-1);
				Text = Text.Remove(TextNavigator.CaretPosition, 1);
			}

			LineCheck(originalLineCount);
		}

		/// <summary>Deletes next character</summary>
		internal void DeleteNextChar()
		{
			if(InputField.ReadOnly)
			{
				return;
			}

			int originalLineCount = TextNavigator.LineCount;

			if(TextNavigator.HasSelection)
			{
				TextNavigator.MoveLineUpTill(TextNavigator.SelectionStartPosition);
				DeleteSelection();
			}
			else
			{
				if(TextNavigator.CaretPosition < Text.Length)
				{
					Text = Text.Remove(TextNavigator.CaretPosition, 1);
				}
			}

			LineCheck(originalLineCount);
		}

		/// <summary>line check for text scroll</summary>
		/// <param name="originalLineCount">The line count before the text is refreshed</param>
		internal void LineCheck(int originalLineCount)
		{
			if(TextNavigator.LineCount < originalLineCount)
			{
				int lineDifference = originalLineCount - TextNavigator.LineCount;
				for(int i = 0; i < lineDifference; i++)
				{
					TextNavigator.MoveLineUp();
				}
			}
		}

		/// <summary>Deletes current text selection</summary>
		internal void DeleteSelection()
		{
			if(InputField.ReadOnly)
			{
				return;
			}

			string text = Text.Remove(TextNavigator.SelectionStartPosition, TextNavigator.SelectionEndPosition - TextNavigator.SelectionStartPosition);

			TextNavigator.CaretToSelectionStart();
			TextNavigator.CancelSelection();
			Text = text;
		}

		/// <summary>Copies current text selection</summary>
		internal virtual void Copy()
		{
			if(!InputField.IsPasswordField)
			{
				Clipboard = TextNavigator.SelectedText;
			}
			else
			{
				Clipboard = string.Empty;
			}
		}

		/// <summary>Pastes clipboard text</summary>
		internal virtual void Paste()
		{
			if(InputField.ReadOnly)
			{
				return;
			}

			string input = Clipboard;
			string processedInput = string.Empty;

			int length = input.Length;
			for(int i = 0; i < length; i++)
			{
				char c = input[i];

				if(c >= ' ' || c == '\t' || c == '\r' || c == 10 || c == '\n')
				{
					processedInput += c;
				}
			}

			if(!string.IsNullOrEmpty(processedInput))
			{
				Insert(processedInput);
			}
		}

		/// <summary>Cuts current text selection</summary>
		internal virtual void Cut()
		{
			if(!InputField.IsPasswordField)
			{
				Clipboard = TextNavigator.SelectedText;
			}
			else
			{
				Clipboard = string.Empty;
			}

			if(InputField.ReadOnly)
			{
				return;
			}

			if(TextNavigator.HasSelection)
			{
				int originalLineCount = TextNavigator.LineCount;

				TextNavigator.MoveLineUpTill(TextNavigator.SelectionStartPosition);
				DeleteSelection();

				LineCheck(originalLineCount);
			}
		}
	}
}
