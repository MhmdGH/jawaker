﻿using System.Linq;
using UnityEngine;

namespace AdvancedInputFieldPlugin
{
	/// <summary>Class to verify text (applies CharacterValidation)</summary>
	public class TextValidator
	{
		private const string EMAIL_SPECIAL_CHARACTERS = "!#$%&'*+-/=?^_`{|}~";

		public CharacterValidation Validation { get; private set; }
		public LineType LineType { get; private set; }

		public TextValidator(CharacterValidation validation, LineType lineType)
		{
			Validation = validation;
			LineType = lineType;
		}

		/// <summary>Validates the text</summary>
		/// <param name="text">The text to check</param>
		/// <param name="pos">The current char position</param>
		/// <param name="ch">The next character</param>
		/// <param name="selectionStartPosition">Current selection start position</param>
		internal char Validate(string text, int pos, char ch, int selectionStartPosition)
		{
			int caretPosition = pos;

			if(LineType != LineType.MultiLineNewline && (ch == '\r' || ch == '\n'))
			{
				return (char)0;
			}

			// Validation is disabled
			if(Validation == CharacterValidation.None)
			{
				return ch;
			}

			if(Validation == CharacterValidation.Integer || Validation == CharacterValidation.Decimal)
			{
				// Integer and decimal
				bool cursorBeforeDash = (pos == 0 && text.Length > 0 && text[0] == '-');
				bool dashInSelection = text.Length > 0 && text[0] == '-' && ((caretPosition == 0 && selectionStartPosition > 0) || (selectionStartPosition == 0 && caretPosition > 0));
				bool selectionAtStart = caretPosition == 0 || selectionStartPosition == 0;
				if(!cursorBeforeDash || dashInSelection)
				{
					if(ch >= '0' && ch <= '9') return ch;
					if(ch == '-' && (pos == 0 || selectionAtStart)) return ch;
					if(ch == '.' && Validation == CharacterValidation.Decimal && !text.Contains(".")) return ch;
					if(ch == ',' && Validation == CharacterValidation.Decimal && !text.Contains(".")) return '.';
				}
			}
			else if(Validation == CharacterValidation.Alphanumeric)
			{
				// All alphanumeric characters
				if(ch >= 'A' && ch <= 'Z') return ch;
				if(ch >= 'a' && ch <= 'z') return ch;
				if(ch >= '0' && ch <= '9') return ch;
			}
			else if(Validation == CharacterValidation.Name)
			{
				// FIXME: some actions still lead to invalid input:
				//        - Hitting delete in front of an uppercase letter
				//        - Selecting an uppercase letter and deleting it
				//        - Typing some text, hitting Home and typing more text (we then have an uppercase letter in the middle of a word)
				//        - Typing some text, hitting Home and typing a space (we then have a leading space)
				//        - Erasing a space between two words (we then have an uppercase letter in the middle of a word)
				//        - We accept a trailing space
				//        - We accept the insertion of a space between two lowercase letters.
				//        - Typing text in front of an existing uppercase letter
				//        - ... and certainly more
				//
				// The rule we try to implement are too complex for this kind of verification.

				if(char.IsLetter(ch))
				{
					// Character following a space should be in uppercase.
					if(char.IsLower(ch) && ((pos == 0) || (text[pos - 1] == ' ')))
					{
						return char.ToUpper(ch);
					}

					// Character not following a space or an apostrophe should be in lowercase.
					if(char.IsUpper(ch) && (pos > 0) && (text[pos - 1] != ' ') && (text[pos - 1] != '\''))
					{
						return char.ToLower(ch);
					}

					return ch;
				}

				if(ch == '\'')
				{
					// Don't allow more than one apostrophe
					if(!text.Contains("'"))
					{
						// Don't allow consecutive spaces and apostrophes.
						if(!(((pos > 0) && ((text[pos - 1] == ' ') || (text[pos - 1] == '\''))) ||
							  ((pos < text.Length) && ((text[pos] == ' ') || (text[pos] == '\'')))))
						{
							return ch;
						}
					}
				}

				if(ch == ' ')
				{
					// Don't allow consecutive spaces and apostrophes.
					if(!(((pos > 0) && ((text[pos - 1] == ' ') || (text[pos - 1] == '\''))) ||
						  ((pos < text.Length) && ((text[pos] == ' ') || (text[pos] == '\'')))))
					{
						return ch;
					}
				}
			}
			else if(Validation == CharacterValidation.EmailAddress)
			{
				// From StackOverflow about allowed characters in email addresses:
				// Uppercase and lowercase English letters (a-z, A-Z)
				// Digits 0 to 9
				// Characters ! # $ % & ' * + - / = ? ^ _ ` { | } ~
				// Character . (dot, period, full stop) provided that it is not the first or last character,
				// and provided also that it does not appear two or more times consecutively.

				if(ch >= 'A' && ch <= 'Z') return ch;
				if(ch >= 'a' && ch <= 'z') return ch;
				if(ch >= '0' && ch <= '9') return ch;
				if(ch == '@' && text.IndexOf('@') == -1) return ch;
				if(EMAIL_SPECIAL_CHARACTERS.IndexOf(ch) != -1) return ch;
				if(ch == '.')
				{
					char lastChar = (text.Length > 0) ? text[Mathf.Clamp(pos, 0, text.Length - 1)] : ' ';
					char nextChar = (text.Length > 0) ? text[Mathf.Clamp(pos + 1, 0, text.Length - 1)] : '\n';
					if(lastChar != '.' && nextChar != '.')
					{
						return ch;
					}
				}
			}
			else if(Validation == CharacterValidation.IPAddress)
			{
				int lastDotIndex = text.LastIndexOf('.');
				if(lastDotIndex == -1)
				{
					int numbersInSection = text.Length;
					if(numbersInSection < 3 && ch >= '0' && ch <= '9') return ch; //Less than 3 numbers, so number add allowed
					if(ch == '.' && text.Length > 0) { return ch; } //Don't start with dot
				}
				else
				{
					if(ch >= '0' && ch <= '9')
					{
						int numbersInSection = (text.Length - 1) - lastDotIndex;
						if(numbersInSection < 3 && ch >= '0' && ch <= '9') return ch; //Less than 3 numbers, so number add allowed
					}
					if(ch == '.' && lastDotIndex != text.Length - 1 && text.Count(x => x == '.') < 3) { return ch; } //Max 4 sections (3 dot characters)
				}
			}
			else if(Validation == CharacterValidation.Sentence)
			{
				if(char.IsLetter(ch) && char.IsLower(ch))
				{
					string trimmedText = text.TrimEnd(' ', '\t'); //Remove whitespace
					if(trimmedText.Length == 0) { return char.ToUpper(ch); }

					char lastChar = trimmedText[trimmedText.Length - 1];
					if(pos == 0 || lastChar == '.' || lastChar == '\n')
					{
						return char.ToUpper(ch);
					}
				}

				return ch;
			}
			return (char)0;
		}
	}
}
