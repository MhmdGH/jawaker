﻿//-----------------------------------------
//			Advanced Input Field
// Copyright (c) 2017 Jeroen van Pienbroek
//------------------------------------------

#if !UNITY_EDITOR && UNITY_IOS
using System.Runtime.InteropServices;

namespace AdvancedInputFieldPlugin
{
	/// <summary>Class that acts as a bridge for the Native iOS Keyboard</summary>
	public class IOSKeyboard: NativeKeyboard
	{
		[DllImport("__Internal")]
		private static extern void _initialize(string gameObjectName);
		[DllImport("__Internal")]
		private static extern void _changeText(string text);
		[DllImport("__Internal")]
		private static extern void _changeSelection(int start, int end);
		[DllImport("__Internal")]
		private static extern void _showKeyboard(string text, int keyboardType, int characterValidation, int lineType, int autocapitalizationType, bool autocorrection, bool secure, int characterLimit);
		[DllImport("__Internal")]
		private static extern void _hideKeyboard();
		[DllImport("__Internal")]
		private static extern void _enableUpdates();
		[DllImport("__Internal")]
		private static extern void _disableUpdates();
		[DllImport("__Internal")]
		private static extern void _enableHardwareKeyboardUpdates();
		[DllImport("__Internal")]
		private static extern void _disableHardwareKeyboardUpdates();

		internal override void Setup()
		{
			_initialize(gameObjectName);
		}

		public override void EnableUpdates()
		{	
			_enableUpdates();
		}

		public override void DisableUpdates()
		{
			_disableUpdates();
		}
	
		public override void EnableHardwareKeyboardUpdates()
		{
			_enableHardwareKeyboardUpdates();
		}

		public override void DisableHardwareKeyboardUpdates()
		{
			_disableHardwareKeyboardUpdates();
		}

		public override void ShowKeyboard(string text, KeyboardType keyboardType, CharacterValidation characterValidation, LineType lineType, AutocapitalizationType autocapitalizationType, bool autocorrection, bool secure, int characterLimit)
		{
			_showKeyboard(text, (int)keyboardType, (int)characterValidation, (int)lineType, (int)autocapitalizationType, autocorrection, secure, characterLimit);
		}

		public override void HideKeyboard()
		{
			_hideKeyboard();
		}

		public override void ChangeText(string text)
		{
			_changeText(text);
		}

		public override void ChangeSelection(int selectionStartPosition, int selectionEndPosition)
		{
			_changeSelection(selectionStartPosition, selectionEndPosition);
		}
	}
}
#endif