﻿#if !UNITY_EDITOR && UNITY_WSA

namespace AdvancedInputFieldPlugin
{
	/// <summary>Class that acts as a bridge for the Native UWP Keyboard</summary>
	public class UWPKeyboard: NativeKeyboard, NativeUWP.IUnityCallback
	{
		private NativeUWP.NativeKeyboard keyboard;

		internal override void Setup()
		{
			keyboard = new NativeUWP.NativeKeyboard();
			keyboard.Initialize(this);
		}

		public override void EnableUpdates()
		{
			keyboard.EnableUpdates();
		}

		public override void DisableUpdates()
		{
			keyboard.DisableUpdates();
		}

		public override void EnableHardwareKeyboardUpdates()
		{
			keyboard.EnableHardwareKeyboardUpdates();
		}

		public override void DisableHardwareKeyboardUpdates()
		{
			keyboard.DisableHardwareKeyboardUpdates();
		}

		public override void ShowKeyboard(string text, KeyboardType keyboardType, CharacterValidation characterValidation, LineType lineType, AutocapitalizationType autocapitalizationType, bool autocorrection, bool secure, int characterLimit)
		{
			keyboard.ShowKeyboard(text, (int)keyboardType, (int)characterValidation, (int)lineType, (int)autocapitalizationType, autocorrection, secure, characterLimit);
		}

		public override void HideKeyboard()
		{
			keyboard.HideKeyboard();
		}

		public override void ChangeText(string text)
		{
			keyboard.ChangeText(text);
		}

		public override void ChangeSelection(int selectionStartPosition, int selectionEndPosition)
		{
			keyboard.ChangeSelection(selectionStartPosition, selectionEndPosition);
		}

		private void OnDestroy()
		{
			keyboard.Destroy();
			keyboard = null;
		}

		public void OnSelectionChanged(int start, int end)
		{
			nativeEventQueue.Enqueue(new Event(EventType.SELECTION_CHANGE, start + ", " + end));
		}

		public void OnKeyboardHeightChanged(int height)
		{
			UWPThreadHelper.Instance.ScheduleActionOnUnityThread((keyboardHeight) =>
			{
				InvokeKeyboardHeightChanged(keyboardHeight);

				if(keyboardHeight == 0) //Safety check if something external caused the keyboard to hide
				{
					State = KeyboardState.HIDDEN;
				}
			},
			height);
		}

		public void OnHardwareKeyboardChanged(bool connected)
		{
			UWPThreadHelper.Instance.ScheduleActionOnUnityThread((keyboardConnected) =>
			{
				HardwareKeyboardConnected = keyboardConnected;
				if(HardwareKeyboardConnected)
				{
					HideKeyboard();
					DisableUpdates();
				}
				else
				{
					EnableUpdates();
				}

				InvokeHardwareKeyboardChanged(connected);
			},
			connected);
		}
	}
}
#endif
