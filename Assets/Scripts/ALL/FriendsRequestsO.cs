using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

[Serializable]
public class FriendsRequestsO
{
	public RecievedFriendsRequestsO MyRecievedRequests;
	public SendedFriendsRequestsO MySendedRequests;
	public FriendsRequestsO(){
		MyRecievedRequests = new RecievedFriendsRequestsO();
		MySendedRequests = new SendedFriendsRequestsO();
	}
}
