﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class Player : MonoBehaviour
{
	public static Player Instance;
	[HideInInspector]
	public int OnlinePlayerID = 1, Level, XPs,WeeklyXPs,Coins, BashaDays, PlayerColor, ColorDays,
	BoosterDays, BashaUpPatch, BashaButtomPatch, GamesPlayedCounter, NameEdited,LastSaturdayDayOfYear,
	CanEditClubColor,CanEditClubVip,BashaSelectedOne;
	[HideInInspector]
	public bool GameCreator, ReloadScene;
	[HideInInspector]
	public string DatabasePlayerID, PlayerName = "", FBID = "", ClubID = "",
	ClubName = "",CountryCode="",JoinRequestID="",JoinClubRequest="",JoinClubRequestName="",CurrentCompID="",
	FriendsInvitationsIDs;
	[HideInInspector]
	public Color PlayerRealColor;
	[HideInInspector]
	public OnlinePlayerO ThisOnlinePlayer;
	[HideInInspector]
	public int[] Badges,Items;
	[HideInInspector]
	public NotificationsO NotificationsListO;

	void Awake ()
	{
		Singlton ();
//		testing ();
		LoadPlayerData ();
	}

	void testing ()
	{
		PlayerPrefs.DeleteAll ();
		PlayerPrefs.Save ();
	}

	void Start ()
	{
		if (DatabasePlayerID.Length < 2) {
			DatabasePlayerID = RandomIDGenerator.Instance.GetRandomID ();
			SavePlayerData ();
		}
		if (PlayerName.Length < 2) {
			PlayerName = "Player";
			SavePlayerData ();
		}
		if (CountryCode.Length < 2)
			StartCoroutine (GetMyFukinCountryCode());
		Items = new int[5]{-1,-1,-1,-1,-1};
		Coins = 9999999;
	}

	public bool IsBasha(){
		return BashaDays > 0;
	}

	public bool IsJoinedClub(){
		return ClubID.Length > 2;
	}

	private IEnumerator GetMyFukinCountryCode(){
		WWW MyIPAddresswww = new WWW ("https://api.ipify.org");
		yield return MyIPAddresswww;
		string MyIPString = MyIPAddresswww.text;
		WWW MyCountrywww = new WWW ("http://api.ipstack.com/" + MyIPString+"?access_key=aec6797a8b32afdeb4e106f8832207a0&format=1");
		CountryO PlayerCountryDetails = new CountryO ();
		yield return MyCountrywww;
		JsonUtility.FromJsonOverwrite (MyCountrywww.text, PlayerCountryDetails);
		CountryCode = PlayerCountryDetails.country_code.ToLower ();
		SavePlayerData ();
	}

	public void AddXPsAndCLubPoints (int XPsNO)
	{
		XPs += XPsNO;
		if (XPs > 1000) {
			Level += 1;
			XPs -= 1000;
		}
		if(IsNewWeek())
			WeeklyXPs = 0;
		WeeklyXPs += XPsNO;
		LastSaturdayDayOfYear = GetNearestSaturdy ();
		PlayingFireBaseFather.ParentInstance.AddPointsToMyClub ();
		SetLevelsBadges ();
		SavePlayerData ();
	}

	int GetNearestSaturdy ()
	{
		int day = (int)DateTime.Now.DayOfWeek;
		int DayzBetweenTodayAndSaturday = Mathf.Abs (1-day);
		return DateTime.UtcNow.DayOfYear - DayzBetweenTodayAndSaturday;
	}

	bool IsNewWeek(){
		return DateTime.UtcNow.DayOfYear >= (LastSaturdayDayOfYear+7);
	}

	void SetLevelsBadges ()
	{
		if (Level >= 10)
			Badges [2] = 1;
		if (Level >= 20)
			Badges [3] = 1;
		if (Level >= 30)
			Badges [4] = 1;
		if (Level >= 60)
			Badges [5] = 1;
	}

	void Singlton ()
	{
		if (Instance == null)
			Instance = this;
		else
			Destroy (gameObject);
		DontDestroyOnLoad (this);
	}

	public void SavePlayerData ()
	{
		if(SceneManager.GetActiveScene().buildIndex==0)
			StartCoroutine (LateSavePlayerDataCor());
		PlayerPrefs.SetString ("DatabasePlayerID", DatabasePlayerID);
		PlayerPrefs.SetInt ("BashaSelectedOne", BashaSelectedOne);
		PlayerPrefs.SetString ("FriendsInvitationsIDs", FriendsInvitationsIDs);
		PlayerPrefs.SetInt ("LastSaturdayDayOfYear", LastSaturdayDayOfYear);
		PlayerPrefs.SetString ("CountryCode", CountryCode);
		PlayerPrefs.SetString ("ClubID", ClubID);
		PlayerPrefs.SetString ("JoinRequestID", JoinRequestID);
		PlayerPrefs.SetString ("JoinClubRequest", JoinClubRequest);
		PlayerPrefs.SetString ("JoinClubRequestName", JoinClubRequestName);
		PlayerPrefs.SetString ("PlayerName", PlayerName);
		PlayerPrefs.SetString ("FBID", "100001468925992");
		PlayerPrefs.SetString ("ClubName", ClubName);
		PlayerPrefs.SetInt ("Level", Level);
		PlayerPrefs.SetInt ("CanEditClubVip", CanEditClubVip);
		PlayerPrefs.SetInt ("CanEditClubColor", CanEditClubColor);
		PlayerPrefs.SetInt ("Level", Level);
		PlayerPrefs.SetInt ("GamesPlayedCounter", GamesPlayedCounter);
		PlayerPrefs.SetInt ("NameEdited", 0);//T.
		PlayerPrefs.SetInt ("XPs", XPs);
		PlayerPrefs.SetInt ("WeeklyXPs", WeeklyXPs);
		PlayerPrefs.SetInt ("BashaDays", BashaDays);
		PlayerPrefs.SetInt ("BoosterDays", BoosterDays);
		PlayerPrefs.SetInt ("BashaUpPatch", BashaUpPatch);
		PlayerPrefs.SetInt ("BashaButtomPatch", BashaButtomPatch);
		PlayerPrefs.SetInt ("PlayerColor", PlayerColor);
		PlayerPrefs.SetInt ("ColorDays", ColorDays);
		PlayerRealColor = GetPlayerColor (PlayerColor);
		for(int i=0;i<10;i++)
			PlayerPrefs.SetInt ("Badges"+i, Badges[i]);
		PlayerPrefs.Save ();
		SaveToOnlineData ();
		if (SceneManager.GetActiveScene ().buildIndex == 0)
			MainPlayerDetails.Instance.SetPlayerData ();
	}

	private IEnumerator LateSavePlayerDataCor(){
		yield return new WaitForSeconds (1);
		SetItemsFromOnlinePlayer ();
	}

	void SetItemsFromOnlinePlayer ()
	{
		try{
			Debug.Log("ThisOnlinePlayer.Items \t"+ThisOnlinePlayer.Items [0]);
			if (ThisOnlinePlayer.Items [0] != -1) {
				StoreManager.Instance.PurchasedData.SeatsColors [ThisOnlinePlayer.Items [0]] = 1;
				Items [0] = -1;
			}else if (ThisOnlinePlayer.Items [1] != -1) {
				StoreManager.Instance.PurchasedData.BashaDays = AddThisBashaNo (ThisOnlinePlayer.Items [1]);
				Items [1] = -1;
			}else if (ThisOnlinePlayer.Items [2] != -1) {
				StoreManager.Instance.PurchasedData.Boosters[0] = ThisOnlinePlayer.Items[2];
				Items [2] = -1;
			}else if (ThisOnlinePlayer.Items [3] != -1) {
				StoreManager.Instance.PurchasedData.VipItems[0] = 1;
				Items [3] = -1;
			}else if (ThisOnlinePlayer.Items [4] != -1) {
				StoreManager.Instance.PurchasedData.VipItems[1] = 1;
				Items [4] = -1;
			}
		}catch{
		}
		StoreManager.Instance.SaveAndRefreshNewPlayerData (false);
	}


	private int AddThisBashaNo (int itemNO)
	{
		if (itemNO == 0)
			return 7;
		else if (itemNO == 1)
			return 30;
		else if (itemNO == 2)
			return 90;
		else if (itemNO == 3)
			return 180;
		else
			return 360;
	}

	void SaveToOnlineData ()
	{
		ThisOnlinePlayer = GetOnlinePlayerDataFrom();
		try{
			MainFireBaseManager.Instance.UpdateThisPlayerData (ThisOnlinePlayer);
		}catch{}
	}

	public void LoadPlayerData ()
	{
		CountryCode	= PlayerPrefs.GetString ("CountryCode", "0");
		BashaSelectedOne	= PlayerPrefs.GetInt ("BashaSelectedOne", 0);
		FriendsInvitationsIDs	= PlayerPrefs.GetString ("FriendsInvitationsIDs", "0");
		LastSaturdayDayOfYear	= PlayerPrefs.GetInt ("LastSaturdayDayOfYear", 0);
		CanEditClubVip	= PlayerPrefs.GetInt ("CanEditClubVip", 0);
		CanEditClubColor	= PlayerPrefs.GetInt ("CanEditClubColor", 0);
		JoinRequestID	= PlayerPrefs.GetString ("JoinRequestID", "0");
		JoinClubRequest	= PlayerPrefs.GetString ("JoinClubRequest", "0");
		JoinClubRequestName	= PlayerPrefs.GetString ("JoinClubRequestName", "0");
		PlayerName = PlayerPrefs.GetString ("PlayerName", "Player");
		FBID	= PlayerPrefs.GetString ("FBID", "100001468925992");
		ClubID	= PlayerPrefs.GetString ("ClubID", "0");
		ClubName	= PlayerPrefs.GetString ("ClubName", "0");
		Level	= PlayerPrefs.GetInt ("Level", 1);
		NameEdited	= PlayerPrefs.GetInt ("NameEdited", 0);
		GamesPlayedCounter	= PlayerPrefs.GetInt ("GamesPlayedCounter", 0);
		XPs = PlayerPrefs.GetInt ("XPs", 0);
		WeeklyXPs = PlayerPrefs.GetInt ("WeeklyXPs", 0);
		BashaDays = PlayerPrefs.GetInt ("BashaDays", 0);
		BashaUpPatch = PlayerPrefs.GetInt ("BashaUpPatch", 0);
		BashaButtomPatch = PlayerPrefs.GetInt ("BashaButtomPatch", 0);
		BoosterDays = PlayerPrefs.GetInt ("BoosterDays", 0);
		PlayerColor = PlayerPrefs.GetInt ("PlayerColor", 0);
		ColorDays = PlayerPrefs.GetInt ("ColorDays", 0);
		DatabasePlayerID = PlayerPrefs.GetString ("DatabasePlayerID","0");
		PlayerRealColor = GetPlayerColor (PlayerColor);
		Badges = new int[10];
		for(int i=0;i<10;i++)
			Badges[i] = PlayerPrefs.GetInt ("Badges"+i,0);
	}

	OnlinePlayerO GetOnlinePlayerDataFrom ()
	{
		OnlinePlayerO NewOnlinePlayerO = new OnlinePlayerO ();
		NewOnlinePlayerO.ClubID = ClubID;
		NewOnlinePlayerO.ClubName = ClubName;
		NewOnlinePlayerO.FBID = FBID;
		NewOnlinePlayerO.PlayerID = DatabasePlayerID;
		NewOnlinePlayerO.PlayerLevel = Level;
		NewOnlinePlayerO.PlayerName = PlayerName;
		NewOnlinePlayerO.CountryCode = CountryCode;
		NewOnlinePlayerO.JoinRequestID = JoinRequestID;
		NewOnlinePlayerO.JoinClubRequest = JoinClubRequest;
		NewOnlinePlayerO.JoinClubRequestName = JoinClubRequestName;
		NewOnlinePlayerO.Coins = Coins;
		NewOnlinePlayerO.Items = Items;
		NewOnlinePlayerO.XPs = XPs;
		NewOnlinePlayerO.WeeklyXPs = WeeklyXPs;
		NewOnlinePlayerO.CurrentCompID = CurrentCompID;
		NewOnlinePlayerO.MyFrinedListO.MyFriends = ThisOnlinePlayer.MyFrinedListO.MyFriends;
		NewOnlinePlayerO.NotificationsListO = NotificationsListO;
		for (int i = 0; i < Badges.Length; i++)
			NewOnlinePlayerO.Badges [i] = Badges [i];
		return NewOnlinePlayerO;
	}

	public void UpdateMyOnlinePlayerData(string OnlinePlayerJson){
		JsonUtility.FromJsonOverwrite (OnlinePlayerJson, ThisOnlinePlayer);
		Coins = ThisOnlinePlayer.Coins;
		Items = ThisOnlinePlayer.Items;
		CurrentCompID = ThisOnlinePlayer.CurrentCompID;
		NotificationsListO = ThisOnlinePlayer.NotificationsListO;
		Player.Instance.SavePlayerData ();
	}

	public Color GetPlayerColor (int playerColor)
	{
		if (playerColor == 1)
			return Color.red;
		else if (playerColor == 2)
			return Color.yellow;
		else if (playerColor == 3)
			return Color.green;
		else if (playerColor == 4)
			return Color.black;
		else if (playerColor == 5)
			return Color.white;
		else if (playerColor == 6)
			return Color.magenta;
		else if (playerColor == 7)
			return Color.gray;
		else if (playerColor == 8)
			return Color.blue;
		else if (playerColor == 9)
			return Color.cyan;
		else if (playerColor == 10)
			return new Color(1,0.3f,0.5f,1);
		else
			return new Color (0, 0, 0, 0);
	}

	public void SetGamePlayedBadges ()
	{
		GamesPlayedCounter++;
		if (GamesPlayedCounter >= 100)
			Badges [8] = 1;
		if (GamesPlayedCounter >= 200)
			Badges [9] = 1;
	}

	public void PlayerLeaveRoom ()
	{
		GameCreator = false;
		OnlinePlayerID = -1;
	}
}