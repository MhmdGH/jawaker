﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomIDGenerator : MonoBehaviour{

	const string glyphs= "abcdefghijklmnopqrstuvwxyz0123456789";
	public static RandomIDGenerator Instance;

	void Awake(){
		Instance = this;
	}

	public string GetRandomID(){
		string myString = "";
		int charAmount = Random.Range(7, 11); //set those to the minimum and maximum length of your string
		for(int i=0; i<charAmount; i++)
			myString += glyphs[Random.Range(0, glyphs.Length)];
		return myString;
	}

}
