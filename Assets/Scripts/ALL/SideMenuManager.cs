﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SideMenuManager : MonoBehaviour
{

	public static SideMenuManager Instance;
	public Image PlayerIMG,PlayerColorIMG;
	public GameObject[] Txts;
	private float MouseFirstPos, MouseLastPos, AcceptedSwipeLong = 200;
	public GameObject BashaIcon;

	void Awake ()
	{
		Instance = this;
	}

	public void RefreshPlayerDetails ()
	{
		/*
		 * 0	Player Name
		 * 1 	PlayerLevel
		 * 2	PlayerLevelCircle
		 * 3	XPMeter
		 * 4	Coins
		 * 5	KingDays
		 * */
		Txts [0].GetComponent<Text> ().text = Player.Instance.PlayerName + "";
		if (Player.Instance.IsBasha ())
			Txts [0].GetComponent<Text> ().color = Color.yellow;
		else
			Txts [0].GetComponent<Text> ().color = Color.white;
		Txts [1].GetComponent<Text> ().text = Player.Instance.Level + "";
		Txts [2].GetComponent<Text> ().text = Player.Instance.Level + "";
		Txts [3].GetComponent<Text> ().text = Player.Instance.XPs + "";
		Txts [4].GetComponent<Text> ().text = Player.Instance.Coins + "";
		Txts [5].GetComponent<Text> ().text = Player.Instance.BashaDays + "";
		BashaIcon.SetActive (Player.Instance.IsBasha ());
		FBManager.Instance.SetFPPhoto (Player.Instance.FBID,PlayerIMG);
		PlayerColorIMG.color = Player.Instance.PlayerRealColor;
	}

	public void ShowOrHide (bool ShowOrHide)
	{
		string AnimBoolString = "IsShow";
		GetComponent<Animator> ().SetBool (AnimBoolString, ShowOrHide);
	}

	public void ShowOrHide ()
	{
		string AnimBoolString = "IsShow";
		GetComponent<Animator> ().SetBool (AnimBoolString,!GetComponent<Animator> ().GetBool(AnimBoolString));
	}

	void Update ()
	{
		if (Input.GetMouseButtonDown (0)) {
			MouseFirstPos = Input.mousePosition.x;
		}
		else if (Input.GetMouseButtonUp (0)) {
			MouseLastPos = Input.mousePosition.x;
			CheckSideSwipe ();
		}
	}

	void CheckSideSwipe ()
	{
		if (SceneManager.GetActiveScene ().buildIndex != 0)
			return;
		float SwipeLong = Mathf.Abs (MouseFirstPos - MouseLastPos);
		if (SwipeLong > AcceptedSwipeLong) {
			if (MouseFirstPos > MouseLastPos)
				ShowOrHide (false);
			else {
				if (MouseFirstPos > 200)
					return;
				ShowOrHide (true);
			}
		}
	}
}