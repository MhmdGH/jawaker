﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public class ArabicFix : MonoBehaviour {

	void Start(){
		GetComponent<Text>().text=ArabicFixer.Fix (GetComponent<Text>().text);
	}

}