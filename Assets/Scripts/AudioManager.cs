﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
	public static AudioManager Instance;
	public static float AudioVolume=1;
	protected AudioSource MyAudioSource;
	public AudioClip[] Audios;

	void Awake ()
	{
		Instance = this;
		MyAudioSource = GetComponent<AudioSource> ();
	}

	public void PlaySound (int SoundNO)
	{
		/*
	 * 	SoundsNO:
	 * 0=>	Shuffel
	 * 1=>	Start Game
	 * 2=>	Player In
	 * 3=>	Player Out
	 * 4-15=> 2-13 (x+2)
	 * 16=>	Pass
	 * 17=>	Clovers
	 * 18=>	Diamonds
	 * 19=>	Spades
	 * 20=>	Hearts
	 * 21=>	Ashkal
	 * 22=>	Bas
	 * 23=>	Bid
	 * 24=>	Hokum
	 * 25=>	Sun
	 * 26=>	Wala
	 * 27=>	Double
	 * 28=>	ThrowCard
	 * 29=>	King
	 * 30=>	Queens
	 * 31=>	Ltoosh
	 * 32=>	Trix
	 * 33=>	Complex
	 * 34=>	Dash
	 * 35=>	Avoid
	 * //Arabic Section
	 * 36-47=> 2-13 
	 * 48=>	Clovers
	 * 49=>	Diamonds
	 * 50=>	Spades
	 * 51=>	Hearts
	 * 52=>	Avoid
	 * 53=>	Bid
	 * 54=>	K
	 * 55=>	Q
	* */
		if (LanguangeManagerFather.ParentInstance.IsArabic)
			SoundNO = GetTranslatedSound (SoundNO);
		MyAudioSource.PlayOneShot (Audios [SoundNO], AudioVolume);
	}

	int GetTranslatedSound (int ThisSoundNO)
	{
		if (ThisSoundNO >= 4 && ThisSoundNO <= 15)
			return ThisSoundNO + 32;
		else if (ThisSoundNO >= 17 && ThisSoundNO <= 20)
			return ThisSoundNO + 31;
		else if (ThisSoundNO == 35)
			return 52;
		else if (ThisSoundNO == 29)
			return 54;
		else if (ThisSoundNO == 30)
			return 55;
		else
			return ThisSoundNO;
	}

	public void ToggleSoundOnOrOff(){
		if (AudioVolume == 1)
			AudioVolume = 0;
		else
			AudioVolume = 1;
	}

}
