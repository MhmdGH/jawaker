﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardsThrowerMangaer : MonoBehaviour {

	public static CardsThrowerMangaer Instance;
	private GameObject[] CardsGO;
	private int AvailabeCardNO;
	public GameObject CardGO;
	private bool IsJackarooGame;

	void Awake(){
		Instance=this;
	}

	void Start(){
		AvailabeCardNO = 0;
		CardsGO = new GameObject[4];
		for (int i = 0; i < 4; i++) {
			CardsGO [i] = Instantiate (CardGO, this.transform.position,
				Quaternion.identity, this.transform) as GameObject;
			CardsGO [i].GetComponent<Animator> ().enabled = true;
			CardsGO [i].SetActive (false);
		}
		if (MainFireBaseManager.GameType.Contains ("Jackaroo"))
			IsJackarooGame = true;
	}

	public void ThrowThisCard(string CardID,int ThrowerID){
		if(!IsJackarooGame)
			CardsGO [AvailabeCardNO].GetComponent<RectTransform> ().localPosition = TargetVect (ThrowerID);
		AudioManager.Instance.PlaySound (28);
		CardsGO [AvailabeCardNO].SetActive (true);
		CardsGO [AvailabeCardNO].GetComponent<Card> ().CardID = CardID;
		CardsGO [AvailabeCardNO].GetComponent<Card> ().SetCardCoverOrFace ();
		CardsGO[AvailabeCardNO].GetComponent<Animator> ().SetInteger ("Thorugh",ThrowerID);
		CardsGO [AvailabeCardNO].transform.SetAsLastSibling ();
		AvailabeCardNO++;
		if (AvailabeCardNO > 3)
			AvailabeCardNO = 0;
		if (IsJackarooGame)
			StartCoroutine (ReturnCard(AvailabeCardNO));
	}

	private IEnumerator ReturnCard(int ThisCard){
		yield return new WaitForSeconds (1);
		CardsGO[ThisCard].GetComponent<Animator> ().SetInteger ("Thorugh",-1);
	}

	int Prev2 (int availabeCardNO)
	{
		availabeCardNO-=3;
		if (availabeCardNO == -1)
			availabeCardNO = 3;
		else if (availabeCardNO == -2)
			availabeCardNO = 2;
		else if (availabeCardNO == -3)
			availabeCardNO = 1;
		return availabeCardNO;
	}

	int FirstAvailabeCard ()
	{
		for (int i = 0; i < CardsGO.Length; i++)
			if (!CardsGO [i].activeSelf)
				return i;
		//not reachable
		return 0;
	}

	protected Vector2 TargetVect (int PLayerNO)
	{
		if (MainFireBaseManager.GameType.Contains ("Jackaroo")) {
			if (PLayerNO == 0)
				return new Vector2 (0, -500);
			else if (PLayerNO == 3)
				return new Vector2 (500 / 2, 0);
			else if (PLayerNO == 2)
				return new Vector2 (0, 500 / 2);
			else
				return new Vector2 (-500 / 2, 0);
		} else {
			if (PLayerNO == 0)
				return new Vector2 (0, -500);
			else if (PLayerNO == 1)
				return new Vector2 (500 / 2, 0);
			else if (PLayerNO == 2)
				return new Vector2 (0, 500 / 2);
			else
				return new Vector2 (-500 / 2, 0);
		}
	}

	public void CollectCardsToThisPlayer(int PlayerNO){
		AvailabeCardNO = 0;
		StartCoroutine (CollectCardsToThisPlayerCor(PlayerNO));
	}

	private IEnumerator CollectCardsToThisPlayerCor(int PlayerNO){
		for (int i = 0; i < CardsGO.Length; i++) {
			CardsGO [i].GetComponent<Animator> ().SetInteger ("Thorugh", -6);
			CardsGO [i].GetComponent<Animator> ().SetInteger ("CollectorPlayerID", PlayerNO);
		}
		yield return new WaitForSeconds (0.5f);
		for (int i = 0; i < CardsGO.Length; i++) {
			CardsGO [i].GetComponent<Animator> ().SetInteger ("Thorugh", -1);
			CardsGO [i].SetActive (false);
		}
	}
}