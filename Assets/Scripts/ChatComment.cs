﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatComment : MonoBehaviour {

	public Image CommenterIMG;
	public Text CommentTxt;

	public void SetPostDetails(CommentO ThisComment){
		FBManager.Instance.SetFPPhoto (ThisComment.PlayerFBID,CommenterIMG);
		CommentTxt.text = ThisComment.CommentMSG;
	}

}