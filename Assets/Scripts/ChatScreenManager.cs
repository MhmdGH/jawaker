﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AdvancedInputFieldPlugin;

public class ChatScreenManager : MonoBehaviour {

	public static ChatScreenManager Instance;
	public RectTransform PostsListContent;
	public GameObject PostPrefab,AddNewPostWindowGO,CreatePostBTNGO;
	public AdvancedInputField NewPostINPTFeild;

	void Awake(){
		Instance=this;
	}

	public void RefreshChatScreen(){
		Debug.Log ("RefreshChatScreen");
		CreatePostBTNGO.SetActive (ClubsManager.Instance.IsClubAdmin());
		MainFireBaseManager.Instance.RefreshClubChatScreen (ClubsManager.Instance.CurrentClubO.ClubID);
	}

	public void AddThisPostO(PostO ThisPostO){
		GameObject NewPostPrefab = Instantiate (PostPrefab,PostsListContent) as GameObject;
		NewPostPrefab.GetComponent<Post> ().SetPostDetails (ThisPostO);
	}

	public void CreateNewPost(){
		PostO NewPost= new PostO();
		NewPost.CreatorFBID = Player.Instance.FBID;
		NewPost.MSGBody = NewPostINPTFeild.Text;
 		NewPost.CreatorName = Player.Instance.PlayerName;
		NewPost.PostID = RandomIDGenerator.Instance.GetRandomID ();
		MainFireBaseManager.Instance.CreateNewPostOrUpdateThisOne (NewPost);
		NewPostINPTFeild.Text = "";
		AddNewPostCloseBTN ();
		RefreshChatScreen ();
	}

	public void CreateNewPost(string PostMSG){
		PostO NewPost= new PostO();
		NewPost.CreatorFBID = Player.Instance.FBID;
		NewPost.MSGBody = PostMSG;
		NewPost.CreatorName = Player.Instance.PlayerName;
		NewPost.PostID = RandomIDGenerator.Instance.GetRandomID ();
		MainFireBaseManager.Instance.CreateNewPostOrUpdateThisOne (NewPost);
	}

	public void ShowThisPostDetails(PostO ThisPostO){
		Debug.Log ("ShowThisPostDetails");
		PostDetails.Instance.ShowThisPostDetails (ThisPostO);
	}

	public void CreateNewPostBTN(){
		AddNewPostWindowGO.SetActive (true);
	}

	public void AddNewPostCloseBTN(){
		AddNewPostWindowGO.SetActive (false);
	}
}