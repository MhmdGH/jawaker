﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Club : MonoBehaviour {

	public Text ClubNameTxt;
	public Slider ClubLevelSlider;
	public Image ClubIMG,VIPBadgeIMG,ClubColorIMG;
	[HideInInspector]
	public ClubO ThisClubO;

	void Start(){
		GetComponent<RectTransform> ().localScale = new Vector3 (1,1,1);
		ClubNameTxt.text = ThisClubO.ClubName;
		ClubLevelSlider.value = ThisClubO.ClubLevel;
		if (ThisClubO.HaveFTPIMG)
			FTPManager.Instance.GetPhotoFromFTPByName (ClubIMG,ThisClubO.ClubID);
		VIPBadgeIMG.enabled = ThisClubO.VIPBadge;
		ClubColorIMG.color = GetClubColor (ThisClubO.ClubColor);
	}

	public Color GetClubColor (int playerColor)
	{
		if (playerColor == 1)
			return Color.red;
		else if (playerColor == 2)
			return Color.yellow;
		else if (playerColor == 3)
			return Color.green;
		else if (playerColor == 4)
			return Color.black;
		else if (playerColor == 5)
			return Color.white;
		else if (playerColor == 6)
			return Color.magenta;
		else if (playerColor == 7)
			return Color.gray;
		else if (playerColor == 8)
			return Color.blue;
		else if (playerColor == 9)
			return Color.cyan;
		else if (playerColor == 10)
			return new Color(1,0.3f,0.5f,1);
		else
			return new Color (0, 0, 0, 0);
	}

	public void EnterClickBTN(){
		ClubsManager.Instance.CurrentSelectedClubID = ThisClubO.ClubID;
		ClubsManager.Instance.ShowClubDetails (ThisClubO);
	}
}