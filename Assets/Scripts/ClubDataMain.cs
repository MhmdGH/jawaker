﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClubDataMain : MonoBehaviour {

	public static ClubDataMain Instance;
	public Text ClubName;
	public Image CLubIMG;
	public GameObject ContainerGO;

	void Awake(){
		Instance=this;
	}

	IEnumerator Start(){
		yield return new WaitForSeconds (1);
		ContainerGO.SetActive (Player.Instance.IsJoinedClub ());
		if (Player.Instance.IsJoinedClub ())
			MainFireBaseManager.Instance.GetThisClubObj (Player.Instance.ClubID, 0);
	}

	public void SetData(bool HaveIMG){
		ClubName.text = Player.Instance.ClubName;
		if (HaveIMG)
			FTPManager.Instance.GetPhotoFromFTPByName (CLubIMG,Player.Instance.ClubID);
	}

}