﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AdvancedInputFieldPlugin;

public class ClubDetails : MonoBehaviour {
	public Text ClubNameTxt,ClubMemebersNoTxt,ClubLevelTxt,ClubRules;
	public Slider ClubLevelSlider;
	public RectTransform MembersContentRect;
	public Image ClubIMG,ClubColor,ClubColorBTN;
	public GameObject JoinBTN,ChangeClubIMGBTN,VIPBadge,VIPBadgeBTN,ClubOptionsBTNSContainerGO,EditRulesWindow,EditRulesBTN;
	public AdvancedInputField ClubDetailsInputFeild;
	public GameObject[] ClubOpts;
}