﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ClubsManager : MonoBehaviour {

	public static ClubsManager Instance;
	[HideInInspector]
	public string CreatedClubName="New Club",CurrentSelectedClubID="";
	public GameObject ClubPrefabeGO,MemberPrefabeGO,PickerGO;
	public RectTransform ClubsContentRect;
	public ClubDetails ClubDetailsGO;
	[HideInInspector]
	public ClubO CurrentClubO;
	public Sprite DefaultClubSprite;
	public Text[] ClubMainBTNSGO;
	public GameObject[] ClubMenus;

	void Awake(){
		Instance = this;
	}

	void Start(){
		StartCoroutine(CheckPlayerRequestStatusCor ());
	}

	IEnumerator CheckPlayerRequestStatusCor ()
	{
		yield return new WaitForSeconds (1);
		if (Player.Instance.JoinRequestID.Length > 2) {
			CurrentSelectedClubID = Player.Instance.JoinClubRequest;
			MainFireBaseManager.Instance.CheckMyJoinRequestStatus ();
		}
	}

	public void HandleRequestStatus(RequestO MyJoinRequest){
		if (MyJoinRequest.Status == -1)
			return;
		if (MyJoinRequest.Status == 1)
			JoinClubFun ();
		MainFireBaseManager.Instance.RemoveThisReuest (MyJoinRequest);
		Player.Instance.JoinRequestID = "";
		Player.Instance.JoinClubRequest = "";
		Player.Instance.JoinClubRequestName = "";
		Player.Instance.SavePlayerData ();
	}

	public void ShowClubDetails(ClubO ShowdClubDetails){
		StartCoroutine(ShowClubsDetailsCor (ShowdClubDetails));
	}

	private IEnumerator ShowClubsDetailsCor(ClubO ShowdClubDetails){
		WindowsManager.Instance.ShowLoader (2);
		yield return new WaitForSeconds (0.1f);
		CurrentClubO = ShowdClubDetails;
		WindowsManager.Instance.ShowWindowAndHideOther (9);
		ClubDetailsGO.ClubNameTxt.text = ShowdClubDetails.ClubName;
		ClubDetailsGO.ClubLevelTxt.text = ShowdClubDetails.ClubLevel.ToString();
		ClubDetailsGO.ClubMemebersNoTxt.text = ShowdClubDetails.ClubMembersNo.ToString();
		ClubDetailsGO.ClubLevelSlider.value = ShowdClubDetails.ClubLevel;
		for (int i = 0; i < ClubDetailsGO.MembersContentRect.childCount; i++)
			ClubDetailsGO.MembersContentRect.GetChild(i).gameObject.SetActive(false);
		for (int i = 0; i < ShowdClubDetails.Members.Count; i++) {
			if (i < 50)
				ShowdClubDetails.Members [i].Level = 1;
			else if(i>=50&&i<75)
				ShowdClubDetails.Members [i].Level = 2;
			else if(i>=75&&i<150)
				ShowdClubDetails.Members [i].Level = 3;
			else
				ShowdClubDetails.Members [i].Level = 4;
				AddMember (ShowdClubDetails.Members [i]);
		}
		if (IsClubAdmin ()) {
			ClubDetailsGO.JoinBTN.SetActive (false);
			ClubDetailsGO.ChangeClubIMGBTN.SetActive (ShowdClubDetails.ClubLevel>=2);
		} else {
			ClubDetailsGO.JoinBTN.SetActive (true);
			ClubDetailsGO.ChangeClubIMGBTN.SetActive (false);
		}
		ClubDetailsGO.ClubOptionsBTNSContainerGO.SetActive (IsClubAdmin());
		CheckShowdClubIMG ();
		CheckClubVipBadge ();
		CheckClubColor ();
		SelecteClubMainBTNSGO (0);
		SetCurrentClubForAllTabs ();
		ClubDetailsGO.ClubRules.text = CurrentClubO.Rules;
		if (IsClubAdmin ()) {
			ClubDetailsGO.ClubOpts [0].SetActive (Player.Instance.CanEditClubVip == 1);
			ClubDetailsGO.ClubOpts [1].SetActive (Player.Instance.CanEditClubVip == 1);
			ClubDetailsGO.ClubOpts [2].SetActive (Player.Instance.CanEditClubColor == 1);
			ClubDetailsGO.ClubOpts [3].SetActive (Player.Instance.CanEditClubColor == 1);
		}
	}

	void SetCurrentClubForAllTabs ()
	{
		ClubMainBTNSGO [1].GetComponent<RectTransform>().parent.gameObject.SetActive (IsClubAdmin());
		if (IsClubAdmin ())
			JoinRequestsManager.Instance.CurrentClub = CurrentClubO;
	}

	private void SelecteClubMainBTNSGO(int SelectedMenuBTN){
		for (int i = 0; i < ClubMainBTNSGO.Length; i++) {
			try{
				ClubMenus[i].SetActive(false);
				ClubMainBTNSGO [i].color = Color.white;}catch{
			}
		}
		ClubMenus[SelectedMenuBTN].SetActive(true);
		ClubMainBTNSGO [SelectedMenuBTN].color = Color.yellow;
	}

	public void BTNSFunction(){
	
		GameObject CurrentBTNGO =EventSystem.current.currentSelectedGameObject;
		string BTNName = CurrentBTNGO.name;
		if (BTNName == "ClubBTN")
			ShowWindowsNo (0);
		else if (BTNName == "JoinRequestsBTN")
			ShowWindowsNo (1);
		else if (BTNName == "RulesBTN")
			ShowWindowsNo (2);
		else if (BTNName == "ChatBTN")
			ShowWindowsNo (3);
		else if (BTNName == "CloseClubRulesBTN")
			ClubDetailsGO.EditRulesWindow.SetActive (false);
		else if (BTNName == "DoneEditingClubRulesBTN") {
			ClubDetailsGO.EditRulesWindow.SetActive (false);
			CurrentClubO.Rules = ClubDetailsGO.ClubDetailsInputFeild.Text;
			ClubDetailsGO.ClubRules.text = CurrentClubO.Rules;
			MainFireBaseManager.Instance.CreateNewClubOrAddMemberOrUpdate (CurrentClubO);
		} else if (BTNName == "EditClubRulesBTN") {
			ClubDetailsGO.ClubDetailsInputFeild.Text = CurrentClubO.Rules;
			ClubDetailsGO.EditRulesWindow.SetActive (true);
		} else if (BTNName == "DoneAddNewPostBTN")
			ChatScreenManager.Instance.CreateNewPost ();
	}

	public void ShowWindowsNo(int WindowNO){
		SelecteClubMainBTNSGO (WindowNO);
		WindowsManager.Instance.ShowLoader (0.5f);
		if (WindowNO == 0)
			ShowClubDetails (CurrentClubO);
		else if (WindowNO == 1)
			MainFireBaseManager.Instance.RefreshJoinRequestsList ();
		else if (WindowNO == 2)
			ClubDetailsGO.EditRulesBTN.SetActive (IsClubAdmin ());
		else if (WindowNO == 3)
			ChatScreenManager.Instance.RefreshChatScreen ();
	}

	public bool IsClubAdmin(){
		return CurrentClubO.ClubAdminID == Player.Instance.DatabasePlayerID;
	}

	public bool IsClubMember(){
		return CurrentClubO.ClubID == Player.Instance.ClubID;
	}

	public void CheckShowdClubIMG(){
		if (CurrentClubO.HaveFTPIMG)
			FTPManager.Instance.GetPhotoFromFTPByName (ClubDetailsGO.ClubIMG, CurrentClubO.ClubID);
		else
			ClubDetailsGO.ClubIMG.sprite = DefaultClubSprite;
	}

	private void AddMember(ClubMemberO ThisMember){
		int GetFirstAvailbeChildInt = GetFirstAvailbeChild ();
		GameObject NewMember = new GameObject ();
		if (GetFirstAvailbeChildInt != -1) {
			NewMember = ClubDetailsGO.MembersContentRect.GetChild (GetFirstAvailbeChildInt).gameObject;
			NewMember.SetActive (true);
		}
		else
			NewMember = Instantiate (MemberPrefabeGO, ClubDetailsGO.MembersContentRect);
		NewMember.GetComponent<Member> ().SetMemberDetails (ThisMember);
	}

	int GetFirstAvailbeChild ()
	{
		for (int i = 0; i < ClubDetailsGO.MembersContentRect.childCount; i++)
			if (!ClubDetailsGO.MembersContentRect.GetChild (i).gameObject.activeSelf)
				return i;
		return -1;
	}

	public void CreateClub(string ClubName){
		CreatedClubName = ClubName;
		StartCoroutine (CreateClubCor());
	}

	private IEnumerator CreateClubCor(){
		WindowsManager.Instance.ShowLoader (2);
		MainFireBaseManager.Instance.CreateNewClubOrAddMemberOrUpdate (GetCreatedClub());
		yield return new WaitForSeconds (1);
		MainFireBaseManager.Instance.RefreshClubsList ();
	}

	private ClubO GetCreatedClub ()
	{
		ClubO CreatedClubO = new ClubO ();
		CreatedClubO.ClubName = CreatedClubName;
		CreatedClubO.ClubID = RandomIDGenerator.Instance.GetRandomID ();
		CreatedClubO.ClubAdminID = Player.Instance.DatabasePlayerID;
		ClubMemberO NewMember = new ClubMemberO ();
		NewMember = GetMyData ();
		CreatedClubO.Members.Add (NewMember);
		Player.Instance.ClubID = CreatedClubO.ClubID;
		Player.Instance.ClubName = CreatedClubO.ClubName;
		Player.Instance.Coins -= 35000;
		Player.Instance.SavePlayerData ();
		return CreatedClubO;
	}

	ClubMemberO GetMyData ()
	{
		ClubMemberO ThisNewMemeber = new ClubMemberO ();
		ThisNewMemeber.MemberID = Player.Instance.DatabasePlayerID;
		ThisNewMemeber.MemberName = Player.Instance.PlayerName;
		ThisNewMemeber.MemberPoints = Player.Instance.XPs;
		ThisNewMemeber.MemberRank = 1;
		if (Player.Instance.FBID.Length > 3)
			ThisNewMemeber.MemberFBID = Player.Instance.FBID;
		return ThisNewMemeber;
	}

	public void AddThisClub (ClubO ThisJsonClubO)
	{
		GameObject NewClubGO = Instantiate (ClubPrefabeGO, ClubsContentRect.transform) as GameObject;
		NewClubGO.GetComponent<Club> ().ThisClubO = ThisJsonClubO;
	}

	public void ChangeClubIMGBTNClicked(){
		Debug.Log ("ChangeClubIMGBTN");
//		PickerGO.GetComponent<PickerController> ().OnPressShowPicker ();
	}

	public void ChangeCurrentClubToHasFTPIMG(){
		CurrentClubO.HaveFTPIMG = true;
		MainFireBaseManager.Instance.CreateNewClubOrAddMemberOrUpdate (CurrentClubO);
	}

	public void JoinClubBTN(){
		if (Player.Instance.JoinClubRequest.Length > 2) {
			PopsMSG.Instance.ShowPopMSG ("You are already sent a request");
			return;
		}
		if (Player.Instance.ClubID.Length > 2) {
			PopsMSG.Instance.ShowPopMSG ("You are already joined a club");
			return;
		}
		RequestO NewJoinRequest = new RequestO ();
		NewJoinRequest.RequestID = RandomIDGenerator.Instance.GetRandomID ();
		NewJoinRequest.SenderFBID = Player.Instance.FBID;
		NewJoinRequest.SenderName = Player.Instance.PlayerName;
		Player.Instance.JoinRequestID = NewJoinRequest.RequestID;
		Player.Instance.JoinClubRequest = ClubsManager.Instance.CurrentSelectedClubID;
		Player.Instance.JoinClubRequestName = CurrentClubO.ClubName;
		Player.Instance.SavePlayerData ();
		MainFireBaseManager.Instance.CreateNewJoinReuqest (NewJoinRequest);
		PopsMSG.Instance.ShowPopMSG ("Send Successfully");
	}

	public void JoinClubFun(){
		Player.Instance.ClubID = Player.Instance.JoinClubRequest;
		Player.Instance.ClubName = Player.Instance.JoinClubRequestName;
		Player.Instance.Badges [6] = 1;
		AddMeToThisClub ();
	}

	private void AddMeToThisClub(){
		MainFireBaseManager.Instance.GetThisClubObj (Player.Instance.ClubID);
	}

	public void AfterGetClubObj(ClubO ThisClub){
		CurrentClubO = ThisClub;
		CurrentClubO.ClubMembersNo += 1;
		CurrentClubO.Members.Add (GetMyData());
		MainFireBaseManager.Instance.CreateNewClubOrAddMemberOrUpdate (CurrentClubO);
	}

	public void ToggleVIPBadge(){
		CurrentClubO.VIPBadge = !CurrentClubO.VIPBadge;
		MainFireBaseManager.Instance.CreateNewClubOrAddMemberOrUpdate (CurrentClubO);
		CheckClubVipBadge ();
	}

	void CheckClubVipBadge ()
	{
		ClubDetailsGO.VIPBadge.SetActive (CurrentClubO.VIPBadge);
		if (CurrentClubO.VIPBadge) {
			ClubDetailsGO.VIPBadgeBTN.GetComponent<Image> ().color = Color.white;
			ClubDetailsGO.VIPBadgeBTN.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().color = Color.white;
		} else {
			ClubDetailsGO.VIPBadgeBTN.GetComponent<Image> ().color = new Color (1, 1, 1, 0.5f);
			ClubDetailsGO.VIPBadgeBTN.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().color = new Color (1, 1, 1, 0.5f);
		}
	}

	public void ChangeClubColorBTNClicked(){
		WindowsManager.Instance.WindowsGO [16].SetActive (true);
	}

	public void ClubColorSelected(){
		WindowsManager.Instance.WindowsGO [16].SetActive (false);
		GameObject CurrentBTNGO =EventSystem.current.currentSelectedGameObject;
		int SelectedColorNO = CurrentBTNGO.GetComponent<RectTransform> ().GetSiblingIndex ()+1;
		CurrentClubO.ClubColor = SelectedColorNO;
		MainFireBaseManager.Instance.CreateNewClubOrAddMemberOrUpdate (CurrentClubO);
		CheckClubColor ();
	}

	void CheckClubColor ()
	{
		if (CurrentClubO.ClubColor == 0)
			ClubDetailsGO.ClubColor.color = new Color (0,0,0,0);
		else {
			ClubDetailsGO.ClubColor.color = GetClubColor (CurrentClubO.ClubColor);
			ClubDetailsGO.ClubColorBTN.color = ClubDetailsGO.ClubColor.color;
		}
	}

	public Color GetClubColor (int playerColor)
	{
		if (playerColor == 1)
			return Color.red;
		else if (playerColor == 2)
			return Color.yellow;
		else if (playerColor == 3)
			return Color.green;
		else if (playerColor == 4)
			return Color.black;
		else if (playerColor == 5)
			return Color.white;
		else if (playerColor == 6)
			return Color.magenta;
		else if (playerColor == 7)
			return Color.gray;
		else if (playerColor == 8)
			return Color.blue;
		else if (playerColor == 9)
			return Color.cyan;
		else if (playerColor == 10)
			return new Color(1,0.3f,0.5f,1);
		else
			return new Color (0, 0, 0, 0);
	}
}