﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ColorSelect : MonoBehaviour {

	public static ColorSelect Instance;
	public int ColorNO;
	public Image SeatColor,PlayerIMG;
	public Text PlayerNameTxt;
	public GameObject ContainerGO;

	void Awake(){
		Instance = this;
	}

	public void ShowSeatsColors(int ThisColorNO){
		ContainerGO.SetActive (true);
		ColorNO = ThisColorNO;
		SeatColor.color = Player.Instance.GetPlayerColor(ThisColorNO);
		FBManager.Instance.SetFPPhoto (Player.Instance.FBID,PlayerIMG);
		PlayerNameTxt.text = Player.Instance.PlayerName;
	}

	public void CloseBTN(){
		ContainerGO.SetActive(false);
	}

	public void BTNSFunction(){
		GameObject CurrentBTNGO = EventSystem.current.currentSelectedGameObject;
		string BTNName = CurrentBTNGO.name;
		if (BTNName == "CloseBTN")
			ContainerGO.SetActive (false);
		else if (BTNName.Contains ("DaysColor")) {
			int ItemNO = CurrentBTNGO.GetComponent<RectTransform> ().GetSiblingIndex ();
			if (Player.Instance.Coins >= StoreManager.Instance.StoreData.SeatsColors [ItemNO]) {
				Debug.Log (ItemNO);
				if(Player.Instance.PlayerColor==ColorNO)
					Player.Instance.ColorDays += GetRealColorDays (ItemNO);
				else
					Player.Instance.ColorDays = GetRealColorDays (ItemNO);
				Debug.Log ("ItemNO\t"+ItemNO);
				StoreManager.Instance.PurchasedData.SeatsColors [ColorNO-1] = 1;
				StoreManager.Instance.CheckColorCounter ();
				StoreManager.Instance.SaveAndRefreshNewPlayerData ();
				PopsMSG.Instance.ShowPopMSG ("Color Purchased!");
			} else
				PopsMSG.Instance.ShowPopMSG ("No Enough Coins!");
		}
	}

	int GetRealColorDays (int itemNO)
	{
		if (itemNO == 0)
			return 3;
		else if (itemNO == 1)
			return 7;
		else
			return 21;
	}
}