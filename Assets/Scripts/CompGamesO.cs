using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]
public class CompGamesO
{
	public List<RoundO> MainRounds;
	public CompGamesO(){
		MainRounds = new List<RoundO> ();
		MainRounds.Add (new RoundO(1));
		MainRounds.Add (new RoundO(2));
		MainRounds.Add (new RoundO(4));
		MainRounds.Add (new RoundO(8));
	}
}