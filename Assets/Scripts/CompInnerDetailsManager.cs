using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using ArabicSupport;

public class CompInnerDetailsManager : MonoBehaviour {

	public static CompInnerDetailsManager Instance;
	public GameObject WallMSGPrefab;
	public Image OrginazorIMG;
	public Text CompTitleTxt,EntryFeesTxt,PrizeTxt,OrginazorNameTxt;
	public GameObject RequestsList,RequestPrefab,CompBTNGO,WallBTNGO,RequestToJoinBTNGO;
	public GameObject[] RoundsGO,CompWindowsGo,CompActionBTN;
	[HideInInspector]
	public MainCompO CurrentMainComp;
	public RectTransform MSGWallRect;
	public Text CompMSGTxt;

	void Awake(){
		Instance = this;
	}

	public void ShowThisMainCompDetails(MainCompO ThisMainComp){
		CurrentMainComp = ThisMainComp;
		ShowWindow (0);
		CompBTNGO.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().color = Color.yellow;
		WallBTNGO.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().color = Color.white;
		EntryFeesTxt.text = CurrentMainComp.CompOptions.EntryFees.ToString("00");
		PrizeTxt.text = CurrentMainComp.CompOptions.Prize.ToString("00");
		CompTitleTxt.text = CurrentMainComp.CompOptions.CompName;
		OrginazorNameTxt.text = CurrentMainComp.CompOptions.CompName;
		FBManager.Instance.SetFPPhoto (ThisMainComp.CompOptions.CreatorFBID,OrginazorIMG);
		SetRounds ();
		SetActiveCurrentRoundTables ();
		ShowCompActionBTN ();
		SetRoundsFBPhotos ();
	}

	void ShowCompActionBTN ()
	{
		RequestToJoinBTNGO.SetActive (Player.Instance.CurrentCompID.Length<2);
		if(CurrentMainComp.CompOptions.CompStatus == 3)
			ShowThisActionBTN ("Competition is ended");
		else if (IsCompCreator () && CurrentMainComp.CompOptions.CompStatus == 1)
			ShowThisActionBTN (0);
		else if (CompManager.Instance.IsAmemberInCurrentRound (CurrentMainComp) && IsCurrentRoundReadyToPlay ()) {
			if(CurrentMainComp.CompOptions.ImediateStart==1)
				ShowThisActionBTN (2);
			else if(CurrentMainComp.CompOptions.MinToStarts > (int)DateTime.UtcNow.TimeOfDay.TotalMinutes) {
				int StartsInTheseMins = Mathf.Abs (CurrentMainComp.CompOptions.MinToStarts - (int)DateTime.UtcNow.TimeOfDay.TotalMinutes);
				ThisStartsInTheseMins = StartsInTheseMins;
				ShowThisActionBTN ("Competition will starts in "+StartsInTheseMins+" minutes");
				return;
			}
			ShowThisActionBTN (2);
		}
		else if (ThereIsSeatAvailableInThisComp ())
			ShowThisActionBTN ("Please wait for players to join");
		else if (CompManager.Instance.IsAmemberInCurrentRound (CurrentMainComp) && !IsCurrentRoundReadyToPlay ())
			ShowThisActionBTN ("Please wait for next round to start");
	}

	private int ThisStartsInTheseMins;

	private void ShowThisActionBTN(string MSG){
		for (int i = 0; i < CompActionBTN.Length; i++)
			CompActionBTN [i].SetActive (false);
		if (MainLanguageManager.Instance.IsArabic) {
			if (MSG.Contains ("Competition will starts in"))
				MSG = ArabicFixer.Fix ("المسابقة ستبدأ خلال "+ThisStartsInTheseMins+" دقيقة");
			else if (MSG.Contains ("Please wait for players to join"))
				MSG = ArabicFixer.Fix ("يرجى انتظار باقي اللاعبين للانضمام");
			else if (MSG.Contains ("Please wait for next round to start"))
				MSG = ArabicFixer.Fix ("يرجى انتظار بدء الجولة الثانية");
			else if (MSG.Contains ("Competition is ended"))
				MSG = ArabicFixer.Fix ("المسابقة انتهت");
		}
		CompMSGTxt.enabled = true;
		CompMSGTxt.text = MSG;
	}

	private string CurrentRoundRoomID(){
		int CurrentRoundNO = CurrentMainComp.CompOptions.CurrentActiveRound;
		RoundO CurrentRoundO = CurrentMainComp.CompGames.MainRounds [CurrentRoundNO - 1];
		for (int i = 0; i < CurrentRoundO.Rounds.Count; i++) {
			OneRound OneRoundIteration = CurrentRoundO.Rounds [i];
			for (int u = 0; u < 4; u++)
				if (OneRoundIteration.PlayersIDs [u] == Player.Instance.DatabasePlayerID) {
					if (OneRoundIteration.RoundStatus == 1)
						return "0";
					return OneRoundIteration.RoomID;
				}
		}
		return "0";
	}

	bool IsCurrentRoundReadyToPlay ()
	{
		int CurrentRoundNO = CurrentMainComp.CompOptions.CurrentActiveRound;
		RoundO FirstMainRound = CurrentMainComp.CompGames.MainRounds [CurrentRoundNO - 1];
		for (int i = 0; i < FirstMainRound.Rounds.Count; i++) {
			OneRound OneRoundIteration = FirstMainRound.Rounds [i];
			if (OneRoundIteration.RoundStatus == 1)
				return false;
			for (int u = 0; u < 4; u++)
				if (OneRoundIteration.PlayersIDs [u] == Player.Instance.DatabasePlayerID) {
					for (int y = 0; y < 4; y++){
						if (OneRoundIteration.PlayersIDs [y].Length < 5)
							return false;
						else if(y==3)
							return true;
					}
				}
		}
		return false;
	}

	private bool IsCompCreator(){
		return CurrentMainComp.CompOptions.CreatorID == Player.Instance.DatabasePlayerID;
	}

	private void ShowThisActionBTN(int BTNNO){
		for (int i = 0; i < CompActionBTN.Length; i++)
			CompActionBTN [i].SetActive (false);
		CompActionBTN [BTNNO].SetActive (true);
		if (BTNNO == 2) {
			if(CurrentRoundRoomID()=="0")
				CompActionBTN [BTNNO].SetActive (false);
			SetPlayBTN ();
		}
		CompMSGTxt.enabled = false;
	}

	void SetPlayBTN ()
	{
		CompActionBTN [2].GetComponent<Button> ().onClick.RemoveAllListeners ();
		CompActionBTN [2].GetComponent<Button> ().onClick.AddListener (CompPlayBTNClicked);
	}

	void CompPlayBTNClicked ()
	{
		MainFireBaseManager.Instance.CheckThisRoomExists (CurrentRoundRoomID(),MainFireBaseManager.GameType);
	}

	public void CreateRoomOrJoinExistedOne(bool CreateOrJoin,string RoomID){
		//Create comp room
		if (CreateOrJoin)
			RoomsManager.Instance.CreateGameNowfunForComp (RoomID,CurrentMainComp);
		else {//Join comp ExistedRoom
			RoomsManager.PlayingRoomID = RoomID;
			Player.Instance.OnlinePlayerID = -1;
			SceneManager.LoadScene (MainGameManager.GameTypeID);
		}
	}

	void SetRoundsFBPhotos ()
	{
		for (int i = 0; i < RoundsGO.Length; i++)
			if (RoundsGO [i].activeSelf)
				RoundsGO [i].GetComponent<RoundManager> ().SetRoundPlayers (CurrentMainComp.CompGames.MainRounds[i].Rounds);
	}

	private void SetActiveCurrentRoundTables ()
	{
		for (int i = 0; i < RoundsGO.Length; i++)
			if(RoundsGO[i].activeSelf)
				RoundsGO [i].GetComponent<RoundManager> ().ActivateOrNot (false);
		RoundsGO [CurrentMainComp.CompOptions.CurrentActiveRound-1].GetComponent<RoundManager> ().ActivateOrNot (true);
	}

	private void SetRounds ()
	{
		for (int i = 0; i < RoundsGO.Length; i++)
			RoundsGO [i].SetActive (false);
		for (int i = 0; i < CurrentMainComp.CompOptions.RoundsNO; i++)
			RoundsGO [i].SetActive (true);
	}

	public void CompBTN(){
		MainFireBaseManager.Instance.GetThisCompObj (Player.Instance.CurrentCompID,2);
		ShowWindow (0);
		CompBTNGO.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().color = Color.yellow;
		WallBTNGO.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().color = Color.white;
	}

	public void WallBTN(){
		WindowsManager.Instance.ShowLoader (0.5f);
		ShowWindow (1);
		MainFireBaseManager.Instance.RefreshMSGWall (MSGWallRect.gameObject);
		CompBTNGO.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().color = Color.white;
		WallBTNGO.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().color = Color.yellow;
	}

	private void ShowWindow(int WindowNO){
		for(int i=0;i<CompWindowsGo.Length;i++)
			CompWindowsGo [i].SetActive (false);
		CompWindowsGo [WindowNO].SetActive (true);
	}

	public void RequestToJoinBTN(){
		if (Player.Instance.CurrentCompID.Length > 2) {
			PopsMSG.Instance.ShowPopMSG ("You are already sent a request Or already Joined a Competition");
			return;
		}
		if (Player.Instance.Coins < CurrentMainComp.CompOptions.EntryFees) {
			PopsMSG.Instance.ShowPopMSG ("No Enough Coins");
			return;
		}
		MainFireBaseManager.Instance.AddMSGWallToComp (Player.Instance.PlayerName+" send join request");
		RequestO NewJoinRequest = new RequestO ();
		NewJoinRequest.RequestID = RandomIDGenerator.Instance.GetRandomID ();
		NewJoinRequest.SenderFBID = Player.Instance.FBID;
		NewJoinRequest.SenderID = Player.Instance.DatabasePlayerID;
		NewJoinRequest.SenderName = Player.Instance.PlayerName;
		Player.Instance.CurrentCompID = CurrentMainComp.CompOptions.ID;
		Player.Instance.SavePlayerData ();
		MainFireBaseManager.Instance.CreateOrUpdateJoinReuqestToComp (NewJoinRequest);
		PopsMSG.Instance.ShowPopMSG ("Request Send Successfully");
		ShowCompActionBTN ();
	}

	public void ShowJoinRequestsBTN(){
		ShowWindow (2);
		MainFireBaseManager.Instance.RefreshCompJoinRequestsList (RequestsList);
	}

	public void AddThisRequest(RequestO ThisRequest){
		GameObject NewRequestPrefab = Instantiate (RequestPrefab,RequestsList.GetComponent<RectTransform>()) as GameObject;
		NewRequestPrefab.GetComponent<CompJoinRequest> ().SetRequestDetails (ThisRequest);
	}

	public void AddThisPlayerToComp (RequestO ThisRequest)
	{
		if (ThereIsSeatAvailableInThisComp (ThisRequest.SenderID, ThisRequest.SenderFBID)) {
			NotificationO NewNotification = new NotificationO ("Your Request To Join Comp Accepted");
			PlayingFireBaseFather.ParentInstance.NotifyThisPlayer (ThisRequest.SenderID,NewNotification);
			MainFireBaseManager.Instance.AddMSGWallToComp (ThisRequest.SenderName+" Accepted in this comp");
			PopsMSG.Instance.ShowPopMSG ("Player Added Successfully!");
			ShowJoinRequestsBTN ();
		}
		else
			PopsMSG.Instance.ShowPopMSG ("Competition is full!");
		if (!ThereIsSeatAvailableInThisComp ())
			CurrentMainComp.CompOptions.CompStatus = 2;
	}

	public bool ThereIsSeatAvailableInThisComp (string JoinedPlayerID,string PlayerFBID)
	{
		int CompRoundsNo = CurrentMainComp.CompOptions.RoundsNO;
		RoundO FirstMainRound = CurrentMainComp.CompGames.MainRounds [CompRoundsNo - 1];
		if (CurrentMainComp.CompOptions.Randomness == 0) {
			for (int i = 0; i < FirstMainRound.Rounds.Count; i++) {
				OneRound RoundOneIteration = FirstMainRound.Rounds [i];
				for (int y = 0; y < 4; y++) {
					if (RoundOneIteration.PlayersIDs [y].Length < 2) {
						RoundOneIteration.PlayersIDs [y] = JoinedPlayerID;
						RoundOneIteration.PlayersFBID [y] = PlayerFBID;
						CurrentMainComp.CompGames.MainRounds [CompRoundsNo - 1].Rounds [i] = RoundOneIteration;
						MainFireBaseManager.Instance.CreateOrUpdateComp (CurrentMainComp);
						return true;
					}
				}
			}
		} else {
			List<int> AvailableEmptyRoundNo = new List<int> ();
			for (int i = 0; i < FirstMainRound.Rounds.Count; i++) {
				OneRound RoundOneIteration = FirstMainRound.Rounds [i];
				for (int y = 0; y < 4; y++) {
					if (RoundOneIteration.PlayersIDs [y].Length < 2){
						AvailableEmptyRoundNo.Add (i);
						break;
					}
				}
			}
			int RandomEmptyRoundNo = AvailableEmptyRoundNo [UnityEngine.Random.Range(0,AvailableEmptyRoundNo.Count)];
			OneRound RandomEmptyRoundOneIteration = FirstMainRound.Rounds [RandomEmptyRoundNo];
			for (int i = 0; i < 4; i++) {
				if (RandomEmptyRoundOneIteration.PlayersIDs [i].Length < 2) {
					RandomEmptyRoundOneIteration.PlayersIDs [i] = JoinedPlayerID;
					RandomEmptyRoundOneIteration.PlayersFBID [i] = PlayerFBID;
					CurrentMainComp.CompGames.MainRounds [CompRoundsNo - 1].Rounds [RandomEmptyRoundNo] = RandomEmptyRoundOneIteration;
					MainFireBaseManager.Instance.CreateOrUpdateComp (CurrentMainComp);
					return true;
				}
			}
		}
		return false;
	}

	bool ThereIsSeatAvailableInThisComp ()
	{
		int CompRoundsNo = CurrentMainComp.CompOptions.RoundsNO;
		RoundO FirstMainRound = CurrentMainComp.CompGames.MainRounds [CompRoundsNo - 1];
		for (int i = 0; i < FirstMainRound.Rounds.Count; i++) {
			OneRound RoundOneIteration = FirstMainRound.Rounds [i];
			for(int y=0;y<4;y++)
			{
				if (RoundOneIteration.PlayersIDs [y].Length < 2)
					return true;
			}
		}
		return false;
	}

	public void CreateMSGWallPrefab(string MSGBody){
		GameObject MsgWallPrefabGO = Instantiate (WallMSGPrefab,MSGWallRect);
		MsgWallPrefabGO.GetComponent<WallMSG> ().SetMSGDetails(MSGBody);
	}
}