﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using ArabicSupport;

public class CompItem : MonoBehaviour {

	public Text CompNameTxt,EntryFeeTxt,PrizeTxt,SeatsAvailableTxt,TimeTxt,StatusTxt,EntryFeeNameTxt,PrizeNameTxt;
	public Image PhotoIMG,ImediateStartIMG,RandomIMG,ClockIMG,StatusIMG;
	private MainCompO MyComp;

	public void SetCompDetails(MainCompO ThisComp){
		MyComp = ThisComp;
		FBManager.Instance.SetFPPhoto (MyComp.CompOptions.CreatorFBID,PhotoIMG);
		CompNameTxt.text = MyComp.CompOptions.CompName;
		EntryFeeTxt.text = MyComp.CompOptions.EntryFees.ToString("00");
		PrizeTxt.text = MyComp.CompOptions.Prize.ToString("00");
		SeatsAvailableTxt.text = MyComp.CompOptions.SeatsAvailable.ToString("00");
		ImediateStartIMG.enabled = (MyComp.CompOptions.ImediateStart == 1);
		RandomIMG.enabled = (MyComp.CompOptions.Randomness == 1);
		ClockIMG.enabled = (MyComp.CompOptions.CompStatus == 1);
		TimeTxt.enabled = (MyComp.CompOptions.CompStatus == 1);
		if (MyComp.CompOptions.CompStatus == 1) {
			if (MyComp.CompOptions.MinToStarts < (int)DateTime.UtcNow.TimeOfDay.TotalMinutes) {
				if(!MainLanguageManager.Instance.IsArabic)
					TimeTxt.text = "Ready To Start";
				else
					TimeTxt.text = ArabicFixer.Fix("جاهزة");
				MyComp.CompOptions.MinToStarts = -1;
				MainFireBaseManager.Instance.CreateOrUpdateComp (MyComp);
			} else {
				int StartsInTheseMins = Mathf.Abs (MyComp.CompOptions.MinToStarts - (int)DateTime.UtcNow.TimeOfDay.TotalMinutes);
				if(!MainLanguageManager.Instance.IsArabic)
					TimeTxt.text = "In " + StartsInTheseMins + " Minutes";
				else
					TimeTxt.text = ArabicFixer.Fix("خلال" + StartsInTheseMins + " دقيقة");
			}
		}
		if(MainLanguageManager.Instance.IsArabic){
			EntryFeeNameTxt.text = ArabicFixer.Fix("رسوم الدخول");
			PrizeNameTxt.text = ArabicFixer.Fix("الجائزة");
		}
		SetCompStatus ();
		GetComponent<Button> ().onClick.AddListener (() => OnCompClickBTN());
	}

	void SetCompStatus ()
	{
		if (MyComp.CompOptions.CompStatus == 1) {
			StatusIMG.color = Color.green;
			if(!MainLanguageManager.Instance.IsArabic)
				StatusTxt.text = "New";
			else
				StatusTxt.text = ArabicFixer.Fix("جديدة");
		} else if (MyComp.CompOptions.CompStatus == 2) {
			StatusIMG.color = Color.yellow;
			if(!MainLanguageManager.Instance.IsArabic)
				StatusTxt.text = "Progress";
			else
				StatusTxt.text = ArabicFixer.Fix("جارية");
		} else {
			StatusIMG.color = Color.grey;
			if(!MainLanguageManager.Instance.IsArabic)
				StatusTxt.text = "Finished";
			else
				StatusTxt.text = ArabicFixer.Fix("منتهية");
		}
	}

	private void OnCompClickBTN(){
		MainFireBaseManager.Instance.GetThisCompObj (MyComp.CompOptions.ID,2);
	}
}