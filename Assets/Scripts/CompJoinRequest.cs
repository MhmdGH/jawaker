﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public class CompJoinRequest : MonoBehaviour {
	public Image RequestIMG;
	public Text RequestNameTxt,YesTxt,NoTxt;
	private RequestO MyRequestO;

	public void SetRequestDetails(RequestO ThisRequestO){
		MyRequestO = ThisRequestO;
		FBManager.Instance.SetFPPhoto (MyRequestO.SenderFBID,RequestIMG);
		RequestNameTxt.text = MyRequestO.SenderName;
		if (MainLanguageManager.Instance.IsArabic) {
			YesTxt.text = ArabicFixer.Fix("نعم");
			NoTxt.text = ArabicFixer.Fix("لا");
		}	
	}

	public void YesBTN(){
		MyRequestO.Status = 1;
		CompInnerDetailsManager.Instance.AddThisPlayerToComp (MyRequestO);
		MainFireBaseManager.Instance.CreateOrUpdateJoinReuqestToComp (MyRequestO);
	}

	public void NoBTN(){
		MyRequestO.Status = 0;
		MainFireBaseManager.Instance.CreateOrUpdateJoinReuqestToComp (MyRequestO);
		CompInnerDetailsManager.Instance.ShowJoinRequestsBTN ();
	}
}