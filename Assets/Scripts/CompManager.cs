﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;
using UnityEngine.UI;

public class CompManager : MonoBehaviour
{

	public static CompManager Instance;
	public CompDetailsGO CompDetails;
	public GameObject CompPrefabsGO;
	public RectTransform CompListViewRect;
	private List<MainCompO> CurrentListOfMainComp;
	private MainCompO CustomizedMainComp;
	private int[] CustomizedKeys;
	private int ChoosedFilterNO;
	private Color WhiteColor, YellowColor, DarkGreen;
	public static bool CompGameStarts;
	public Text Windows2Title,Windows3Title;

	void Awake ()
	{
		Instance = this;
	}

	void Start ()
	{
		CustomizedMainComp = new MainCompO ();
		CurrentListOfMainComp = new List<MainCompO> ();
		WhiteColor = Color.white;
		YellowColor = Color.yellow;
		DarkGreen = new Color (0, 0.8f, 0, 1);
		CompGameStarts = false;
	}

	public void ShowWithDefaultResaults ()
	{
		CustomizedKeys = new int[4]{ -1, -1, -1, 1 };
		CustomizedMainComp = new MainCompO ();
		CustomizedMainComp.CompOptions.GameType = MainFireBaseManager.GameType;
		SetDefaultIMGAndTxts ();
		CustomizeTheCurrentMainComp ();
		ShowThisTypeOfComp (CustomizedMainComp);
	}

	void SetDefaultIMGAndTxts ()
	{
		CompDetails.ImediateStartIMG.color = WhiteColor;
		CompDetails.RandomnessIMG.color = WhiteColor;
		CompDetails.EntryFeesIMG.color = WhiteColor;
		CompDetails.TimeIMG.color = WhiteColor;
		CompDetails.PeopleIMG.color = WhiteColor;
		CompDetails.NewTxt.color = YellowColor;
		CompDetails.InProgressTxt.color = WhiteColor;
		CompDetails.FinishedTxt.color = WhiteColor;
	}

	private void RefreshCustomizedMainComp ()
	{
		CustomizeTheCurrentMainComp ();
		ShowThisTypeOfComp (CustomizedMainComp);
	}

	public void BTNSFun ()
	{
		GameObject CurrentBTNGO = EventSystem.current.currentSelectedGameObject;
		string BTNName = CurrentBTNGO.name;
		if (BTNName == "MyCompBTN") {
			CompDetails.GamesListType.SetActive (false);
			CompDetails.CompListGO.SetActive (true);
			CompDetails.Windows1MainBTNSCompsTxt.color = WhiteColor;
			CompDetails.Windows1MainBTNSMyCompsTxt.color = YellowColor;
			CheckMyCompStatus ();
		} else if (BTNName == "CompBTN") {
			CompitionBTNClicked ();
		} else if (BTNName == "CreateBTN") {
			if (Player.Instance.BashaDays < 1) {
				PopsMSG.Instance.ShowPopMSG ("Only Basha Can Create Compititions");
				return;
			} else if (Player.Instance.Coins < 500) {
				PopsMSG.Instance.ShowPopMSG ("No Enough Coins");
				return;
			}
			CompOptionsWindow.Instance.ShowIT ();
		}
		else if (BTNName == "RulesBTN")
			Debug.Log ("RulesBTN");
		else if (CurrentBTNGO.GetComponent<RectTransform> ().parent.name == "GamesList") {
			MainFireBaseManager.GameType = BTNName;
			MainGameManager.GameTypeID = BTNSManager.Instance.GetActualGameID (CurrentBTNGO);
			ShowThisWindow (2);
			CompDetails.CompListGO.SetActive (true);
			ShowWithDefaultResaults ();
			Windows2Title.text = BTNName;
			Windows3Title.text = BTNName;
		}
	}

	public void ShowThisCompDetails (MainCompO ThisMainComp)
	{
		ShowThisWindow (3);
		CompInnerDetailsManager.Instance.ShowThisMainCompDetails (ThisMainComp);
	}

	public void ShowThisWindow (int WindowNO)
	{
		WindowsManager.Instance.ClearContentGO (CompListViewRect.gameObject);
		for (int i = 0; i < CompDetails.WindowsGo.Length; i++)
			CompDetails.WindowsGo [i].SetActive (false);
		CompDetails.WindowsGo [WindowNO - 1].SetActive (true);
		CompDetails.CompListGO.SetActive (WindowNO != 3);
		if (WindowNO == 2)
			RefreshCustomizedMainComp ();
	}

	void CompitionBTNClicked ()
	{
		ShowThisWindow (1);
		CompDetails.GamesListType.SetActive (true);
		CompDetails.CompListGO.SetActive (false);
		CompDetails.Windows1MainBTNSCompsTxt.color = YellowColor;
		CompDetails.Windows1MainBTNSMyCompsTxt.color = WhiteColor;
	}

	public void FilterResaults ()
	{
		GameObject CurrentBTNGO = EventSystem.current.currentSelectedGameObject;
		string BTNName = CurrentBTNGO.name;
		Debug.Log ("FilterResaults");
		if (BTNName == "ImediateStartBTN")
			ToggleThisInt (0);
		else if (BTNName == "RandomnessBTN")
			ToggleThisInt (1);
		else if (BTNName == "EntryFeesBTN")
			ToggleThisInt (2, 1);
		else if (BTNName == "TimeBTN")
			ToggleThisInt (2, 2);
		else if (BTNName == "NoOfPeopleIn")
			ToggleThisInt (2, 3);
		else if (BTNName == "NewBTN")
			CustomizedKeys [3] = 1;
		else if (BTNName == "InProgressBTN")
			CustomizedKeys [3] = 2;
		else if (BTNName == "FinishedBTN")
			CustomizedKeys [3] = 3;
		ToggleIMG (3);
		RefreshCustomizedMainComp ();
	}

	public void Show ()
	{
		CompitionBTNClicked ();
	}

	/*
	 * 0=>Imediate Start 
	 * 1=>Randomness
	 * 2=>Prize,Time,No Of Players
	 * 3=>Game Status
	 * */

	void CustomizeTheCurrentMainComp ()
	{
		try{
		CustomizedMainComp.CompOptions.ImediateStart = CustomizedKeys [0];
		CustomizedMainComp.CompOptions.Randomness = CustomizedKeys [1];
		CustomizedMainComp.CompOptions.CompStatus = CustomizedKeys [3];
			ChoosedFilterNO = CustomizedKeys [2];}catch{
		}
	}

	void ToggleIMG (int IMGNO)
	{
		if (IMGNO == 0) {
			if (CustomizedKeys [0] == 0)
				CompDetails.ImediateStartIMG.color = WhiteColor;
			else
				CompDetails.ImediateStartIMG.color = YellowColor;
		} else if (IMGNO == 1) {
			if (CustomizedKeys [1] == 0)
				CompDetails.RandomnessIMG.color = WhiteColor;
			else
				CompDetails.RandomnessIMG.color = YellowColor;
		} else if (IMGNO == 3) {
			CompDetails.NewTxt.color = WhiteColor;
			CompDetails.InProgressTxt.color = WhiteColor;
			CompDetails.FinishedTxt.color = WhiteColor;
			if (CustomizedKeys [3] == 1)
				CompDetails.NewTxt.color = YellowColor;
			else if (CustomizedKeys [3] == 2)
				CompDetails.InProgressTxt.color = YellowColor;
			else if (CustomizedKeys [3] == 3)
				CompDetails.FinishedTxt.color = YellowColor;
		}
	}

	void ToggleIMG (int IMGNO, int KeyNO)
	{
		CompDetails.EntryFeesIMG.color = WhiteColor;
		CompDetails.TimeIMG.color = WhiteColor;
		CompDetails.PeopleIMG.color = WhiteColor;
		if (KeyNO == 1)
			CompDetails.EntryFeesIMG.color = YellowColor;
		else if (KeyNO == 2)
			CompDetails.TimeIMG.color = YellowColor;
		else if (KeyNO == 3)
			CompDetails.PeopleIMG.color = YellowColor;
	}

	void ToggleThisInt (int ToggledCustomized)
	{
		if (CustomizedKeys [ToggledCustomized] == 1)
			CustomizedKeys [ToggledCustomized] = 0;
		else
			CustomizedKeys [ToggledCustomized] = 1;
		ToggleIMG (ToggledCustomized);
	}

	void ToggleThisInt (int ToggledCustomized, int KeyNO)
	{
		if (CustomizedKeys [ToggledCustomized] == KeyNO)
			CustomizedKeys [ToggledCustomized] = 0;
		else
			CustomizedKeys [ToggledCustomized] = KeyNO;
		ToggleIMG (ToggledCustomized, KeyNO);
	}

	public void ShowThisTypeOfComp (MainCompO CustomizedComp)
	{
		CurrentListOfMainComp.Clear ();
		MainFireBaseManager.Instance.ShowThisCustomizedComp (CustomizedComp, CompListViewRect.gameObject);
	}

	public void AddThisMainComp (MainCompO ThisMainComp)
	{
		GameObject ThisCompPrefabsGO = Instantiate (CompPrefabsGO, CompListViewRect) as GameObject;
		ThisCompPrefabsGO.GetComponent<CompItem> ().SetCompDetails (ThisMainComp);
	}

	public void AddFilteredComp (MainCompO ThisMainComp)
	{
		CurrentListOfMainComp.Add (ThisMainComp);
	}

	public void ShowFilteredComp (MainCompO ThisMainComp)
	{
		List<MainCompO> SortedCurrentListOfMainComp = new List<MainCompO> ();
		if (ChoosedFilterNO == 1)
			SortedCurrentListOfMainComp = CurrentListOfMainComp.OrderBy (o => o.CompOptions.EntryFees).ToList ();
		else if (ChoosedFilterNO == 2)
			SortedCurrentListOfMainComp = CurrentListOfMainComp.OrderBy (o => o.CompOptions.MinToStarts).ToList ();
		else if (ChoosedFilterNO == 3)
			SortedCurrentListOfMainComp = CurrentListOfMainComp.OrderBy (o => o.CompOptions.SeatsAvailable).ToList ();
		else
			SortedCurrentListOfMainComp = CurrentListOfMainComp;
		for (int i = 0; i < SortedCurrentListOfMainComp.Count; i++)
			AddThisMainComp (SortedCurrentListOfMainComp [i]);
	}

	public void CheckMyCompStatus ()
	{
		Debug.Log ("CheckMyCompStatus");
		MainFireBaseManager.Instance.CheckMyCompJoinRequest ();
		if (Player.Instance.CurrentCompID.Length > 2)
			MainFireBaseManager.Instance.GetThisCompObj (Player.Instance.CurrentCompID, 1);
	}

	public void HanldeReturnedCompObj (MainCompO ThisMainComp, int FunNO)
	{
		if (FunNO == 1)
			CheckMyCompStatusAfterCompOjbReturned (ThisMainComp);
		else if (FunNO == 2)
			ShowThisCompDetails (ThisMainComp);
	}

	private void CheckMyCompStatusAfterCompOjbReturned (MainCompO ThisMainComp)
	{
		if (IsAmemberInCurrentRound (ThisMainComp))
			Debug.Log ("I am Accepted");
		else
			Debug.Log ("Not Accepted");
	}

	public bool IsAmemberInCurrentRound (MainCompO CurrentMainComp)
	{
		int CurrentRoundNO = CurrentMainComp.CompOptions.CurrentActiveRound;
		RoundO FirstMainRound = CurrentMainComp.CompGames.MainRounds [CurrentRoundNO - 1];
		for (int i = 0; i < FirstMainRound.Rounds.Count; i++) {
			OneRound RoundOneIteration = FirstMainRound.Rounds [i];
			for (int y = 0; y < 4; y++)
				if (RoundOneIteration.PlayersIDs [y] == Player.Instance.DatabasePlayerID) {
					return true;
				}
		}
		return false;
	}
}