using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AdvancedInputFieldPlugin;

public class CompOptionsDetailsO : MonoBehaviour
{
	//Window 1
	public GameObject Window1GO;
	public Button[] RoundBTNs,TimeBTNs;
	public AdvancedInputField EntryFeesINPT, PrizeINPT;
	//Window 2
	public GameObject Window2GO;
	public Button ImediateStartBTN,RandomBTN,ChatBTN,SchuldedStartBTN;
	public Button[] GameSpeedBTNs;
	//Bottom Part
	public Text SeatsNOTxt,PriceTxt;
}