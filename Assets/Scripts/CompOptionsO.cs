using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]
public class CompOptionsO
{
	public string ID,CompName,Type,CreatorFBID,GameType,CreatorID;
	public int RoundsNO,EntryFees,Prize,SeatsAvailable,CompStatus,ImediateStart,
	Randomness,MinToStarts,CurrentActiveRound,ChatAvalability,SchuldedDate,CompSpeed;
	/*
	 * CompStatus:
	 * 1=>New
	 * 2=>Playing
	 * 3=>Ends
	 * */
	public CompOptionsO(){
		CreatorID = "";
		GameType = "";
		RoundsNO = 1;
		CompStatus = 1;
		ImediateStart = 0;
		Randomness = 0;
		CurrentActiveRound = 1;
		SchuldedDate = 0;
		CompSpeed = 1;
	}
}