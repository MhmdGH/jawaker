﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using ArabicSupport;

public class CompOptionsWindow : MonoBehaviour {

	public static CompOptionsWindow Instance;
	public GameObject ContainerGO;
	[HideInInspector]
	public MainCompO ThisMainComp;
	public CompOptionsDetailsO CompOptionsDetailsGO;
	private Color DarkGreen;

	void Awake(){
		Instance = this;
	}

	void Start(){
		DarkGreen = new Color (0,0.8f,0,1);
	}

	public void ShowIT(){
		if (Player.Instance.CurrentCompID.Length > 2) {
			PopsMSG.Instance.ShowPopMSG ("You are already in a competition!");
			return;
		}
		RestartDefaultOptions ();
		ContainerGO.SetActive (true);
	}

	public void HideIT(){
		ContainerGO.SetActive (false);
	}

	public void RestartDefaultOptions(){
		ThisMainComp = new MainCompO();
		ThisMainComp.CompOptions.ID = RandomIDGenerator.Instance.GetRandomID ();
		for (int i = 0; i < CompOptionsDetailsGO.RoundBTNs.Length; i++) {
			ToggleThisBTNOnOrOff (CompOptionsDetailsGO.RoundBTNs[i],false,false);
			ToggleThisBTNOnOrOff (CompOptionsDetailsGO.TimeBTNs[i],false,false);
		}
		ToggleThisBTNOnOrOff (CompOptionsDetailsGO.RoundBTNs[0],true,false);
		ToggleThisBTNOnOrOff (CompOptionsDetailsGO.TimeBTNs[0],true,false);
		CompOptionsDetailsGO.EntryFeesINPT.Text = "200";
		CompOptionsDetailsGO.PrizeINPT.Text = "1500";
		ThisMainComp.CompOptions.EntryFees = 200;
		ThisMainComp.CompOptions.Prize = 1500;
		ThisMainComp.CompOptions.GameType = MainFireBaseManager.GameType;
		ThisMainComp.CompOptions.CreatorFBID = Player.Instance.FBID;
		ThisMainComp.CompOptions.CompName = Player.Instance.PlayerName;
		ToggleThisBTNOnOrOff (CompOptionsDetailsGO.ImediateStartBTN,false,true);
		ToggleThisBTNOnOrOff (CompOptionsDetailsGO.RandomBTN,false,true);
		ToggleThisBTNOnOrOff (CompOptionsDetailsGO.ChatBTN,false,true);
		ToggleThisBTNOnOrOff (CompOptionsDetailsGO.SchuldedStartBTN,false,true);
		for (int i = 0; i < CompOptionsDetailsGO.GameSpeedBTNs.Length; i++)
			ToggleThisBTNOnOrOff (CompOptionsDetailsGO.GameSpeedBTNs[i],false,false);
		ToggleThisBTNOnOrOff (CompOptionsDetailsGO.GameSpeedBTNs[1],true,false);
		CompOptionsDetailsGO.PriceTxt.text = "1200";
		CompOptionsDetailsGO.SeatsNOTxt.text =	"4";
		ThisMainComp.CompOptions.MinToStarts = 15+(int)DateTime.UtcNow.TimeOfDay.TotalMinutes;
	}
	void ToggleThisBTNOnOrOff (Button CheckBTN,bool WithTxt)
	{
		if (CheckBTN.GetComponent<Image> ().color==Color.grey) {
			CheckBTN.GetComponent<Image> ().color = DarkGreen;
			if (WithTxt) {
				if(MainLanguageManager.Instance.IsArabic)
					CheckBTN.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().text = ArabicFixer.Fix ("نعم");
				else
					CheckBTN.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().text = "On";
			}
		} else {
			CheckBTN.GetComponent<Image> ().color = Color.grey;
			if (WithTxt) {
				if(MainLanguageManager.Instance.IsArabic)
					CheckBTN.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().text = ArabicFixer.Fix ("لا");
				else
					CheckBTN.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().text = "OFF";
			}
		}
	}
	void ToggleThisBTNOnOrOff (Button CheckBTN,bool CheckedOrNot,bool WithTxt)
	{
		if (CheckedOrNot) {
			CheckBTN.GetComponent<Image> ().color = DarkGreen;
			if (WithTxt) {
				if(MainLanguageManager.Instance.IsArabic)
					CheckBTN.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().text = ArabicFixer.Fix ("نعم");
				else
					CheckBTN.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().text = "On";
			}
		} else {
			CheckBTN.GetComponent<Image> ().color = Color.grey;
			if (WithTxt) {
				if(MainLanguageManager.Instance.IsArabic)
					CheckBTN.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().text = ArabicFixer.Fix ("لا");
				else
					CheckBTN.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().text = "OFF";
			}
		}
	}

	public void BTNSFun(){
		GameObject CurrentBTNGO = EventSystem.current.currentSelectedGameObject;
		string BTNName = CurrentBTNGO.name;
		if (CurrentBTNGO.GetComponent<RectTransform> ().parent.name == "RoundsBTN")
			SetRoundsNO (CurrentBTNGO);
		else if (CurrentBTNGO.GetComponent<RectTransform> ().parent.name == "StartsInBTNS")
			SetCompTimetoStart (CurrentBTNGO);
		else if (BTNName == "MoreOptionsBTN") {
			if (CompOptionsDetailsGO.Window1GO.activeSelf) {
				CompOptionsDetailsGO.Window1GO.SetActive (false);
				CompOptionsDetailsGO.Window2GO.SetActive (true);
			} else {
				CompOptionsDetailsGO.Window1GO.SetActive (true);
				CompOptionsDetailsGO.Window2GO.SetActive (false);
			}
		} else if (BTNName == "ImmediateStartBTN")
			ToggleImmediateOption ();
		else if (BTNName == "RandomBTN")
			ToggleRandomOption ();
		else if (BTNName == "ChatBTN")
			ToggleChatOption ();
		else if (BTNName == "DateStartBTN")
			ToggleDateStartOption ();
		if (CurrentBTNGO.GetComponent<RectTransform> ().parent.name == "CompSpeed")
			SetGameSpeed (CurrentBTNGO);
		else if (BTNName == "CreateCompNowBTN") {
			SetSomeStuffBeforCreatingNewComp ();
			MainFireBaseManager.Instance.CreateOrUpdateComp (ThisMainComp);
			WindowsManager.Instance.ShowLoader (1);
			HideIT ();
			CompManager.Instance.ShowThisCompDetails (ThisMainComp);
			if(Player.Instance.ClubID.Length>2)
				ChatScreenManager.Instance.CreateNewPost (Player.Instance.PlayerName+" created new "+ ThisMainComp.CompOptions.GameType+" compitition, The prize is:"
				+ThisMainComp.CompOptions.Prize+",and the cost to join is:"+ThisMainComp.CompOptions.EntryFees+", Comp ID is:"+ThisMainComp.CompOptions.ID);
		}
	}

	void SetSomeStuffBeforCreatingNewComp ()
	{
		ThisMainComp.CompOptions.CreatorFBID = Player.Instance.FBID;
		ThisMainComp.CompOptions.CreatorID = Player.Instance.DatabasePlayerID;
		SetRoomsIDs ();
	}

	void SetRoomsIDs ()
	{
		int CompRoundsNo = ThisMainComp.CompOptions.RoundsNO;
		for (int u = 0; u < ThisMainComp.CompGames.MainRounds.Count; u++) {
			RoundO ThisRoundIteration = ThisMainComp.CompGames.MainRounds [u];
			for (int i = 0; i < ThisRoundIteration.Rounds.Count; i++)
				ThisMainComp.CompGames.MainRounds [u].Rounds [i].RoomID = RandomIDGenerator.Instance.GetRandomID ();
		}
	}

	void SetGameSpeed (GameObject SpeedBTNGO)
	{
		for (int i = 0; i < CompOptionsDetailsGO.GameSpeedBTNs.Length; i++)
			ToggleThisBTNOnOrOff(CompOptionsDetailsGO.GameSpeedBTNs[i],false,false);
		int CompSpeed = SpeedBTNGO.GetComponent<RectTransform>().GetSiblingIndex()+1;
		ToggleThisBTNOnOrOff(CompOptionsDetailsGO.GameSpeedBTNs[CompSpeed-1],true,false);
		if (CompSpeed == 1)
			CompSpeed = 3;
		else if (CompSpeed == 3)
			CompSpeed = 1;
		ThisMainComp.CompOptions.CompSpeed = CompSpeed;
	}

	void ToggleDateStartOption ()
	{
		ToggleThisBTNOnOrOff(CompOptionsDetailsGO.SchuldedStartBTN,true);
		if (ThisMainComp.CompOptions.SchuldedDate == 0)
			ThisMainComp.CompOptions.SchuldedDate = 1;
		else
			ThisMainComp.CompOptions.SchuldedDate = 0;
	}
	void ToggleChatOption ()
	{
		ToggleThisBTNOnOrOff(CompOptionsDetailsGO.ChatBTN,true);
		if (ThisMainComp.CompOptions.ChatAvalability == 0)
			ThisMainComp.CompOptions.ChatAvalability = 1;
		else
			ThisMainComp.CompOptions.ChatAvalability = 0;
	}
	void ToggleRandomOption ()
	{
		ToggleThisBTNOnOrOff(CompOptionsDetailsGO.RandomBTN,true);
		if (ThisMainComp.CompOptions.Randomness == 0)
			ThisMainComp.CompOptions.Randomness = 1;
		else
			ThisMainComp.CompOptions.Randomness = 0;
	}
	void ToggleImmediateOption ()
	{
		ToggleThisBTNOnOrOff(CompOptionsDetailsGO.ImediateStartBTN,true);
		if (ThisMainComp.CompOptions.ImediateStart == 0)
			ThisMainComp.CompOptions.ImediateStart = 1;
		else
			ThisMainComp.CompOptions.ImediateStart = 0;
	}
	public void SetEntryFeesOnEndEdit(){
		int EntryFeesValue = int.Parse(CompOptionsDetailsGO.EntryFeesINPT.Text);
		if (EntryFeesValue < 1) {
			PopsMSG.Instance.ShowPopMSG ("Enter Valid Value");
			CompOptionsDetailsGO.EntryFeesINPT.Text = "";
			return;
		}
		ThisMainComp.CompOptions.EntryFees = EntryFeesValue;
	}
	public void SetPrizeOnEndEdit(){
		int PrizeValue = int.Parse(CompOptionsDetailsGO.PrizeINPT.Text);
		if (PrizeValue < 1) {
			PopsMSG.Instance.ShowPopMSG ("Enter Valid Value");
			CompOptionsDetailsGO.PrizeINPT.Text = "";
			return;
		}
		ThisMainComp.CompOptions.Prize = PrizeValue;
	}
	void SetCompTimetoStart (GameObject DateBTNGO)
	{
		for (int i = 0; i < CompOptionsDetailsGO.TimeBTNs.Length; i++)
			ToggleThisBTNOnOrOff(CompOptionsDetailsGO.TimeBTNs[i],false,false);
		int TimeNO = DateBTNGO.GetComponent<RectTransform>().GetSiblingIndex()+1;
		ToggleThisBTNOnOrOff(CompOptionsDetailsGO.TimeBTNs[TimeNO-1],true,false);
		ThisMainComp.CompOptions.MinToStarts = (TimeNO*15)+(int)DateTime.UtcNow.TimeOfDay.TotalMinutes;
	}
	void SetRoundsNO (GameObject RoundBTNGO)
	{
		for (int i = 0; i < CompOptionsDetailsGO.RoundBTNs.Length; i++)
			ToggleThisBTNOnOrOff(CompOptionsDetailsGO.RoundBTNs[i],false,false);
		int RoundNO = RoundBTNGO.GetComponent<RectTransform>().GetSiblingIndex()+1;
		ToggleThisBTNOnOrOff(CompOptionsDetailsGO.RoundBTNs[RoundNO-1],true,false);
		CompOptionsDetailsGO.PriceTxt.text = (RoundNO * 1200).ToString();
		int SeatsAvailableNo = 2;
		for(int i=0;i<RoundNO;i++)
			SeatsAvailableNo*=2;
		CompOptionsDetailsGO.SeatsNOTxt.text =	SeatsAvailableNo.ToString();
		ThisMainComp.CompOptions.RoundsNO = RoundNO;
		ThisMainComp.CompOptions.SeatsAvailable = SeatsAvailableNo;
		ThisMainComp.CompOptions.CurrentActiveRound = RoundNO;
	}
}