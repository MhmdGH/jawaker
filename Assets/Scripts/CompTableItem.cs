﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompTableItem : MonoBehaviour {

	[HideInInspector]
	public Image[] PlayersPhotosIMG;

	public void SetPlayersPhotos(string[] PlayersPhotos){
		for (int i = 0; i < PlayersPhotos.Length; i++)
			FBManager.Instance.SetFPPhoto (PlayersPhotos[i],PlayersPhotosIMG[i]);
	}

	public void ActiveOrNot(bool Activate){
		if (Activate)
			GetComponent<Image> ().color = Color.white;
		else
			GetComponent<Image> ().color = Color.gray;
	}
}