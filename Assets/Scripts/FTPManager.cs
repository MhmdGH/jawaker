﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Text;
using System.Linq;
using System.Net;

public class FTPManager : MonoBehaviour {

	public static FTPManager Instance;

	void Awake(){
		Instance=this;
	}

	public void UploadThisIMG(Image UploadIMG,string IMGName){
		string IMGPath = Application.persistentDataPath+"/"+IMGName+".png";
		File.WriteAllBytes(IMGPath, UploadIMG.sprite.texture.EncodeToPNG());
		Upload(IMGPath,"ftp://www.kotshena1992new.somee.com","arcenal","Admeral123","ClubsName");
	}

	public void GetPhotoFromFTPByName(Image SetThisIMG,string ClubName){
		Debug.Log ("ClubName\t"+ClubName);
		if (ClubName.Length < 3)
			return;
		StartCoroutine (GetPhotoFromFTPByNameCor (SetThisIMG,ClubName));
	}

	private IEnumerator GetPhotoFromFTPByNameCor(Image SetThisIMG,string ClubName){
		WWW www = new WWW ("www.kotshena1992new.somee.com/ClubsName/"+ClubName+".png");
		yield return www;
		if (www.texture.width < 10)
			yield break;
		SetThisIMG.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0,0));
	}

	static void Upload (string filename, string server, string username, string password, string initialPath)
	{
		var file = new FileInfo(filename);
		var address = new Uri("ftp://" + server + "/" + Path.Combine(initialPath, file.Name));
		var request = FtpWebRequest.Create(address) as FtpWebRequest;
		request.Credentials = new NetworkCredential(username, password);
		request.KeepAlive = false;
		request.Method = WebRequestMethods.Ftp.UploadFile;
		request.UseBinary = true;
		request.ContentLength = file.Length;
		var bufferLength = 2048;
		var buffer = new byte[bufferLength];
		var contentLength = 0;
		var fs = file.OpenRead();
		try {
				var stream = request.GetRequestStream();
				contentLength = fs.Read(buffer, 0, bufferLength);
				while (contentLength != 0) {
				stream.Write(buffer, 0, contentLength);
				contentLength = fs.Read(buffer, 0, bufferLength);
			}
			stream.Close();
			fs.Close();
		} catch (Exception e) {
			Debug.LogError("Error uploading file: " + e.Message);
			return;
		}
		Debug.Log("Upload successful.");
		ClubsManager.Instance.CheckShowdClubIMG ();
	}
	private byte[] downloadWithFTP(string ftpUrl, string savePath = "", string userName = "", string password = "")
	{
		FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(ftpUrl));
		request.UsePassive = true;
		request.UseBinary = true;
		request.KeepAlive = true;
		if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
		{
			request.Credentials = new NetworkCredential(userName, password);
		}
		request.Method = WebRequestMethods.Ftp.DownloadFile;
		if (!string.IsNullOrEmpty(savePath))
		{
			return null;
		}
		else
		{
			return downloadAsbyteArray(request.GetResponse());
		}
	}

	byte[] downloadAsbyteArray(WebResponse request)
	{
		using (Stream input = request.GetResponseStream())
		{
			byte[] buffer = new byte[16 * 1024];
			using (MemoryStream ms = new MemoryStream())
			{
				int read;
				while (input.CanRead && (read = input.Read(buffer, 0, buffer.Length)) > 0)
				{
					ms.Write(buffer, 0, read);
				}
				return ms.ToArray();
			}
		}
	}
}
