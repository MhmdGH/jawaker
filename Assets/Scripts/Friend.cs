﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Friend : MonoBehaviour {

	public Image FriendIMG;
	public Text FriendNameTxt;
	public Button ShowPlayerDataBTN;
	[HideInInspector]
	public FriendsO ThisFriend;
	private bool ForSendingItem;

	public void SetFriendDetails(FriendsO NewFriend){
		GetComponent<RectTransform> ().localScale = new Vector3 (1,1,1);
		ThisFriend = NewFriend;
		FBManager.Instance.SetFPPhoto (ThisFriend.FriendsFBID,FriendIMG);
		FriendNameTxt.text = ThisFriend.FriendName;
		GetComponent<Button> ().onClick.AddListener (OnFriendClick);
		ShowPlayerDataBTN.onClick.AddListener (ShowPlayerDataOnClick);
	}

	public void SetFriendDetails(FriendsO NewFriend,bool ForSendingItem){
		GetComponent<RectTransform> ().localScale = new Vector3 (1,1,1);
		ThisFriend = NewFriend;
		this.ForSendingItem = ForSendingItem;
		FBManager.Instance.SetFPPhoto (ThisFriend.FriendsFBID,FriendIMG);
		FriendNameTxt.text = ThisFriend.FriendName;
		GetComponent<Button> ().onClick.AddListener (OnFriendClick);
	}

	private void OnFriendClick(){
		if (ForSendingItem) {
			Debug.Log ("Fuk You");
			StoreManager.Instance.CurrentFriendSendID = ThisFriend.FriendID;
			StoreManager.Instance.ShowMyItem ();
		} else {
			WindowsManager.Instance.ShowLoader (true);
			PlayingFireBaseFather.ParentInstance.GetThisOnlinePlayerAndCheckIfCanWith (ThisFriend.FriendID, true);
		}
	}

	private void ShowPlayerDataOnClick(){
		PlayingFireBaseFather.ParentInstance.ShowThisPlayerData (ThisFriend.FriendID);

	}
}