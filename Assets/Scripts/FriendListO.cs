using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class FriendListO
{
	public List<FriendsO> MyFriends;
	public FriendListO(){
		MyFriends = new List<FriendsO> ();
	}
}
