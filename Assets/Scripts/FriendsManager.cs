﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AdvancedInputFieldPlugin;

public class FriendsManager : MonoBehaviour {

	public static FriendsManager Instance;
	public GameObject FriendsContainerGO, ChatContainerGO,FriendItemPrefab,ChatContentGO,ContainerGO;
	public RectTransform FriendsRect;
	public Text[] FriendsMainBTNTxt;
	public GameObject[] FriendsMenusGOs;
	public AdvancedInputField FriendsSearchINPT;

	void Awake(){
		Instance=this;
	}

	void Start(){
		StartCoroutine(CheckPlayerRequestStatusCor ());
	}

	IEnumerator CheckPlayerRequestStatusCor ()
	{
		yield return new WaitForSeconds (1);
		MainFireBaseManager.Instance.CheckMySendedFriendsRequestStatus ();
	}

	public void CloseChatWindow(){
		ChatContainerGO.SetActive (false);
		MainFireBaseManager.Instance.StopChatListeting ();
	}

	public void RequestMyFriendsList(){
		WindowsManager.Instance.HideAllWindows ();
		ContainerGO.SetActive (true);
		MainFireBaseManager.Instance.GiveMeMyFriendsList(true);
	}

	public void ShowMyFriendsList(List<FriendsO> MyFriendsList){
		bool ThereIsFriends = false;
		ShowFriendsList ();
		FriendsContainerGO.SetActive (true);
		for (int i = 0; i < FriendsRect.childCount; i++)
			DestroyObject (FriendsRect.GetChild (i).gameObject);
		foreach(FriendsO FriendItem in MyFriendsList){
			ThereIsFriends = true;
			GameObject NewFriendItem = Instantiate (FriendItemPrefab,FriendsRect);
			NewFriendItem.GetComponent<Friend> ().SetFriendDetails(FriendItem);
		}
		if (!ThereIsFriends)
			PopsMSG.Instance.ShowPopMSG ("You Have No Friends!");
	}

	public void GoToChatWithThisBitch(string FriendID){
		WindowsManager.Instance.ClearContentGO (ChatContentGO);
		ChatContainerGO.SetActive (true);
		PlayingFireBaseFather.ParentInstance.SetChatListnerForThisBitch (FriendID);
	}

	public void ShowFriendsList(){
		SelectFriendsWindow (0);
	}

	public void ShowRequestsList(){
		SelectFriendsWindow (1);
		FriendsRequetsManager.Instance.RefreshRequests ();
	}

	public void ShowSearchWindow(){
		SelectFriendsWindow (2);
		FriendsSearchINPT.ManualSelect (BeginEditReason.PROGRAMMATIC_SELECT);
	}

	private void SelectFriendsWindow(int SelectedMenuBTN){
		WindowsManager.Instance.ShowLoader (1);
		for (int i = 0; i < FriendsMainBTNTxt.Length; i++) {
			FriendsMenusGOs[i].SetActive(false);
			FriendsMainBTNTxt [i].color = Color.white;
		}
		FriendsMenusGOs[SelectedMenuBTN].SetActive(true);
		FriendsMainBTNTxt [SelectedMenuBTN].color = Color.yellow;
	}

	public void AddFriend (OnlinePlayerO ThisOnlinePlayer)
	{
		//Add This New Friend In My Data
		FriendsO NewFriend = new FriendsO ();
		NewFriend.FriendID = ThisOnlinePlayer.PlayerID;
		NewFriend.FriendName = ThisOnlinePlayer.PlayerName;
		NewFriend.FriendsFBID = ThisOnlinePlayer.FBID;
		Player.Instance.ThisOnlinePlayer.MyFrinedListO.MyFriends.Add (NewFriend);
		if (Player.Instance.ThisOnlinePlayer.MyFrinedListO.MyFriends.Count >= 10) {
			Player.Instance.Badges [1] = 1;
			Player.Instance.ThisOnlinePlayer.Badges [1] = 1;
		}
		PlayingFireBaseFather.ParentInstance.UpdateThisPlayerData (Player.Instance.ThisOnlinePlayer);
		//Add Me As a friend to his data
		FriendsO NewFriend2 = new FriendsO ();
		NewFriend2.FriendID = Player.Instance.DatabasePlayerID;
		NewFriend2.FriendName = Player.Instance.PlayerName;
		NewFriend2.FriendsFBID = Player.Instance.FBID;
		ThisOnlinePlayer.MyFrinedListO.MyFriends.Add (NewFriend2);
		NotificationO NewNotification = new NotificationO ("" + Player.Instance.PlayerName + " Is a friend with you now!");
		PlayingFireBaseFather.ParentInstance.NotifyThisPlayer (ThisOnlinePlayer.PlayerID,NewNotification);
		PlayingFireBaseFather.ParentInstance.UpdateThisPlayerData (ThisOnlinePlayer);
	}
}