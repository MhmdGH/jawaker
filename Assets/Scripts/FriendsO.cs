using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class FriendsO
{
	public string FriendID,FriendName,FriendsFBID;
	public bool Blocked;
	public MessagesO Messages;
	public FriendsO(){
		Blocked = false;
	}
}