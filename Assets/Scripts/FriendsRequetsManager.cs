﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendsRequetsManager : MonoBehaviour {

	public static FriendsRequetsManager Instance;
	public RectTransform RequestListRect;
	public GameObject FriendRequestPrefab;

	void Awake(){
		Instance=this;
	}

	public void RefreshRequests(){
		Debug.Log ("RefreshRequests");
		MainFireBaseManager.Instance.RefreshFriendRequestList ();
	}

	public void AddThisRequest(RequestO ThisRequest){
		GameObject NewRequestPrefab = Instantiate (FriendRequestPrefab,RequestListRect) as GameObject;
		NewRequestPrefab.GetComponent<FriendRequest> ().SetRequestDetails (ThisRequest);
	}

	public void HandleRequestStatus(RequestO ThisFriendRequest){
		if (ThisFriendRequest.Status == -1)
			return;
		if (ThisFriendRequest.Status == 1)
			MainFireBaseManager.Instance.GetThisOnlinePlayerAndAddAsFriend (ThisFriendRequest.RecieverID);
		MainFireBaseManager.Instance.RemoveThisFriendReuest (ThisFriendRequest,true);
	}
}