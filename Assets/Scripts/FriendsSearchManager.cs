﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AdvancedInputFieldPlugin;

public class FriendsSearchManager : MonoBehaviour {

	public static FriendsSearchManager Instance;
	public RectTransform ResaultsListRect;
	public GameObject ResaulPrefabO,SearchResaulsWindowGO,SearchTypeWindowGO;
	public AdvancedInputField SearchInputFeild;
	[HideInInspector]
	public int SearchType;

	void Awake(){
		Instance=this;
	}

	public void SearchThisBitch(){
		if (SearchInputFeild.Text.Length == 0)
			return;
//		if (SearchType == 1)
			MainFireBaseManager.Instance.SearchThisPlayer (SearchInputFeild.Text, ResaultsListRect);
//		else
//			Debug.Log ("Search By Facebook Friends");
	}

	public void AddThisRequest(OnlinePlayerO ThisResaultO){
		GameObject NewResaultO = Instantiate (ResaulPrefabO,ResaultsListRect) as GameObject;
		NewResaultO.GetComponent<SearchResaults> ().SetResaultsDetails (ThisResaultO);
	}

	public void ShowWindow(int WindowNO){
		if (WindowNO == 1) {
			SearchTypeWindowGO.SetActive (true);
			SearchResaulsWindowGO.SetActive (false);
		} else {
			SearchTypeWindowGO.SetActive (false);
			SearchResaulsWindowGO.SetActive (true);
		}
	}

	public void SearchByUserName(){
//		ShowWindow (2);
//		SearchType = 1;
	}
	public void SearchByFacebookFriends(){
//		ShowWindow (2);
//		SearchType = 2;
	}

}