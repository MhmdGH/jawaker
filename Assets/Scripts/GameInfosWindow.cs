﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameInfosWindow : MonoBehaviour {

	public static GameInfosWindow Instance;
	public GameObject ContainerGO;
	public Text[] OptionsArray;
	/*
	 * OptionsArray:
	 * 0=> GameID
	 * 1=> GameType
	 * 2=> GameLevel
	 * 3=> GameCreator
	 * 4=> Game Speed
	 * 5=> AllowKicking
	 * 6=> NoLEaving
	 * 7=> Private
	 * */

	void Awake(){
		Instance=this;
	}

	public void Show(){
		JsonRoomO ThisJsonRoom = PlayersManagerParent.ParentInstance.TempCurrenRoom;
		OptionsArray [0].text = "GameID: " + ThisJsonRoom.RoomID;
		OptionsArray [1].text = "GameType: " + ThisJsonRoom.GameType;
		OptionsArray [2].text = "Min.Level: " + ThisJsonRoom.RoomOptions.MinLevel;
		OptionsArray [3].text = "Creator: " + ThisJsonRoom.CreatorName;
		OptionsArray [4].text = "Game Speed: " + GetRealSpeed(ThisJsonRoom.RoomOptions.GameSpeed);
		OptionsArray [5].text = "Allow Kicking: " + ThisJsonRoom.RoomOptions.AllowKicking;
		OptionsArray [6].text = "No Leaving: " + ThisJsonRoom.RoomOptions.NoLeaving;
		OptionsArray [7].text = "Private: " + ThisJsonRoom.RoomOptions.Private;
		ContainerGO.SetActive (true);
	}

	string GetRealSpeed (int gameSpeed)
	{
		if (gameSpeed == 80)
			return "Fast";
		else if (gameSpeed == 120)
			return "Normal";
		else
			return "Slow";
	}

	public void Hide(){
		ContainerGO.SetActive (false);
	}

}