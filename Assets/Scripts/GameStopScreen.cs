﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class GameStopScreen : MonoBehaviour {

	public static GameStopScreen Instance;
	public Text TimeToStartTxt;
	public GameObject ContainerGO,Container2GO;
	private TimeO MyTime;
	[HideInInspector]
	public bool IsShowed;

	void Awake(){
		Instance = this;
	}

	public void Show(){
		MyTime=new TimeO();
		IsShowed = true;
		MyTime.Hour= PlayersManagerParent.ParentInstance.TempCurrenRoom.PauseTime.Hour;
		MyTime.Minutes = PlayersManagerParent.ParentInstance.TempCurrenRoom.PauseTime.Minutes;
		TimeToStartTxt.text = MyTime.Hour.ToString ("00") + ":"	+ MyTime.Minutes.ToString ("00");
		ContainerGO.SetActive (true);
		CancelInvoke ();
		InvokeRepeating ("CheckGameStarts",0,3);
	}

	void CheckGameStarts(){
		if (PlayersManagerParent.ParentInstance.TempCurrenRoom.GamePaused) {
			if (DateTime.UtcNow.DayOfYear >= MyTime.DayOfYear && DateTime.UtcNow.Hour >= MyTime.Hour && DateTime.UtcNow.Minute >= MyTime.Minutes) {
				PlayersManagerParent.ParentInstance.TempCurrenRoom.GamePaused = false;
				PlayersManagerParent.ParentInstance.TempCurrenRoom.ActionID = PlayersManagerParent.ParentInstance.TempCurrenRoom.PrevActionID;
				PlayingFireBaseFather.ParentInstance.UploudThisJson (true);
				CancelInvoke ();
			}
		}
	}

	public void Hide(){
		IsShowed = false;
		ContainerGO.SetActive (false);
	}

	public void ShowChooseStopWindow(){
		Container2GO.SetActive (true);
	}

	public void StopGame(){
		GameObject CurrentBTNGO = EventSystem.current.currentSelectedGameObject;
		string BTNName = CurrentBTNGO.name;
		ChooseTimeToStop (int.Parse(BTNName));
		HideChooseTimeStop ();
	}

	public void ChooseTimeToStop(int MinToStop){
		PlayersManagerParent.ParentInstance.TempCurrenRoom.GamePaused = true;
		TimeO PasuedTime = TimeO.GetCurrentTime ();
		PasuedTime.Minutes += MinToStop;
		if (PasuedTime.Minutes >= 60) {
			PasuedTime.Minutes -= 60;
			PasuedTime.Hour += 1;
			if (PasuedTime.Hour >= 24) {
				PasuedTime.Hour -= 24;
				PasuedTime.DayOfYear += 1;
			}
		}
		PlayersManagerParent.ParentInstance.TempCurrenRoom.PrevActionID =
			PlayersManagerParent.ParentInstance.TempCurrenRoom.ActionID;
		PlayersManagerParent.ParentInstance.TempCurrenRoom.ActionID = -3;
		PlayersManagerParent.ParentInstance.TempCurrenRoom.PauseTime = PasuedTime;
		PlayingFireBaseFather.ParentInstance.UploudThisJson (true);
	}

	public void HideChooseTimeStop(){
		Container2GO.SetActive (false);
	}
}