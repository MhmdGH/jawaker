﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HelpWindow : MonoBehaviour {

	public static HelpWindow Instance;
	public GameObject[] MainSubjectsBTNS;
	public Sprite[] DetailsTxtsIMG;
	public Image DetailsIMG;
	public GameObject ContainerGO,DetailsWindowGO;

	void Awake(){
		Instance=this;
	}

	public void ShowMainMainSubjectsBTNS(){
		GameObject CurrentBTNGO =EventSystem.current.currentSelectedGameObject;
		for (int i = 0; i < MainSubjectsBTNS.Length; i++)
			MainSubjectsBTNS [i].SetActive (false);
		MainSubjectsBTNS [CurrentBTNGO.GetComponent<RectTransform>().GetSiblingIndex()].SetActive (true);
	}

	public void SHowDetailsWindow(int DetailsIMGNOToShow){
		DetailsIMG.sprite = DetailsTxtsIMG [DetailsIMGNOToShow];
		DetailsWindowGO.SetActive (true);
	}

	public void DetailsCloseBTN(){
		DetailsWindowGO.SetActive(false);
	}
}