﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using ArabicSupport;
using AdvancedInputFieldPlugin;
using System;

public class InputArabicFix : MonoBehaviour
{
	private AdvancedInputField MyinputFeild;
	private Text InPutTextTxt;
	public Text OutPutTextTxt;

	void Start ()
	{
		try {
			MyinputFeild = GetComponent<AdvancedInputField> ();
			MyinputFeild.PlaceHolderText = ArabicFixer.Fix(MyinputFeild.PlaceHolderText,false,false);
			MyinputFeild.OnValueChanged.AddListener (onValueChanged);
			MyinputFeild.OnEndEdit.AddListener (EndEdit);
			SetThisOnPointerDownTrigger ();
		} catch (Exception e){
			Debug.Log("Error\t"+e);
			return;
		}
		InPutTextTxt = MyinputFeild.TextRenderer;
	}

	void onValueChanged (string arg0)
	{
		string InputStr = MyinputFeild.Text;
		OutPutTextTxt.text = ArabicFixer.Fix (InputStr,false,false);
	}

	void EndEdit (string arg0, EndEditReason arg1)
	{
		Debug.Log ("EndEdit");
		string OutPutStr = OutPutTextTxt.text;
		MyinputFeild.Text = OutPutTextTxt.text;
		InPutTextTxt.color = Color.black;
		OutPutTextTxt.text = "";
	}

	void SetThisOnPointerDownTrigger ()
	{
		EventTrigger trigger = gameObject.AddComponent (typeof(EventTrigger)) as EventTrigger;
		EventTrigger.Entry entry = new EventTrigger.Entry ();
		entry.eventID = EventTriggerType.PointerClick;
		entry.callback.AddListener ((eventData) => {
			OnItemClicked ();
		});
		trigger.triggers.Add (entry);
	}

	void OnItemClicked ()
	{
		InPutTextTxt.color = Color.clear;
		MyinputFeild.Text = "";
	}
}