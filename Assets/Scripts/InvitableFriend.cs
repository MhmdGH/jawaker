﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InvitableFriend : MonoBehaviour {

	public Image ProfileIMG;
	public Text PlayerName;
	public GameObject CheckedIcon;
	public FriendsO MyFriend;

	public void SetDetails(FriendsO ThisFriend){
		MyFriend = ThisFriend;
		FatherFBManager.ParentInstance.SetFPPhoto (MyFriend.FriendsFBID,ProfileIMG);
		PlayerName.text = MyFriend.FriendName;
		CheckedIcon.SetActive (false);
		GetComponent<Button> ().onClick.AddListener (ToggleOnClick);
	}

	public void ToggleOnClick(){
		CheckMe (!CheckedIcon.activeSelf);
	}

	public void CheckMe(bool YesOrNo){
		Debug.Log ("CheckMe\t"+YesOrNo);
		if ((YesOrNo && CheckedIcon.activeSelf)||(!YesOrNo && !CheckedIcon.activeSelf))
			return;
		CheckedIcon.SetActive (YesOrNo);
		if (YesOrNo)
			InvitationManager.Instance.InviteThisOne (MyFriend.FriendID);
		else
			InvitationManager.Instance.RemoveThisOne (MyFriend.FriendID);
	}
}