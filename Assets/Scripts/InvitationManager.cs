﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InvitationManager : MonoBehaviour {

	public static InvitationManager Instance;
	private List<string> InvitedPlayers;
	public GameObject InvitableFriendPrefabGO,ContainerGO;
	public RectTransform InvitableFriendsListContent;
	private bool AllIsChecked;
	public Image AllCheckedSignIMG;
	public string GooglePlayURL,IOSURL;

	void Awake(){
		Instance=this;
	}

	void Start(){
		InvitedPlayers = new List<string> ();
	}

	public void ShowFriendsList(){
		Debug.Log ("ShowFriendsList");
		ContainerGO.SetActive (true);
		InvitedPlayers.Clear ();
		PlayingFireBaseFather.ParentInstance.ShowInvitableFriends (InvitableFriendsListContent);
	}

	public void ShareLink(){
		#if UNITY_ANDROID
		NativeShare.Share("Try Kotshena and challenge your friends now "+GooglePlayURL, "", GooglePlayURL, "", "text/plain", true, "");
		#elif UNITY_IOS
		NativeShare.Share("Try Kotshena and challenge your friends now "+IOSURL, "", IOSURL, "", "text/plain", true, "");
		#endif
		StartCoroutine (GiveHimSomeCoinsCor());
	}

	private IEnumerator GiveHimSomeCoinsCor(){
		yield return new WaitForSeconds (2);
		Player.Instance.Coins += 100;
		Player.Instance.SavePlayerData ();
		PopsMSG.Instance.ShowPopMSG ("Congrats, You got +100 Coins!");
	}

	public void AddThisFriend(FriendsO ThisFriendO){
		GameObject NewFriendPrfab = Instantiate (InvitableFriendPrefabGO,InvitableFriendsListContent) as GameObject;
		NewFriendPrfab.GetComponent<InvitableFriend> ().SetDetails(ThisFriendO);
	}

	public void InviteThisOne(string PlayerID){
		InvitedPlayers.Add (PlayerID);
	}

	public void RemoveThisOne(string PlayerID){
		InvitedPlayers.Remove (PlayerID);
	}

	public void CloseInvitiationWindow(){
		ContainerGO.SetActive (false);
	}

	public void ToggleSelectAll(){
		for (int i = 0; i < InvitableFriendsListContent.childCount; i++)
			InvitableFriendsListContent.GetChild (i).GetComponent<InvitableFriend> ().CheckMe (!AllIsChecked);
		AllIsChecked = !AllIsChecked;
		AllCheckedSignIMG.enabled = AllIsChecked;
	}

	public void SendInvitations(){
		for (int i = 0; i < InvitedPlayers.Count; i++) {
			Debug.Log (InvitedPlayers [i]);
			PlayingFireBaseFather.ParentInstance.NotifyThisPlayer (InvitedPlayers [i],
				new NotificationO ("" + Player.Instance.PlayerName + " invited you to play!"));
		}
		CloseInvitiationWindow ();
		if(InvitedPlayers.Count>0)
			PopsMSG.Instance.ShowPopMSG ("Invitiations Send Successfully!");
	}
}