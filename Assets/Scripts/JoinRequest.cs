using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public class JoinRequest : MonoBehaviour
{
	public Image RequestIMG;
	public Text RequestNameTxt,YesTxt,NoTxt;
	private RequestO MyRequestO;

	public void SetRequestDetails(RequestO ThisRequestO){
		MyRequestO = ThisRequestO;
		FBManager.Instance.SetFPPhoto (MyRequestO.SenderFBID,RequestIMG);
		RequestNameTxt.text = MyRequestO.SenderName;
		if (MainLanguageManager.Instance.IsArabic) {
			YesTxt.text = ArabicFixer.Fix("نعم");
			NoTxt.text = ArabicFixer.Fix("لا");
		}	
	}

	public void YesBTN(){
		MyRequestO.Status = 1;
		MainFireBaseManager.Instance.UpdateJoinReuqest (MyRequestO);
		WindowsManager.Instance.ShowLoader (1);
		ClubsManager.Instance.ShowWindowsNo (1);
	}
	public void NoBTN(){
		MyRequestO.Status = 0;
		MainFireBaseManager.Instance.UpdateJoinReuqest (MyRequestO);
		WindowsManager.Instance.ShowLoader (1);
		ClubsManager.Instance.ShowWindowsNo (1);
	}
}