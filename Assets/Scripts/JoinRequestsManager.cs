﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoinRequestsManager : MonoBehaviour {

	public static JoinRequestsManager Instance;
	public RectTransform RequestListRect;
	[HideInInspector]
	public ClubO CurrentClub;
	public GameObject RequestPrefab;

	void Awake(){
		Instance=this;
	}

	public void RefreshRequests(){
		Debug.Log ("RefreshRequests");
		MainFireBaseManager.Instance.RefreshClubChatScreen (ClubsManager.Instance.CurrentClubO.ClubID);
	}

	public void AddThisRequest(RequestO ThisRequest){
		GameObject NewRequestPrefab = Instantiate (RequestPrefab,RequestListRect) as GameObject;
		NewRequestPrefab.GetComponent<JoinRequest> ().SetRequestDetails (ThisRequest);
	}
}