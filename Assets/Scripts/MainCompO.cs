using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]
public class MainCompO
{
	public CompGamesO CompGames;
	public CompOptionsO CompOptions;
	public MainCompO(){
		CompOptions = new CompOptionsO ();
		CompGames = new CompGamesO ();
	}
}