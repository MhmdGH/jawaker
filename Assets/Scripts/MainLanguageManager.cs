using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ArabicSupport;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainLanguageManager : LanguangeManagerFather {
	public static MainLanguageManager Instance;
	public GameObject[] SplashTitles;
	public Text InternetCheckMSG,StoreTable,LoadingTxt;
	public Text[] MainTxts,StoreTitles,StoreBasha,StoreCoins,StoreBoosters,StoreColors;
	public Text[] PlayerDetailsTxts;
	public Text[] NormalGamesList, CompeitionsGamesList,GamesListsTitles;
	public Text[] ServersWindowTitles, RoomOptionsWindow;
	public Text[] SideMenuTxts;
	public Text[] ClubsWindow, NotificationsWindow, FriendsWindow;
	public Text[] CompStuff;

	public override void Awake(){
		base.Awake ();
		Instance = this;
	}

	public override void Start(){
		base.Start ();
		if(IsArabic)
			SetTheFukinArabicLang ();
		
	}

	public override void SetTheFukinArabicLang ()
	{
		SitArabicFonts ();
		MainTxts [0].text = ArabicFixer.Fix("الألعاب");
		MainTxts [1].text = ArabicFixer.Fix("المسابقات");
		MainTxts [2].text = ArabicFixer.Fix("المتجر");
		MainTxts [3].text = ArabicFixer.Fix("إنضم لمسابقة");
		InternetCheckMSG.text = ArabicFixer.Fix("إتصال الإنترنت ضعيف أو\n لايوجد إتصال بالإنترنت");
		StoreTitles [0].text = ArabicFixer.Fix ("أغراض المتجر");
		StoreTitles [1].text = ArabicFixer.Fix ("مشترياتي");
		StoreTitles [2].text = ArabicFixer.Fix ("لون المقعد");
		StoreTitles [3].text = ArabicFixer.Fix ("الباشا");
		StoreTitles [4].text = ArabicFixer.Fix ("الذهب");
		StoreTitles [5].text = ArabicFixer.Fix ("مسرعات الخبرة");
		StoreTitles [6].text = ArabicFixer.Fix ("أغراض خاصة");
		StoreTitles [7].text = ArabicFixer.Fix ("الطاولة");
		StoreBasha [0].text = ArabicFixer.Fix ("7 أيام");
		StoreBasha [1].text = ArabicFixer.Fix ("30 يوم");
		StoreBasha [2].text = ArabicFixer.Fix ("90 يوم");
		StoreBasha [3].text = ArabicFixer.Fix ("180 يوم");
		StoreBasha [4].text = ArabicFixer.Fix ("360 يوم");
		StoreCoins [0].text = ArabicFixer.Fix ("10,000 ذهب");
		StoreCoins [1].text = ArabicFixer.Fix ("25,000 ذهب");
		StoreBoosters [0].text = ArabicFixer.Fix ("مسرع الخبرة 24 ساعة");
		StoreBoosters [1].text = ArabicFixer.Fix ("مسرع الخبرة 48 ساعة");
		StoreTable.text = ArabicFixer.Fix ("متاحة في المستوى الثالث");
		PlayerDetailsTxts [0].text = ArabicFixer.Fix ("ربط الفيسبوك");
		PlayerDetailsTxts [1].text = ArabicFixer.Fix ("متصل");
		PlayerDetailsTxts [2].text = ArabicFixer.Fix ("المستوى");
		PlayerDetailsTxts [3].text = ArabicFixer.Fix ("المستوى");
		PlayerDetailsTxts [4].text = ArabicFixer.Fix ("الإعدادات");
		PlayerDetailsTxts [5].text = ArabicFixer.Fix ("اللغة");
		PlayerDetailsTxts [6].text = ArabicFixer.Fix ("الصوت");
		PlayerDetailsTxts [7].text = ArabicFixer.Fix ("متاح");
		LoadingTxt.text = ArabicFixer.Fix("جاري التحميل...");
		GamesListsTitles[0].text = ArabicFixer.Fix ("الألعاب");
		NormalGamesList [0].text = ArabicFixer.Fix ("طرنيب");
		CompeitionsGamesList [0].text = ArabicFixer.Fix ("طرنيب");
		NormalGamesList [1].text = ArabicFixer.Fix ("طرنيب سوري");
		CompeitionsGamesList [1].text = ArabicFixer.Fix ("طرنيب سوري");
		NormalGamesList [2].text = ArabicFixer.Fix ("تركس");
		CompeitionsGamesList [2].text = ArabicFixer.Fix ("تركس");
		NormalGamesList [3].text = ArabicFixer.Fix ("تركس شراكة");
		CompeitionsGamesList [3].text = ArabicFixer.Fix ("تركس شراكة");
		NormalGamesList [4].text = ArabicFixer.Fix ("تركس كومبلكس");
		CompeitionsGamesList [4].text = ArabicFixer.Fix ("تركس كومبلكس");
		NormalGamesList [5].text = ArabicFixer.Fix ("تركس كومبلكس شراكة");
		CompeitionsGamesList [5].text = ArabicFixer.Fix ("تركس كومبلكس شراكة");
		NormalGamesList [6].text = ArabicFixer.Fix ("هاند");
		CompeitionsGamesList [6].text = ArabicFixer.Fix ("هاند");
		NormalGamesList [7].text = ArabicFixer.Fix ("هاند شراكة");
		CompeitionsGamesList [7].text = ArabicFixer.Fix ("هاند شراكة");
		NormalGamesList [8].text = ArabicFixer.Fix ("هاند سعودي");
		CompeitionsGamesList [8].text = ArabicFixer.Fix ("هاند سعودي");
		NormalGamesList [9].text = ArabicFixer.Fix ("استميشن");
		CompeitionsGamesList [9].text = ArabicFixer.Fix ("استميشن");
		NormalGamesList [10].text = ArabicFixer.Fix ("بلوت");
		CompeitionsGamesList [10].text = ArabicFixer.Fix ("بلوت");
		NormalGamesList [11].text = ArabicFixer.Fix ("جاكارو ");
		CompeitionsGamesList [11].text = ArabicFixer.Fix ("جاكارو");
		NormalGamesList [12].text = ArabicFixer.Fix ("جاكارو كومبلكس");
		CompeitionsGamesList [12].text = ArabicFixer.Fix ("جاكارو كومبلكس");
		ServersWindowTitles[0].text = ArabicFixer.Fix ("إلعب الأن");
		ServersWindowTitles[1].text = ArabicFixer.Fix ("أنشئ لعبة");
		RoomOptionsWindow [0].text = ArabicFixer.Fix ("لعبة ");
		RoomOptionsWindow [1].text = ArabicFixer.Fix ("سرعة اللعب");
		RoomOptionsWindow [2].text = ArabicFixer.Fix ("الطرد من اللعبة");
		RoomOptionsWindow [3].text = ArabicFixer.Fix ("المغادرة");
		RoomOptionsWindow [4].text = ArabicFixer.Fix ("اقل مستوى");
		RoomOptionsWindow [5].text = ArabicFixer.Fix ("لعبة خاصة");
		RoomOptionsWindow [6].text = ArabicFixer.Fix ("لعبة جديدة");
		RoomOptionsWindow [7].text = ArabicFixer.Fix ("متاح");
		RoomOptionsWindow [8].text = ArabicFixer.Fix ("متاح");
		RoomOptionsWindow [9].text = ArabicFixer.Fix ("متاح");
		RoomOptionsWindow [10].text = ArabicFixer.Fix ("إبدأ اللعبة");
		RoomOptionsWindow [11].text = ArabicFixer.Fix ("النتيجة النهائية");
		RoomOptionsWindow [12].text = ArabicFixer.Fix ("عدد اللاعبين");
		SideMenuTxts [0].text = ArabicFixer.Fix ("القائمة الرئيسية");
		SideMenuTxts [1].text = ArabicFixer.Fix ("الإشعارات");
		SideMenuTxts [2].text = ArabicFixer.Fix ("الألعاب");
		SideMenuTxts [3].text = ArabicFixer.Fix ("المسابقات");
		SideMenuTxts [4].text = ArabicFixer.Fix ("الأصدقاء");
		SideMenuTxts [5].text = ArabicFixer.Fix ("النوادي");
		SideMenuTxts [6].text = ArabicFixer.Fix ("الإعدادات");
		SideMenuTxts [7].text = ArabicFixer.Fix ("المستوى");
		SideMenuTxts [8].text = ArabicFixer.Fix ("مركز المساعدة");
		ClubsWindow [0].text = ArabicFixer.Fix ("عضو");
		ClubsWindow [1].text = ArabicFixer.Fix ("المستوى");
		ClubsWindow [2].text = ArabicFixer.Fix ("الانضمام");
		ClubsWindow [3].text = ArabicFixer.Fix ("الأعضاء");
		ClubsWindow [4].text = ArabicFixer.Fix ("الإسم");
		ClubsWindow [5].text = ArabicFixer.Fix ("النقاط");
		ClubsWindow [6].text = ArabicFixer.Fix ("النوادي");
		ClubsWindow [7].text = ArabicFixer.Fix ("+إنشاءالنادي");
		ClubsWindow [8].text = ArabicFixer.Fix ("النوادي");
		ClubsWindow [9].text = ArabicFixer.Fix ("إنشاء نادي سيكلفك\n35000 ذهبة");
		ClubsWindow [10].text = ArabicFixer.Fix ("أنشئ النادي");
		ClubsWindow [11].text = ArabicFixer.Fix ("شارة VIP");
		ClubsWindow [12].text = ArabicFixer.Fix ("متاح");
		ClubsWindow [13].text = ArabicFixer.Fix (": لون النادي");
		ClubsWindow [14].text = ArabicFixer.Fix ("النادي");
		ClubsWindow [15].text = ArabicFixer.Fix ("الطلبات");
		ClubsWindow [16].text = ArabicFixer.Fix ("القوانين");
		ClubsWindow [17].text = ArabicFixer.Fix ("النشاطات");
		if(ClubsWindow [18].text=="No Rules")
			ClubsWindow [18].text = ArabicFixer.Fix ("لا قوانين");
		ClubsWindow [20].text = ArabicFixer.Fix ("تم");
		ClubsWindow [21].text = ArabicFixer.Fix ("القوانين");
		ClubsWindow [22].text = ArabicFixer.Fix ("انشاء");
		ClubsWindow [23].text = ArabicFixer.Fix ("اضافة بوست");
		ClubsWindow [25].text = ArabicFixer.Fix ("تم");
		ClubsWindow [26].text = ArabicFixer.Fix ("اعجاب");
		ClubsWindow [28].text = ArabicFixer.Fix ("البوست");
		ClubsWindow [29].text = ArabicFixer.Fix ("التعليقات");
		ClubsWindow [30].text = ArabicFixer.Fix ("اضافة قوانين");
		NotificationsWindow [0].text = ArabicFixer.Fix ("الإشعارات");
		FriendsWindow [0].text = ArabicFixer.Fix ("الأصدقاء");
		FriendsWindow [1].text = ArabicFixer.Fix ("الشات");
		StoreColors[0].text = ArabicFixer.Fix ("3 أيام");
		StoreColors[1].text = ArabicFixer.Fix ("7 أيام");
		StoreColors[2].text = ArabicFixer.Fix ("21 أيام");
		CompStuff [0].text = ArabicFixer.Fix ("مسابقاتي");
		CompStuff [1].text = ArabicFixer.Fix ("المسابقات");
		CompStuff [2].text = ArabicFixer.Fix ("المسابقات");
		CompStuff [3].text = ArabicFixer.Fix ("انشاء");
		CompStuff [4].text = ArabicFixer.Fix ("القوانين");
		CompStuff [5].text = ArabicFixer.Fix ("جديدة");
		CompStuff [6].text = ArabicFixer.Fix ("حالية");
		CompStuff [7].text = ArabicFixer.Fix ("منتهية");
		CompStuff [8].text = ArabicFixer.Fix ("المسابقات");
		CompStuff [10].text = ArabicFixer.Fix ("رسوم الدخول");
		CompStuff [11].text = ArabicFixer.Fix ("الجائزة");
		CompStuff [12].text = ArabicFixer.Fix ("المنظم");
		CompStuff [13].text = ArabicFixer.Fix ("المسابقة");
		CompStuff [14].text = ArabicFixer.Fix ("اخبار المسابقة");
		CompStuff [15].text = ArabicFixer.Fix ("الطلبات");
		CompStuff [16].text = ArabicFixer.Fix ("الانضمام");
		CompStuff [17].text = ArabicFixer.Fix ("إلعب");
		CompStuff [18].text = ArabicFixer.Fix ("خيارات المسابقة");
		CompStuff [19].text = ArabicFixer.Fix ("عدد الجولات");
		CompStuff [20].text = ArabicFixer.Fix ("تبدأ خلال");
		CompStuff [21].text = ArabicFixer.Fix ("رسوم الدخول");
		CompStuff [22].text = ArabicFixer.Fix ("الجائزة");
		CompStuff [23].text = ArabicFixer.Fix ("البدء الفوري");
		CompStuff [24].text = ArabicFixer.Fix ("عشوائي");
		CompStuff [25].text = ArabicFixer.Fix ("المحادثة");
		CompStuff [26].text = ArabicFixer.Fix ("سرعة اللعب");
		CompStuff [27].text = ArabicFixer.Fix ("بداية مؤجلة");
		CompStuff [28].text = ArabicFixer.Fix ("لا");
		CompStuff [29].text = ArabicFixer.Fix ("لا");
		CompStuff [30].text = ArabicFixer.Fix ("لا");
		CompStuff [31].text = ArabicFixer.Fix ("بطيء");
		CompStuff [32].text = ArabicFixer.Fix ("متوسط");
		CompStuff [33].text = ArabicFixer.Fix ("سريع");
		CompStuff [34].text = ArabicFixer.Fix ("لا");
		CompStuff [35].text = ArabicFixer.Fix ("المقاعد");
		CompStuff [36].text = ArabicFixer.Fix ("التكلفة");
		CompStuff [37].text = ArabicFixer.Fix ("انشاء المسابقة");
		CompStuff [38].text = ArabicFixer.Fix ("خيارات متقدمة");
		CompStuff [39].text = ArabicFixer.Fix ("خيارات متقدمة");
	}

	void SitArabicFonts ()
	{
		SetFontForThisTxts (MainTxts);
		SetFontForThisTxts (StoreBasha);
		SetFontForThisTxts (StoreCoins);
		SetFontForThisTxts (StoreBoosters);
		SetFontForThisTxts (PlayerDetailsTxts);
		SetFontForThisTxts (NormalGamesList);
		SetFontForThisTxts (CompeitionsGamesList);
		SetFontForThisTxts (GamesListsTitles);
		SetFontForThisTxts (ServersWindowTitles);
		SetFontForThisTxts (RoomOptionsWindow);
		SetFontForThisTxts (InternetCheckMSG);
		SetFontForThisTxts (StoreTable);
		SetFontForThisTxts (LoadingTxt);
		SetFontForThisTxts (StoreColors);
		SetFontForThisTxts (ClubsWindow);
		SetFontForThisTxts (CompStuff);
	}

	public void ChangeLang(){
		int SavedLangValue = 0;
		IsArabic = !IsArabic;
		if (IsArabic)
			SavedLangValue = 1;
		PlayerPrefs.SetInt ("IsArabic",SavedLangValue);
		PlayerPrefs.Save ();
		SceneManager.LoadScene (0);
	}

	public void InputFeildOnEndFix(int Status){
		GameObject CurrentBTNGO =EventSystem.current.currentSelectedGameObject;
		string InputTxt = CurrentBTNGO.GetComponent<AdvancedInputFieldPlugin.AdvancedInputField> ().Text;
		if (Status == 1)
			CurrentBTNGO.GetComponent<AdvancedInputFieldPlugin.AdvancedInputField> ().Text =  InputTxt;
		else
			CurrentBTNGO.GetComponent<AdvancedInputFieldPlugin.AdvancedInputField> ().Text = ArabicFixer.Fix (InputTxt,false,false);
	}
}