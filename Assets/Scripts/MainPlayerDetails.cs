﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainPlayerDetails : MonoBehaviour {

	public static MainPlayerDetails Instance;
	public Text XPsTxt,CircledLevelTxt,CoinsTxt,PlayerNameTxt,BashaDaysTxt;

	void Awake(){
		Instance=this;
	}

	void Start(){
		SetPlayerData ();
	}

	public void SetPlayerData ()
	{
		try{SideMenuManager.Instance.RefreshPlayerDetails ();}catch{
		}
		PlayerNameTxt.text = Player.Instance.PlayerName;
		CircledLevelTxt.text = Player.Instance.Level.ToString();
		CoinsTxt.text = Player.Instance.Coins.ToString();
		XPsTxt.text = Player.Instance.XPs.ToString();
		BashaDaysTxt.text = Player.Instance.BashaDays.ToString ();
	}
}