﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BTNSManager : MonoBehaviour {

	public static BTNSManager Instance;

	void Awake(){
		Instance = this;
	}

	void Start(){
		SetInitials();
	}

	void SetInitials ()
	{
		MainFireBaseManager.Instance.IsACompition = false;
	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.Escape))
			WindowsManager.Instance.EscapeBTN ();
	}

	public void BTNSFunction(){
		GameObject CurrentBTNGO =EventSystem.current.currentSelectedGameObject;
		string BTNName = CurrentBTNGO.name;
		Debug.Log ("BTNName\t"+BTNName);
		if (BTNName == "PlayNow")
			PlayNowBTN ();
		else if (BTNName == "CreateGame")
			CreateGameBTN ();
		else if (BTNName == "BackXBTN")
			WindowsManager.Instance.EscapeBTN ();
		else if (BTNName == "HomeBTN")
			WindowsManager.Instance.ShowWindowAndHideOther (1);
		else if (BTNName == "RefreshBTN")
			MainFireBaseManager.Instance.RefreshRoomList ();
		else if (EventSystem.current.currentSelectedGameObject.transform.parent.name == "GamesList")
			SelectGame (CurrentBTNGO);
		else if (BTNName == "GamesBTN") {
			MainFireBaseManager.Instance.IsACompition = false;
			WindowsManager.Instance.ShowWindowAndHideOther (2);
		} else if (BTNName == "ExitOptionsBTN")
			WindowsManager.Instance.ShowWindowAndHideOther (3);
		else if (BTNName == "CreateGameNow")
			RoomsManager.Instance.CreateGameNow ();
		else if (BTNName == "PrivacyPolicyBTN")
			Application.OpenURL ("http://textuploader.com/dcbck");
		else if (BTNName == "Private" || BTNName == "NoLeaveing" || BTNName == "AllowKicking"
		         || BTNName == "GameSpeedBTN" || BTNName == "FinalScoreBTN" || BTNName == "NOofPlayerBTN")
			ToggleThisBTNAlpha (EventSystem.current.currentSelectedGameObject);
		else if (BTNName == "PlayerBTN") {
			WindowsManager.Instance.HideAllWindows ();
			PlayerDetails.Instance.PlayerDetailsStuffGO.SetActive (true);
		} else if (BTNName == "ClubsBTN") {
			ShowClubs ();
		} else if (BTNName == "StoreBTN") {
			WindowsManager.Instance.HideAllWindows ();
			StoreManager.Instance.StoreStuffContainerGO.SetActive (true);
		} else if (BTNName == "CreateClubBTN") {
			if (Player.Instance.Coins < 35000)
				PopsMSG.Instance.ShowPopMSG ("No Enough Coins");
			else
				WindowsManager.Instance.WindowsGO [8].SetActive (true);
		} else if (BTNName == "CreateClubNow") {
			WindowsManager.Instance.WindowsGO [8].SetActive (false);
			string ClubName = WindowsManager.Instance.CreateClubInputFeildGO.Text;
			ClubsManager.Instance.CreateClub (ClubName);
		} else if (BTNName == "CompititionsBTN") {
			MainFireBaseManager.Instance.IsACompition = true;
			CompManager.Instance.Show ();
			WindowsManager.Instance.ShowWindowAndHideOther (6);
		} else if (BTNName == "FriendsBTN") {
			WindowsManager.Instance.HideAllWindows ();
			FriendsManager.Instance.RequestMyFriendsList ();
		} else if (BTNName == "ShowPlayerDataBTN")
			PlayingFireBaseFather.ParentInstance.ShowThisPlayerData (Player.Instance.ThisOnlinePlayer.PlayerID);
		else if (BTNName == "ShowPlayerNotificationsBTN") {
			WindowsManager.Instance.HideAllWindows ();
			NotificationsManager.Instance.RequestMyNotificationsList ();
		} else if (BTNName == "ChatXBTN")
			FriendsManager.Instance.CloseChatWindow ();
		else if (BTNName == "MenuBTN")
			SideMenuManager.Instance.ShowOrHide ();
		else if (BTNName == "HelpBTN")
			WindowsManager.Instance.ShowWindowAndHideOther (15);
		else if (BTNName == "SoundsBTN") {
			AudioManager.Instance.ToggleSoundOnOrOff ();
			PlayerDetails.Instance.RefreshSoundBTN ();
		} else if (BTNName == "ClubVIPBadgeBTN")
			ClubsManager.Instance.ToggleVIPBadge ();
		else if (BTNName == "ClubColorBTN")
			ClubsManager.Instance.ChangeClubColorBTNClicked ();
		else if (BTNName == "RulesBTN")
			ShowRightRules ();
		else if (BTNName == "MyClubBTN") {
			ClubsManager.Instance.CurrentSelectedClubID = Player.Instance.ClubID;
			MainFireBaseManager.Instance.GetThisClubObj (Player.Instance.ClubID,1);
		}
	}

	public void ShowClubs ()
	{
		StartCoroutine (ShowClubsCor ());
	}

	void ShowRightRules ()
	{
		Debug.Log ("ShowRightRules");
		if (MainFireBaseManager.Instance.IsACompition) {
			RulesManager.Instance.ShowTheseRules (6);
			return;
		}
		string GameName = MainFireBaseManager.GameType;
		if(GameName.Contains("Tarneeb"))
			RulesManager.Instance.ShowTheseRules (0);
		else if(GameName.Contains("Trix"))
			RulesManager.Instance.ShowTheseRules (1);
		else if(GameName.Contains("Hand"))
			RulesManager.Instance.ShowTheseRules (2);
		else if(GameName.Contains("Estimation"))
			RulesManager.Instance.ShowTheseRules (3);
		else if(GameName.Contains("Baloot"))
			RulesManager.Instance.ShowTheseRules (4);
		else if(GameName.Contains("Jackaroo"))
			RulesManager.Instance.ShowTheseRules (5);
	}

	private IEnumerator ShowClubsCor ()
	{
		WindowsManager.Instance.ShowWindowAndHideOther (7);
		WindowsManager.Instance.ShowLoader (1);
		yield return new WaitForSeconds (0.1f);
		MainFireBaseManager.Instance.RefreshClubsList ();
	}

	void SelectGame (GameObject BTNGO)
	{
		string GameType = BTNGO.name;
		MainGameManager.GameTypeID = GetActualGameID (BTNGO);
		MainFireBaseManager.GameType = GameType;
		WindowsManager.Instance.GameTypeNameTxt.text = GameType;
		if(MainFireBaseManager.Instance.IsACompition)
			WindowsManager.Instance.ShowWindowAndHideOther (3);	
		else
			WindowsManager.Instance.ShowWindowAndHideOther (3);
		MainFireBaseManager.Instance.RefreshRoomList ();
	}

	public int GetActualGameID (GameObject BTNGO)
	{
		if (BTNGO.transform.GetSiblingIndex () == 3 || BTNGO.transform.GetSiblingIndex () == 4 || BTNGO.transform.GetSiblingIndex () == 5)
			return 3;//Trix
		else if (BTNGO.transform.GetSiblingIndex () == 6 || BTNGO.transform.GetSiblingIndex () == 7 || BTNGO.transform.GetSiblingIndex () == 8)
			return 4;//Hand
		else if (BTNGO.transform.GetSiblingIndex () == 9)
			return 5;//Estimation
		else if (BTNGO.transform.GetSiblingIndex () == 10)
			return 6;//Baloot
		else if (BTNGO.transform.GetSiblingIndex () == 11||BTNGO.transform.GetSiblingIndex () == 12)
			return 7;//Jackaroo
		else
			return BTNGO.transform.GetSiblingIndex() + 1;
	}

	private void ToggleThisBTNAlpha(GameObject BTNGO){
		Color BTNGOColor = BTNGO.GetComponent<Image> ().color;
		Debug.Log (BTNGO.name);
		if (BTNGO.name == "GameSpeedBTN") {
			for (int i = 0; i < 3; i++)
				BTNGO.transform.parent.GetChild (i).GetComponent<Image> ().color =
					new Color (BTNGOColor.r, BTNGOColor.g, BTNGOColor.b, 0.5f);
			BTNGO.GetComponent<Image> ().color =
				new Color (BTNGOColor.r, BTNGOColor.g,BTNGOColor.b, 1);
		}else if (BTNGO.name == "FinalScoreBTN") {
			for (int i = 0; i < 3; i++)
				BTNGO.transform.parent.GetChild (i).GetComponent<Image> ().color =
					new Color (BTNGOColor.r, BTNGOColor.g, BTNGOColor.b, 0.5f);
			BTNGO.GetComponent<Image> ().color =
				new Color (BTNGOColor.r, BTNGOColor.g,BTNGOColor.b, 1);
		}
		else if (BTNGO.name == "NOofPlayerBTN") {
			for (int i = 0; i < 3; i++)
				BTNGO.transform.parent.GetChild (i).GetComponent<Image> ().color =
					new Color (BTNGOColor.r, BTNGOColor.g, BTNGOColor.b, 0.5f);
			BTNGO.GetComponent<Image> ().color =
				new Color (BTNGOColor.r, BTNGOColor.g,BTNGOColor.b, 1);
		}
		else {
			if (BTNGOColor ==Color.green) {
				BTNGO.GetComponent<Image> ().color = Color.yellow;
				BTNGO.GetComponent<RectTransform>().GetChild(0).GetComponent<Text>().text = "Off";
			} else {
				BTNGO.GetComponent<Image> ().color = Color.green;
				BTNGO.GetComponent<RectTransform>().GetChild(0).GetComponent<Text>().text = "On";
			}
		}
		if (BTNGO.name == "Private") {
			if (BTNGO.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().text == "Off")
				RoomsManager.Instance.RoomOptionsWindowScript.PrivateRoomID.SetActive (false);
			else {
				RoomsManager.Instance.RoomOptionsWindowScript.PrivateRoomID.SetActive (true);
				string RandomID = RandomIDGenerator.Instance.GetRandomID ();
				RoomsManager.Instance.RoomOptionsWindowScript.PrivateRoomID.GetComponent<AdvancedInputFieldPlugin.AdvancedInputField> ().Text = "";
				RoomsManager.Instance.RoomOptionsWindowScript.PrivateRoomID.GetComponent<AdvancedInputFieldPlugin.AdvancedInputField> ().Text =
					RandomID.Substring (0, 4);
			}
		}
	}
	void CreateGameBTN ()
	{
		WindowsManager.Instance.WindowsGO [4].SetActive (true);
	}
	void PlayNowBTN ()
	{
		RoomsFather.Instance.JoinFirstAvailbeRoom ();
	}
}