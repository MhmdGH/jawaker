﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System;

public class CheckInternet : MonoBehaviour {

	public static CheckInternet Instance;
	[HideInInspector]
	private const int RequestRate = 10;

	void Awake(){
		Instance = this;
	}

	private void CheckInternetNow ()
	{
		if (Application.internetReachability != NetworkReachability.ReachableViaLocalAreaNetwork &&
			Application.internetReachability != NetworkReachability.ReachableViaCarrierDataNetwork)
			MainGameManager.InternetAvailability = false;
		else
			MainGameManager.InternetAvailability = true;
	}

	void Start(){
		StartChecking ();
	}

	public void StartChecking()
	{
		InvokeRepeating ("CheckInternetNow",0,RequestRate);
	}
}