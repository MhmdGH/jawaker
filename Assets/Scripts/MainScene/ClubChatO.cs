using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ClubChatO
{
	public LikesO Likes;
	public CommentsO Comments;
	public ClubChatO(){
		Likes = new LikesO ();
		Comments = new CommentsO ();
	}
}