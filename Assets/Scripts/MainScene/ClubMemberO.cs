using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ClubMemberO
{
	public string MemberID,MemberName,MemberFBID;
	public int MemberRank,MemberPoints,WeeklyPoints,Level;
}