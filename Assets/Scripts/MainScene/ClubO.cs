using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ClubO
{
	public string ClubID,ClubAdminID,ClubName,Rules;
	public int ClubMembersNo,ClubLevel,ClubPoints,ClubColor;
	public List<ClubMemberO> Members;
	public RequestsO Requests;
	public bool HaveFTPIMG,VIPBadge;
	public ClubChatO ClubChat;
	public ClubO(){
		ClubColor = 0;
		VIPBadge = false;
		HaveFTPIMG = false;
		ClubAdminID = "";
		ClubName = "New Club";
		ClubMembersNo = 1;
		ClubPoints = 0;
		Rules="No Rules";
		ClubLevel = GetLevel();
		Members = new List<ClubMemberO> ();
		Requests = new RequestsO();
		ClubChat = new ClubChatO ();
	}

	public int GetLevel(){
		return (int)Mathf.Round (ClubPoints/100000);
	}
}