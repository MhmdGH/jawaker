﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using System.IO;
using System.Linq;
using UnityEngine.UI;
using SimpleJSON;
using Facebook.Unity;

public class FBManager : FatherFBManager {

	public static FBManager Instance;

	public override void Awake(){
		base.Awake ();
		Instance = this;
	}

	public void FBLogInBTNClick ()
	{
		if (!FB.IsInitialized)
			FB.Init (InitCallback, OnHideUnity);
		else
			InitCallback ();
	}

	private void InitCallback ()
	{
		if (FB.IsInitialized) {
			if (FB.IsLoggedIn)
				AuthCallback (null);
			else
				LogInFun ();
		}
	}

	private void LogInFun(){
		var perms = new List<string>(){};
		FB.LogInWithReadPermissions(perms, AuthCallback);
	}

	private void AuthCallback (ILoginResult result) {
		if (FB.IsLoggedIn)
			SetFBID ();
		else
			Debug.Log("User cancelled login");
	}

	private void OnHideUnity (bool isGameShown){
	}

	private void SetFBID(){
		var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
		string FBID = aToken.UserId;
		Player.Instance.FBID = FBID;
		Player.Instance.Badges [0] = 1;
		Player.Instance.SavePlayerData ();
		PlayerDetails.Instance.RefreshFBConnectedData ();
	}
}