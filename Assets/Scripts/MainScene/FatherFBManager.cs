﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public abstract class FatherFBManager : MonoBehaviour
{
	public static FatherFBManager ParentInstance;

	public virtual void Awake(){
		ParentInstance = this;
	}

	private void Start(){
		Courotines = new IEnumerator[4];
	}

	public void SetFPPhoto (string FBID,Image image)
	{
		try{if (FBID.Length < 4)
			return;}catch{
			return;
		}
		try{StartCoroutine (SetFPPhotoCor(FBID,image));}catch(Exception e){
			Debug.Log (e.Message);
		}
	}

	protected IEnumerator[] Courotines;
	public void SetFPPhoto (string FBID,Image image,int PlayerNO)
	{
		if (FBID.Length < 5)
			return;
		for (int i = 0; i < Courotines.Length; i++)
			Courotines [i] = SetFPPhotoCor (FBID,image);
		try{
			StopCoroutine(Courotines[PlayerNO]);
			StartCoroutine (Courotines[PlayerNO]);
		}catch{}
	}

	private IEnumerator SetFPPhotoCor (string FBID, Image image)
	{
		WWW www = new WWW ("https://graph.facebook.com/" + FBID + "/picture?type=normal");
		yield return www;
		try{
			image.sprite = Sprite.Create (www.texture, new Rect (0, 0, www.texture.width, www.texture.height), new Vector2 (0, 0));
		}
		catch(Exception e){
			Debug.Log(e.Message);
		}
	}

	private void OnDestroy(){
		StopAllCoroutines ();
	}
}