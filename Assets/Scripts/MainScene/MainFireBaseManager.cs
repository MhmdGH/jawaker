﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Database;
using Firebase;
using Firebase.Unity.Editor;
using SimpleJSON;
using System;
using UnityEngine.UI;
using System.Net;
using System.Net.Sockets;

public class MainFireBaseManager : PlayingFireBaseFather
{
	#region implemented abstract members of PlayingFireBaseFather

	protected override void SetInitials ()
	{
		TempRoomToUpload = new JsonRoomO ("", new string[4]{ "1", "2", "3", "4" }, new Game1 ());
		CurrentRoomUpdate = TempRoomToUpload;
	}

	#endregion

	public static MainFireBaseManager Instance;
	public GameObject GamesContectListGO, ClubsContectGO,RequestContentGO,FriendsRequestsContentGo,PostsContentGO;
	public static string GameType;
	[HideInInspector]
	public bool IsACompition;

	public override void Awake ()
	{
		base.Awake ();
		Instance = this;
	}

	public override void Start ()
	{
		InitilizeFireBase ();
		SetHandle ();
		RemoveThisPlayerFromComp (Player.Instance.DatabasePlayerID);
	}

	private void SetHandle(){
		reference.Child ("Players").Child (Player.Instance.DatabasePlayerID).ValueChanged += OnlinePlayerUpates;
	}

	// new room from json object
	public void CreateNewRoomServer (JsonRoomO CurrentRoom)
	{
		string Room2Json = JsonUtility.ToJson (CurrentRoom);
		reference.Child ("Rooms").Child (CurrentRoom.GameType).Child (CurrentRoom.RoomID).SetRawJsonValueAsync (Room2Json);
	}

	public void RefreshRoomList ()
	{
		WindowsManager.Instance.ShowLoader (2);
		try {
			WindowsManager.Instance.ClearContentGO (GamesContectListGO);
			FirebaseDatabase.DefaultInstance
				.GetReference ("Rooms")
				.Child (GameType)
				.GetValueAsync ().ContinueWith (task => {
					if (task.IsFaulted) {
						Debug.Log (task.Result.ToString ());
					} else if (task.IsCompleted) {
						DataSnapshot snapshot = task.Result;
						foreach (var childSnapshot in snapshot.Children) {
							try {
								//Need To Check Errors here
								if (childSnapshot.Child ("RoomID").Value.ToString () == "NULL") {
									childSnapshot.Reference.RemoveValueAsync ();
									continue;
								}

							} catch (Exception e) {
								Debug.Log (e.Message);
								{
									childSnapshot.Reference.RemoveValueAsync ();
									continue;}

							}
							//Default Values
							JsonRoomO ThisNewRoom = new JsonRoomO ("", new string[4]{ "1", "2", "3", "4" }, new Game1 ());
							JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), ThisNewRoom);
							if(ThisNewRoom.RoomOptions.MinLevel>Player.Instance.Level)
								continue;
							if(ThisNewRoom.RoomOptions.Private)
								continue;
							if (ThisNewRoom.Available) {
								if (IsACompition && ThisNewRoom.IsACompititons)
									RoomsManager.Instance.CeateNewRoom (ThisNewRoom);
								else if (!IsACompition)
									RoomsManager.Instance.CeateNewRoom (ThisNewRoom);
							}
						}
					}
				});
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error Fuking Dick Hole:\t" + e.Message);
		}
	}

	public void GiveMeMyNotificationList ()
	{
		FirebaseDatabase.DefaultInstance
			.GetReference ("Notifications")
			.Child (Player.Instance.DatabasePlayerID)
			.Child("NotificationsListO")
			.GetValueAsync ().ContinueWith (task => {
				if (task.IsFaulted) {
					Debug.Log (task.Result.ToString ());
				} else if (task.IsCompleted) {
					List<NotificationO> NotificationList = new List<NotificationO>();
					DataSnapshot snapshot = task.Result;
					foreach (var childSnapshot in snapshot.Children) {
						NotificationO NewNotify = new NotificationO("");
						JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), NewNotify);
						NotificationList.Add(NewNotify);
						RemoveThisNotify(childSnapshot.Key);
					}
					NotificationsManager.Instance.ShowMyNotificationList(NotificationList);
				}
			});
	}

	private void RemoveThisNotify(string NotifyID){
		reference.Child ("Notifications").Child (Player.Instance.DatabasePlayerID).Child ("NotificationsListO").Child (NotifyID).RemoveValueAsync ();
	}

	public void RefreshClubsList ()
	{
		try {
			WindowsManager.Instance.ClearContentGO (ClubsContectGO);
			FirebaseDatabase.DefaultInstance
				.GetReference ("Clubs")
				.GetValueAsync ().ContinueWith (task => {
					if (task.IsFaulted) {
						Debug.Log (task.Result.ToString ());
					} else if (task.IsCompleted) {
						DataSnapshot snapshot = task.Result;
						foreach (var childSnapshot in snapshot.Children) {
							//Default Values
							ClubO ThisNewClub = new ClubO ();
							JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), ThisNewClub);
							ClubsManager.Instance.AddThisClub (ThisNewClub);
						}
					}
				});
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error Fuking Dick Hole:\t" + e.Message);
		}
	}

	public void GetThisClubObj (string ThisClubID)
	{
		try {
			WindowsManager.Instance.ClearContentGO (ClubsContectGO);
			FirebaseDatabase.DefaultInstance
				.GetReference ("Clubs")
				.GetValueAsync ().ContinueWith (task => {
					if (task.IsFaulted) {
						Debug.Log (task.Result.ToString ());
					} else if (task.IsCompleted) {
						DataSnapshot snapshot = task.Result;
						foreach (var childSnapshot in snapshot.Children) {
							//Default Values
							ClubO ThisNewClub = new ClubO ();
							JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), ThisNewClub);
							if(ThisNewClub.ClubID==ThisClubID){
								ClubsManager.Instance.AfterGetClubObj(ThisNewClub);
								break;
							}
						}
					}
				});
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error Fuking Dick Hole:\t" + e.Message);
		}
	}

	public void GetThisClubObj (string ThisClubID,int FunToDoAfter)
	{
		try {
			FirebaseDatabase.DefaultInstance
				.GetReference ("Clubs")
				.GetValueAsync ().ContinueWith (task => {
					if (task.IsFaulted) {
						Debug.Log (task.Result.ToString ());
					} else if (task.IsCompleted) {
						DataSnapshot snapshot = task.Result;
						foreach (var childSnapshot in snapshot.Children) {
							//Default Values
							ClubO ThisNewClub = new ClubO ();
							JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), ThisNewClub);
							if(ThisNewClub.ClubID==ThisClubID){
								if(FunToDoAfter==0)
									ClubDataMain.Instance.SetData(ThisNewClub.HaveFTPIMG);
								else if(FunToDoAfter==1)
									ClubsManager.Instance.ShowClubDetails(ThisNewClub);
								break;
							}
						}
					}
				});
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error Fuking Dick Hole:\t" + e.Message);
		}
	}

	/// <summary>
	/// Join Requests Functions
	/// </summary>
	/// <param name="ThisRequest">This request.</param>
	//Create new Join Request from json object
	public void CreateNewJoinReuqest(RequestO ThisRequest)
	{
		string Request2Json = JsonUtility.ToJson (ThisRequest);
		reference.Child ("Clubs").Child (Player.Instance.JoinClubRequest).Child("Requests").Child(ThisRequest.RequestID).SetRawJsonValueAsync (Request2Json);
	}

	public void UpdateJoinReuqest(RequestO ThisRequest)
	{
		string Request2Json = JsonUtility.ToJson (ThisRequest);
		reference.Child ("Clubs").Child (Player.Instance.ClubID).Child("Requests").Child(ThisRequest.RequestID).SetRawJsonValueAsync (Request2Json);
	}

	public void RemoveThisReuest(RequestO ThisRequest){
		reference.Child ("Clubs").Child (Player.Instance.JoinClubRequest).Child ("Requests").Child(ThisRequest.RequestID).RemoveValueAsync ();
	}

	public void CheckMyJoinRequestStatus(){
		try {
			FirebaseDatabase.DefaultInstance
				.GetReference ("Clubs")
				.Child(Player.Instance.JoinClubRequest)
				.Child("Requests")
				.GetValueAsync ().ContinueWith (task => {
					if (task.IsFaulted) {
						Debug.Log (task.Result.ToString ());
					} else if (task.IsCompleted) {
						DataSnapshot snapshot = task.Result;
						foreach (var childSnapshot in snapshot.Children) {
							RequestO ThisNewRequestO = new RequestO();
							JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), ThisNewRequestO);
							if(ThisNewRequestO.RequestID==Player.Instance.JoinRequestID)
								ClubsManager.Instance.HandleRequestStatus(ThisNewRequestO);
						}
					}
				});
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error Fuking Dick Hole:\t" + e.Message);
		}
	}

	public void RefreshJoinRequestsList ()
	{
		WindowsManager.Instance.ShowLoader (1);
		try {
			WindowsManager.Instance.ClearContentGO (RequestContentGO);
			FirebaseDatabase.DefaultInstance
				.GetReference ("Clubs")
				.Child(Player.Instance.ClubID)
				.Child("Requests")
				.GetValueAsync ().ContinueWith (task => {
					if (task.IsFaulted) {
						Debug.Log (task.Result.ToString ());
					} else if (task.IsCompleted) {
						DataSnapshot snapshot = task.Result;
						foreach (var childSnapshot in snapshot.Children) {
							RequestO ThisNewJoinRequest = new RequestO ();
							JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), ThisNewJoinRequest);
							if (ThisNewJoinRequest.Status == -1)
								JoinRequestsManager.Instance.AddThisRequest (ThisNewJoinRequest);
						}
					}
				});
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error Fuking Dick Hole:\t" + e.Message);
		}
	}

	/// <summary>
	/// Friends Requests Functions
	/// </summary>
	/// <param name="ThisRequest">This request.</param>

	public void UpdateFriendReuqest(RequestO ThisRequest)
	{
		string Request2Json = JsonUtility.ToJson (ThisRequest);
		RemoveThisFriendReuest (ThisRequest,false);
		reference.Child ("FriendsRequests").Child (ThisRequest.SenderID).Child("MySendedRequests").Child(ThisRequest.RequestID).SetRawJsonValueAsync (Request2Json);
	}

	public void RemoveThisFriendReuest(RequestO ThisRequest,bool SenderOrReciever){
		string TargetedList = "";
		if (SenderOrReciever)
			TargetedList = "MySendedRequests";
		else
			TargetedList = "MyRecievedRequests";
		reference.Child ("FriendsRequests").Child (Player.Instance.DatabasePlayerID).Child (TargetedList).Child(ThisRequest.RequestID).RemoveValueAsync ();
	}

	public void CheckMySendedFriendsRequestStatus(){
		try {
			FirebaseDatabase.DefaultInstance
				.GetReference ("FriendsRequests")
				.Child(Player.Instance.DatabasePlayerID)
				.Child("MySendedRequests")
				.GetValueAsync ().ContinueWith (task => {
					if (task.IsFaulted) {
						Debug.Log (task.Result.ToString ());
					} else if (task.IsCompleted) {
						DataSnapshot snapshot = task.Result;
						foreach (var childSnapshot in snapshot.Children) {
							RequestO ThisNewRequestO = new RequestO();
							JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), ThisNewRequestO);
							FriendsRequetsManager.Instance.HandleRequestStatus(ThisNewRequestO);
						}
					}
				});
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error Fuking Dick Hole:\t" + e.Message);
		}
	}

	public void GetThisOnlinePlayerAndAddAsFriend(string DatabasePlayerID){
		if (DatabasePlayerID.Length < 2)
			return;
		FirebaseDatabase.DefaultInstance
			.GetReference ("Players")
			.Child(DatabasePlayerID)
			.GetValueAsync ().ContinueWith (task => {
				if (task.IsFaulted) {
					Debug.Log (task.Result.ToString ());
				} else if (task.IsCompleted) {
					DataSnapshot snapshot = task.Result;
					OnlinePlayerO ThisNewFriend= new OnlinePlayerO();
					JsonUtility.FromJsonOverwrite (snapshot.GetRawJsonValue (), ThisNewFriend);
					FriendsManager.Instance.AddFriend(ThisNewFriend);
				}
			});
	}

	public void SendItemForFriend(string FriendID,int itemNO,int ItemValue){
		reference.Child ("Players").Child (FriendID).Child ("Items").Child (itemNO+"").SetRawJsonValueAsync (ItemValue+"");
	}

	public void RefreshFriendRequestList ()
	{
		WindowsManager.Instance.ShowLoader (1);
		try {
			WindowsManager.Instance.ClearContentGO (FriendsRequestsContentGo);
			FirebaseDatabase.DefaultInstance
				.GetReference ("FriendsRequests")
				.Child(Player.Instance.DatabasePlayerID)
				.Child("MyRecievedRequests")
				.GetValueAsync ().ContinueWith (task => {
					if (task.IsFaulted) {
						Debug.Log (task.Result.ToString ());
					} else if (task.IsCompleted) {
						DataSnapshot snapshot = task.Result;
						foreach (var childSnapshot in snapshot.Children) {
							//Default Values
							RequestO ThisNewFriendRequest = new RequestO ();
							JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), ThisNewFriendRequest);
							if(ThisNewFriendRequest.Status==-1)
								FriendsRequetsManager.Instance.AddThisRequest (ThisNewFriendRequest);
						}
					}
				});
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error Fuking Dick Hole:\t" + e.Message);
		}
	}

	public void SearchThisPlayer (string PlayerName,RectTransform ResaultsContentRect)
	{
		WindowsManager.Instance.ShowLoader (1);
		try {
			WindowsManager.Instance.ClearContentGO (ResaultsContentRect.gameObject);
			FirebaseDatabase.DefaultInstance
				.GetReference ("Players")
				.GetValueAsync ().ContinueWith (task => {
					if (task.IsFaulted) {
						Debug.Log (task.Result.ToString ());
					} else if (task.IsCompleted) {
						DataSnapshot snapshot = task.Result;
						foreach (var childSnapshot in snapshot.Children) {
							//Default Values
							OnlinePlayerO ThisResault = new OnlinePlayerO ();
							JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), ThisResault);
							string LoweredString = ThisResault.PlayerName.ToLower();
							if(LoweredString.Contains(PlayerName.ToLower()))
								FriendsSearchManager.Instance.AddThisRequest(ThisResault);
						}
					}
				});
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error Fuking Dick Hole:\t" + e.Message);
		}
	}

	/// <summary>
	/// ClubChatFunctions
	/// </summary>
	public void RefreshClubChatScreen (string ThisClubID)
	{
		WindowsManager.Instance.ShowLoader (1f);
		try {
			WindowsManager.Instance.ClearContentGO (PostsContentGO);
			FirebaseDatabase.DefaultInstance
				.GetReference ("Clubs")
				.Child(ThisClubID)
				.Child("ClubChat")
				.GetValueAsync ().ContinueWith (task => {
					if (task.IsFaulted) {
						Debug.Log (task.Result.ToString ());
					} else if (task.IsCompleted) {
						DataSnapshot snapshot = task.Result;
						foreach (var childSnapshot in snapshot.Children) {
							//Default Values
							PostO ThisNewPost = new PostO ();
							JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), ThisNewPost);
							ThisNewPost.LikesNO = (int)childSnapshot.Child("Likes").ChildrenCount;
							ThisNewPost.CommentsNO = (int)childSnapshot.Child("Comments").ChildrenCount;
							ChatScreenManager.Instance.AddThisPostO(ThisNewPost);
						}
					}
				});
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error Fuking Dick Hole:\t" + e.Message);
		}
	}

	/// <summary>
	/// CompList
	/// </summary>
	public void ShowThisCustomizedComp (MainCompO ThisComp,GameObject CompListContent)
	{
		WindowsManager.Instance.ShowLoader (1f);
		try {
			WindowsManager.Instance.ClearContentGO (CompListContent);
			FirebaseDatabase.DefaultInstance
				.GetReference ("Comp")
				.GetValueAsync ().ContinueWith (task => {
					if (task.IsFaulted) {
						Debug.Log (task.Result.ToString ());
					} else if (task.IsCompleted) {
						DataSnapshot snapshot = task.Result;
						foreach (var childSnapshot in snapshot.Children) {
							//Default Values
							MainCompO CurrentCompIteration = new MainCompO ();
							JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), CurrentCompIteration);
							if(CurrentCompIteration.CompOptions.GameType!=MainFireBaseManager.GameType)
								continue;
							if(CurrentCompIteration.CompOptions.ImediateStart!=ThisComp.CompOptions.ImediateStart&&ThisComp.CompOptions.ImediateStart!=-1)
								continue;
							if(CurrentCompIteration.CompOptions.Randomness!=ThisComp.CompOptions.Randomness&&ThisComp.CompOptions.Randomness!=-1)
								continue;
							if(CurrentCompIteration.CompOptions.CompStatus!=ThisComp.CompOptions.CompStatus)
								continue;
								CompManager.Instance.AddFilteredComp(CurrentCompIteration);
						}
						CompManager.Instance.ShowFilteredComp(ThisComp);
					}
				});
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error Fuking Dick Hole:\t" + e.Message);
		}
	}

	public void CreateOrUpdateComp(MainCompO ThisComp)
	{
		string CompToJson = JsonUtility.ToJson (ThisComp);
		reference.Child ("Comp").Child (ThisComp.CompOptions.ID).SetRawJsonValueAsync (CompToJson);
	}

	public void CheckThisRoomExists(string RoomID,string RoomType){

		WindowsManager.Instance.ShowLoader (1f);
		try {
			FirebaseDatabase.DefaultInstance
				.GetReference ("Rooms")
				.Child(RoomType)
				.GetValueAsync ().ContinueWith (task => {
					if (task.IsFaulted) {
						Debug.Log (task.Result.ToString ());
					} else if (task.IsCompleted) {
						DataSnapshot snapshot = task.Result;
						foreach (var childSnapshot in snapshot.Children) {
							JsonRoomO ThisNewRoom = new JsonRoomO ("", new string[4]{ "1", "2", "3", "4" }, new Game1 ());
							JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), ThisNewRoom);
							if(ThisNewRoom.RoomID==RoomID)
								CompInnerDetailsManager.Instance.CreateRoomOrJoinExistedOne(false,RoomID);
						}
						CompInnerDetailsManager.Instance.CreateRoomOrJoinExistedOne(true,RoomID);
					}
				});
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error Fuking Dick Hole:\t" + e.Message);
		}

	}

	/// <summary>
	/// Competitions Join Requests Functions
	/// </summary>
	/// <param name="ThisRequest">This request.</param>
	//Create new Join Request from json object
	public void CreateOrUpdateJoinReuqestToComp(RequestO ThisRequest)
	{
		if (Player.Instance.CurrentCompID.Length < 2)
			return;
		string Request2Json = JsonUtility.ToJson (ThisRequest);
		reference.Child ("Comp").Child (Player.Instance.CurrentCompID).Child("Requests").Child(ThisRequest.RequestID).SetRawJsonValueAsync (Request2Json);
	}

	public void RemoveThisReuestComp(RequestO ThisRequest){
		if (Player.Instance.CurrentCompID.Length < 2)
			return;
		reference.Child ("Comp").Child (Player.Instance.CurrentCompID).Child ("Requests").Child(ThisRequest.RequestID).RemoveValueAsync ();
	}

	public void RefreshCompJoinRequestsList (GameObject CompReqContent)
	{
		WindowsManager.Instance.ShowLoader (1);
		try {
			WindowsManager.Instance.ClearContentGO (CompReqContent);
			FirebaseDatabase.DefaultInstance
				.GetReference ("Comp")
				.Child(Player.Instance.CurrentCompID)
				.Child("Requests")
				.GetValueAsync ().ContinueWith (task => {
					if (task.IsFaulted) {
						Debug.Log (task.Result.ToString ());
					} else if (task.IsCompleted) {
						DataSnapshot snapshot = task.Result;
						foreach (var childSnapshot in snapshot.Children) {
							RequestO ThisNewJoinRequest = new RequestO ();
							JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), ThisNewJoinRequest);
							if (ThisNewJoinRequest.Status == -1)
								CompInnerDetailsManager.Instance.AddThisRequest (ThisNewJoinRequest);
						}
					}
				});
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error Fuking Dick Hole:\t" + e.Message);
		}
	}

	public void CheckMyCompJoinRequest()
	{
		WindowsManager.Instance.ShowLoader (1f);
		try {
			FirebaseDatabase.DefaultInstance
				.GetReference ("Comp")
				.Child(Player.Instance.CurrentCompID)
				.Child("Requests")
				.GetValueAsync ().ContinueWith (task => {
					if (task.IsFaulted) {
						Debug.Log (task.Result.ToString ());
					} else if (task.IsCompleted) {
						DataSnapshot snapshot = task.Result;
						foreach (var childSnapshot in snapshot.Children) {
							//Default Values
							RequestO ThisJoinRequest = new RequestO ();
							JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), ThisJoinRequest);
							if((ThisJoinRequest.SenderID==Player.Instance.DatabasePlayerID)&&ThisJoinRequest.Status!=-1)
								RemoveThisReuestComp(ThisJoinRequest);
						}
					}
				});
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error Fuking Dick Hole:\t" + e.Message);
		}
	}

	public void RefreshMSGWall (GameObject MSGWallContentToClear)
	{
		WindowsManager.Instance.ClearContentGO (MSGWallContentToClear);
		try{
		FirebaseDatabase.DefaultInstance
			.GetReference ("Comp")
				.Child (Player.Instance.CurrentCompID)
			.Child("MSGWall")
			.GetValueAsync ().ContinueWith (task => {
				if (task.IsFaulted) {
					Debug.Log (task.Result.ToString ());
				} else if (task.IsCompleted) {
					DataSnapshot snapshot = task.Result;
					foreach (var childSnapshot in snapshot.Children)
							CompInnerDetailsManager.Instance.CreateMSGWallPrefab(childSnapshot.Value.ToString());
				}
				});}catch{
		}
	}

	/// <summary>
	/// Club Stuff Under
	/// </summary>
	/// <param name="ThisPost">This post.</param>
	public void CreateNewPostOrUpdateThisOne(PostO ThisPost)
	{
		string PostToJson = JsonUtility.ToJson (ThisPost);
		reference.Child ("Clubs").Child (Player.Instance.ClubID).Child ("ClubChat").Child (ThisPost.PostID).SetRawJsonValueAsync (PostToJson);
	}
	/// <summary>
	/// Add Comments And Refersh The fuck of them
	/// </summary>
	/// <param name="ThisPost">This post.</param>
		
	public void ToggleNewLikeThisOne(LikeO ThisLike,string ThisPostID,bool AddOrRemoveLike)
	{
		string Like2Json = JsonUtility.ToJson (ThisLike);
		if(AddOrRemoveLike)
			reference.Child ("Clubs").Child (Player.Instance.ClubID).Child("ClubChat").Child(ThisPostID).Child("Likes").Child(ThisLike.LikeID).SetRawJsonValueAsync (Like2Json);
		else
			reference.Child ("Clubs").Child (Player.Instance.ClubID).Child("ClubChat").Child(ThisPostID).Child("Likes").Child(ThisLike.LikeID).RemoveValueAsync();
	}

	public void CreateNewCommentThisOne(CommentO ThisComment,string ThisPostID)
	{
		string Comment2Json = JsonUtility.ToJson (ThisComment);
		reference.Child ("Clubs").Child (Player.Instance.ClubID).Child("ClubChat").Child(ThisPostID).Child("Comments").Child(ThisComment.CommentID).SetRawJsonValueAsync (Comment2Json);
	}

	public void CheckIfIHitLikeOnThisPost (string PostID,bool JustACheckOrAddLike)
	{
		try {
			FirebaseDatabase.DefaultInstance
				.GetReference ("Clubs")
				.Child(Player.Instance.ClubID)
				.Child("ClubChat")
				.Child(PostID)
				.Child("Likes")
				.GetValueAsync ().ContinueWith (task => {
					if (task.IsFaulted) {
						Debug.Log (task.Result.ToString ());
					} else if (task.IsCompleted) {
						DataSnapshot snapshot = task.Result;
						foreach (var childSnapshot in snapshot.Children) {
							LikeO ThisLike = new LikeO ();
							JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), ThisLike);
							if(ThisLike.PlayerID==Player.Instance.DatabasePlayerID)
							{
								if(JustACheckOrAddLike)
									PostDetails.Instance.AfterCheckLikeBTN(true);
								else
								{
									ToggleNewLikeThisOne(ThisLike,PostID,false);
									PostDetails.Instance.AfterCheckLikeBTN(false);
								}
								return;
							}
						}
						if(JustACheckOrAddLike)
							PostDetails.Instance.AfterCheckLikeBTN(false);
						else
							StartCoroutine(PostDetails.Instance.AddLikeToThisPostAfterChecked());
					}
				});
		} catch (Exception e) {
		}
	}

	public void RefreshThisPostComments (RectTransform ClearThisContent,string PostID)
	{
		WindowsManager.Instance.ShowLoader (1f);
		try {
			WindowsManager.Instance.ClearContentGO (ClearThisContent.gameObject);
			FirebaseDatabase.DefaultInstance
				.GetReference ("Clubs")
				.Child(ClubsManager.Instance.CurrentClubO.ClubID)
				.Child("ClubChat")
				.Child(PostID)
				.Child("Comments")
				.GetValueAsync ().ContinueWith (task => {
					if (task.IsFaulted) {
						Debug.Log (task.Result.ToString ());
					} else if (task.IsCompleted) {
						DataSnapshot snapshot = task.Result;
						foreach (var childSnapshot in snapshot.Children) {
							//Default Values
							CommentO ThisNewComment = new CommentO ();
							JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), ThisNewComment);
							PostDetails.Instance.AddThisComment(ThisNewComment);
						}
					}
				});
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error Fuking Dick Hole:\t" + e.Message);
		}
	}
}