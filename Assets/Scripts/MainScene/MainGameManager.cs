﻿using System;
using System.Collections;
using System.Collections.Generic;
//using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class MainGameManager : MonoBehaviour {

	public static MainGameManager Instance;
	public GameObject RoomGO,NoInternetSign;
	public static int GameTypeID;
	private CheckInternet CheckInternetCO;
	public static bool InternetAvailability=true;
	public static bool CallShuffelOnce;
	public Image PlayerIMG,ClubIMG;

	void Awake(){
		Instance=this;
		Application.targetFrameRate = 45;
		Screen.orientation = ScreenOrientation.Portrait;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		PlayerPrefs.SetInt ("ReloadScene",0);
	}

	void Start(){
		FBManager.Instance.SetFPPhoto (Player.Instance.FBID,PlayerIMG);
		FTPManager.Instance.GetPhotoFromFTPByName (ClubIMG, Player.Instance.ClubID);
		gameObject.AddComponent<CheckInternet> ();
		InvokeRepeating ("ShowNoInternetSign",0,2);
		if (!CallShuffelOnce) {
			AudioManager.Instance.PlaySound (0);
			CallShuffelOnce = true;
		}
	}

	private void ShowNoInternetSign(){
		if (InternetAvailability)
			NoInternetSign.SetActive (false);
		else
			NoInternetSign.SetActive (true);
	}
}