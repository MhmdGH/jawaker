﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class JsonRoomO{
	public string RoomID,GameType,KickThisPlayerID,CreatorName;
	public JsonRoomOptions RoomOptions;
	public string[] DatabasePlayersIDs,PlayersFBIDs,PlayersNames;
	public int[] SeatsStatus,PlayersIDs,GreenOrRed;
	public int Update,ActionID,MasterID,GameTypeID,PrevActionID;
	public TimeO PauseTime;
	public bool[] PlayersReady;
	public bool NoMaster,Available,ReloadScene,IsACompititons,GamePaused;
	public GameOBParent GameOB;
	public ChatAndEmotions chatAndEmotions;
	public List<string> WhiteWatchersList;
	public JsonRoomO(string GameType,string[] PlayersFBIDs,GameOBParent GameOBType){
		WhiteWatchersList = new List<string> ();
		IsACompititons = false;
		GamePaused = false;
		ReloadScene = false;
		KickThisPlayerID = "";
		this.GameType = GameType;
		this.PlayersFBIDs = PlayersFBIDs;
		DatabasePlayersIDs = new string[4];
		PlayersReady = new bool[4]{false,false,false,false};
		this.SeatsStatus = new int[4]{1,0,0,0};
		GreenOrRed = new int[4]{0,0,0,0};
		this.PlayersNames = new string[]{"0","1","2","3"};
		this.PlayersIDs = new int[]{0,1,2,3};
		RoomOptions = new JsonRoomOptions ();
		this.GameOB = GameOBType;
		Available = true;
		chatAndEmotions = new ChatAndEmotions ();
	}
}
