﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class JsonRoomOptions{
	public bool Private=false,AllowKicking=false,NoLeaving=false,Full=false,Random=false,Chat=false,GameStarts=false;
	public int GameSpeed=120,MinLevel=1,FinalScore=41,NoOfPlayers=4;//GameSpeed testing
}