using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class TimeO
{
	public int DayOfYear,Hour,Minutes;
	public static TimeO GetCurrentTime(){
		Debug.Log ("Here");
		TimeO CurrentTime = new TimeO ();
		CurrentTime.DayOfYear = DateTime.UtcNow.DayOfYear;
		CurrentTime.Hour = DateTime.UtcNow.Hour;
		CurrentTime.Minutes = DateTime.UtcNow.Minute;
		return CurrentTime;
	}
}