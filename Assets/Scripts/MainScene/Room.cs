﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Room : MonoBehaviour {

	public GameObject[] ProfileImgs,GameOptions;
	[HideInInspector]
	public JsonRoomO MyJsonRoomO;

	void Start(){
		GetComponent<RectTransform> ().localScale = new Vector3 (1,1,1);
		try{
			SetRoomIconsDetails ();}catch{
		}
		GetComponent<Button> ().onClick.AddListener (() => ClickOnRoomBTN());
	}

	private void SetRoomIconsDetails ()
	{
		/*GameOptions
		 * 0	=>	GameSpeedFather
		 * 1	=>	NoLeaving
		 * 2	=>	GameStatusFather
		 * */
		for(int i=0;i<4;i++)
			FBManager.Instance.SetFPPhoto (MyJsonRoomO.PlayersFBIDs[i], ProfileImgs [i].GetComponent<Image> ());
		if(MyJsonRoomO.RoomOptions.GameSpeed==30)
			GameOptions [0].transform.GetChild (0).gameObject.SetActive (true);
		else if(MyJsonRoomO.RoomOptions.GameSpeed==20)
			GameOptions [0].transform.GetChild (1).gameObject.SetActive (true);
		else
			GameOptions [0].transform.GetChild (2).gameObject.SetActive (true);
		if (MyJsonRoomO.RoomOptions.NoLeaving)
			GameOptions [1].SetActive (true);
		if (!RoomIsFull ())
			GameOptions [2].transform.GetChild (0).gameObject.SetActive (true);
		else
			GameOptions [2].transform.GetChild (1).gameObject.SetActive (true);
	}

	bool RoomIsFull ()
	{
		for (int i = 0; i < MyJsonRoomO.SeatsStatus.Length; i++)
			if (MyJsonRoomO.SeatsStatus [i] == 0)
				return false;
		return true;
	}

	public void ClickOnRoomBTN(){
		string RoomID = MyJsonRoomO.RoomID;
		RoomsManager.PlayingRoomID = RoomID;
		Player.Instance.OnlinePlayerID = -1;
		SceneManager.LoadScene (MainGameManager.GameTypeID);
	}
}