﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class RoomsManager : MonoBehaviour
{

	public static RoomsManager Instance;
	public GameObject RoomGO,ContentGO;
	public static string PlayingRoomID;
	public RoomOptionsWindow RoomOptionsWindowScript;

	void Awake ()
	{
		Instance = this;
	}
	void Start ()
	{
		SetInitials ();
		AssingGO ();
	}
	void SetInitials ()
	{
		
	}
	void AssingGO ()
	{
	}

	public void CeateNewRoom (JsonRoomO ThisJsonRoomO)
	{
		GameObject NewRoomGO = Instantiate (RoomGO, ContentGO.transform.position, Quaternion.identity,ContentGO.transform) as GameObject;
		NewRoomGO.GetComponent<Room> ().MyJsonRoomO = ThisJsonRoomO;
	}
	public void CreateGameNow ()
	{
		CreateGameNowfun ();
	}

	private void CreateGameNowfun(){
		string[] PlayersFPBIDs = new string[4]{Player.Instance.FBID,"0","0","0"};
		JsonRoomO NewRoomGO = new JsonRoomO (MainFireBaseManager.GameType,PlayersFPBIDs,GetGameType());
		NewRoomGO.PlayersNames [0] = Player.Instance.PlayerName;
		NewRoomGO.DatabasePlayersIDs [0] = Player.Instance.DatabasePlayerID;
		string RandomID = RandomIDGenerator.Instance.GetRandomID ();
		NewRoomGO.RoomID = RandomID;
		NewRoomGO.CreatorName = Player.Instance.PlayerName;
		RoomsManager.PlayingRoomID = RandomID;
		NewRoomGO.RoomOptions = GetRoomOptionsFromWindow (WindowsManager.Instance.WindowsGO[4]);
		NewRoomGO.IsACompititons = MainFireBaseManager.Instance.IsACompition;
		WindowsManager.Instance.WindowsGO[4].SetActive (false);
		Player.Instance.GameCreator = true;
		Player.Instance.OnlinePlayerID = 0;
		NewRoomGO.GameTypeID = MainGameManager.GameTypeID;
		NewRoomGO.GameOB.PlayersNO = NewRoomGO.RoomOptions.NoOfPlayers;
		MainFireBaseManager.Instance.CreateNewRoomServer (NewRoomGO);
		SceneManager.LoadScene (MainGameManager.GameTypeID);
	}

	public void CreateGameNowfunForComp(string RoomID,MainCompO ThisMainComp){
		string[] PlayersFPBIDs = new string[4]{Player.Instance.FBID,"0","0","0"};
		JsonRoomO NewRoomGO = new JsonRoomO (MainFireBaseManager.GameType,PlayersFPBIDs,GetGameType());
		NewRoomGO.PlayersNames [0] = Player.Instance.PlayerName;
		NewRoomGO.DatabasePlayersIDs [0] = Player.Instance.DatabasePlayerID;
		NewRoomGO.RoomID = RoomID;
		RoomsManager.PlayingRoomID = RoomID;
		NewRoomGO.RoomOptions = GetRoomOptionsFromWindow (WindowsManager.Instance.WindowsGO[4]);
		NewRoomGO.IsACompititons = true;
		Player.Instance.GameCreator = true;
		Player.Instance.OnlinePlayerID = 0;
		NewRoomGO.GameTypeID = MainGameManager.GameTypeID;
		NewRoomGO.GameOB.PlayersNO = 4;
		NewRoomGO.RoomOptions.Random = ThisMainComp.CompOptions.Randomness==1;
		NewRoomGO.RoomOptions.Chat = ThisMainComp.CompOptions.ChatAvalability==1;
		NewRoomGO.RoomOptions.GameSpeed = GetActualSpeedFromComp (ThisMainComp.CompOptions.CompSpeed);
		NewRoomGO.RoomOptions.AllowKicking = false;
		NewRoomGO.Available = false;
		NewRoomGO.RoomOptions.NoLeaving = false;
		NewRoomGO.RoomOptions.Private = true;
		MainFireBaseManager.Instance.CreateNewRoomServer (NewRoomGO);
		SceneManager.LoadScene (MainGameManager.GameTypeID);
	}

	int GetActualSpeedFromComp (int compSpeed)
	{
		if (compSpeed == 3)
			return 180;
		else if (compSpeed == 2)
			return 120;
		else
			return 80;
	}

	private GameOBParent GetGameType ()
	{
		switch(MainGameManager.GameTypeID){
		case 1:	return new Game1();
		case 2:	return new Game2();
		case 3:	return new Game3();
		case 4:	return new Game4(4);
		case 5:	return new Game5();
		case 6:	return new Game6();
		case 7:	return new Game7();
		}
		return new Game1 ();
	}

	private JsonRoomOptions GetRoomOptionsFromWindow(GameObject WindowToRead){
		JsonRoomOptions ThisRoomOptions = new JsonRoomOptions ();
		int StartChildNO=1;
		//Game Speed, Default 120
		if (WindowToRead.transform.GetChild (StartChildNO+1).GetChild (0).GetComponent<Image> ().color.a == 1)
			ThisRoomOptions.GameSpeed = 180;
		else if (WindowToRead.transform.GetChild (StartChildNO+1).GetChild (2).GetComponent<Image> ().color.a == 1)
			ThisRoomOptions.GameSpeed = 80;
		//Allow Kicking
		if (WindowToRead.transform.GetChild (StartChildNO+2).GetComponent<Image> ().color == Color.green)
			ThisRoomOptions.AllowKicking = true;
		//Allow Leaving
		if (WindowToRead.transform.GetChild (StartChildNO+3).GetComponent<Image> ().color == Color.green)
			ThisRoomOptions.NoLeaving = true;
		//Min Level
		ThisRoomOptions.MinLevel = (int)WindowToRead.transform.GetChild (StartChildNO+4).GetComponent<Slider>().value;
		//Private
		if (WindowToRead.transform.GetChild (StartChildNO+5).GetComponent<Image> ().color == Color.green)
			ThisRoomOptions.Private = true;
		//FinalScore
		if (MainGameManager.GameTypeID == 1) {
			if (WindowToRead.transform.GetChild (StartChildNO + 8).GetChild (0).GetComponent<Image> ().color.a == 1)
				ThisRoomOptions.FinalScore = 31;
			else if (WindowToRead.transform.GetChild (StartChildNO + 8).GetChild (2).GetComponent<Image> ().color.a == 1)
				ThisRoomOptions.FinalScore = 61;
		}
		if (MainGameManager.GameTypeID == 4) {
			if (WindowToRead.transform.GetChild (StartChildNO + 9).GetChild (0).GetComponent<Image> ().color.a == 1)
				ThisRoomOptions.NoOfPlayers = 2;
			else if (WindowToRead.transform.GetChild (StartChildNO + 9).GetChild (1).GetComponent<Image> ().color.a == 1)
				ThisRoomOptions.NoOfPlayers = 3;
		}
		return ThisRoomOptions;
	}
}