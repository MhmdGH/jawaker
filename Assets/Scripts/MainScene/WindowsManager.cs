﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using AdvancedInputFieldPlugin;

public class WindowsManager : MonoBehaviour
{

	public static WindowsManager Instance;
	public GameObject[] WindowsGO;
	public AdvancedInputField CreateClubInputFeildGO;
	public static bool FirstLunch = true;
	public GameObject LoaderGO;
	public Text GameTypeNameTxt;

	void Awake ()
	{
		Instance = this;
	}

	public void ShowWindowAndHideOther (int WindowNO)
	{
		StoreManager.Instance.CurrentFriendSendID = "";
		/*
	 * 0	=>	Splash
	 * 1	=>	MainMenu
	 * 2	=>	GamesMenu
	 * 3	=>	RoomsMenu
	 * 4	=>	RoomOptions
	 * 5	=>	LoadingWindow
	 * 6	=>  CompitionsWindow
	 * 7	=>  ClubsWindow
	 * 8	=>	Clubs Create Window
	 * 9	=> 	Club Details
	 * */
		if (WindowNO == 4) {
			WindowsGO [WindowNO].transform.GetChild (9).gameObject.SetActive (false);
			WindowsGO [WindowNO].transform.GetChild (10).gameObject.SetActive (false);
			if (MainGameManager.GameTypeID == 1)//Tarneeb Syrian
				WindowsGO [WindowNO].transform.GetChild (9).gameObject.SetActive (true);
			else if (MainGameManager.GameTypeID == 4 && !MainFireBaseManager.GameType.Contains ("Partner"))//Hand
				WindowsGO [WindowNO].transform.GetChild (10).gameObject.SetActive (true);
		}
		for (int i = 0; i < WindowsGO.Length; i++)
			WindowsGO [i].SetActive (false);
		WindowsGO [WindowNO].SetActive (true);
	}

	public void HideAllWindows(){
		for (int i = 0; i < WindowsGO.Length; i++)
			WindowsGO [i].SetActive (false);
	}

	public void EscapeBTN ()
	{
		if (WindowsGO [9].activeSelf) {
			BTNSManager.Instance.ShowClubs ();
			return;
		}
		if (CompManager.Instance.CompDetails.WindowsGo [2].activeSelf) {
			CompManager.Instance.ShowThisWindow (2);
			return;
		}
		if (CompManager.Instance.CompDetails.WindowsGo [1].activeSelf) {
			CompManager.Instance.ShowThisWindow (1);
			return;
		}
		if (WindowsGO [4].activeSelf) {
			WindowsManager.Instance.ShowWindowAndHideOther (3);
			return;
		}
		if (WindowsGO [3].activeSelf) {
			WindowsManager.Instance.ShowWindowAndHideOther (2);
			return;
		}
		if (WindowsGO [2].activeSelf) {
			WindowsManager.Instance.ShowWindowAndHideOther (1);
			return;
		}

		if (PopsMSG.Instance.ContainerGO.activeSelf) {
			PopsMSG.Instance.OkBTN ();
			return;
		}
		if (RulesManager.Instance.IsShowed) {
			RulesManager.Instance.HideRules ();
			return;
		}

		if (WindowsGO [1].activeSelf) {
			Application.Quit ();
			return;
		}
		if (OnlinePlayerData.Instance.ContainerGO.activeSelf) {
			OnlinePlayerData.Instance.CloseBTNClick ();
			return;
		}
		SideMenuManager.Instance.ShowOrHide (false);
		ShowWindowAndHideOther (1);
	}

	public void ShowLoader (float DelayTime)
	{
		StartCoroutine (ShowLoaderCor (DelayTime));
	}

	public void ShowLoader (bool ShowOrHide)
	{
		LoaderGO.SetActive (ShowOrHide);
	}

	private IEnumerator ShowLoaderCor (float DelayTime)
	{
		LoaderGO.SetActive (true);
		yield return new WaitForSeconds (DelayTime);
		LoaderGO.SetActive (false);
	}

	public void ClearContentGO (GameObject ClearThisListGO)
	{
		RectTransform ClearThisListGORectTransform = ClearThisListGO.GetComponent<RectTransform> ();
		try {
			if (ClearThisListGORectTransform.childCount != 0)
				for (int i = 0; i < ClearThisListGORectTransform.childCount; i++)
					Destroy (ClearThisListGORectTransform.GetChild (i).gameObject);
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error Fuking Dick Hole:\t" + e.Message);
		}
	}
}