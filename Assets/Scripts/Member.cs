﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Member : MonoBehaviour {

	public Image MemberIMG;
	public Text MemberNameTxt,MemberPointsTxt,LevelTxt,WeeklyPointsTxt;
	[HideInInspector]
	public string MemberFBID, MemberName, MemberPoints;

	public void SetMemberDetails(ClubMemberO ThisMember){
		try {FatherFBManager.ParentInstance.SetFPPhoto (ThisMember.MemberFBID, MemberIMG);}catch{}
		MemberNameTxt.text = ThisMember.MemberName;
		MemberPointsTxt.text = ThisMember.MemberPoints+"";
		WeeklyPointsTxt.text = ThisMember.WeeklyPoints+"";
		LevelTxt.text = ThisMember.Level+"";
	}
}