﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Notification : MonoBehaviour {

	public Text NotificationTxt;

	public void SetNotification(string NotificationMSGBody){
		NotificationTxt.text = NotificationMSGBody;
		GetComponent<RectTransform> ().localScale = new Vector3 (1,1,1);
	}
}
