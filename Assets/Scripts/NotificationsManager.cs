﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificationsManager : MonoBehaviour {

	public static NotificationsManager Instance;
	public GameObject NotificationsContainerGO,NotificationItemPrefabGO;
	public RectTransform NotificationsContentRect;
	void Awake(){
		Instance=this;
	}

	public void AddNotify(string NotificationMSGBody){
		if (NotificationMSGBody.Length < 2)
			return;
		GameObject NewNotify = Instantiate (NotificationItemPrefabGO,NotificationsContentRect.position,
			Quaternion.identity,NotificationsContentRect) as GameObject;
		NewNotify.GetComponent<Notification> ().SetNotification(NotificationMSGBody);
		NotificationsContentRect.sizeDelta = new Vector2 (0,NotificationsContentRect.sizeDelta.y+170);
	}

	public void RequestMyNotificationsList(){
		MainFireBaseManager.Instance.GiveMeMyNotificationList();
	}

	public void ShowMyNotificationList(List<NotificationO> NotificationList){
		Debug.Log ("ShowMyNotificationList");
		bool ThereISNotification = false;
		NotificationsContainerGO.SetActive (true);
		for (int i = 0; i < NotificationsContentRect.childCount; i++)
			DestroyObject (NotificationsContentRect.GetChild (i).gameObject);
		NotificationsContentRect.sizeDelta = new Vector2 (0,0);
		foreach (NotificationO NotificationItem in NotificationList) {
			ThereISNotification = true;
			AddNotify (NotificationItem.MSGBody);
		}
		if(!ThereISNotification)
			PopsMSG.Instance.ShowPopMSG("There No Notification!");
	}
}