using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]
public class OneRound
{
	public string[] PlayersIDs,PlayersFBID;
	public string RoomID;
	public int RoundStatus;
	public OneRound(){
		PlayersIDs = new string[4];
		PlayersIDs [0] = "0";
		PlayersIDs [1] = "0";
		PlayersIDs [2] = "0";
		PlayersIDs [3] = "0";
		PlayersFBID = new string[4];
		PlayersFBID [0] = "0";
		PlayersFBID [1] = "0";
		PlayersFBID [2] = "0";
		PlayersFBID [3] = "0";
		RoundStatus = 0;
	}
}