﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System;

public class OnlinePlayerData : MonoBehaviour
{

	public static OnlinePlayerData Instance;
	public Image PlayerIMG, FlagIMG;
	public GameObject[] ActionsBTNSGO, BadgesGO;
	public GameObject ContainerGO, CloseBTNGO, KickPlayerBTNGO;
	public Text PlayerNameTxt, PlayerLevelTxt, ClubNameTxt;
	[HideInInspector]
	public OnlinePlayerO OnlinePlayer;
	private Color FullAlpha, LowAlpha;
	public Sprite DefaultIMGSprite;

	void Awake ()
	{
		Instance = this;
	}

	void Start ()
	{
		FullAlpha = Color.white;
		LowAlpha = new Color (1, 1, 1, 0.5f);
		CloseBTNGO.GetComponent<Button> ().onClick.AddListener (CloseBTNClick);
	}

	public void CloseBTNClick ()
	{
		ContainerGO.SetActive (false);
	}

	public void ShowDataForThisPlayer (OnlinePlayerO ThisPlayer)
	{
		PlayerIMG.sprite = DefaultIMGSprite;
		try {
			OnlinePlayer = ThisPlayer;
			try {
				if (SceneManager.GetActiveScene ().buildIndex != 0 && PlayersManagerParent.ParentInstance.TempCurrenRoom.RoomOptions.AllowKicking
				  && Player.Instance.GameCreator && ThisPlayer.PlayerID != Player.Instance.DatabasePlayerID)
					KickPlayerBTNGO.SetActive (true);
				else
					KickPlayerBTNGO.SetActive (false);
			} catch {

				try {
					KickPlayerBTNGO.SetActive (false);
				} catch {
				}
			}
			FatherFBManager.ParentInstance.SetFPPhoto (ThisPlayer.FBID, PlayerIMG);
			ClubNameTxt.text = OnlinePlayer.ClubName;
			PlayerNameTxt.text = OnlinePlayer.PlayerName;
			PlayerLevelTxt.text = OnlinePlayer.PlayerLevel.ToString ();
			for (int i = 0; i < BadgesGO.Length; i++)
				BadgesGO [i].SetActive (ThisPlayer.Badges [i] == 1);
			SetPlayerFlag ();
			for (int i = 0; i < ActionsBTNSGO.Length; i++)
				ActionsBTNSGO [i].SetActive (true);
			SetActionBtns ();
			ContainerGO.SetActive (true);
		} catch (Exception e) {
			Debug.Log (e.Message);
		}
	}

	void SetActionBtns ()
	{
		List<FriendsO> HisFriendsList = new List<FriendsO> ();
		List<FriendsO> MyFriendsList = new List<FriendsO> ();
		HisFriendsList = OnlinePlayer.MyFrinedListO.MyFriends;
		MyFriendsList = Player.Instance.ThisOnlinePlayer.MyFrinedListO.MyFriends;
		string MyDatabasePlayerID = Player.Instance.DatabasePlayerID;
		if (OnlinePlayer.PlayerID == MyDatabasePlayerID) {
			for (int i = 0; i < ActionsBTNSGO.Length; i++)
				ActionsBTNSGO [i].SetActive (false);
			return;
		}
		for (int i = 0; i < HisFriendsList.Count; i++)
			if (HisFriendsList [i].FriendID == MyDatabasePlayerID)
				ActionsBTNSGO [0].SetActive (false);
		for (int i = 0; i < MyFriendsList.Count; i++) {
			if (MyFriendsList [i].FriendID == OnlinePlayer.PlayerID) {
				if (MyFriendsList [i].Blocked)
					ActionsBTNSGO [1].GetComponent<Button> ().image.color = LowAlpha;
				else
					ActionsBTNSGO [1].GetComponent<Button> ().image.color = FullAlpha;
				return;
			}
		}
	}


	private void SetPlayerFlag ()
	{
		StartCoroutine (SetPlayerFlagCor ());
	}

	private IEnumerator SetPlayerFlagCor ()
	{
		string CountryCode = OnlinePlayer.CountryCode;
		FlagIMG.enabled = false;
		WWW MyCountryIMG = new WWW ("https://ipfind.co/flags/128/" + CountryCode + ".png");
		yield return MyCountryIMG;
		FlagIMG.enabled = true;
		FlagIMG.sprite = 
			Sprite.Create (MyCountryIMG.texture, new Rect (0, 0, MyCountryIMG.texture.width, MyCountryIMG.texture.height), new Vector2 (0, 0));
	}

	public void BTNSFunction ()
	{
		GameObject CurrentBTNGO = EventSystem.current.currentSelectedGameObject;
		string BTNName = CurrentBTNGO.name;
		if (BTNName == "AddFriendBTN")
			SendFriendRequest ();
		else if (BTNName == "BlockBTN")
			BlockHim ();
		else if (BTNName == "SendMoneyBTN")
			SendMoneyToCurrentOnlinePlayer ();
		else if (BTNName == "KickPlayerBTN")
			KickThisPlayer ();
		SetActionBtns ();
	}

	void SendFriendRequest ()
	{
		//Check if request sended
		RequestO NewFriendRequest = new RequestO ();
		NewFriendRequest.RequestID = RandomIDGenerator.Instance.GetRandomID ();
		NewFriendRequest.SenderFBID = Player.Instance.FBID;
		NewFriendRequest.SenderName = Player.Instance.PlayerName;
		NewFriendRequest.SenderID = Player.Instance.DatabasePlayerID;
		NewFriendRequest.RecieverID = OnlinePlayer.PlayerID;
		CheckToGetTokens (OnlinePlayer.PlayerID);
		PlayingFireBaseFather.ParentInstance.DuplicatedRequest (NewFriendRequest);
		PopsMSG.Instance.ShowPopMSG ("You are already sent a request");
	}

	void CheckToGetTokens (string ThisPlayerID)
	{
		if (!Player.Instance.FriendsInvitationsIDs.Contains (ThisPlayerID))			
			StartCoroutine (ShowPopAfter(2,ThisPlayerID));
	}

	private IEnumerator ShowPopAfter(float WaitingTime,string ThisPlayerID){
		yield return new WaitForSeconds (WaitingTime);
		Player.Instance.Coins += 1000;
		Player.Instance.SavePlayerData ();
		PopsMSG.Instance.ShowPopMSG ("Congrats, You got +1000 Coins!");
		Player.Instance.FriendsInvitationsIDs += ThisPlayerID;
		Player.Instance.SavePlayerData ();
	}

	public void SendFriendRequestAfterDuplicateCheckd (RequestO ThisFriendRequest, bool DupOrNot)
	{
		if (DupOrNot) {
			PopsMSG.Instance.ShowPopMSG ("You are already sent a request");
			return;
		}
		PlayingFireBaseFather.ParentInstance.CreateNewFriendReuqest (ThisFriendRequest);
		PopsMSG.Instance.ShowPopMSG ("Request Sent Successfully");
	}

	void KickThisPlayer ()
	{
		PlayersManagerParent.ParentInstance.KickThisPlayer (OnlinePlayer.PlayerID);
		CloseBTNClick ();
	}

	void BlockHim ()
	{
		List<FriendsO> MyFriendsList = Player.Instance.ThisOnlinePlayer.MyFrinedListO.MyFriends;
		for (int i = 0; i < MyFriendsList.Count; i++)
			if (MyFriendsList [i].FriendID == OnlinePlayer.PlayerID)
				MyFriendsList [i].Blocked = !MyFriendsList [i].Blocked;
		Player.Instance.ThisOnlinePlayer.MyFrinedListO.MyFriends = MyFriendsList;
		MainFireBaseManager.Instance.UpdateThisPlayerData (Player.Instance.ThisOnlinePlayer);
	}

	void SendMoneyToCurrentOnlinePlayer ()
	{
		if (Player.Instance.Coins < 100) {
			PopsMSG.Instance.ShowPopMSG ("No Enough Coins");
			return;
		}
		Player.Instance.Coins -= 100;
		Player.Instance.SavePlayerData ();
		OnlinePlayer.Coins += 100;
		PlayingFireBaseFather.ParentInstance.NotifyThisPlayer (OnlinePlayer.PlayerID,
			new NotificationO ("" + Player.Instance.PlayerName + " Send you money!"));
		MainFireBaseManager.Instance.UpdateThisPlayerData (OnlinePlayer);
		PopsMSG.Instance.ShowPopMSG ("Send Successfully!");
	}

}