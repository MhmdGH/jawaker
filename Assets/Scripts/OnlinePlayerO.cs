using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class OnlinePlayerO
{
	public string PlayerID,PlayerName,FBID,CountryCode,ClubID,ClubName,
	JoinRequestID,JoinClubRequest,JoinClubRequestName,CurrentCompID;
	public int PlayerLevel,Coins,XPs,WeeklyXPs;
	public int[] Badges,Items;
	public FriendListO MyFrinedListO;
	public NotificationsO NotificationsListO;
	public OnlinePlayerO(){
		Coins = 99999;
		XPs = 0;
		WeeklyXPs = 0;
		ClubID = "";
		CurrentCompID = "";
		JoinRequestID = "";
		JoinClubRequest = "";
		JoinClubRequestName = "";
		Badges = new int[10];
		Items = new int[5]{-1,-1,-1,-1,-1};
		ClubName = "-";
		MyFrinedListO = new FriendListO ();
		NotificationsListO = new NotificationsO ();
	}
}