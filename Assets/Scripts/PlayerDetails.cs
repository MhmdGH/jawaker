﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerDetails : MonoBehaviour {

	public static PlayerDetails Instance;
	//MainStuffFather
	public GameObject PlayerDetailsStuffGO;
	//NameStuff
	public GameObject UpPatchGO,ButtomPatchGO,PlayerColorIMGGO,PlayerIMGGO,PlayerNameGO,ChangeNameWindowGO,
	ChangeNameInputFeildGO,PlayerLevelGO,EditNameGO;
	//FB Stuff
	public GameObject FBStatusGO,FBBTNGO;
	//SettingSStuff
	public Text LanguangeBTNGOTxt;
	private Color FullAlpha,LowAlpha;
	public Image SoundBTNIMG;

	void Awake(){
		Instance=this;
	}

	void Start(){
		SetInitials ();
	}

	void SetInitials ()
	{
		if (MainLanguageManager.Instance.IsArabic)
			LanguangeBTNGOTxt.text = "ع";
		PlayerNameGO.GetComponent<Text> ().text = Player.Instance.PlayerName;
		if (Player.Instance.IsBasha())
			PlayerNameGO.GetComponent<Text> ().color = Color.yellow;
		else 
			PlayerNameGO.GetComponent<Text> ().color = Color.white;
		RefreshFBConnectedData ();
		PlayerLevelGO.GetComponent<Text> ().text = Player.Instance.Level+"";
		FullAlpha = Color.white;
		LowAlpha = new Color (1,1,1,0.5f);
		RefreshSoundBTN ();
		EditNameGO.SetActive (Player.Instance.NameEdited==0);
	}

	public void BTNSFunction(){
		LanguangeBTNGOTxt.text = "ع";
		GameObject CurrentBTNGO = EventSystem.current.currentSelectedGameObject;
		string BTNName = CurrentBTNGO.name;
		if (BTNName == "EditNameBTN")
			ChangeNameWindowGO.SetActive (true);
		else if (BTNName == "FBConnectBTN")
			FBManager.Instance.FBLogInBTNClick ();
		else if (BTNName == "ChangeLanguange")
			MainLanguageManager.Instance.ChangeLang ();
		else if (BTNName == "CloseBTN")
			PlayerDetailsStuffGO.SetActive (false);
		else if (BTNName == "DoneBTN") {
			string PlayerNewName = ChangeNameInputFeildGO.GetComponent<AdvancedInputFieldPlugin.AdvancedInputField> ().Text;
			Player.Instance.PlayerName = PlayerNewName;
			EditNameGO.SetActive (false);
			Player.Instance.NameEdited = 1;
			Player.Instance.SavePlayerData ();
			ChangeNameWindowGO.SetActive (false);
			PlayerNameGO.GetComponent<Text> ().text = PlayerNewName;
			MainPlayerDetails.Instance.PlayerNameTxt.text = PlayerNewName;
		}
	}

	public void RefreshPlayerDetailsData ()
	{
		FBManager.Instance.SetFPPhoto (Player.Instance.FBID,PlayerIMGGO.GetComponent<Image>());
		PlayerColorIMGGO.GetComponent<Image> ().color = Player.Instance.PlayerRealColor;
		if (Player.Instance.BashaUpPatch == 1)
			UpPatchGO.SetActive (true);
		if (Player.Instance.BashaButtomPatch == 1)
			ButtomPatchGO.SetActive (true);
	}

	public void RefreshFBConnectedData(){
		if (Player.Instance.FBID.Length > 3) {
			FBBTNGO.SetActive (false);
			FBStatusGO.SetActive (true);
			RefreshPlayerDetailsData ();
		} else {
			FBBTNGO.SetActive (true);
			FBStatusGO.SetActive (false);
		}
	}

	public void RefreshSoundBTN(){
		if (AudioManager.AudioVolume == 0) {
			SoundBTNIMG.color = LowAlpha;
			SoundBTNIMG.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().color= LowAlpha;
		} else {
			SoundBTNIMG.color = FullAlpha;
			SoundBTNIMG.GetComponent<RectTransform> ().GetChild (0).GetComponent<Text> ().color = FullAlpha;
		}
	}
}