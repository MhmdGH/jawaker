﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayingChatFriendItem : MonoBehaviour {

	public Image FriendIMG;
	public Text FriendNameTxt;
	[HideInInspector]
	public FriendsO ThisFriend;

	public void SetFriendDetails(FriendsO NewFriend){
		GetComponent<RectTransform> ().localScale = new Vector3 (1,1,1);
		ThisFriend = NewFriend;
		FatherFBManager.ParentInstance.SetFPPhoto (ThisFriend.FriendsFBID,FriendIMG);
		FriendNameTxt.text = ThisFriend.FriendName;
		GetComponent<Button> ().onClick.AddListener (OnFriendClick);
	}

	private void OnFriendClick(){
		PlayingFireBaseFather.ParentInstance.GetThisOnlinePlayerAndCheckIfCanWith (ThisFriend.FriendID,false);
	}
}