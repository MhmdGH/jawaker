﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayingFriendsManager : MonoBehaviour {

	public static PlayingFriendsManager Instance;
	public RectTransform FriendsRect;
	public GameObject PlayingFriendPrefab;

	void Awake(){
		Instance=this;
	}

	void Start(){
		RequestMyFriendsList ();
	}

	public void RequestMyFriendsList(){
		PlayingFireBaseFather.ParentInstance.GiveMeMyFriendsList(false);
	}

	public void ShowMyFriendsList(List<FriendsO> MyFriendsList){
		foreach(FriendsO FriendItem in MyFriendsList){
			GameObject NewFriendItem = Instantiate (PlayingFriendPrefab,FriendsRect);
			NewFriendItem.GetComponent<PlayingChatFriendItem> ().SetFriendDetails(FriendItem);
		}
	}
}