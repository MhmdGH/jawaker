﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatMSG : MonoBehaviour {

	[HideInInspector]
	public string MSGBody,IMGURL;
	public GameObject MSGIMGGO,MSGBodyGO;

	void Start(){
		FatherFBManager.ParentInstance.SetFPPhoto (IMGURL,MSGIMGGO.GetComponent<Image>());
		MSGBodyGO.GetComponent<Text> ().text = MSGBody;
	}
}