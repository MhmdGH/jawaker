﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ArabicSupport;
using System;
using UnityEngine.SceneManagement;

public class ChatManager : MonoBehaviour {

	public static ChatManager Instance;
	public GameObject MSGGO,ChatContainterGO,ChatInputGO;
	//PlayingStuff
	public GameObject GameChatContainerGO,GameChatContentGO,PlayingFriendsChatMenuContentGO,FriendsPlayingChatContainerGO,
	SpecificFriendInputFeildGO;
	private Vector2 ChatInputStartPos,GameChatContainerGOStartPos;

	void Awake(){
		Instance = this;
	}

	void Strat(){
		GameChatContainerGOStartPos = GameChatContainerGO.GetComponent<RectTransform> ().localPosition;
		ChatInputStartPos = ChatInputGO.GetComponent<RectTransform> ().localPosition;
	}

	public void AddMSG(string IMGURL,string MSGBody,bool GameChatOrSpecifcFreindChat){
		if (MSGBody.Length < 1)
			return;
		if (MSGBody.Contains ("http")) {
			PopsMSG.Instance.ShowPopMSG ("You cant send links");
			return;
		}
		GameObject ThisMSG = new GameObject ();
		if (GameChatOrSpecifcFreindChat) {
			if (SceneManager.GetActiveScene ().buildIndex == 0) {
				if (ChatContainterGO.transform.childCount >= 8)
					Destroy (ChatContainterGO.transform.GetChild (0).gameObject);
				if (MSGBody.EndsWith ("!@#"))
					return;
				ThisMSG = Instantiate (MSGGO, ChatContainterGO.transform) as GameObject;
			} else {
				if (GameChatContentGO.transform.childCount >= 8)
					Destroy (GameChatContentGO.transform.GetChild (0).gameObject);
				if (MSGBody.EndsWith ("!@#"))
					return;
				ThisMSG = Instantiate (MSGGO, GameChatContentGO.transform) as GameObject;
			}
		} else {
			if (PlayingFriendsChatMenuContentGO.transform.childCount >= 8)
				Destroy (PlayingFriendsChatMenuContentGO.transform.GetChild (0).gameObject);
			if (MSGBody.EndsWith ("!@#"))
				return;
			ThisMSG = Instantiate (MSGGO, PlayingFriendsChatMenuContentGO.transform) as GameObject;
		}
		if(!ThisMSG.name.Contains("LAndScape"))
			ThisMSG.GetComponent<RectTransform> ().localScale = new Vector3 (1,1,1);
		else
			ThisMSG.GetComponent<RectTransform> ().localScale = new Vector3 (0.27f,0.27f,1);
		ThisMSG.GetComponent<ChatMSG> ().IMGURL = IMGURL;
		ThisMSG.GetComponent<ChatMSG> ().MSGBody = MSGBody;
	}

	public void OnInputFeildStartEdit(){
		ChatInputGO.GetComponent<AdvancedInputFieldPlugin.AdvancedInputField> ().Text = "";
	}

	public void OnInputFeildEndEdit(){
		string InputTxt = ChatInputGO.GetComponent<AdvancedInputFieldPlugin.AdvancedInputField> ().Text;
		string FixedTxt = ArabicFixer.Fix (InputTxt);
		ChatInputGO.GetComponent<AdvancedInputFieldPlugin.AdvancedInputField> ().Text = FixedTxt;
	}

	public void SendMSG(bool MainOrFriend){
		string MSGBody = "";
		MessageO NewMSG = new MessageO("","");
		if (MainOrFriend) {
			MSGBody = ChatInputGO.GetComponent<AdvancedInputFieldPlugin.AdvancedInputField> ().Text;
			NewMSG = new MessageO (Player.Instance.FBID, MSGBody);
			ChatInputGO.GetComponent<AdvancedInputFieldPlugin.AdvancedInputField> ().Text = "";
		} else {
			MSGBody = SpecificFriendInputFeildGO.GetComponent<AdvancedInputFieldPlugin.AdvancedInputField> ().Text;
			NewMSG = new MessageO (Player.Instance.FBID, MSGBody);
			SpecificFriendInputFeildGO.GetComponent<AdvancedInputFieldPlugin.AdvancedInputField> ().Text = "";
		}
		if (NewMSG.MessageBody.Contains ("http")) {
			PopsMSG.Instance.ShowPopMSG ("You cant send links");
			return;
		}
			PlayingFireBaseFather.ParentInstance.SendMSGToThisFriend (NewMSG);
	}

	public void SetFocus(bool Focused){
		if (Focused)
			ChatInputGO.GetComponent<RectTransform> ().localPosition = new Vector2 (ChatInputStartPos.x, Screen.height/3);
		else
			ChatInputGO.GetComponent<RectTransform> ().localPosition = ChatInputStartPos;
	}

	public void PlayingSetFocus(bool Focused){
		if (Focused) {
			GameChatContainerGO.GetComponent<RectTransform> ().localPosition = new Vector2 (GameChatContainerGOStartPos.x, Screen.height/3);
			SpecificFriendInputFeildGO.GetComponent<RectTransform> ().localPosition = new Vector2 (GameChatContainerGOStartPos.x, Screen.height/3);
		} else {
			GameChatContainerGO.GetComponent<RectTransform> ().localPosition = GameChatContainerGOStartPos;
				SpecificFriendInputFeildGO.GetComponent<RectTransform> ().localPosition = GameChatContainerGOStartPos;
		}
	}

	//Playing Scene
	public void ClearFriendsChatContent ()
	{
		RectTransform ClearThisListGORectTransform = PlayingFriendsChatMenuContentGO.GetComponent<RectTransform> ();
		try {
			if (ClearThisListGORectTransform.childCount != 0)
				for (int i = 0; i < ClearThisListGORectTransform.childCount; i++)
					Destroy (ClearThisListGORectTransform.GetChild (i).gameObject);
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error:\t" + e.Message);
		}
	}

	public void ShowOrMainChatContainer(bool ShowOrHide){
		PlayingFireBaseFather.ParentInstance.StopChatListeting ();
		ChatContainterGO.SetActive (ShowOrHide);
	}

	public void ShowOrHideGameChat(bool ShowOrHide){
		PlayingFireBaseFather.ParentInstance.StopChatListeting ();
		GameChatContainerGO.SetActive (ShowOrHide);
	}

	public void ShowOrHidePlayingFriendsChat(bool ShowOrHide){
		PlayingFireBaseFather.ParentInstance.StopChatListeting ();
		FriendsPlayingChatContainerGO.SetActive (ShowOrHide);
	}
}