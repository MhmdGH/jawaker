using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class GameOBParent
{
	public PlayersCardsOB1 Cards;
	public int PlayersNO = 4;
	//Game 1 + Game 2 (Tarneeb Egyptian + Tarneeb Syrian)
	public int PlayerBidTurn = 0, TrumpPlayerID = 0, HighestBid = 0, CollectorID = 0, FinalScore = 41;
	public string TrumpType = "NT";
	public string[] PlayersBids;
	public int[] PlayersScores;
	public Round1 CurrentRound;
	//Game2
	public string TrumpCard="";
	//Game 3 (Trix)
	public Kingdom KingdomsDetails;
	//Game 4 Hand
	public Hand HandDetails;
	//Game 5
	public EstimationDetails EstimationDetailsOB;
	//Game 6
	public BalootDetails BalootDetailsOB;
	//Game7
	public JackarooDetails JackarooDetailsOB;
}