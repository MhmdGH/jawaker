using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class JackarooDetails
{
	public int PlayerWhoStartGame,RoundNO,CurrentThrower,StoneNO;
	public string CurrentCardID;
	public PlayersStonesO[] PlayersStones;
	public MoveStoneO MoveStone;
	public List<int> PlayersIDStonesControllers;
	public SwitchedStoneO SwitchStone;
	public bool DevideStatus,DevideChoosed3Or4,Complex,RemoveAll;

	public JackarooDetails(){
		RemoveAll = false;
		Complex = false;
		DevideStatus = false;
		DevideChoosed3Or4 = false;
		SwitchStone = new SwitchedStoneO ();
		PlayersIDStonesControllers = new List<int> (4);
		for(int i=0;i<4;i++)
			PlayersIDStonesControllers.Add (i);
		StoneNO = 0;
		MoveStone = new MoveStoneO ();
		CurrentThrower = 0;
		CurrentCardID = "";
		PlayersStones = new PlayersStonesO[4];
		PlayerWhoStartGame = 0;
		RoundNO = 1;
	}
}