﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public abstract class LanguangeManagerFather : MonoBehaviour {

	public static LanguangeManagerFather ParentInstance;
	[HideInInspector]
	public bool IsArabic;
	public Text[] ForAllScenes;
	[HideInInspector]
	public Font NiceArabicFont;

	public virtual void Awake(){
		ParentInstance = this;
		IsArabic = PlayerPrefs.GetInt ("IsArabic", 0) == 1;
	}

	public virtual void Start(){
		NiceArabicFont = Resources.Load<Font> ("ArabicFont");
	}

	public virtual void SetTheFukinArabicLang (){
		SetFontForThisTxts (ForAllScenes);
		ForAllScenes [0].text = ArabicFixer.Fix ("اجلس هنا");
		ForAllScenes [1].text = ArabicFixer.Fix ("عد للعبة");
		ForAllScenes [2].text = ArabicFixer.Fix ("اجلس هنا");
		ForAllScenes [3].text = ArabicFixer.Fix ("اجلس هنا");
		ForAllScenes [4].text = ArabicFixer.Fix ("اجلس هنا");
		ForAllScenes [5].text = ArabicFixer.Fix ("ابدأ اللعبة");
		ForAllScenes [6].text = ArabicFixer.Fix ("ادعو صديق");
		ForAllScenes [7].text = ArabicFixer.Fix ("من فضلك اختر شريكك");
		ForAllScenes [10].text = ArabicFixer.Fix ("الفائز");
		ForAllScenes [12].text = ArabicFixer.Fix ("لعبة جديدة");
		ForAllScenes [13].text = ArabicFixer.Fix ("أخر جولة");
		ForAllScenes [14].text = ArabicFixer.Fix ("مرحبا");
		ForAllScenes [15].text = ArabicFixer.Fix ("تمام");
		ForAllScenes [16].text = ArabicFixer.Fix ("فضلا \nاسرع");
		ForAllScenes [17].text = ArabicFixer.Fix ("اسف");
		ForAllScenes [18].text = ArabicFixer.Fix ("شكرا");
		ForAllScenes [19].text = ArabicFixer.Fix ("مبارك");
		ForAllScenes [20].text = ArabicFixer.Fix ("ههههه");
		ForAllScenes [21].text = ArabicFixer.Fix ("سنفوز");
		ForAllScenes [22].text = ArabicFixer.Fix ("لعبة\n جديدة؟");
		ForAllScenes [23].text = ArabicFixer.Fix ("سنخسر");
		ForAllScenes [24].text = ArabicFixer.Fix ("الشات");
	}

	protected void SetFontForThisTxts(Text[] TextsArray){
		for (int i = 0; i < TextsArray.Length; i++) {
			try{
				TextsArray [i].font = NiceArabicFont;}catch{
			}
		}
	}

	protected void SetFontForThisTxts(Text TextCO){
			TextCO.font = NiceArabicFont;
	}
}