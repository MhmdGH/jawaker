﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LastRound : MonoBehaviour {

	public GameObject XBTN;
	public GameObject[] LastRoundCards;
	private GameObject BackGroundGO, TitleGO;
	private RectTransform MyRectTranform;

	void Awake(){
		MyRectTranform = GetComponent<RectTransform>();
	}

	public void Start(){
		BackGroundGO = MyRectTranform.GetChild (0).gameObject;
		TitleGO = MyRectTranform.GetChild (1).gameObject;
		BackGroundGO.SetActive (false);
		TitleGO.SetActive (false);
		XBTN.GetComponent<Button> ().onClick.AddListener (() => MinimizeIt ());
		XBTN.SetActive (false);
	}

	public void MaximizeIt(){
		XBTN.SetActive (true);
		BackGroundGO.SetActive (true);
		TitleGO.SetActive (true);
		MyRectTranform.anchoredPosition = new Vector3 (400,750,0);
		MyRectTranform.localScale = new Vector3 (3,3,1);
	}

	public void MinimizeIt(){
		XBTN.SetActive (false);
		BackGroundGO.SetActive (false);
		TitleGO.SetActive (false);
		MyRectTranform.anchoredPosition = new Vector3 (120,150,0);
		MyRectTranform.localScale = new Vector3 (1,1,1);
	}

	public void SetCards(string[] CardsIDs){
		for(int i=0;i<LastRoundCards.Length;i++)
		{
			LastRoundCards [i].GetComponent<Card> ().CardID = CardsIDs [i];
			LastRoundCards [i].GetComponent<Card> ().IsCover = false;
			LastRoundCards [i].GetComponent<Card> ().CanDrag = false;
			LastRoundCards [i].GetComponent<Card> ().SetCardCoverOrFace ();
		}
	}
}