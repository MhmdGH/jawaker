using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class MoveStoneO
{
	public int MoverID,StoneNO,TargetPlace;
	public MoveStoneO(){
		MoverID = -1;
		StoneNO = -1;
		TargetPlace = -1;
	}

}
