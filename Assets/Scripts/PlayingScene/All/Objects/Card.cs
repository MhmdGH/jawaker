﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class Card : MonoBehaviour,IBeginDragHandler,IEndDragHandler,IDragHandler{

	//D Diamonds,H Hearts,S Spades,C Clover
	[HideInInspector]
	public string CardID = "";
	[HideInInspector]
	public bool IsCover=true,CanDrag,CardIsUp;
	protected int PrevSiblingIndex;
	protected RectTransform MyRectTransform;
	protected Transform MyContentParent;
	public Sprite[] CardSuits;
	protected IEnumerator ChooseCardCor;
	public Sprite CardSprite;
	private Color RedColor,BlackColor;

	void Awake(){
		MyRectTransform = GetComponent<RectTransform>();
		RedColor = new Color (0.78f,0.192f,0,1);
		BlackColor = new Color (0.239f,0.239f,0.239f,1);
	}

	void OnEnable(){
		MyContentParent = MyRectTransform.parent;
	}


	public void SetCardCoverOrFace ()
	{
		if (IsCover||CardID.Length<2)
			return;
		for (int i = 0; i < MyRectTransform.childCount; i++) {
			if(i<3)
				MyRectTransform.GetChild (i).gameObject.SetActive (true);
			else
				MyRectTransform.GetChild (i).gameObject.SetActive (false);
		}
		char[] CardIDChars = CardID.ToCharArray ();

		string CardNO = (CardIDChars.GetValue(1).ToString()+""+CardIDChars.GetValue(2).ToString());
		name = CardNO;
		GetComponent<Image> ().sprite = CardSprite;
		if (CardNO.StartsWith ("0"))//Below 10
			CardNO = CardIDChars [2].ToString();
		if (CardID [0].ToString () == "D" || CardID [0].ToString () == "H")
			MyRectTransform.GetChild (0).GetComponent<Text> ().color = RedColor;
		else
			MyRectTransform.GetChild (0).GetComponent<Text> ().color = BlackColor;
		MyRectTransform.GetChild (0).GetComponent<Text> ().text = CardIDToNO(CardNO);
		if (CardNO == "15") {
			CardID = "J"+CardIDChars [1].ToString()+CardIDChars [2].ToString();
			MyRectTransform.GetChild (1).GetComponent<Image> ().sprite = CardSuits[GetTheRightSprite("J")];
			MyRectTransform.GetChild (2).GetComponent<Image> ().sprite = CardSuits [GetTheRightSprite ("J")];
			return;
		}
		MyRectTransform.GetChild (1).GetComponent<Image> ().sprite = CardSuits [GetTheRightSprite (CardIDChars[0]+"")];
		MyRectTransform.GetChild (2).GetComponent<Image> ().sprite = CardSuits [GetTheRightSprite (CardIDChars[0]+"")];
	}

	protected IEnumerator CardUp(){
		CardIsUp = true;
		float time = 0f;
		Vector2 StartPosition = MyRectTransform.localPosition;
		Vector2 EndPosition = new Vector2 (MyRectTransform.localPosition.x,MyRectTransform.localPosition.y+30);
		float TimeToMove=0.1f;
		while (time < TimeToMove) {
			MyRectTransform.localPosition = Vector2.Lerp (StartPosition, EndPosition, time / TimeToMove);
			time = time + Time.deltaTime;
			yield return null;
		}
		MyRectTransform.localPosition = EndPosition;
	}

	private int GetTheRightSprite (string CardSuit)
	{
		if (CardSuit == "J")
			return 3;
		else if (CardSuit == "D")
			return 1;
		else if (CardSuit == "C")
			return 0;
		else if (CardSuit == "H")
			return 2;
		else if (CardSuit == "S")
			return 4;
		//Not Fukin Reatchable
		return 0;
	}

	private string CardIDToNO(string ThisCardNO){
		if (ThisCardNO == "11")
			return "J";
		else if (ThisCardNO == "12")
			return "Q";
		else if (ThisCardNO == "13")
			return "K";
		else if (ThisCardNO == "14")
			return "A";
		else if (ThisCardNO == "15")
			return "";
		else
			return ThisCardNO;
	}

	public abstract void ThroughIt (int StartPos);

	#region IBeginDragHandler implementation

	public void OnBeginDrag (PointerEventData eventData)
	{Debug.Log ("OnBeginDrag");
//		return;
		OnBeginDragFun ();
	}

	protected void OnBeginDragFun ()
	{
		if (!CanDrag)
			return;
		try{StopCoroutine (ChooseCardCor);}catch{
		}
		PlayersManagerParent.PlayerHoldingCard = true;
		GetComponent<Animator> ().enabled = false;
		PrevSiblingIndex = MyRectTransform.GetSiblingIndex ();
		MyRectTransform.SetParent(PlayersManagerParent.ParentInstance.CanvasGO.transform);
		MyRectTransform.SetAsLastSibling ();
	}

	#endregion

	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{
		if (!CanDrag)
			return;
		MyRectTransform.anchoredPosition = new Vector3 (0,0,0);
		Vector3 screenPoint = Input.mousePosition;
		screenPoint.z = 10f; //distance of the plane from the camera
		MyRectTransform.position= Camera.main.ScreenToWorldPoint (screenPoint);
	}

	#endregion

	#region IEndDragHandler implementation
	public virtual void OnEndDrag (PointerEventData eventData)
	{
		OnEndDragFun ();
	}

	public void OnEndDragFun ()
	{
		if (!CanDrag)
			return;
		CardIsUp = false;
		PlayersManagerParent.PlayerHoldingCard = false;
		GetComponent<Animator> ().enabled = true;
		if (PutCardOnBoard ())
			SendCard ();
		else
			ReturnCardBack ();
	}

	public void OnEndDragFun (bool OnlyPullDown)
	{
		CardIsUp = false;
		GetComponent<Animator> ().enabled = true;
		ReturnCardBack ();
	}

	public void ReturnCardBack ()
	{
		MyRectTransform.SetParent(MyContentParent);
		MyRectTransform.SetSiblingIndex (PrevSiblingIndex);
		PlayersManagerParent.PlayerHoldingCard = false;
		if(PlayersManagerParent.ParentInstance.TempCurrenRoom.GameType.Contains("Jackaroo"))
			JackarooManager7.Instance.ArrangeCards ();
	}

	#endregion

	protected virtual bool PutCardOnBoard ()
	{
		if (MyRectTransform.localPosition.y > -200)
			return true;
		else
			return false;
	}

	public int GetCardNO (string ThisCardID)
	{
		return int.Parse (GetChar (ThisCardID, 1) + GetChar (ThisCardID, 2));
	}

	public string GetChar (string ThisString, int IntIndex)
	{
		return ThisString.ToCharArray ().GetValue (IntIndex).ToString ();
	}

	public int GetThisCardNO(){
		return int.Parse (GetChar (CardID, 1) + GetChar (CardID, 2));
	}

	public abstract void SendCard ();
}