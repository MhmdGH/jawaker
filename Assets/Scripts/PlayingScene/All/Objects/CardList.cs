﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class CardList{

	public List<string> PlayerCardsList;
	public CardList(){
		PlayerCardsList = new List<string>();
	}
}