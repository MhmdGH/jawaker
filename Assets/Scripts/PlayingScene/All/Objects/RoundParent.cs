﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class RoundParent{
	//Game 1 + Game 2
	public int RoundNO,PlayerIDTurnToPlay=-1,PlayerIDWhoStartRound=-1;
	public string[] PlayersCards;
	public int[] PlayersScores;
	//Game 3 Trix
	public PlayersDoubles[] playerDoubles;
}
