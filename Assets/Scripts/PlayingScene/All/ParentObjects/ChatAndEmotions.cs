﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ChatAndEmotions{

	public int ActionID;
	public List<MessageO> ChatMessages;
	public ChatAndEmotions(){
		ActionID = 0;
		ChatMessages = new List<MessageO> ();
	}
}
