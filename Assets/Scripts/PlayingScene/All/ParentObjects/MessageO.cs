﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class MessageO{
	public string SenderID;
	public string MessageBody;
	public MessageO(string SenderID,string MessageBody){
		this.SenderID = SenderID;
		this.MessageBody = MessageBody;
	}
}