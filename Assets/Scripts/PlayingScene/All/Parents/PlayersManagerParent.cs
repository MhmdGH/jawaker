﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;
using System;

public abstract class PlayersManagerParent : MonoBehaviour
{
	public static PlayersManagerParent ParentInstance;
	public GameObject[] PlayersIMGS, PlayersNamesGO, SeatsGO, PlayersScores, PlayersCardsGO;
	[HideInInspector]
	public bool MyPlayerTakeASeat;
	public static bool PlayerHoldingCard;
	public Sprite DefaultPlayerImage;
	[HideInInspector]
	public JsonRoomO TempCurrenRoom;
	protected GameObject LastRoundGO;
	protected GameObject CardsListGO;
	[HideInInspector]
	public GameObject CanvasGO;
	protected const int WaitingTime = 7;
	protected int ScoreValue, XPsValue, AutoPlayCounter = 0;
	private bool ReloadSceneTrigger;
	public Image[] PlayersGreedAndRedIMGS;
	private Color DarkRed, DarkGreen;

	public virtual void Awake ()
	{
		ParentInstance = this;
	}

	public virtual void Start ()
	{
		DarkRed = new Color (0.5f,0,0,1);
		DarkGreen = new Color (0,0.5f,0,1);
		LastRoundGO = GameObject.Find ("LastRound");
		CanvasGO = GameObject.FindGameObjectWithTag ("Canvas");
		gameObject.AddComponent<CheckInternet> ();
		InvokeRepeating ("InterNetCheck", 2, 3);
		SetWinsPoints ();
		PlayerDatabaseIDs = new string[4];
		SeatsStatus = new int[4];
		ReloadSceneTrigger = true;
		if (PlayerPrefs.GetInt ("ReloadScene", 0) == 0) {
			ReloadSceneTrigger = false;
			PlayerPrefs.SetInt ("ReloadScene", 1);
		}
	}

	private string FirstWinnerID, SocendWinnerID;
	private string FirstWinnerFBID, SocendWinnerFBID;

	protected bool IsThisFuckerTrulyTurnToPlay ()
	{
		return(PlayerHoldingCard&&(TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound != Player.Instance.OnlinePlayerID &&
			TempCurrenRoom.GameOB.CurrentRound.PlayersCards [PreviosTurn (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay)].Length > 1) && MyTurnToPlay ())
			;
	}

	protected int PreviosTurn (int TurnNO)
	{
		int ThisTurnInt = TurnNO - 1;
		if (ThisTurnInt < 0)
			return 3;
		else
			return ThisTurnInt;
	}

	public void SetCompData (string ThisFirstWinnerID, string ThisSocendWinnerID, string ThisFirstWinnerFBID, string ThisSocendWinnerFBID)
	{
		FirstWinnerID = ThisFirstWinnerID;
		SocendWinnerID = ThisSocendWinnerID;
		FirstWinnerFBID = ThisFirstWinnerFBID;
		SocendWinnerFBID = ThisSocendWinnerFBID;
		PlayingFireBaseFather.ParentInstance.GetThisCompObj (Player.Instance.CurrentCompID, 3);
	}

	public void SetCompDataAfterCompObjGetted (MainCompO ThisMainComp)
	{
		if (ThisMainComp.CompOptions.CurrentActiveRound == 1) {
			PlayingFireBaseFather.ParentInstance.AddMSGWallToComp ("Competition is Ended");
			ThisMainComp.CompOptions.CompStatus = 3;
			PlayingFireBaseFather.ParentInstance.AddCoinsToThisPlayer (FirstWinnerID, ThisMainComp.CompOptions.Prize);
			PlayingFireBaseFather.ParentInstance.AddCoinsToThisPlayer (SocendWinnerID, ThisMainComp.CompOptions.Prize);
		} else {
			AddThisPlayerToNextRound (ThisMainComp, FirstWinnerID, FirstWinnerFBID, GetThisPlayerCurrentRoundNo (ThisMainComp, FirstWinnerID));
			AddThisPlayerToNextRound (ThisMainComp, SocendWinnerID, SocendWinnerFBID, GetThisPlayerCurrentRoundNo (ThisMainComp, SocendWinnerID));
			if (!ThereIsSeatAvailableInThisComp (ThisMainComp)) {
				PlayingFireBaseFather.ParentInstance.AddMSGWallToComp ("Next Round Begins");
				ThisMainComp.CompOptions.CurrentActiveRound -= 1;
			}
		}
		MainFireBaseManager.Instance.CreateOrUpdateComp (ThisMainComp);
	}

	private bool ThereIsSeatAvailableInThisComp (MainCompO ThisMainComp)
	{
		RoundO FirstMainRound = ThisMainComp.CompGames.MainRounds [ThisMainComp.CompOptions.CurrentActiveRound - 1];
		for (int i = 0; i < FirstMainRound.Rounds.Count; i++) {
			OneRound RoundOneIteration = FirstMainRound.Rounds [i];
			for (int y = 0; y < 4; y++) {
				if (RoundOneIteration.PlayersIDs [y].Length < 2)
					return true;
			}
		}
		return false;
	}

	private int GetThisPlayerCurrentRoundNo (MainCompO ThisMainComp, string PlayerID)
	{
		int CurrentRoundNo = ThisMainComp.CompOptions.CurrentActiveRound;
		RoundO PlayerCurrentMainRound = ThisMainComp.CompGames.MainRounds [CurrentRoundNo - 1];
		for (int i = 0; i < PlayerCurrentMainRound.Rounds.Count; i++) {
			OneRound RoundOneIteration = PlayerCurrentMainRound.Rounds [i];
			for (int y = 0; y < 4; y++) {
				if (RoundOneIteration.PlayersIDs [y] == PlayerID) {
					return i;
				}
			}
		}
		return 0;
	}

	private void AddThisPlayerToNextRound (MainCompO MainCompObj, string PlayerID, string PlayerFBID, int CurrentPlayerRoundNo)
	{
		int CurrentRoundNo = MainCompObj.CompOptions.CurrentActiveRound;
		int NextMainRoundNo = CurrentRoundNo - 2;
		RoundO NextMainRoundO = MainCompObj.CompGames.MainRounds [NextMainRoundNo];
		int NextRoundNo = GetRoundNoInNextMainRound (CurrentPlayerRoundNo);
		OneRound RoundOneIteration = NextMainRoundO.Rounds [NextRoundNo];
		for (int y = 0; y < 4; y++) {
			if (RoundOneIteration.PlayersIDs [y].Length < 2) {
				MainCompObj.CompGames.MainRounds [NextMainRoundNo].Rounds [NextRoundNo].PlayersIDs [y] = PlayerID;
				MainCompObj.CompGames.MainRounds [NextMainRoundNo].Rounds [NextRoundNo].PlayersFBID [y] = PlayerFBID;
				MainCompObj.CompGames.MainRounds [NextMainRoundNo].Rounds [NextRoundNo].RoundStatus = 1;
				return;
			}
		}
	}

	int GetRoundNoInNextMainRound (int CurrentPlayerRoundNo)
	{
		if (CurrentPlayerRoundNo == 0 || CurrentPlayerRoundNo == 1)
			return 0;
		else if (CurrentPlayerRoundNo == 2 || CurrentPlayerRoundNo == 3)
			return 1;
		else if (CurrentPlayerRoundNo == 4 || CurrentPlayerRoundNo == 5)
			return 2;
		else
			return 3;
	}

	protected int GetHighestPlaydCard ()
	{
		List<int> PlayedCardsNO = new List<int> ();
		string[] PlayedCardsArray = TempCurrenRoom.GameOB.CurrentRound.PlayersCards;
		for (int i = 0; i < PlayedCardsArray.Length; i++)
			if (PlayedCardsArray [i].Length > 2)
				PlayedCardsNO.Add (GetCardNO (PlayedCardsArray [i]));
		PlayedCardsNO.Sort ();
		if (PlayedCardsNO.Count > 1)
			return PlayedCardsNO [PlayedCardsNO.Count - 1];
		else
			return 0;
	}

	protected bool ThisPlayerHaveThisCardType (List<string> ThisPlayerCardsList1, string PlayedTypeStr)
	{
		for (int i = 0; i < ThisPlayerCardsList1.Count; i++)
			if (ThisPlayerCardsList1 [i].StartsWith (PlayedTypeStr))
				return true;
		return false;
	}

	protected bool MyPartenerCardIsTheLowestOne ()
	{
		int PlayerIDTurn = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		string PartnerThrowdCard = TempCurrenRoom.GameOB.CurrentRound.PlayersCards [MyPartnerID (PlayerIDTurn)];
		string[] ThrowdCardsList = TempCurrenRoom.GameOB.CurrentRound.PlayersCards;
		for (int i = 0; i < ThrowdCardsList.Length; i++) {
			if (PartnerThrowdCard.Length < 2)
				return true;
			if (ThrowdCardsList [i].Length < 2)
				continue;
			if (GetCardNO (PartnerThrowdCard) < GetCardNO (ThrowdCardsList [i]))
				return true;
		}
		return false;
	}

	protected bool ThisIsThePlayerWhoStartsTheRound ()
	{
		return TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound == TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
	}

	public int TrumpTypeSound (string TrumpTypeString)
	{
		if (TrumpTypeString == "C")
			return 17;
		else if (TrumpTypeString == "D")
			return 18;
		else if (TrumpTypeString == "S")
			return 19;
		else
			return 20;
	}

	public string GetCardType (string CardID)
	{
		return GetChar (CardID, 0);
	}

	protected int MyPartnerID (int PlayerID)
	{
		if (PlayerID == 0)
			return 2;
		else if (PlayerID == 1)
			return 3;
		else if (PlayerID == 2)
			return 0;
		else
			return 1;
	}

	protected void CheckPlayerKick ()
	{
//		return;
		AutoPlayCounter++;
		PlayerIsBack (false);
		ShowBackBTN ();
		if (AutoPlayCounter > 3) {
			PopsMSG.Instance.ShowPopMSG ("Please Choose Another Game");
			PlayingBTNSManagerFather.ParentInstance.LeaveRoom ();
		}
	}

	void SetWinsPoints ()
	{
		ScoreValue = 100;
		XPsValue = 10;
		if (Player.Instance.BoosterDays > 0)
			XPsValue *= 2;
	}

	private void InterNetCheck ()
	{
		if (!MainGameManager.InternetAvailability)
			PlayingBTNSManagerFather.ParentInstance.LeaveRoom ();
	}

	public virtual void UpdateRoom (JsonRoomO CurrentRoomJSON)
	{
		if (TempCurrenRoom.chatAndEmotions.ActionID == 1) {
			TempCurrenRoom = CurrentRoomJSON;
			try {
				ReceiveEmotionAndShow ();
			} catch (Exception e) {
				Debug.Log (e.Message);
			}
			return;
		} else
			TempCurrenRoom = CurrentRoomJSON;
		MasterKickPlayerCheck ();
		UpdatePlayersDetails ();
		CheckGameStop ();
	}

	void CheckGameStop ()
	{
		if (!GameStopScreen.Instance.IsShowed && TempCurrenRoom.GamePaused)
			GameStopScreen.Instance.Show ();
		else if (GameStopScreen.Instance.IsShowed && !TempCurrenRoom.GamePaused)
			GameStopScreen.Instance.Hide ();
	}

	void MasterKickPlayerCheck ()
	{
		if (TempCurrenRoom.KickThisPlayerID.Length < 2)
			return;
		if (TempCurrenRoom.KickThisPlayerID == Player.Instance.DatabasePlayerID)
			StartCoroutine (KickPlayerCor ());
	}

	private IEnumerator KickPlayerCor ()
	{
		PopsMSG.Instance.ShowPopMSG ("Master Kicked You!");
		TempCurrenRoom.KickThisPlayerID = "";
		TempCurrenRoom.SeatsStatus [Player.Instance.OnlinePlayerID] = 0;
		PlayingFireBaseFather.ParentInstance.UploudThisJson (TempCurrenRoom, false);
		yield return new WaitForSeconds (1);
		PlayingBTNSManagerFather.ParentInstance.QuitNow ();
	}

	public void KickThisPlayer (string PlayerIDToKick)
	{
		TempCurrenRoom.KickThisPlayerID = PlayerIDToKick;
		PlayingFireBaseFather.ParentInstance.UploudThisJson (TempCurrenRoom, false);
	}

	//Emotion = Chat MSG
	public void SendEmotion (string ChatMSG)
	{
		MessageO NewMSG = new MessageO (Player.Instance.OnlinePlayerID + "", ChatMSG);
		TempCurrenRoom.chatAndEmotions.ActionID = 1;
		TempCurrenRoom.chatAndEmotions.ChatMessages.Add (NewMSG);
		PlayingFireBaseFather.ParentInstance.UploudThisJson (TempCurrenRoom, false);
	}

	private void ReceiveEmotionAndShow ()
	{
		int ChatCursorCounter = TempCurrenRoom.chatAndEmotions.ChatMessages.Count - 1;
		string SenderID = TempCurrenRoom.chatAndEmotions.ChatMessages [ChatCursorCounter].SenderID;
		string MSGBody = TempCurrenRoom.chatAndEmotions.ChatMessages [ChatCursorCounter].MessageBody;
		PlayingWindowsManagerFather.ParentInstance.ShowChatPop (FromOnlineToLocal (int.Parse (SenderID)), MSGBody);
		ChatManager.Instance.AddMSG (TempCurrenRoom.PlayersFBIDs [int.Parse (SenderID)], MSGBody, true);
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.chatAndEmotions.ActionID = 0;
		PlayingFireBaseFather.ParentInstance.UploudThisJson (TempCurrenRoom, false);
	}

	protected void ReturnsMyCardsAvailibity ()
	{
		for (int i = 0; i < CardsListGO.transform.childCount; i++)
			CardAvailability (CardsListGO.transform.GetChild (i).gameObject, true);
	}

	protected void CardAvailability (GameObject CardGO, bool TurnOnOrOff)
	{
		if (TurnOnOrOff) {
			CardGO.GetComponent<Image> ().color = Color.white;
			if (TempCurrenRoom.SeatsStatus [Player.Instance.OnlinePlayerID] == 1)
				CardGO.GetComponent<Card> ().CanDrag = true;
		} else {
			CardGO.GetComponent<Image> ().color = new Color (0.8f, 0.8f, 0.8f, 1);
			CardGO.GetComponent<Card> ().CanDrag = false;
		}
	}

	public int FromOnlineToLocal (int OtherPlayerOnlineID)
	{
		int ActualPlayerID = Player.Instance.OnlinePlayerID;
		if (ActualPlayerID == -1)
			ActualPlayerID = 0;
		int Result = OtherPlayerOnlineID - ActualPlayerID;
		if (TempCurrenRoom.GameTypeID == 4 && TempCurrenRoom.GameOB.PlayersNO != 4) {
			//HandHere & number of player ==2||3
			if (TempCurrenRoom.GameOB.PlayersNO == 2) {
				if (Result == -1)
					Result = 1;
			} else if (TempCurrenRoom.GameOB.PlayersNO == 2) {
				if (Result == -1)
					Result = 1;
				else if (Result == -2)
					Result = 2;
			}
		} else {
			if (Result == -1)
				Result = 3;
			else if (Result == -2)
				Result = 2;
			else if (Result == -3)
				Result = 1;
		}
		return Result;
	}

	private string[] PlayerDatabaseIDs;

	protected void UpdatePlayersDetails ()
	{
		if(TempCurrenRoom.ActionID>5&&!TempCurrenRoom.GameType.Contains("Hand"))
			for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++)
				SetPlayerRedOrGreed (FromOnlineToLocal (i), TempCurrenRoom.GreenOrRed[i]);
		if (TempCurrenRoom.ActionID > 2 && !ThereIsPlayersUpdates ())
			return;
		if (Player.Instance.OnlinePlayerID != -1)
			Player.Instance.OnlinePlayerID = TempCurrenRoom.PlayersIDs [Player.Instance.OnlinePlayerID];
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++) {
			UpdatePlayerPhotos (FromOnlineToLocal (i), TempCurrenRoom.PlayersFBIDs [i]);
			UpdatePlayersDatabaseID (FromOnlineToLocal (i), TempCurrenRoom.DatabasePlayersIDs [i]);
			UpdatePlayersNames (FromOnlineToLocal (i), TempCurrenRoom.PlayersNames [i]);
			ShowAvailabeSeats (FromOnlineToLocal (i), TempCurrenRoom.SeatsStatus [i]);
		}
		CheckPlayersInOrOut ();
	}

	private void SetPlayerRedOrGreed (int ThisPlayerID, int GreenOrRedStatus){
		if (GreenOrRedStatus == 1)
			PlayersGreedAndRedIMGS [ThisPlayerID].color = DarkGreen;
		else if (GreenOrRedStatus == 2)
			PlayersGreedAndRedIMGS [ThisPlayerID].color = DarkRed;

	}

	private int[] SeatsStatus;

	private void CheckPlayersInOrOut ()
	{
		if (ReloadSceneTrigger)
			return;
		if (NoOfOnesInThisIntarray (TempCurrenRoom.SeatsStatus) > NoOfOnesInThisIntarray (SeatsStatus))
			AudioManager.Instance.PlaySound (2);
		else if (NoOfOnesInThisIntarray (TempCurrenRoom.SeatsStatus) < NoOfOnesInThisIntarray (SeatsStatus))
			AudioManager.Instance.PlaySound (3);
		for (int i = 0; i < TempCurrenRoom.SeatsStatus.Length; i++) {
			int TempSeatStatus = TempCurrenRoom.SeatsStatus [i];
			SeatsStatus [i] = TempSeatStatus;
		}
		if (MainFireBaseManager.Instance.IsACompition && !TempCurrenRoom.RoomOptions.GameStarts)
		if (Player.Instance.GameCreator && NoOfOnesInThisIntarray (TempCurrenRoom.SeatsStatus) == 4) {
			TempCurrenRoom.RoomOptions.GameStarts = true;
			PlayingFireBaseFather.ParentInstance.StartGame ();
		}
	}

	private int NoOfOnesInThisIntarray (int[] seatsStatus)
	{
		int OnesCounter = 0;
		for (int i = 0; i < seatsStatus.Length; i++)
			if (seatsStatus [i] == 1)
				OnesCounter++;
		return OnesCounter;
	}

	bool ThereIsPlayersUpdates ()
	{
		bool ReturnBool = false;
		for (int i = 0; i < TempCurrenRoom.DatabasePlayersIDs.Length; i++) {
			if (PlayerDatabaseIDs [i] != TempCurrenRoom.DatabasePlayersIDs [i])
				ReturnBool = true;
			string tempPlayerIDHolderDatabase = TempCurrenRoom.DatabasePlayersIDs [i];
			PlayerDatabaseIDs [i] = tempPlayerIDHolderDatabase;
		}
		return ReturnBool;
	}

	protected void UpdatePlayersDatabaseID (int PlayerNO, string PlayerDatabaseID)
	{
		PlayingWindowsManagerFather.ParentInstance.ShowPlayerDataBTN [PlayerNO].name = PlayerDatabaseID;
	}

	protected void UpdatePlayerPhotos (int PlayerNO, string FBID)
	{
		try {
			FatherFBManager.ParentInstance.SetFPPhoto (FBID, PlayersIMGS [PlayerNO].GetComponent<Image> (), PlayerNO);
		} catch (Exception e) {
			Debug.Log (e.Message);
		}
	}

	protected void UpdatePlayersNames (int PlayerNO, string NameToSet)
	{
		PlayersNamesGO [PlayerNO].GetComponent<Text> ().text = NameToSet;
	}

	protected virtual void ShowAvailabeSeats (int SeatNO, int SeatStatus)
	{
		if (TempCurrenRoom.ActionID >= 2 || TempCurrenRoom.ActionID == -1) {
			SeatsGO [SeatNO].SetActive (false);
			return;
		}
		try {
			if (MyPlayerTakeASeat || Player.Instance.GameCreator) {
				SeatsGO [SeatNO].SetActive (false);
				return;
			}
		} catch (Exception e) {
			Debug.Log (e.Message);
		}
		bool Availabe = true;
		if (SeatStatus == 1)
			Availabe = false;
		SeatsGO [SeatNO].SetActive (Availabe);
	}

	protected void UpdateRoomPhase2 ()
	{
		if (Player.Instance.OnlinePlayerID == -1)
			return;
		PlayingWindowsManagerFather.ParentInstance.SetPlayerStuff ();
		Player.Instance.OnlinePlayerID = TempCurrenRoom.PlayersIDs [Player.Instance.OnlinePlayerID];
	}

	public int NoOfPlayersInTheRoom ()
	{
		int Counter = 0;
		for (int i = 0; i < 4; i++) {
			if (TempCurrenRoom.SeatsStatus [i] == 1)
				Counter++;
		}
		return Counter;
	}

	public void IfPlayerHoldingCardGetItBack ()
	{
		try {
			if (PlayersManagerParent.PlayerHoldingCard) {
				PlayersManagerParent.PlayerHoldingCard = false;
				CanvasGO.GetComponent<RectTransform> ().GetChild (CanvasGO.GetComponent<RectTransform> ().childCount - 1).
			GetComponent<Card> ().ReturnCardBack ();
				PlayersManagerParent.PlayerHoldingCard = false;
			}
		} catch {
		}
	}

	//Master Out//Basic Is 4 Players, just 1 or 2 game is 2 players in, ovverride it
	protected virtual void AssignNewMasterOrRemoveRoom ()
	{
		//Master Out Here
		if (!TempCurrenRoom.NoMaster)
			return;
		for (int i = 0; i < 4; i++) {
			if (Player.Instance.OnlinePlayerID == TempCurrenRoom.MasterID) {
				//Assign New Master
				Player.Instance.GameCreator = true;
				TempCurrenRoom.NoMaster = false;
				PlayingFireBaseFather.ParentInstance.UploudThisJson (TempCurrenRoom, true);
				PlayingFireBaseFather.ParentInstance.SetOnDissconnectHandle ();
				return;
			}
		}
	}

	public abstract void SetLastRoundCards ();

	private void OnDestroy ()
	{
		StopAllCoroutines ();
	}

	public abstract void OnApplicationPause (bool pauseStatus);

	public abstract void IamBack ();

	protected void ShowBackBTN ()
	{
		PlayingBTNSManagerFather.ParentInstance.IamBackGO.SetActive (true);
		PlayerIsBack (false);
	}

	protected void PlayerIsBack (bool YesOrNo)
	{
		if (YesOrNo)
			AutoPlayCounter = 0;
		IfPlayerHoldingCardGetItBack ();
		if (YesOrNo)
			for (int i = 0; i < CardsListGO.transform.childCount; i++) {
				if (CardsListGO.transform.GetChild (i).GetComponent<Image> ().color.r == 1)
					CardsListGO.transform.GetChild (i).GetComponent<Card> ().CanDrag = true;
			}
		else
			for (int i = 0; i < CardsListGO.transform.childCount; i++)
				CardsListGO.transform.GetChild (i).GetComponent<Card> ().CanDrag = false;
	}

	protected int MarginValue = 500;

	protected Vector2 TargetVect (int PLayerNO)
	{
		if (PLayerNO == 0)
			return new Vector2 (0, -MarginValue);
		else if (PLayerNO == 1)
			return new Vector2 (MarginValue / 2, 0);
		else if (PLayerNO == 2)
			return new Vector2 (0, MarginValue / 2);
		else
			return new Vector2 (-MarginValue / 2, 0);
	}

	public void CreatorPauseOrExit (bool ExitOrPause)
	{
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (PlayerID == -1)
			return;
		TempCurrenRoom.SeatsStatus [PlayerID] = 0;
		if (Player.Instance.GameCreator && NoOfPlayersInTheRoom () > 0) {
			TempCurrenRoom.NoMaster = true;
			Player.Instance.GameCreator = false;
			for (int i = 0; i < 4; i++) {
				if (TempCurrenRoom.SeatsStatus [i] == 1) {
					TempCurrenRoom.MasterID = i;
					break;
				}
			}
		}
		if (ExitOrPause) {
			TempCurrenRoom.PlayersFBIDs [PlayerID] = "";
			TempCurrenRoom.PlayersNames [PlayerID] = "";
			TempCurrenRoom.DatabasePlayersIDs [PlayerID] = "";
		}
		PlayingFireBaseFather.ParentInstance.UploudThisJson (TempCurrenRoom, TempCurrenRoom.NoMaster);
	}

	protected int GetHighestOrSmallestCardIFromThisList (List<string> ThisCardsList, bool HighOrLow)
	{
		List<int> ThisCardsListNOs = new List<int> ();
		for (int i = 0; i < ThisCardsList.Count; i++) {
			if (ThisCardsList [i].Length < 2)
				ThisCardsListNOs.Add (0);
			else
				ThisCardsListNOs.Add (GetCardNO (ThisCardsList [i]));
		}
		ThisCardsListNOs.Sort ();
		if (HighOrLow) {
			for (int i = 0; i < ThisCardsList.Count; i++) {
				if (ThisCardsList [i].Length < 2)
					continue;
				if (GetCardNO (ThisCardsList [i]) == ThisCardsListNOs [ThisCardsListNOs.Count - 1])
					return i;
			}
		} else {
			int SmallestCardNoIterator = 0;
			for (int i = 0; i < ThisCardsListNOs.Count; i++)
				if (ThisCardsListNOs [i] != 0) {
					SmallestCardNoIterator = i;
					break;
				}
			for (int i = 0; i < ThisCardsList.Count; i++) {
				if (ThisCardsList [i].Length < 2)
					continue;
				if (GetCardNO (ThisCardsList [i]) == ThisCardsListNOs [SmallestCardNoIterator])
					return i;
			}
		}
		return 0;
	}

	protected int GetHighestOrSmallestCardIFromThisList (List<string> ThisCardsList, bool HighOrLow, string CardType, bool OtherThanChoosedCardType)
	{
		List<string> ChoosedTypeThisCardsList = new List<string> ();
		if (OtherThanChoosedCardType) {
			if (HaveTypesOtherThanThisType (ThisCardsList, CardType)) {
				for (int i = 0; i < ThisCardsList.Count; i++) {
					if (ThisCardsList [i].Length < 2) {
						ChoosedTypeThisCardsList.Add ("0");
						continue;
					}
					if (GetCardType (ThisCardsList [i]) != CardType)
						ChoosedTypeThisCardsList.Add (ThisCardsList [i]);
					else
						ChoosedTypeThisCardsList.Add ("0");
				}
			} else
				ChoosedTypeThisCardsList = ThisCardsList;
		} else {
			if (ThisPlayerHaveThisCardType (ThisCardsList, CardType)) {
				for (int i = 0; i < ThisCardsList.Count; i++) {
					if (ThisCardsList [i].Length < 2) {
						ChoosedTypeThisCardsList.Add ("0");
						continue;
					}
					if (GetCardType (ThisCardsList [i]) == CardType)
						ChoosedTypeThisCardsList.Add (ThisCardsList [i]);
					else
						ChoosedTypeThisCardsList.Add ("0");
				}
			}else
				ChoosedTypeThisCardsList = ThisCardsList;
		}
		List<int> ThisCardsListNOs = new List<int> ();
		for (int i = 0; i < ChoosedTypeThisCardsList.Count; i++) {
			if (ChoosedTypeThisCardsList [i].Length < 2)
				ThisCardsListNOs.Add (0);
			else
				ThisCardsListNOs.Add (GetCardNO (ChoosedTypeThisCardsList [i]));
		}
		ThisCardsListNOs.Sort ();
		if (HighOrLow) {
			for (int i = 0; i < ChoosedTypeThisCardsList.Count; i++) {
				if (ChoosedTypeThisCardsList [i].Length < 2)
					continue;
				if (GetCardNO (ChoosedTypeThisCardsList [i]) == ThisCardsListNOs [ThisCardsListNOs.Count - 1])
					return i;
			}
		} else {
			int SmallestCardNoIterator = 0;
			for (int i = 0; i < ThisCardsListNOs.Count; i++)
				if (ThisCardsListNOs [i] != 0) {
					SmallestCardNoIterator = i;
					break;
				}
			for (int i = 0; i < ChoosedTypeThisCardsList.Count; i++) {
				if (ChoosedTypeThisCardsList [i].Length < 2)
					continue;
				if (GetCardNO (ChoosedTypeThisCardsList [i]) == ThisCardsListNOs [SmallestCardNoIterator])
					return i;
			}
		}
		return 0;
	}

	private bool HaveTypesOtherThanThisType (List<string> CardsList, string CardType)
	{
	
		for (int i = 0; i < CardsList.Count; i++)
			if (GetCardType (CardsList [i]) != CardType)
				return true;
		return false;
	}

	public bool MyTurnToPlay ()
	{
		if (Player.Instance.OnlinePlayerID == TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay)
			return true;
		else
			return false;
	}

	public int GetCardNO (string ThisCardID)
	{
		return int.Parse (GetChar (ThisCardID, 1) + GetChar (ThisCardID, 2));
	}

	public string GetChar (string ThisString, int IntIndex)
	{
		return ThisString.ToCharArray ().GetValue (IntIndex).ToString ();
	}

	protected int TimerWaitingTime (int SeatNO)
	{
		if (TempCurrenRoom.SeatsStatus [SeatNO] == 0)
			return WaitingTime;
		else
			return TempCurrenRoom.RoomOptions.GameSpeed;
	}
}