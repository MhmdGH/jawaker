﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using ArabicSupport;

public abstract class PlayingBTNSManagerFather : MonoBehaviour {

	public static PlayingBTNSManagerFather ParentInstance;
	protected string BTNName;
	protected GameObject CurrentBTNGO;
	public GameObject EmotionsContainer, OnStartBTNS,ChatSonGroupGO,IamBackGO,OnStartTxt;

	protected virtual void Awake(){
		ParentInstance = this;
	}

	void Start(){
		try{
			if (Player.Instance.GameCreator&&!Player.Instance.ReloadScene&&!MainFireBaseManager.Instance.IsACompition)
				OnStartBTNS.SetActive (true);
			else
				OnStartBTNS.SetActive (false);
			Player.Instance.ReloadScene=true;
		}catch{}
	}

	public virtual void BTNSFunction(){
		CurrentBTNGO =EventSystem.current.currentSelectedGameObject;
		BTNName = CurrentBTNGO.name;
		Debug.Log (BTNName);
		if (BTNName == "SitHereBTN")
			PlayingFireBaseFather.ParentInstance.SeatTaken (CurrentBTNGO.transform.parent.GetSiblingIndex ());
		else if (BTNName == "StartGameBTN") {
			OnStartBTNS.SetActive (false);
			PlayersManagerParent.ParentInstance.TempCurrenRoom.RoomOptions.GameStarts = true;
			PlayingFireBaseFather.ParentInstance.StartGame ();
		} else if (BTNName == "InviteBTN")
			InviteBTN ();
		else if (BTNName == "P1BTN" || BTNName == "P2BTN" || BTNName == "P3BTN")
			PartnerChoosed (GetChar (BTNName, 1));
		else if (BTNName == "ChatBTN") {
			if (MainFireBaseManager.Instance.IsACompition && !PlayersManagerParent.ParentInstance.TempCurrenRoom.RoomOptions.Chat)
				return;
			ChatManager.Instance.ShowOrMainChatContainer (true);
		} else if (BTNName == "ChatXBTN")
			ChatManager.Instance.ShowOrMainChatContainer (false);
		else if (BTNName == "AddFriendBTN")
			InviteBTN ();
		else if (BTNName == "ExitBTN") {
			if (PlayersManagerParent.ParentInstance.TempCurrenRoom.RoomOptions.NoLeaving)
				PopsMSG.Instance.ShowPopMSG ("If You Leave, You Will Lose 500 Coins!");
			QuitWindow.Instance.ContainerGO.SetActive (true);
		} else if (BTNName == "IamBack")
			IamBack (true);
		else if (BTNName == "MenuBTN")
			SideMenuManager.Instance.ShowOrHide (!SideMenuManager.Instance.GetComponent<Animator> ().GetBool ("IsShow"));
		else if (BTNName == "EmotionsBTN") {
			if (!PlayerIsJoinedThePlaying ())
				return;
			if (MainFireBaseManager.Instance.IsACompition && !PlayersManagerParent.ParentInstance.TempCurrenRoom.RoomOptions.Chat)
				return;
			EmotionsContainer.SetActive (true);
		} else if (BTNName.Contains ("Emotion") && !BTNName.Contains ("BTN"))
			SendEmotion (GetChar (BTNName, 0));
		else if (BTNName == "ChatSendBTN")
			SendChatMSGInputFeild ();
		else if (BTNName == "MoreOptionsBTN")
			ToggleMoreOptionsBTN ();
		else if (BTNName == "WhiteWatchersBTN") {
			ToggleMoreOptionsBTN ();
			WhiteWatchers.Instance.ShowWatchersList ();
		} else if (BTNName == "InfosBTN") {
			ToggleMoreOptionsBTN ();
			GameInfosWindow.Instance.Show ();
		}
		else if (BTNName == "PauseBTN"||BTNName=="GameDateBTN") {
			if (!Player.Instance.GameCreator) {
				PopsMSG.Instance.ShowPopMSG ("Only creator stops the game!");
			}
			ToggleMoreOptionsBTN ();
			GameStopScreen.Instance.ShowChooseStopWindow ();
		}
	}

	private void ToggleMoreOptionsBTN(){
		PlayingWindowsManagerFather.ParentInstance.RoomMoreInfosContainerGO.SetActive (
			!PlayingWindowsManagerFather.ParentInstance.RoomMoreInfosContainerGO.activeSelf);
	}
		
	void Update(){
		if (!QuitingOnce && Input.GetKeyDown (KeyCode.Escape)) {
			if (ChatSonGroupGO.activeSelf) {
				ChatSonGroupGO.SetActive (false);
				return;
			}
			QuitWindow.Instance.ContainerGO.SetActive (true);
		}
	}

	private bool QuitingOnce;
	public void QuitNow(){
		PopsMSG.Instance.ShowPopMSG ("Please Wait!");
		if (PlayersManagerParent.ParentInstance.TempCurrenRoom.RoomOptions.NoLeaving&&Player.Instance.Coins>=500) {
			Player.Instance.Coins -= 500;
			Player.Instance.SavePlayerData ();
		}
		QuitWindow.Instance.ContainerGO.SetActive (false);
		LeaveRoom ();
		QuitingOnce = true;
	}

	public void IamBack(bool YesOrNo){
		if (YesOrNo) {
			IamBackGO.SetActive (false);
			PlayersManagerParent.ParentInstance.IamBack ();
		} else
			IamBackGO.SetActive (true);
	}

	public void ShowOnlinePlayerData(){
		CurrentBTNGO =EventSystem.current.currentSelectedGameObject;
		BTNName = CurrentBTNGO.name;
		PlayingFireBaseFather.ParentInstance.ShowThisPlayerData (BTNName);
	}

	public void LeaveRoom ()
	{
		if(Player.Instance.GameCreator)
			PlayersManagerParent.ParentInstance.CreatorPauseOrExit (true);
		PlayingFireBaseFather.ParentInstance.LeaveRoomFireBase ();
		Player.Instance.ReloadScene = false;
		Player.Instance.PlayerLeaveRoom ();
		RoomsManager.PlayingRoomID = "";
		SceneManager.LoadScene (0);
	}

	private void SendEmotion (string EmoNO)
	{
		if (!PlayerIsJoinedThePlaying())
			return;
		EmotionsContainer.SetActive (false);
		PlayersManagerParent.ParentInstance.SendEmotion (EmoNO+"!@#");
	}

	private bool PlayerIsJoinedThePlaying(){
		if (Player.Instance.OnlinePlayerID >= 0)
			return true;
		else
			return false;
	}

	public void SendChatMSG(){
		if (!PlayerIsJoinedThePlaying())
			return;
		EmotionsContainer.SetActive (false);
		CurrentBTNGO =EventSystem.current.currentSelectedGameObject;
		BTNName = CurrentBTNGO.name;
		PlayersManagerParent.ParentInstance.SendEmotion (ActualBTNName(BTNName));
	}

	string ActualBTNName (string BTNName)
	{
		if (!LanguangeManagerFather.ParentInstance.IsArabic)
			return BTNName;
		if (BTNName.Contains ("Hello"))
			return ArabicFixer.Fix ("مرحبا");
		else if (BTNName.Contains ("It's OK"))
			return ArabicFixer.Fix ("تمام");
		else if (BTNName.Contains ("Faster Please"))
			return ArabicFixer.Fix ("فضلا أسرع");
		else if (BTNName.Contains ("Sorry"))
			return ArabicFixer.Fix ("أسف");
		else if (BTNName.Contains ("Thanks!"))
			return ArabicFixer.Fix ("شكرا");
		else if (BTNName.Contains ("Congrats"))
			return ArabicFixer.Fix ("مبارك");
		else if (BTNName.Contains ("Hahaha!"))
			return ArabicFixer.Fix ("ههههه!");
		else if (BTNName.Contains ("We Will Win"))
			return ArabicFixer.Fix ("سنفوز!");
		else if (BTNName.Contains ("Play Again?"))
			return ArabicFixer.Fix ("لعبة اخرى؟");
		else if (BTNName.Contains ("We Will Lose"))
			return ArabicFixer.Fix ("سنخسر!");
		return BTNName;
	}

	public void SendChatMSGInputFeild(){
		if (!PlayerIsJoinedThePlaying())
			return;
		GameObject ChatInputFeildGO = ChatSonGroupGO.transform.GetChild (3).gameObject;
		if (ChatInputFeildGO.GetComponent<AdvancedInputFieldPlugin.AdvancedInputField> ().Text.Length < 1)
			return;
		string MSGBody = ChatInputFeildGO.GetComponent<AdvancedInputFieldPlugin.AdvancedInputField> ().Text;
		ChatInputFeildGO.GetComponent<AdvancedInputFieldPlugin.AdvancedInputField> ().Text = "";
		if (MSGBody.Contains ("http"))
			return;
		PlayersManagerParent.ParentInstance.SendEmotion (MSGBody);
	}

	protected string GetChar(string ThisString,int IntIndex){
		return ThisString.ToCharArray ().GetValue (IntIndex).ToString ();
	}

	protected virtual void PartnerChoosed (string PartnerNO)
	{
		PlayingWindowsManagerFather.ParentInstance.ChoosePartnerGO.SetActive (false);
		PlayingFireBaseFather.ParentInstance.SetPartnersAndReorderPlayers (int.Parse(PartnerNO));
	}

	protected virtual void InviteBTN ()
	{
		InvitationManager.Instance.ShowFriendsList ();
	}
	private void OnDestroy(){
		StopAllCoroutines ();
	}
}