﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Database;
using Firebase;
using Firebase.Unity.Editor;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public abstract class PlayingFireBaseFather : MonoBehaviour
{

	public static PlayingFireBaseFather ParentInstance;
	[HideInInspector]
	public DatabaseReference reference;
	protected JsonRoomO TempRoomToUpload, CurrentRoomUpdate;
	private string DataBaseUrl = "https://kotshena-a66a8.firebaseio.com/";

	public virtual void Awake ()
	{
		ParentInstance = this;
	}

	public virtual void Start ()
	{
		InitilizeFireBase ();
		SetHandles ();
		SetInitials ();
		StartCoroutine (SetRoomStuff());
	}

	IEnumerator SetRoomStuff ()
	{
		yield return new WaitForSeconds (1);
		if (!Player.Instance.GameCreator) {
			for (int i = 0; i < PlayersManagerParent.ParentInstance.TempCurrenRoom.WhiteWatchersList.Count; i++)
				if (Player.Instance.PlayerName == PlayersManagerParent.ParentInstance.TempCurrenRoom.WhiteWatchersList [i])
					yield break;
			PlayersManagerParent.ParentInstance.TempCurrenRoom.WhiteWatchersList.Add (Player.Instance.PlayerName);
			PlayingFireBaseFather.ParentInstance.UploudThisJson (PlayersManagerParent.ParentInstance.TempCurrenRoom,
				false);
		}
	}

	protected void InitilizeFireBase ()
	{
		try {
			FirebaseDatabase.DefaultInstance.GoOnline ();
		} catch {
		}
		FirebaseApp.DefaultInstance.SetEditorDatabaseUrl (DataBaseUrl);
		reference = FirebaseDatabase.DefaultInstance.RootReference;
	}

	protected abstract void SetInitials ();

	public void SetHandles ()
	{
		reference.Child ("Rooms").Child (MainFireBaseManager.GameType).Child (RoomsManager.PlayingRoomID).ValueChanged += RoomUpdates;
		reference.Child ("Players").Child (Player.Instance.DatabasePlayerID).ValueChanged += OnlinePlayerUpates;
	}

	protected void OnlinePlayerUpates (object sender, ValueChangedEventArgs e)
	{
		if (e.DatabaseError != null)
			return;
		Player.Instance.UpdateMyOnlinePlayerData (e.Snapshot.GetRawJsonValue ());
	}

	private void RoomUpdates (object sender, Firebase.Database.ValueChangedEventArgs e)
	{
		if (e.DatabaseError != null)
			return;
		UpdateRoomNow (e.Snapshot.GetRawJsonValue ());
	}

	public virtual void UpdateRoomNow (string UpdatedRoomJson)
	{

	}

	//Create new Club from json object
	public void CreateNewClubOrAddMemberOrUpdate (ClubO NewClub)
	{
		string Club2Json = JsonUtility.ToJson (NewClub);
		reference.Child ("Clubs").Child (NewClub.ClubID).SetRawJsonValueAsync (Club2Json);
	}

	public void ShowThisPlayerData (string DatabasePlayerID)
	{
		if (DatabasePlayerID.Length < 2)
			return;
		FirebaseDatabase.DefaultInstance
			.GetReference ("Players")
			.Child (DatabasePlayerID)
			.GetValueAsync ().ContinueWith (task => {
			if (task.IsFaulted) {
				Debug.Log (task.Result.ToString ());
			} else if (task.IsCompleted) {
				DataSnapshot snapshot = task.Result;
				OnlinePlayerO ThisNewPlayer = new OnlinePlayerO ();
				JsonUtility.FromJsonOverwrite (snapshot.GetRawJsonValue (), ThisNewPlayer);
				OnlinePlayerData.Instance.ShowDataForThisPlayer (ThisNewPlayer);
			}
		});
	}

	public void AddCoinsToThisPlayer (string DatabasePlayerID, int CoinsPlusValue)
	{
		if (DatabasePlayerID.Length < 2)
			return;
		FirebaseDatabase.DefaultInstance
			.GetReference ("Players")
			.Child (DatabasePlayerID)
			.GetValueAsync ().ContinueWith (task => {
			if (task.IsFaulted) {
				Debug.Log (task.Result.ToString ());
			} else if (task.IsCompleted) {
				DataSnapshot snapshot = task.Result;
				OnlinePlayerO ThisNewPlayer = new OnlinePlayerO ();
				JsonUtility.FromJsonOverwrite (snapshot.GetRawJsonValue (), ThisNewPlayer);
				ThisNewPlayer.Coins += CoinsPlusValue;
				UpdateThisPlayerData (ThisNewPlayer);
			}
		});
	}

	public void ShowInvitableFriends (RectTransform ClearContent)
	{
		try {
			WindowsManager.Instance.ClearContentGO (ClearContent.gameObject);
			FirebaseDatabase.DefaultInstance
			.GetReference ("Players")
				.Child (Player.Instance.DatabasePlayerID)
				.Child ("MyFrinedListO")
				.Child ("MyFriends")
			.GetValueAsync ().ContinueWith (task => {
				if (task.IsFaulted) {
					Debug.Log (task.Result.ToString ());
				} else if (task.IsCompleted) {
					DataSnapshot snapshot = task.Result;
					foreach (var childSnapshot in snapshot.Children) {
						FriendsO ThisNewFriend = new FriendsO ();
						JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), ThisNewFriend);
						InvitationManager.Instance.AddThisFriend (ThisNewFriend);
					}
				}
			});
		} catch {
		
		}
	}

	public void NotifyThisPlayer (string NotifiedPalyerID, NotificationO ThisNotification)
	{
		string Notify2Json = JsonUtility.ToJson (ThisNotification);
		reference.Child ("Notifications").Child (NotifiedPalyerID).Child ("NotificationsListO").Child (RandomIDGenerator.Instance.GetRandomID ()).
		SetRawJsonValueAsync (Notify2Json);
	}

	public void DuplicatedRequest (RequestO ThisRequestO)
	{
		bool IsDuplicated = false;
		try {
			FirebaseDatabase.DefaultInstance
			.GetReference ("FriendsRequests")
			.Child (ThisRequestO.RecieverID)
			.Child ("MyRecievedRequests")
			.GetValueAsync ().ContinueWith (task => {
				if (task.IsFaulted) {
					Debug.Log (task.Result.ToString ());
				} else if (task.IsCompleted) {
					DataSnapshot snapshot = task.Result;
					foreach (var childSnapshot in snapshot.Children) {
						RequestO ThisNewRequestO = new RequestO ();
						JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), ThisNewRequestO);
						if (ThisNewRequestO.RecieverID == ThisRequestO.RecieverID) {
							IsDuplicated = true;
							break;
						}
					}
					if (!IsDuplicated)
						OnlinePlayerData.Instance.SendFriendRequestAfterDuplicateCheckd (ThisRequestO, IsDuplicated);
				}
			});
		} catch {
		}
	}

	public void AddPointsToMyClub ()
	{
		if (Player.Instance.ClubID.Length < 2)
			return;
		FirebaseDatabase.DefaultInstance
			.GetReference ("Clubs")
			.Child (Player.Instance.ClubID)
			.GetValueAsync ().ContinueWith (task => {
			if (task.IsFaulted) {
				Debug.Log (task.Result.ToString ());
			} else if (task.IsCompleted) {
				DataSnapshot snapshot = task.Result;
				ClubO ThisNewClub = new ClubO ();
				JsonUtility.FromJsonOverwrite (snapshot.GetRawJsonValue (), ThisNewClub);
				if (MainFireBaseManager.Instance.IsACompition)
					ThisNewClub.ClubPoints += 100;
				else
					ThisNewClub.ClubPoints += 10;
					for(int i=0;i<ThisNewClub.Members.Count;i++)
						if(ThisNewClub.Members[i].MemberID==Player.Instance.DatabasePlayerID){
							ThisNewClub.Members[i].WeeklyPoints = Player.Instance.WeeklyXPs;
							ThisNewClub.Members[i].MemberPoints = Player.Instance.XPs;
						}
				ThisNewClub.ClubLevel = ThisNewClub.GetLevel ();
				CreateNewClubOrAddMemberOrUpdate (ThisNewClub);
			}
		});
	}

	/// <summary>
	/// Friends Requests Function
	/// </summary>
	/// <param name="ThisRequest">This request</param>
	public void CreateNewFriendReuqest (RequestO ThisRequest)
	{
		string Request2Json = JsonUtility.ToJson (ThisRequest);
		reference.Child ("FriendsRequests").Child (ThisRequest.SenderID).Child ("MySendedRequests").Child (ThisRequest.RequestID).SetRawJsonValueAsync (Request2Json);
		reference.Child ("FriendsRequests").Child (ThisRequest.RecieverID).Child ("MyRecievedRequests").Child (ThisRequest.RequestID).SetRawJsonValueAsync (Request2Json);
	}

	public void GetThisOnlinePlayerAndCheckIfCanWith(string DatabasePlayerID,bool MainOrPlaying){
		if (DatabasePlayerID.Length < 2) {
			WindowsManager.Instance.ShowLoader (false);
			return;
		}
		FirebaseDatabase.DefaultInstance
			.GetReference ("Players")
			.Child(DatabasePlayerID)
			.GetValueAsync ().ContinueWith (task => {
				if (task.IsFaulted) {
					Debug.Log (task.Result.ToString ());
				} else if (task.IsCompleted) {
					DataSnapshot snapshot = task.Result;
					OnlinePlayerO ThisNewFriend= new OnlinePlayerO();
					JsonUtility.FromJsonOverwrite (snapshot.GetRawJsonValue (), ThisNewFriend);
					WindowsManager.Instance.ShowLoader (false);
					if(ThisNewFriend.CurrentCompID.Length>2 &&  ThisNewFriend.CurrentCompID==Player.Instance.CurrentCompID)
						PopsMSG.Instance.ShowPopMSG("You can't have chat with a friend in the same compition");
					else{
						if(MainOrPlaying)
							FriendsManager.Instance.GoToChatWithThisBitch (ThisNewFriend.PlayerID);
						else{
							ChatManager.Instance.ClearFriendsChatContent ();
							ChatManager.Instance.FriendsPlayingChatContainerGO.SetActive (true);
							PlayingFireBaseFather.ParentInstance.SetChatListnerForThisBitch (ThisNewFriend.PlayerID);
						}
					}
				}
			});
	}

	public void SeatTaken (int SeatNO)
	{
		int PlayerID = Player.Instance.OnlinePlayerID;
		TempRoomToUpload = CurrentRoomUpdate;
		Player.Instance.OnlinePlayerID = SeatNO;
		PlayerID = SeatNO;
		TempRoomToUpload.PlayersIDs [SeatNO] = SeatNO;
		PlayersManagerParent.ParentInstance.MyPlayerTakeASeat = true;
		TempRoomToUpload.SeatsStatus [SeatNO] = 1;
		TempRoomToUpload.PlayersFBIDs [PlayerID] = Player.Instance.FBID;
		TempRoomToUpload.PlayersNames [PlayerID] = Player.Instance.PlayerName;
		TempRoomToUpload.DatabasePlayersIDs [PlayerID] = Player.Instance.DatabasePlayerID;
		for (int i = 0; i < TempRoomToUpload.WhiteWatchersList.Count; i++)
			if (Player.Instance.PlayerName == TempRoomToUpload.WhiteWatchersList [i]) {
				TempRoomToUpload.WhiteWatchersList.RemoveAt (i);
				break;
			}
		UploudThisJson (TempRoomToUpload, false);
	}

	public void SetOnDissconnectHandle ()
	{
		if (Player.Instance.OnlinePlayerID == -1)
			return;
		reference.Child ("Rooms").Child (MainFireBaseManager.GameType).Child (RoomsManager.PlayingRoomID).OnDisconnect ().Cancel ();
		if (PlayersManagerParent.ParentInstance.NoOfPlayersInTheRoom () <= 1)
			reference.Child ("Rooms").Child (MainFireBaseManager.GameType).
			Child (RoomsManager.PlayingRoomID).OnDisconnect ().RemoveValue ();
		else {
			reference.Child ("Rooms").Child (MainFireBaseManager.GameType).
			Child (RoomsManager.PlayingRoomID).Child ("Update").OnDisconnect ().SetValue (0);

			reference.Child ("Rooms").Child (MainFireBaseManager.GameType).
			Child (RoomsManager.PlayingRoomID).Child ("SeatsStatus").Child (Player.Instance.OnlinePlayerID + "").OnDisconnect ().SetValue (0);

			reference.Child ("Rooms").Child (MainFireBaseManager.GameType).
			Child (RoomsManager.PlayingRoomID).Child ("PlayersFBIDs").Child (Player.Instance.OnlinePlayerID + "").OnDisconnect ().SetValue ("0");

			reference.Child ("Rooms").Child (MainFireBaseManager.GameType).
			Child (RoomsManager.PlayingRoomID).Child ("PlayersNames").Child (Player.Instance.OnlinePlayerID + "").OnDisconnect ().SetValue ("0");

			reference.Child ("Rooms").Child (MainFireBaseManager.GameType).
			Child (RoomsManager.PlayingRoomID).Child ("DatabasePlayersIDs").Child (Player.Instance.OnlinePlayerID + "").OnDisconnect ().SetValue ("");

			if (Player.Instance.GameCreator)
				reference.Child ("Rooms").Child (MainFireBaseManager.GameType).
				Child (RoomsManager.PlayingRoomID).Child ("NoMaster").OnDisconnect ().SetValue (true);

			reference.Child ("Rooms").Child (MainFireBaseManager.GameType).
			Child (RoomsManager.PlayingRoomID).Child ("Update").OnDisconnect ().SetValue (1);
		}
	}

	public void GiveMeMyFriendsList (bool MainOrPlaying)
	{
		FirebaseDatabase.DefaultInstance
			.GetReference ("Players")
			.Child (Player.Instance.DatabasePlayerID)
			.GetValueAsync ().ContinueWith (task => {
			if (task.IsFaulted) {
				Debug.Log (task.Result.ToString ());
			} else if (task.IsCompleted) {
				DataSnapshot snapshot = task.Result;
				OnlinePlayerO MyPlayerO = new OnlinePlayerO ();
				JsonUtility.FromJsonOverwrite (snapshot.GetRawJsonValue (), MyPlayerO);
				List<FriendsO> MyFriendsList = new List<FriendsO> ();
				MyFriendsList =	MyPlayerO.MyFrinedListO.MyFriends;
				if (MainOrPlaying)
					FriendsManager.Instance.ShowMyFriendsList (MyFriendsList);
				else
					PlayingFriendsManager.Instance.ShowMyFriendsList (MyFriendsList);
			}
		});
	}

	public void GiveMeMyFriendsListForSendItems ()
	{
		FirebaseDatabase.DefaultInstance
			.GetReference ("Players")
			.Child (Player.Instance.DatabasePlayerID)
			.GetValueAsync ().ContinueWith (task => {
				if (task.IsFaulted) {
					Debug.Log (task.Result.ToString ());
				} else if (task.IsCompleted) {
					DataSnapshot snapshot = task.Result;
					OnlinePlayerO MyPlayerO = new OnlinePlayerO ();
					JsonUtility.FromJsonOverwrite (snapshot.GetRawJsonValue (), MyPlayerO);
					List<FriendsO> MyFriendsList = new List<FriendsO> ();
					MyFriendsList =	MyPlayerO.MyFrinedListO.MyFriends;
					StoreManager.Instance.ShowMyFriendsList (MyFriendsList);
				}
			});
	}

	public void LeaveRoomFireBase ()
	{
		if (Player.Instance.OnlinePlayerID == -1)
			return;
		reference.Child ("Rooms").Child (MainFireBaseManager.GameType).Child (RoomsManager.PlayingRoomID).ValueChanged -= RoomUpdates;
		reference.Child ("Rooms").Child (MainFireBaseManager.GameType).Child (RoomsManager.PlayingRoomID).OnDisconnect ().Cancel ();
		if (PlayersManagerParent.ParentInstance.NoOfPlayersInTheRoom () == 0) {
			reference.Child ("Rooms").Child (MainFireBaseManager.GameType).
			Child (RoomsManager.PlayingRoomID).RemoveValueAsync ();
		} else {
			reference.Child ("Rooms").Child (MainFireBaseManager.GameType).
			Child (RoomsManager.PlayingRoomID).Child ("Update").SetValueAsync (0);

			reference.Child ("Rooms").Child (MainFireBaseManager.GameType).
			Child (RoomsManager.PlayingRoomID).Child ("SeatsStatus").Child (Player.Instance.OnlinePlayerID + "").SetValueAsync (0);

			reference.Child ("Rooms").Child (MainFireBaseManager.GameType).
			Child (RoomsManager.PlayingRoomID).Child ("PlayersFBIDs").Child (Player.Instance.OnlinePlayerID + "").SetValueAsync ("0");

			reference.Child ("Rooms").Child (MainFireBaseManager.GameType).
			Child (RoomsManager.PlayingRoomID).Child ("PlayersNames").Child (Player.Instance.OnlinePlayerID + "").SetValueAsync ("0");

			reference.Child ("Rooms").Child (MainFireBaseManager.GameType).
			Child (RoomsManager.PlayingRoomID).Child ("DatabasePlayersIDs").Child (Player.Instance.OnlinePlayerID + "").SetValueAsync ("");

			if (Player.Instance.GameCreator)
				reference.Child ("Rooms").Child (MainFireBaseManager.GameType).
				Child (RoomsManager.PlayingRoomID).Child ("NoMaster").SetValueAsync (true);
		}
	}

	public void UpdateThisPlayerData (OnlinePlayerO ThisPlayer)
	{
		string PlayerbjectToJson = JsonUtility.ToJson (ThisPlayer);
		reference.Child ("Players").Child (ThisPlayer.PlayerID).SetRawJsonValueAsync (PlayerbjectToJson);
	}

	public virtual void StartGame ()
	{
		PlayingBTNSManagerFather.ParentInstance.OnStartTxt.SetActive (false);
		TempRoomToUpload = CurrentRoomUpdate;
		TempRoomToUpload.ActionID = 2;
		string[] PartnersImgsURL = new string[3]{ "", "", "" };
		try {
			for (int i = 0; i < PartnersImgsURL.Length; i++)
				PartnersImgsURL [i] = TempRoomToUpload.PlayersFBIDs [i + 1];
		} catch {
		}
		PlayingWindowsManagerFather.ParentInstance.ShowAndSetPartnersPhotos (PartnersImgsURL);
		UploudThisJson (TempRoomToUpload, true);
	}

	public void UploudThisJson (JsonRoomO ThistempRoomToUpload, bool Update)
	{
		if (Update)
			ThistempRoomToUpload.Update = 1;
		else
			ThistempRoomToUpload.Update = 0;
		string RoomObjectToJson = JsonUtility.ToJson (ThistempRoomToUpload);
		reference.Child ("Rooms").Child (MainFireBaseManager.GameType).
		Child (RoomsManager.PlayingRoomID).SetRawJsonValueAsync (RoomObjectToJson);
	}

	public void UploudThisJson (bool Update)
	{
		if (Update)
			TempRoomToUpload.Update = 1;
		else
			TempRoomToUpload.Update = 0;
		string RoomObjectToJson = JsonUtility.ToJson (TempRoomToUpload);
		reference.Child ("Rooms").Child (MainFireBaseManager.GameType).
		Child (RoomsManager.PlayingRoomID).SetRawJsonValueAsync (RoomObjectToJson);
	}

	public void SetPartnersAndReorderPlayers (int PartnerNO)
	{
		if (PartnerNO == 1)
			SwitchThese (PartnerNO, 2);
		else if (PartnerNO == 3)
			SwitchThese (PartnerNO, 2);
		TempRoomToUpload.ActionID = 3;
		UploudThisJson (TempRoomToUpload, true);
	}

	private void SwitchThese (int NO1, int NO2)
	{
		TempRoomToUpload = CurrentRoomUpdate;
		string Name1 = TempRoomToUpload.PlayersNames [NO1];
		TempRoomToUpload.PlayersNames [NO1] = TempRoomToUpload.PlayersNames [NO2];
		TempRoomToUpload.PlayersNames [NO2] = Name1;
		string FBID1 = TempRoomToUpload.PlayersFBIDs [NO1];
		TempRoomToUpload.PlayersFBIDs [NO1] = TempRoomToUpload.PlayersFBIDs [NO2];
		TempRoomToUpload.PlayersFBIDs [NO2] = FBID1;
		int Seat1Status = TempRoomToUpload.SeatsStatus [NO1];
		TempRoomToUpload.SeatsStatus [NO1] = TempRoomToUpload.SeatsStatus [NO2];
		TempRoomToUpload.SeatsStatus [NO2] = Seat1Status;
		string DataBasePlayerID = TempRoomToUpload.DatabasePlayersIDs [NO1];
		TempRoomToUpload.DatabasePlayersIDs [NO1] = TempRoomToUpload.DatabasePlayersIDs [NO2];
		TempRoomToUpload.DatabasePlayersIDs [NO2] = DataBasePlayerID;
		TempRoomToUpload.PlayersIDs [NO1] = NO2;
		TempRoomToUpload.PlayersIDs [NO2] = NO1;
	}

	//CompStuff

	public void GetThisCompObj (string CompID, int AfterFunNo)
	{
		try {
			FirebaseDatabase.DefaultInstance
				.GetReference ("Comp")
				.GetValueAsync ().ContinueWith (task => {
				if (task.IsFaulted) {
					Debug.Log (task.Result.ToString ());
				} else if (task.IsCompleted) {
					DataSnapshot snapshot = task.Result;
					foreach (var childSnapshot in snapshot.Children) {
						//Default Values
						MainCompO CurrentCompIteration = new MainCompO ();
						JsonUtility.FromJsonOverwrite (childSnapshot.GetRawJsonValue (), CurrentCompIteration);
						if (CurrentCompIteration.CompOptions.ID == CompID) {
							if (AfterFunNo == 3)
								PlayersManagerParent.ParentInstance.SetCompDataAfterCompObjGetted (CurrentCompIteration);
							else
								CompManager.Instance.HanldeReturnedCompObj (CurrentCompIteration, AfterFunNo);
						}
					}
				}
			});
		} catch (Exception e) {
			Debug.Log ("Here IS AN Error Fuking Dick Hole:\t" + e.Message);
		}
	}

	public void AddMSGWallToComp (string MSGBody)
	{
		if (Player.Instance.CurrentCompID.Length < 2)
			return;
		reference.Child ("Comp").Child (Player.Instance.CurrentCompID).Child ("MSGWall").Child (RandomIDGenerator.Instance.GetRandomID ()).SetValueAsync (MSGBody);
	}

	public void RemoveThisPlayerFromComp (string PlayerID)
	{
		reference.Child ("Players").Child (PlayerID).Child ("CurrentCompID").SetValueAsync ("");
	}

	/// <summary>
	/// </summary>
	/// <param name="MSGBody">Chat Stuff.</param>
	public void SendMSGToThisFriend (MessageO MSGBody)
	{
		if (MSGBody.MessageBody.Length < 1)
			return;
		FirebaseDatabase.DefaultInstance
			.GetReference ("Players")
			.Child (CurrentFriendID)
			.Child ("MyFrinedListO")
			.GetValueAsync ().ContinueWith (task => {
			if (task.IsFaulted) {
				Debug.Log (task.Result.ToString ());
			} else if (task.IsCompleted) {
				try {
					DataSnapshot snapshot = task.Result;
					FriendListO ThisFriendListO = new FriendListO ();
					JsonUtility.FromJsonOverwrite (snapshot.GetRawJsonValue (), ThisFriendListO);
					int FriendNO = -1;
					foreach (FriendsO FriendItem in ThisFriendListO.MyFriends) {
						FriendNO++;
						if (FriendItem.FriendID == Player.Instance.DatabasePlayerID) {
							if (FriendItem.Blocked) {
								PopsMSG.Instance.ShowPopMSG ("This Player Blocked You");
								return;
							}
							MessagesO FriendMSGsListO = new MessagesO ();
							FriendMSGsListO = FriendItem.Messages;
							FriendMSGsListO.MSGs.Add (MSGBody);
							string FriendMSGsListOJson = JsonUtility.ToJson (FriendMSGsListO);
							reference.Child ("Players").Child (CurrentFriendID).
								Child ("MyFrinedListO").Child ("MyFriends").Child (FriendNO + "").Child ("Messages").SetRawJsonValueAsync (FriendMSGsListOJson);
							return;
						} else
							continue;
					}
					//						AddMSGToThisFriendMSGsList (FriendMessagesO, MSGBody);
				} catch (Exception e) {
					Debug.Log (e.Message);
				}
			}
		});
	}

	public static string CurrentFriendID;

	public void SetChatListnerForThisBitch (string FriendID)
	{
		Debug.Log ("CurrentFriendID\t"+CurrentFriendID);
		try {
			SetOrUnSetChatListnerOnThisGuy (Player.Instance.DatabasePlayerID, CurrentFriendID, false);
			SetOrUnSetChatListnerOnThisGuy (CurrentFriendID, Player.Instance.DatabasePlayerID, false);
		} catch {
		}
		CurrentFriendID = FriendID;
		SetOrUnSetChatListnerOnThisGuy (Player.Instance.DatabasePlayerID, FriendID, true);
		SetOrUnSetChatListnerOnThisGuy (FriendID, Player.Instance.DatabasePlayerID, true);
	}

	private string CurrentPlayerChatDatabaseID,CurrentChatFriendNO;

	private void SetOrUnSetChatListnerOnThisGuy (string PlayerDatabaseID, string FriendDatabaseID, bool SetOrUnSet)
	{
		FirebaseDatabase.DefaultInstance
			.GetReference ("Players")
			.Child (PlayerDatabaseID)
			.Child ("MyFrinedListO")
			.GetValueAsync ().ContinueWith (task => {
			if (task.IsFaulted) {
				Debug.Log (task.Result.ToString ());
			} else if (task.IsCompleted) {
				DataSnapshot snapshot = task.Result;
				FriendListO ThisFriendListO = new FriendListO ();
				JsonUtility.FromJsonOverwrite (snapshot.GetRawJsonValue (), ThisFriendListO);
				int FriendNO = -1;
				foreach (FriendsO FriendItem in ThisFriendListO.MyFriends) {
					FriendNO++;
					if (FriendItem.FriendID == FriendDatabaseID) {
							ChatListenerCounter = 0;
							CurrentChatFriendNO = FriendNO+"";
							CurrentPlayerChatDatabaseID = PlayerDatabaseID;
							if (SetOrUnSet){
//								Debug.Log("Set This:PlayerDatabaseID\t"+PlayerDatabaseID+"||"+"Set This:FriendNO\t"+FriendNO);
							reference.Child ("Players").Child (PlayerDatabaseID).Child ("MyFrinedListO").
								Child ("MyFriends").Child (FriendNO + "").Child ("Messages").ValueChanged += RecieveChat;
							}
							else{
								Debug.Log("PlayerDatabaseID+FriendNO");
							reference.Child ("Players").Child (PlayerDatabaseID).Child ("MyFrinedListO").
								Child ("MyFriends").Child (FriendNO + "").Child ("Messages").ValueChanged -= RecieveChat;	
							}
						return;
					} else
						continue;
				}
			}
		});
	}


	private int ChatListenerCounter=0;
	void RecieveChat (object sender, ValueChangedEventArgs e)
	{
		if (e.DatabaseError != null)
			return;
		MessagesO NewMessagesO = new MessagesO ();
		MessageO NewMSG = new MessageO ("", "");
		bool ItsTheMainScene = SceneManager.GetActiveScene ().buildIndex == 0;
		try {
			JsonUtility.FromJsonOverwrite (e.Snapshot.GetRawJsonValue (), NewMessagesO);
		} catch {
			return;
		}
		ChatListenerCounter += 1;
//		Debug.Log (ChatListenerCounter);
		if (ChatListenerCounter == 1) {
			ChatListenerCounter = 3;
			int IterationCounter = 0;
			for (int i = NewMessagesO.MSGs.Count - 10; IterationCounter < 10; i++) {
				IterationCounter+=1;
				try{
					NewMSG = new MessageO (NewMessagesO.MSGs [i].SenderID, NewMessagesO.MSGs [i].MessageBody);
					ChatManager.Instance.AddMSG (NewMSG.SenderID.ToString (), NewMSG.MessageBody, ItsTheMainScene);
				}catch{
				}
			}
		}
		else {
			try{
				NewMSG = new MessageO (NewMessagesO.MSGs [NewMessagesO.MSGs.Count - 1].SenderID, NewMessagesO.MSGs [NewMessagesO.MSGs.Count - 1].MessageBody);}
			catch{return;
			}
				ChatManager.Instance.AddMSG (NewMSG.SenderID.ToString (), NewMSG.MessageBody, ItsTheMainScene);
		}
	}

	public void StopChatListeting ()
	{
		ChatListenerCounter = 0;
		try{
		SetOrUnSetChatListnerOnThisGuy (Player.Instance.DatabasePlayerID, CurrentFriendID, false);
			SetOrUnSetChatListnerOnThisGuy (CurrentFriendID, Player.Instance.DatabasePlayerID, false);}catch{
		}
		try{
			reference.Child ("Players").Child (Player.Instance.DatabasePlayerID).Child ("MyFrinedListO").
			Child ("MyFriends").Child (CurrentChatFriendNO + "").Child ("Messages").ValueChanged -= RecieveChat;	
		}catch{
		}
		try{
			reference.Child ("Players").Child (CurrentPlayerChatDatabaseID).Child ("MyFrinedListO").ValueChanged -= RecieveChat;
		}catch{
		}
		try {
			reference.Child ("Players").Child (Player.Instance.DatabasePlayerID).Child ("MyFrinedListO").ValueChanged -= RecieveChat;

		} catch {
		}
		try {
			reference.Child ("Players").Child (CurrentFriendID).Child ("MyFrinedListO").ValueChanged -= RecieveChat;
		} catch {
		}
	}

	private void OnApplicationQuit ()
	{
		if (SceneManager.GetActiveScene ().buildIndex == 0)
			return;
		try {
			reference.Child ("Rooms").Child (MainFireBaseManager.GameType).Child (RoomsManager.PlayingRoomID).ValueChanged -= RoomUpdates;
		} catch {
		}
		#if UNITY_IOS
		PlayingBTNSManagerFather.ParentInstance.QuitNow ();
		return;
		#endif
	}

	private void OnApplicationPause (bool pauseStatus)
	{

//		return;
		if (SceneManager.GetActiveScene ().buildIndex == 0)
			return;
		try {
			if (pauseStatus)
				reference.Child ("Rooms").Child (MainFireBaseManager.GameType).Child (RoomsManager.PlayingRoomID).ValueChanged -= RoomUpdates;
			else {
				if (reference.Child ("Rooms").Child (MainFireBaseManager.GameType).Child (RoomsManager.PlayingRoomID).Key == RoomsManager.PlayingRoomID) {
					reference.Child ("Rooms").Child (MainFireBaseManager.GameType).Child (RoomsManager.PlayingRoomID).Child ("Update").SetValueAsync (0);
					reference.Child ("Rooms").Child (MainFireBaseManager.GameType).Child (RoomsManager.PlayingRoomID).ValueChanged += RoomUpdates;
				}
			}
		} catch (Exception e) {
			Debug.Log ("There is no room fukin bitch\t" + e.Message);
		}
	}


	private void OnDestroy ()
	{
		if (SceneManager.GetActiveScene ().buildIndex != 0)
			try {
				reference.Child ("Rooms").Child (MainFireBaseManager.GameType).Child (RoomsManager.PlayingRoomID).ValueChanged -= RoomUpdates;
			} catch {
			}
		try {
			reference.Child ("Players").Child (Player.Instance.DatabasePlayerID).ValueChanged -= OnlinePlayerUpates;
		} catch {
		}
		StopAllCoroutines ();
		try {
			StopChatListeting ();
		} catch {
		}
		try {
			reference.Child ("Players").Child (Player.Instance.DatabasePlayerID).ValueChanged -= OnlinePlayerUpates;
		} catch {
		}
	}
}