﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;
using System;

public abstract class PlayingWindowsManagerFather : MonoBehaviour {

	public static PlayingWindowsManagerFather ParentInstance;
	public GameObject[] ChatPopsGO,ChooseParternersGOs,PlayersScoresGO,TeamsScores,PlayersTimer,PlayerStuffGO,ShowPlayerDataBTN;
	public GameObject OnStartBTNGO,ChoosePartnerGO,WinsWindow,RoomMoreInfosContainerGO;
	public Sprite[] EmotionsIMGS;
	protected float TimerTikValue = 0.1f,SliderSmothnessMultiplier=100;
	public Text GameTitleTxt;

	public virtual void Awake(){
		ParentInstance = this;
	}

	void Start(){
		SetInitials ();
	}

	void SetInitials ()
	{
		GameTitleTxt.text = MainFireBaseManager.GameType;
		PlayingBTNSManagerFather.ParentInstance.OnStartBTNS.SetActive (!MainFireBaseManager.Instance.IsACompition);
		if (MainFireBaseManager.Instance.IsACompition) {
			PlayingBTNSManagerFather.ParentInstance.OnStartTxt.SetActive (true);
			if (LanguangeManagerFather.ParentInstance.IsArabic)
				PlayingBTNSManagerFather.ParentInstance.OnStartTxt.GetComponent<Text> ().text =
					ArabicFixer.Fix ("سبتدأ اللعبة\n مباشرة عند \nانضمام اللاعبين");
		}
	}

	//Partners Objects
	public virtual void ShowAndSetPartnersPhotos(string[] IMGSUrl){
		return;
	}

	public void SetPlayerStuff(){
		/*
		 * PlayerStuffGO:
		 * 0=>Color
		 * 1=>UpPatch
		 * 2=>ButtomPatch
		 * */
		PlayerStuffGO [0].GetComponent<Image> ().color = Player.Instance.PlayerRealColor;
		PlayerStuffGO [1].SetActive (Player.Instance.BashaUpPatch==1);
		PlayerStuffGO [2].SetActive (Player.Instance.BashaButtomPatch==1);
	}

	//Chat Stuff
	public void ShowChatPop(int PlayerNO,string MessageBody){
		StartCoroutine (ShowChatPopCor(PlayerNO,MessageBody));
	}

	private IEnumerator ShowChatPopCor(int PlayerNO,string MessageBody){
		ChatPopsGO [PlayerNO].SetActive (true);
		ChatPopsGO [PlayerNO].transform.GetChild (0).GetComponent<Text> ().text = "";
		ChatPopsGO [PlayerNO].transform.GetChild (1).GetComponent<Image> ().enabled = false;
		if (MessageBody.EndsWith ("!@#")) {
			ChatPopsGO [PlayerNO].transform.GetChild (1).GetComponent<Image> ().enabled = true;
			ChatPopsGO [PlayerNO].transform.GetChild (1).GetComponent<Image> ().sprite = EmotionsIMGS [int.Parse(GetChar (MessageBody, 0))];
		}
		else
			ChatPopsGO [PlayerNO].transform.GetChild (0).GetComponent<Text> ().text = MessageBody;
		yield return new WaitForSeconds (2);
		ChatPopsGO [PlayerNO].SetActive (false);
	}
	//Chat Stuff
	private IEnumerator Cor;
	//Timer Stuff
	public void SetTimer(int PlayerNO,float WaitingTime,int FunToDOAfterEnd,bool JustCreatorDoIT){
		Cor = StartThisTimer (PlayerNO, WaitingTime, FunToDOAfterEnd, JustCreatorDoIT);
		StartCoroutine (Cor);
	}

	protected virtual IEnumerator StartThisTimer(int PlayerNO,float WaitingTime,int FunToDOAfterEnd,bool JustCreatorDoIT){
		yield return null;
	}

	public virtual void StopTimer(){
		try{
		for (int i = 0; i < PlayersTimer.Length; i++)
				PlayersTimer [i].SetActive (false);}catch{
		}
		try{StopCoroutine (Cor);}catch{}
	}

	//Win Windows Stuff
	public virtual void ShowWindWindow(int WinnerTeamNO){
		int RealNO = WinnerTeamNO + 1;
		string WinsTxt = "";
		if(LanguangeManagerFather.ParentInstance.IsArabic)
			WinsTxt = ArabicFixer.Fix("الفائز "+RealNO+" فريق");
		else
			WinsTxt = "Team "+RealNO+" Wins";
		WinsWindow.transform.GetChild (1).GetComponent<Text> ().text = WinsTxt;
		WinsWindow.SetActive (true);
		WinsWindow.GetComponent<RectTransform> ().GetChild (4).gameObject.SetActive (!MainFireBaseManager.Instance.IsACompition);
		Player.Instance.SetGamePlayedBadges ();
		Player.Instance.SavePlayerData ();
	}

	protected string GetChar(string ThisString,int IntIndex){
		return ThisString.ToCharArray ().GetValue (IntIndex).ToString ();
	}

	private void OnDestroy(){
		StopAllCoroutines ();
	}
}