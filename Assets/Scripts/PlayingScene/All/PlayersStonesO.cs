using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PlayersStonesO
{
	public List<int> Stones;
	public PlayersStonesO(){
		Stones = new List<int> (4);
		for(int i=0;i<4;i++)
			Stones.Add (-1);
	}
}