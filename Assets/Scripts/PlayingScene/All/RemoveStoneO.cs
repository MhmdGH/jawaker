using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class RemoveStoneO
{
	public int OwnerID,StoneNO;
	public bool ThereIsStoneToRemove;
	public RemoveStoneO(){
		OwnerID = -1;
		StoneNO = -1;
		ThereIsStoneToRemove = false;
	}
}