using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class SafeZoneStoneO
{
	public List<bool> StoneIsInSafeZone;
	public SafeZoneStoneO(){
		StoneIsInSafeZone = new List<bool> (4);
		for(int i=0;i<4;i++)
			StoneIsInSafeZone.Add (false);
	}
}