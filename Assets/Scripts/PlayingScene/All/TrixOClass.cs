using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class TrixOClass
{
	public int TrixCounter;
	public int[] PlayersWinnersOrder;
	public string[] AvailableCardPlace;
	public TrixOClass(){
		TrixCounter = 0;
		AvailableCardPlace = new string[8]{"","","","","","","",""};
		PlayersWinnersOrder = new int[4]{-1,-1,-1,-1};
	}
}
