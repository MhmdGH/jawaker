﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Card1 : Card,IPointerDownHandler,IPointerUpHandler{
	#region IPointerUpHandler implementation

	public void OnPointerUp (PointerEventData eventData)
	{
		return;
	}

	#endregion

	#region IPointerDownHandler implementation
	public void OnPointerDown (PointerEventData eventData)
	{
		Debug.Log ("OnPointerDown");
		if (GetComponent<Image> ().color.r < 0.9f)
			return;
		if (CardIsUp) {
			OnEndDragFun ();
			return;
		}
		else {
			PlayersManager1.Instance.UnChooseAllOtherCards ();
			OnBeginDragFun ();
			StopAllCoroutines ();
			StartCoroutine (CardUp ());
		}
	}
	#endregion

	#region implemented abstract members of Card

	public override void ThroughIt (int StartPos)
	{
		GetComponent<Animator> ().SetInteger ("Thorugh",StartPos);
	}

	#endregion

	#region implemented abstract members of Card

	public override void SendCard ()
	{
		PlayersManager1.Instance.ThrowCard (this.gameObject);
	}

	#endregion
}