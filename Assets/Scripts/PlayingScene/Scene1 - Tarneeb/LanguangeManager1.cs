﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;
using System;

public class LanguangeManager1 : LanguangeManagerFather {

	public Text[] Game1Txts; 

	public override void Start(){
		base.Start ();
		if(IsArabic)
			SetTheFukinArabicLang ();
	}

	public override void SetTheFukinArabicLang ()
	{
		base.SetTheFukinArabicLang ();
		SetFontForThisTxts (Game1Txts);
		Game1Txts [0].text = ArabicFixer.Fix ("باص");
		Game1Txts [1].text = ArabicFixer.Fix ("اختر طرنيبك");
	}
}