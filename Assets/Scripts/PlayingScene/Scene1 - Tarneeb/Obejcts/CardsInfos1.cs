﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardsInfos1{
	
	public string[] CardTypes,CardNumpers;
	public List<string> HalfOfCards;
	public CardsInfos1(){
		CardTypes = new string[4]{ "D","H","S","C"};
		CardNumpers = new string[]{"02","03","04","05","06","07","08","09","10","11","12","13","14"};
		HalfOfCards = new List<string> ();
		for (int i = 0; i < 13; i++) {
			HalfOfCards.Add (CardTypes[0]+""+CardNumpers[i]);
			HalfOfCards.Add (CardTypes[1]+""+CardNumpers[i]);
			HalfOfCards.Add (CardTypes[2]+""+CardNumpers[i]);
			HalfOfCards.Add (CardTypes[3]+""+CardNumpers[i]);
		}
	}
}