﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Game1:GameOBParent{
	public Game1(){
		PlayersBids = new string[4]{"0","0","0","0"};
		PlayersScores = new int[2]{0,0};
		Cards = new PlayersCardsOB1 ();
		CurrentRound = new Round1 ();
	}
}