﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PlayersCardsOB1{

	public List<CardList> PlayersCards;
	public PlayersCardsOB1(){
		PlayersCards = new List<CardList> ();
		for (int i = 0; i < 5; i++) 
			PlayersCards.Add (new CardList ());
	}
}