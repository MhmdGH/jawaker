﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Round1:RoundParent{
	public Round1(){
		PlayersCards = new string[4]{"0","0","0","0"};
		PlayersScores = new int[4]{0,0,0,0};
	}
}