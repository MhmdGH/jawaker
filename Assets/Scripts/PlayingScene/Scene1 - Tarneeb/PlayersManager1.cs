﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class PlayersManager1 : PlayersManagerParent
{
	public static PlayersManager1 Instance;
	public GameObject CardGO;

	public override void Awake ()
	{
		base.Awake ();
		Instance = this;
	}

	public override void Start ()
	{
		TempCurrenRoom = new JsonRoomO ("Default", new string[4]{ "0", "0", "0", "0"},new Game1());
		base.Start ();
		CardsListGO = PlayersCardsGO [0].transform.GetChild (1).GetChild (0).gameObject;
		SceneDoneLoading = false;
	}

	//Main Reciever Fun
	public override void UpdateRoom (JsonRoomO CurrentRoomJSON)
	{
		base.UpdateRoom (CurrentRoomJSON);
		if (TempCurrenRoom.NoMaster)
			AssignNewMasterOrRemoveRoom ();
		if (TempCurrenRoom.Update == 0) {
			PlayingFireBaseManager1.Instance.SetOnDissconnectHandle ();
			return;
		}
		if (TempCurrenRoom.KickThisPlayerID.Length > 2)
			return;
		PlayingWindowsManager1.Instance.StopTimer ();
		switch (TempCurrenRoom.ActionID) {
		case -1:
			UpdateRoomPhaseMinus1();
			break;
		case 0:
			UpdateRoomPhase0 ();
			break;
		case 2:
			UpdateRoomPhase2 ();
			break;
		case 3:
			UpdateRoomPhase3 ();
			break;
		case 4:
			UpdateRoomPhase4 ();
			break;
		case 5:
			UpdateRoomPhase5 ();
			break;
		case 6:
			UpdateRoomPhase6 ();
			break;
		case 7:
			UpdateRoomPhase7 ();
			break;
		case 8:
			UpdateRoomPhase8 ();
			break;
		case 9:
			UpdateRoomPhase9 ();
			break;
		case 10:
			UpdateRoomPhase10 ();
			break;
		case 12:
			UpdateRoomPhase12 ();
			break;
		case 13:
			UpdateRoomPhase13 ();
			break;
		case 14:
			UpdateRoomPhase14 ();
			break;
		case 15:
			UpdateRoomPhase15 ();
			break;
		}
	}

	void UpdateRoomPhase0 ()
	{
		if (TempCurrenRoom.ReloadScene)
			ReloadScene ();
	}

	public void MasterTakeControl (int funToDOAfterEnd)
	{
		if (funToDOAfterEnd == 1)
			SetBid ();
		else if (funToDOAfterEnd == 2) {
			string[] TrumpsTypes = new string[4]{ "C", "H", "D", "S" };
			PlayingBTNSManager1.Instance.ChooseTrump (TrumpsTypes [UnityEngine.Random.Range (0, TrumpsTypes.Length)]);
		} else if (funToDOAfterEnd == 3)
			MasterOrPlayerThrowCardNOW ();
	}

	void MasterOrPlayerThrowCardNOW ()
	{
		//If Player Is Here
		int PlayerSeatStatus = TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay];
		//If Player Is Here
		if (MyTurnToPlay () && PlayerSeatStatus == 1) {
			if (PlayersManagerParent.PlayerHoldingCard) {
				GameObject CardHolded = CanvasGO.GetComponent<RectTransform> ().GetChild (CanvasGO.GetComponent<RectTransform> ().childCount - 1).gameObject;
				CardHolded.GetComponent<Card1> ().ReturnCardBack ();
				CardHolded.GetComponent<Card1> ().SendCard ();
				CheckPlayerKick ();
				return;
			} else {
				IfPlayerHoldingCardGetItBack ();
				List<GameObject> AvailbeCardsToThrow = new List<GameObject> ();
				for (int i = 0; i < CardsListGO.transform.childCount; i++)
					if (CardsListGO.transform.GetChild (i).GetComponent<Image> ().color.r == 1)
						AvailbeCardsToThrow.Add (CardsListGO.transform.GetChild (i).gameObject);
				AvailbeCardsToThrow[GetBestCardFromThisList(AvailbeCardsToThrow)].GetComponent<Card> ().SendCard ();
				CheckPlayerKick ();
				return;
			}
			//If Player Not Here, Master Do IT
		} else if (Player.Instance.GameCreator && PlayerSeatStatus == 0) {
			TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay] = 0;
			ThrowCardNow (FirstAvailabeCardOnOtherPlayer ());
		}
	}

	public void UnChooseAllOtherCards(){
		for (int i = 0; i < TarneebManager1.Instance.MyPlayerCardsGO.Length; i++)
			try{TarneebManager1.Instance.MyPlayerCardsGO[i].GetComponent<Card1> ().OnEndDragFun (true);}catch{
			}
//		try{TarneebManager1.Instance.SetAndArrangeMyCards ();}catch{
//		}
	}

	int GetBestCardFromThisList (List<GameObject> availbeCardsToThrow)
	{
		List<string> availbeCardsToThrowIDsStrings = new List<string> ();
		for (int i = 0; i < availbeCardsToThrow.Count; i++)
			availbeCardsToThrowIDsStrings.Add (availbeCardsToThrow[i].GetComponent<Card>().CardID);
		//ThisIsThePlayerWhoStartsTheRound
		if (ThisIsThePlayerWhoStartsTheRound ())
			return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,true);
		//Not the one who starts the round, tow main conditions

		//1.If i have the played card type
		if(IHaveThisCardType(PlayedType(),availbeCardsToThrowIDsStrings)){
		//A.If there is a high played card,I play small one
			if(GetCardNO(HighestThrowdCard (PlayedType()))>=11)
				return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,false);
			else //B.If there is not a high played card,I play high one
				return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,true);
		}else{ //2.If i havent the played card type
		//A.If i have tarneeb card and my partner not played high card
			if (IHaveThisCardType (TarneebType (), availbeCardsToThrowIDsStrings)) {
				if (PartnerThrowdCard()!="0" && GetCardNO(PartnerThrowdCard())<=11)
					return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,true,TarneebType(),false);
				else if(PartnerThrowdCard()=="0") //A.2.If i have tarneeb card and my partner not played yet
					return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,false,TarneebType(),false);
				else
					return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,false);
			}else{
				//B.If i havent tarneeb card, i play smallest card i have
				return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,false);
			}
		}
	}

	string FirstAvailabeCardOnOtherPlayer ()
	{
		List<string> ThisPlayerCardsList = new List<string> ();
		ThisPlayerCardsList = TempCurrenRoom.GameOB.
			Cards.PlayersCards [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay].PlayerCardsList;
		//ThisIsThePlayerWhoStartsTheRound
		if (ThisIsThePlayerWhoStartsTheRound())
			return ThisPlayerCardsList[GetHighestOrSmallestCardIFromThisList (ThisPlayerCardsList,true)];

		List<string> availbeCardsToThrowIDsStrings = new List<string> ();
		if (ThisPlayerHaveThisCardType (ThisPlayerCardsList, PlayedType ())) {
			for (int i = 0; i < ThisPlayerCardsList.Count; i++) {
				if (ThisPlayerCardsList [i].StartsWith (PlayedType ()))
					availbeCardsToThrowIDsStrings.Add (ThisPlayerCardsList [i]);
				else
					availbeCardsToThrowIDsStrings.Add ("0");
			}
		} else
			availbeCardsToThrowIDsStrings = ThisPlayerCardsList;
		

		//1.If i have the played card type
		if(ThisPlayerHaveThisCardType(availbeCardsToThrowIDsStrings,PlayedType())){
			//A.If there is a high played card,I play small one
			if(GetCardNO(HighestThrowdCard (PlayedType()))>=11)
				return availbeCardsToThrowIDsStrings [GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings, false)];
			else //B.If there is not a high played card,I play high one
				return availbeCardsToThrowIDsStrings[GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,true)];
		}else{//2.If i havent the played card type
			//A.1.If i have tarneeb card and my partner not played high card
			if (ThisPlayerHaveThisCardType (availbeCardsToThrowIDsStrings,TarneebType ())) {
				if (PartnerThrowdCard()!="0" && GetCardNO(PartnerThrowdCard())<11)
					return availbeCardsToThrowIDsStrings[GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,true,TarneebType(),false)];
				else if(PartnerThrowdCard()=="0") //A.2.If i have tarneeb card and my partner not played yet
					return availbeCardsToThrowIDsStrings[GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,false,TarneebType(),false)];
				else
					return availbeCardsToThrowIDsStrings[GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,false)];
			}else{
				//B.If i havent tarneeb card, i play smallest card i have
				return availbeCardsToThrowIDsStrings[GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,false)];
			}
		}
	}

	int PlayerIDTurn (){
		return TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
	}

	string PartnerThrowdCard(){
		return TempCurrenRoom.GameOB.CurrentRound.PlayersCards[MyPartnerID(PlayerIDTurn())];
	}

	string HighestThrowdCard(string CardType){
		List<string> ThisPlayersThrowdCardsList = GetPlayersThrowdCardsList ();
		ThisPlayersThrowdCardsList.Sort ();
		for (int i = 3; i >= 0; i--) {
			if (ThisPlayersThrowdCardsList [i].Length < 2)
				continue;
			if (GetCardType (ThisPlayersThrowdCardsList [i]) == CardType)
				return ThisPlayersThrowdCardsList [i];
		}
		for (int i = 0; i < 4; i++) {
			if (ThisPlayersThrowdCardsList [i].Length < 2)
				continue;
			return ThisPlayersThrowdCardsList [i];
		}
		return ThisPlayersThrowdCardsList [3];
	}

	private string PlayedType(){
		return GetChar (TempCurrenRoom.GameOB.CurrentRound.
			PlayersCards [TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound],0);
	}

	private string TarneebType(){
		return TempCurrenRoom.GameOB.TrumpType;
	}

	private bool HaveTarneebCard(List<GameObject> availbeCardsToThrow){
		List<string> availbeCardsToThrowIDsStrings = new List<string> ();
		for (int i = 0; i < availbeCardsToThrow.Count; i++)
			availbeCardsToThrowIDsStrings.Add (availbeCardsToThrow[i].GetComponent<Card>().CardID);
		for (int i = 0; i < availbeCardsToThrowIDsStrings.Count; i++)
			if (GetChar (availbeCardsToThrowIDsStrings [i], 0) == TempCurrenRoom.GameOB.TrumpCard)
				return true;
		return false;
	}


	//Creator Fun
	void SetBid ()
	{
		PlayingWindowsManager1.Instance.BidWindow.SetActive (false);
		int PlayerSeatStatus = TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.PlayerBidTurn];
		if (IsThisMyBidTurn()&& PlayerSeatStatus==1)
			SetBidNOW ();
		else if(Player.Instance.GameCreator&&PlayerSeatStatus==0)
			SetBidNOW ();
	}

	private void SetBidNOW(){
		for (int i = 0; i < 4; i++) {
			if (int.Parse (TempCurrenRoom.GameOB.PlayersBids [i].ToString ()) == 0) {
				PlayerSetBid ("-1", TempCurrenRoom.GameOB.PlayerBidTurn);
				return;
			}
			if (int.Parse (TempCurrenRoom.GameOB.PlayersBids [i].ToString ()) >= 7) {
				PlayerSetBid ("-1", TempCurrenRoom.GameOB.PlayerBidTurn);
				return;
			}
		}
		PlayerSetBid ("7", TempCurrenRoom.GameOB.PlayerBidTurn);
	}

	public void RestartGame ()
	{
		PlayingWindowsManager1.Instance.WinsWindow.SetActive (false);
		Game1 NewGameOB = new Game1 ();
		NewGameOB.PlayersScores [0] = 0;
		NewGameOB.PlayersScores [1] = 0;
		TempCurrenRoom.GameOB = NewGameOB;
		TempCurrenRoom.ReloadScene = true;
		TempCurrenRoom.ActionID = 0;
		PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase15 ()
	{
		//One Of The teams wins//Game End
		AddWinsPointsForPlayers ();
		if (MainFireBaseManager.Instance.IsACompition)
			InnerSetCompData ();
		PlayingWindowsManager1.Instance.ShowWindWindow (WinnerNO);
	}

	private void InnerSetCompData ()
	{
		if (Player.Instance.GameCreator) {
			for (int i = 0; i < TempCurrenRoom.DatabasePlayersIDs.Length; i++)
				if (TempCurrenRoom.DatabasePlayersIDs [i].Length < 4)
					return;
			if (WinnerNO == 0) {
				SetCompData (TempCurrenRoom.DatabasePlayersIDs [0], TempCurrenRoom.DatabasePlayersIDs [2],
					TempCurrenRoom.PlayersFBIDs [0], TempCurrenRoom.PlayersFBIDs [2]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [1]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [3]);
			} else {
				SetCompData (TempCurrenRoom.DatabasePlayersIDs [1], TempCurrenRoom.DatabasePlayersIDs [3],
					TempCurrenRoom.PlayersFBIDs [1], TempCurrenRoom.PlayersFBIDs [3]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [0]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [2]);
			}
		}
	}

	private void AddWinsPointsForPlayers(){
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (WinnerNO == 0 && (PlayerID == 0 || PlayerID == 2)) {
			Player.Instance.AddXPsAndCLubPoints (XPsValue);
			Player.Instance.Coins += ScoreValue;
		}else if (WinnerNO == 1 && (PlayerID == 1 || PlayerID == 3)) {
			Player.Instance.AddXPsAndCLubPoints (XPsValue);
			Player.Instance.Coins += ScoreValue;
		}
	}

	void UpdateRoomPhase14 ()
	{
		if (!Player.Instance.GameCreator)
			return;
		if (OneOfTeamsWins ()) {
			TempCurrenRoom.ActionID = 15;
			PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
		} else {
			Game1 NewGameOB = new Game1 ();
			NewGameOB.PlayersScores [0] = TempCurrenRoom.GameOB.PlayersScores [0];
			NewGameOB.PlayersScores [1] = TempCurrenRoom.GameOB.PlayersScores [1];
			TempCurrenRoom.GameOB = NewGameOB;
			TempCurrenRoom.ReloadScene = true;
			TempCurrenRoom.ActionID = 0;
			PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
		}
	}

	int WinnerNO;
	bool OneOfTeamsWins ()
	{
		int FinalScore = TempCurrenRoom.RoomOptions.FinalScore;
		if (TempCurrenRoom.GameOB.PlayersScores [0] >= FinalScore || TempCurrenRoom.GameOB.PlayersScores [1] >= FinalScore) {
			if (TempCurrenRoom.GameOB.PlayersScores [0] >= FinalScore)
				WinnerNO = 0;
			else
				WinnerNO = 1;
			return true;
		} else
			return false;
	}

	void UpdateRoomPhase13 ()
	{
		//Full Round Ends
		for (int i = 0; i < 4; i++)
			PlayingWindowsManager1.Instance.PlayersScoresGO [i].GetComponent<Text> ().text = "0";
		int Team1Score = TempCurrenRoom.GameOB.CurrentRound.PlayersScores [0] + TempCurrenRoom.GameOB.CurrentRound.PlayersScores [2];
		int Team2Score = TempCurrenRoom.GameOB.CurrentRound.PlayersScores [1] + TempCurrenRoom.GameOB.CurrentRound.PlayersScores [3];
		int PlayerTrump = TempCurrenRoom.GameOB.TrumpPlayerID;
		int HighestBid = TempCurrenRoom.GameOB.HighestBid;
		if (PlayerTrump == 0 || PlayerTrump == 2) {
			if (Team1Score >= HighestBid) {//Win
				if (Team1Score == 13) {
					if (HighestBid == 13)
						Team1Score = 26;
					else
						Team1Score = 16;
				}
				TempCurrenRoom.GameOB.PlayersScores [0] += Team1Score;
			}
			else {//Lose

				if (HighestBid == 13)
					HighestBid = 16;
				TempCurrenRoom.GameOB.PlayersScores [0] -= HighestBid;
				TempCurrenRoom.GameOB.PlayersScores [1] += Team2Score;
			}
		} else {
			if (Team2Score >= HighestBid) {

				if (Team1Score == 13) {
					if (HighestBid == 13)
						Team1Score = 26;
					else
						Team1Score = 16;
				}

				TempCurrenRoom.GameOB.PlayersScores [1] += Team2Score;
			}
			else {

				if (HighestBid == 13)
					HighestBid = 16;

				TempCurrenRoom.GameOB.PlayersScores [1] -= HighestBid;
				TempCurrenRoom.GameOB.PlayersScores [0] += Team1Score;
			}
		}
		SetPlayersScores ();
		if (Player.Instance.GameCreator) {
			TempCurrenRoom.ActionID = 14;
			PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
		}
	}

	private int ThisTeamCurrentScore(int TeamNO){
		if(TeamNO==0)
			return TempCurrenRoom.GameOB.CurrentRound.PlayersScores [0] + TempCurrenRoom.GameOB.CurrentRound.PlayersScores [2];
		else
			return TempCurrenRoom.GameOB.CurrentRound.PlayersScores [1] + TempCurrenRoom.GameOB.CurrentRound.PlayersScores [3];
	}

	void UpdateRoomPhase12 ()
	{
		//COllect Cards Here And Throw it to winner Player
		StartCoroutine (CollectionCor ());
	}

	private IEnumerator CollectionCor ()
	{
		ReturnsMyCardsAvailibity ();
		SetAvailableCardsOnce = false;
		int LastWinnerID = TempCurrenRoom.GameOB.CollectorID;
		yield return new WaitForSeconds (1);
		CardsThrowerMangaer.Instance.CollectCardsToThisPlayer (FromOnlineToLocal (LastWinnerID));
		PlayingWindowsManager1.Instance.PlayersScoresGO [FromOnlineToLocal (LastWinnerID)].GetComponent<Text> ().text =
			TempCurrenRoom.GameOB.CurrentRound.PlayersScores [LastWinnerID].ToString ();
		yield return new WaitForSeconds (1);
		if (Player.Instance.GameCreator) {
			SetPlayersGreenOrRed ();
			if (FullRoundEnds ()) {
				TempCurrenRoom.ActionID = 13;
				PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
			} else {
				TempCurrenRoom.ActionID = 10;
				TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound = LastWinnerID;
				TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = LastWinnerID;
				PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
			}
		}
	}

	void SetPlayersGreenOrRed ()
	{
		if (!NotGreendOrRed ())
			return;

		//I am highest bid and win with my bid
		if (TempCurrenRoom.GameOB.TrumpPlayerID == 0 || TempCurrenRoom.GameOB.TrumpPlayerID == 2) {
			if (ThisTeamCurrentScore (0) >= TempCurrenRoom.GameOB.HighestBid) {
				TempCurrenRoom.GreenOrRed [0] = 1;
				TempCurrenRoom.GreenOrRed [2] = 1;return;
			}
		}else if (TempCurrenRoom.GameOB.TrumpPlayerID == 1 || TempCurrenRoom.GameOB.TrumpPlayerID == 3) {
			if (ThisTeamCurrentScore (1) >=TempCurrenRoom.GameOB.HighestBid) {
				TempCurrenRoom.GreenOrRed [1] = 1;
				TempCurrenRoom.GreenOrRed [3] = 1;return;
			}
		}
		//I am highest Bid And lose with my bid
		if (TempCurrenRoom.GameOB.TrumpPlayerID == 0 || TempCurrenRoom.GameOB.TrumpPlayerID == 2) {
			if (ThisTeamCurrentScore (1) > (13 - TempCurrenRoom.GameOB.HighestBid)) {
				TempCurrenRoom.GreenOrRed [0] = 2;
				TempCurrenRoom.GreenOrRed [2] = 2;
				TempCurrenRoom.GreenOrRed [1] = 1;
				TempCurrenRoom.GreenOrRed [3] = 1;return;
			}
		}else if (TempCurrenRoom.GameOB.TrumpPlayerID == 1 || TempCurrenRoom.GameOB.TrumpPlayerID == 3) {
			if (ThisTeamCurrentScore (0) > (13 - TempCurrenRoom.GameOB.HighestBid)) {
				TempCurrenRoom.GreenOrRed [1] = 2;
				TempCurrenRoom.GreenOrRed [3] = 2;
				TempCurrenRoom.GreenOrRed [0] = 1;
				TempCurrenRoom.GreenOrRed [2] = 1;return;
			}
		}
	}

	private bool NotGreendOrRed(){
		for (int i = 0; i < TempCurrenRoom.GreenOrRed.Length; i++)
			if (TempCurrenRoom.GreenOrRed [i] != 0)
				return false;
		return true;
	}

	#region implemented abstract members of PlayersManagerParent

	public override void SetLastRoundCards ()
	{
		string[] LastRoundCards = new string[4];
		for (int i = 0; i < 4; i++)
			LastRoundCards [i] = TempCurrenRoom.GameOB.CurrentRound.PlayersCards [FromOnlineToLocal(i)];
		LastRoundGO.GetComponent<LastRound> ().SetCards (LastRoundCards);
	}

	#endregion

	bool FullRoundEnds ()
	{
		if (TempCurrenRoom.GameOB.CurrentRound.RoundNO >= 13)
			return true;
		else
			return false;
	}

	private void UpdateRoomPhase11 ()
	{
		//Just a fukin Holder
	}

	private void UpdateRoomPhase10 ()
	{
		//GameStart
		PlayingWindowsManager1.Instance.SetTimer (FromOnlineToLocal (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay),
			TimerWaitingTime(TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay), 3,false);
		SetAvailableCards ();
		int PrevPlayerID = PreviosTurn (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay);
		if (MyTurnToPlay ()) {
			if (IsThisFuckerTrulyTurnToPlay()) {
				StartCoroutine (UpdateRoomPhase10Cor());
			} else {
				IfPlayerHoldingCardGetItBack ();
				TarneebManager1.Instance.SetAndArrangeMyCards ();
			}
		}
		if (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [PrevPlayerID].Length < 2)
			return;
			ThrowThisCard (PrevPlayerID, TempCurrenRoom.GameOB.CurrentRound.PlayersCards [PrevPlayerID]);
	}

	private IEnumerator UpdateRoomPhase10Cor(){
		yield return new WaitForSeconds (1);
		try{GameObject CardHolded = CanvasGO.GetComponent<RectTransform> ().GetChild (CanvasGO.GetComponent<RectTransform> ().childCount - 1).gameObject;
		CardHolded.GetComponent<Card1> ().ReturnCardBack ();
			CardHolded.GetComponent<Card1> ().SendCard ();}catch{
		}
	}



	private bool SetAvailableCardsOnce;
	void SetAvailableCards ()
	{
		if (SetAvailableCardsOnce)
			return;
		//Watcher
		if (Player.Instance.OnlinePlayerID == -1)
			return;
		List<string> PlayedCards = new List<string> ();
		IfPlayerHoldingCardGetItBack();
		int Counter = 0;
		int PlayedPlayerID = 0;
		for (int i = 0; i < 4; i++)
			PlayedCards.Add (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i]);
		for (int i = 0; i < 4; i++) {
			if (PlayedCards [i].Length > 1) {
				Counter++;
				PlayedPlayerID = i;
			}
		}
		if (Counter != 1 || PreviosTurn (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay) == Player.Instance.OnlinePlayerID)
			return;
		string CardType = GetChar (PlayedCards [PlayedPlayerID], 0);
		if (IHaveThisCardType (CardType)) {
			for (int i = 0; i < CardsListGO.transform.childCount; i++) {
				if (!CardsListGO.transform.GetChild (i).GetComponent<Card1> ().CardID.StartsWith (CardType)) {
					CardAvailability (CardsListGO.transform.GetChild (i).gameObject, false);
					SetAvailableCardsOnce = true;
				}
			}
		}
	}

	bool IHaveThisCardType (string Cardtype)
	{
		for (int i = 0; i < CardsListGO.transform.childCount; i++) {
			if (CardsListGO.transform.GetChild (i).GetComponent<Card1> ().CardID.StartsWith (Cardtype))
				return true;
		}
		return false;
	}

	bool IHaveThisCardType (string Cardtype,List<string> ThisCardsList)
	{
		for (int i = 0; i < ThisCardsList.Count; i++) {
			if (ThisCardsList[i].StartsWith(Cardtype))
				return true;
		}
		return false;
	}

	void ThrowThisCard (int PlayerThrowerID, string ThrowdCardID)
	{
		int PlayerIDOnMyDevice = FromOnlineToLocal (PlayerThrowerID);
		CardsThrowerMangaer.Instance.ThrowThisCard (ThrowdCardID,PlayerIDOnMyDevice);
		if (PlayerIDOnMyDevice != 0 || Player.Instance.OnlinePlayerID==-1)
			PlayersCardsGO [PlayerIDOnMyDevice].transform.GetChild (1).GetChild (0).
			GetChild (TempCurrenRoom.GameOB.CurrentRound.RoundNO).gameObject.SetActive (false);
		if (RoundEnds ()) {
			SetLastRoundCards ();
			if (Player.Instance.GameCreator)
				CloseRound ();
		}
	}

	public void ThrowCard (GameObject Cardjo)
	{
		if (MyTurnToPlay () && TempCurrenRoom.ActionID == 10 && CanThrowCard)
			StartCoroutine (RemoveOrderly (Cardjo));
		else
			Cardjo.GetComponent<Card> ().ReturnCardBack ();
	}

	private bool CanThrowCard = true;

	private IEnumerator RemoveOrderly (GameObject Cardjo)
	{
		CanThrowCard = false;
		ThrowCardNow (Cardjo.GetComponent<Card> ().CardID);
		Destroy (Cardjo);
		yield return new WaitForSeconds (0.03f);try{
			TarneebManager1.Instance.SetAndArrangeMyCards ();}catch{
		}
		yield return new WaitForSeconds (2f);
		CanThrowCard = true;
		yield return null;
	}

	void ThrowCardNow (string CardID)
	{
//		Debug.Log ("CardID\t"+CardID);
		string CardIDString = CardID;
		int PlayerTurn = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		TempCurrenRoom.GameOB.CurrentRound.PlayersCards [PlayerTurn] = CardIDString;
		RemoveThisCardFromOnlineCardsList (PlayerTurn,CardIDString);
		int NextPlayerID = PlayerTurn;
		NextPlayerID++;
		if (NextPlayerID >= 4)
			NextPlayerID = 0;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = NextPlayerID;
		PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private void RemoveThisCardFromOnlineCardsList(int PlayerNO,string CardID){
		for (int i = 0; i < TempCurrenRoom.GameOB.Cards.PlayersCards[PlayerNO].PlayerCardsList.Count; i++) {
			if (TempCurrenRoom.GameOB.Cards.PlayersCards[PlayerNO].PlayerCardsList[i] == CardID) {
				TempCurrenRoom.GameOB.Cards.PlayersCards[PlayerNO].PlayerCardsList[i] = "0";
				return;
			}
		}
	}

	private bool RoundEnds ()
	{
		int Over1Counter = 0;
		for (int i = 0; i < 4; i++) {
			if (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i].Length > 1)
				Over1Counter++;
		}
		if (Over1Counter == 4)
			return true;
		else
			return false;
	}

	private List<string> PlayersCardsList;

	void CloseRound ()
	{
		int LastWinnderID = GetWinnerID ();
		TempCurrenRoom.GameOB.CurrentRound.PlayersScores [LastWinnderID]++;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = LastWinnderID;
		TempCurrenRoom.GameOB.CollectorID = LastWinnderID;
		TempCurrenRoom.GameOB.CurrentRound.RoundNO++;
		for (int i = 0; i < 4; i++)
			TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i] = "0";
		TempCurrenRoom.ActionID = 12;
		PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private int GetWinnerID ()
	{
		TempCurrenRoom.ActionID = 11;
		PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
		PlayersCardsList = new List<string> ();
		for (int i = 0; i < 4; i++)
			PlayersCardsList.Add (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i]);
		if (ThereIsTrumpCard ())
			return GetHighestFromThisType (TempCurrenRoom.GameOB.TrumpType);
		else
			return GetHighestFromThisType (GetChar (PlayersCardsList [TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound], 0));
	}

	private List<string> GetPlayersThrowdCardsList(){
		List<string> NewPlayersCardsList = new List<string> ();
		for (int i = 0; i < 4; i++)
			NewPlayersCardsList.Add (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i]);
		return NewPlayersCardsList;
	}

	private int GetHighestFromThisType (string CardType)
	{
		List<string> NewCardsList = new List<string> ();
		for (int i = 0; i < 4; i++)
			if (PlayersCardsList [i].StartsWith (CardType))
				NewCardsList.Add (PlayersCardsList [i]);
		//After List Ready
		NewCardsList.Sort ();
		for (int i = 0; i < 4; i++) {
			if (NewCardsList [NewCardsList.Count - 1] == PlayersCardsList [i])
				return i;
		}
		return 0;
	}


	private bool ThereIsTrumpCard ()
	{
		for (int i = 0; i < 4; i++)
			if (PlayersCardsList [i].StartsWith (TempCurrenRoom.GameOB.TrumpType))
				return true;
		return false;
	}


	void UpdateRoomPhase9 ()
	{
		AudioManager.Instance.PlaySound (1);
		AudioManager.Instance.PlaySound (TrumpTypeSound(TempCurrenRoom.GameOB.TrumpType));
		PlayingWindowsManager1.Instance.ChooseTrumpWindow.SetActive (false);
		PlayingWindowsManager1.Instance.SetTrumpAndBidder (TempCurrenRoom.GameOB.TrumpType, TempCurrenRoom.GameOB.HighestBid);
		TempCurrenRoom.ActionID = 10;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = TempCurrenRoom.GameOB.TrumpPlayerID;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound = TempCurrenRoom.GameOB.TrumpPlayerID;
		if (Player.Instance.GameCreator)
			PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase8 ()
	{
		//Here Trump Player Chooses TrumpType
		PlayingWindowsManager1.Instance.BidWindow.SetActive(false);
		PlayingWindowsManager1.Instance.SetTimer (FromOnlineToLocal (TempCurrenRoom.GameOB.TrumpPlayerID),
			TimerWaitingTime(TempCurrenRoom.GameOB.TrumpPlayerID), 2,true);
		if (Player.Instance.OnlinePlayerID == TempCurrenRoom.GameOB.TrumpPlayerID)
			PlayingWindowsManager1.Instance.ChooseTrumpWindow.SetActive (true);
	}

	public void PlayerChooseTrump (string TrumpType)
	{
		TempCurrenRoom.GameOB.TrumpType = TrumpType;
		TempCurrenRoom.ActionID = 9;
		PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private void UpdateRoomPhase7 ()
	{
		PlayingWindowsManager1.Instance.RefreshBidWindow ();
		AudioManager.Instance.PlaySound (HighestBid()+2);
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.GameOB.HighestBid = HighestBid ();
		TempCurrenRoom.GameOB.TrumpPlayerID = GetHighestBidderID ();
		TempCurrenRoom.ActionID = 8;
		PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private int GetHighestBidderID ()
	{
		if (CheckBidsEndsOrAllPass (4, false))
			return 0;
		int HigherBid = 0;
		int HigherBidderID = 0;
		for (int i = 0; i < 4; i++) {
			int CurrentBid = int.Parse (TempCurrenRoom.GameOB.PlayersBids [i]);
			if (CurrentBid > HigherBid) {
				HigherBid = CurrentBid;
				HigherBidderID = i;
			}
		}
		return HigherBidderID;
	}

	//Bidding Phase
	void UpdateRoomPhase6 ()
	{
		int Turn = TempCurrenRoom.GameOB.PlayerBidTurn;
		int LastBid = int.Parse (TempCurrenRoom.GameOB.PlayersBids [PreviosTurn (Turn)].ToString ());
		if (LastBid == -1)
			PlayingWindowsManager1.Instance.ShowBidPop (FromOnlineToLocal (PreviosTurn (Turn)), "Pass");
		else if (LastBid != 0)
			PlayingWindowsManager1.Instance.ShowBidPop (FromOnlineToLocal (PreviosTurn (Turn)), LastBid.ToString ());
		if (Player.Instance.GameCreator && NOOFOBids () == 0 && CheckBidsEndsOrAllPass (4, false)) {
			TempCurrenRoom.GameOB.PlayersBids [Player.Instance.OnlinePlayerID] = "7";
			TempCurrenRoom.ActionID = 7;
			PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom,true);
			return;
		}
		if (Player.Instance.GameCreator && NOOFOBids () == 1 && CheckBidsEndsOrAllPass (2, false)) {
			TempCurrenRoom.ActionID = 7;
			PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom,true);
			return;
		}
		CallBidSounds (LastBid);
		PlayingWindowsManager1.Instance.SetTimer (FromOnlineToLocal (TempCurrenRoom.GameOB.PlayerBidTurn),
			TimerWaitingTime(TempCurrenRoom.GameOB.PlayerBidTurn), 1,false);
		if (CheckBidsEndsOrAllPass (4, false))
			return;
		if (Turn != Player.Instance.OnlinePlayerID)
			return;
		if (AllOthersIsPass()) {
			PlayingWindowsManager1.Instance.ShowBidWindow (true);
		} else {
			PlayingWindowsManager1.Instance.ShowBidWindow (false);
		}
	}

	void CallBidSounds (int LastBidNo)
	{
		if (LastBidNo == 0)
			return;
		else if (LastBidNo == -1)
			AudioManager.Instance.PlaySound (16);
		else
			AudioManager.Instance.PlaySound (LastBidNo+2);
	}

	private bool AllOthersIsPass(){
		int PassCounter=0;
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.PlayersBids [i] == "-1")
				PassCounter++;
		if (PassCounter == 3)
			return true;
		else
			return false;
	}

	private int NOOFOBids(){
		int BidsCounter = 0;
		for (int i = 0; i < 4; i++)
			if (int.Parse(TempCurrenRoom.GameOB.PlayersBids [i]) >= 7)
				BidsCounter++;
		return BidsCounter;
	}

	private bool CheckBidsEndsOrAllPass (int CounterTarget, bool CheckTurn)
	{
		if (CheckTurn && !IsThisMyBidTurn ())
			return false;
		int PassCounter = 0;
		for (int i = 0; i < 4; i++) {
			int CurrentBid = int.Parse (TempCurrenRoom.GameOB.PlayersBids [i]);
			if (CurrentBid == -1)
				PassCounter++;
			else
				PassCounter--;
		}
		if (PassCounter == CounterTarget)
			return true;
		else
			return false;
	}

	bool IsThisMyBidTurn ()
	{
		if (Player.Instance.OnlinePlayerID == TempCurrenRoom.GameOB.PlayerBidTurn)
			return true;
		else
			return false;
	}

	//FirstBid
	void UpdateRoomPhase5 ()
	{
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.ActionID = 6;
		TempCurrenRoom.GameOB.PlayerBidTurn = 1;
		PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private bool SceneDoneLoading;
	void UpdateRoomPhase4 ()
	{
		if (Player.Instance.OnlinePlayerID == -1)
			return;
		SceneDoneLoading = true;
		TarneebManager1.Instance.Cards.PlayersCards[Player.Instance.OnlinePlayerID].PlayerCardsList =
			TempCurrenRoom.GameOB.Cards.PlayersCards[Player.Instance.OnlinePlayerID].PlayerCardsList;
		TarneebManager1.Instance.SetAndArrangeMyCards ();
		ReturnsMyCardsAvailibity ();
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.ActionID = 5;
		PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase3 ()
	{
		if (SceneDoneLoading)
			return;
		if (IsAllPlayersReady () && Player.Instance.GameCreator) {
			TempCurrenRoom.ActionID = -1;
			PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
			return;
		}
		UpdateRoomPhase2 ();
		if (TempCurrenRoom.PlayersReady [Player.Instance.OnlinePlayerID])
			return;
		SetPlayersScores ();
		PlayingFireBaseManager1.Instance.SetOnDissconnectHandle ();
		TarneebManager1.Instance.AssignPlayerCardsAndActivateOthersCards ();
		TempCurrenRoom.PlayersReady [Player.Instance.OnlinePlayerID] = true;
		PlayingWindowsManager1.Instance.WinsWindow.SetActive (false);
		if (Player.Instance.GameCreator)
			SetReadyForComputer ();
		PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void SetReadyForComputer ()
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.SeatsStatus [i] == 0)
				TempCurrenRoom.PlayersReady [i] = true;
	}

	bool IsAllPlayersReady ()
	{
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++)
			if (!TempCurrenRoom.PlayersReady [i])
				return false;
		return true;
	}

	void UpdateRoomPhaseMinus1(){
		AudioManager.Instance.PlaySound (0);
		if (!Player.Instance.GameCreator)
			return;
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++)
			TempCurrenRoom.PlayersReady [i] = false;
		TarneebManager1.Instance.SetCardsForPlayers ();
		TempCurrenRoom.ActionID = 4;
		TempCurrenRoom.Available = false;
		for(int i=0;i<4;i++)
			TempCurrenRoom.GameOB.Cards.PlayersCards[i].PlayerCardsList = TarneebManager1.Instance.Cards.PlayersCards[i].PlayerCardsList;
		PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void SetPlayersScores ()
	{
		if (Player.Instance.OnlinePlayerID == 0 || Player.Instance.OnlinePlayerID == 2) {
			PlayingWindowsManager1.Instance.TeamsScores [0].GetComponent<Text> ().text = TempCurrenRoom.GameOB.PlayersScores [0].ToString ();
			PlayingWindowsManager1.Instance.TeamsScores [2].GetComponent<Text> ().text = TempCurrenRoom.GameOB.PlayersScores [0].ToString ();
			PlayingWindowsManager1.Instance.TeamsScores [1].GetComponent<Text> ().text = TempCurrenRoom.GameOB.PlayersScores [1].ToString ();
			PlayingWindowsManager1.Instance.TeamsScores [3].GetComponent<Text> ().text = TempCurrenRoom.GameOB.PlayersScores [1].ToString ();
		} else {
			PlayingWindowsManager1.Instance.TeamsScores [0].GetComponent<Text> ().text = TempCurrenRoom.GameOB.PlayersScores [1].ToString ();
			PlayingWindowsManager1.Instance.TeamsScores [2].GetComponent<Text> ().text = TempCurrenRoom.GameOB.PlayersScores [1].ToString ();
			PlayingWindowsManager1.Instance.TeamsScores [1].GetComponent<Text> ().text = TempCurrenRoom.GameOB.PlayersScores [0].ToString ();
			PlayingWindowsManager1.Instance.TeamsScores [3].GetComponent<Text> ().text = TempCurrenRoom.GameOB.PlayersScores [0].ToString ();
		}
	}

	void ReloadScene ()
	{
		TempCurrenRoom.ReloadScene = false;
		TempCurrenRoom.ActionID = 3;
		SceneDoneLoading = true;
		if(Player.Instance.GameCreator)
			PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom,true);
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
	}

	public void PlayerSetBid (string BidValue, int PlayerID)
	{
		TempCurrenRoom.GameOB.PlayerBidTurn = NextBidder();
		if (TempCurrenRoom.GameOB.PlayerBidTurn > 3)
			TempCurrenRoom.GameOB.PlayerBidTurn = 0;
		TempCurrenRoom.GameOB.PlayersBids [PlayerID] = BidValue;
		PlayingFireBaseManager1.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private int NextBidder(){
		int PlayerID = TempCurrenRoom.GameOB.PlayerBidTurn;
		int u = 0;
		for (int i = PlayerID+1; u < 4; i++) {
			if (i >= 4)
				i = 0;
			if (TempCurrenRoom.GameOB.PlayersBids [i] != "-1" && i != PlayerID)
				return i;
			u++;
		}
		return Player.Instance.OnlinePlayerID;
	}

	public int HighestBid ()
	{
		List<int> BidsList = new List<int> ();
		for (int i = 0; i < 4; i++)
			BidsList.Add (int.Parse (TempCurrenRoom.GameOB.PlayersBids [i]));
		BidsList.Sort ();
		return BidsList [3];
	}

	#region implemented abstract members of PlayersManagerParent
	public override void OnApplicationPause (bool pauseStatus)
	{
		if (TempCurrenRoom.ActionID < 4)
			return;
		try {
			if (pauseStatus)
				CreatorPauseOrExit(false);
			else
				StartCoroutine (SetandarrangeCor (pauseStatus));
		} catch {
		}
	}

	#endregion
	private IEnumerator SetandarrangeCor(bool pauseStatus){
		ShowBackBTN ();
		yield return new WaitForSeconds (2.5f);
		try{
			TarneebManager1.Instance.SetAndArrangeMyCards ();
		}catch{
		}
	}


	#region implemented abstract members of PlayersManagerParent

	public override void IamBack ()
	{
		TempCurrenRoom.SeatsStatus [Player.Instance.OnlinePlayerID] = 1;
		PlayerIsBack (true);
		PlayingFireBaseFather.ParentInstance.UploudThisJson (TempCurrenRoom, false);
	}

	#endregion
	private void OnDestroy(){
		StopAllCoroutines ();
	}
}