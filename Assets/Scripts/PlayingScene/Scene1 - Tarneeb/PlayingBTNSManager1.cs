﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayingBTNSManager1 : PlayingBTNSManagerFather {
	
	public static PlayingBTNSManager1 Instance;
	public GameObject ChooseTrumpWindow;

	protected override void Awake(){
		base.Awake ();
		Instance = this;
	}

	public override void BTNSFunction(){
		base.BTNSFunction ();
		if (BTNName.Contains ("BidBTN")) {
			if (BTNName.StartsWith ("Pass"))
				SetBid ("-1");
			else
				SetBid (GetChar (BTNName, 0) + GetChar (BTNName, 1));
		} else if (BTNName.Contains ("Trump"))
			ChooseTrump (GetChar (BTNName, 0));
		else if (BTNName.Contains ("RestartBTN"))
			PlayersManager1.Instance.RestartGame ();
	}

	public void ChooseTrump(string TrumpType){
		PlayingWindowsManager1.Instance.ChooseTrumpWindow.SetActive (false);
		PlayersManager1.Instance.PlayerChooseTrump (TrumpType);
	}



	public void SetBid (string BidValue)
	{
		if (BidValue.Contains ("B"))
			BidValue = GetChar (BidValue,0);
		PlayersManager1.Instance.PlayerSetBid (BidValue,Player.Instance.OnlinePlayerID);
		PlayingWindowsManager1.Instance.BidWindow.SetActive (false);
	}
}