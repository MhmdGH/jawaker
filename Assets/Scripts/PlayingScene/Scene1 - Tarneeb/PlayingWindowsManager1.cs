﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public class PlayingWindowsManager1 : PlayingWindowsManagerFather {

	public static PlayingWindowsManager1 Instance;
	public GameObject[] BidsPops;
	public GameObject BidWindow,ChooseTrumpWindow,TrumpDetailsWindow;

	public override void Awake(){
		base.Awake ();
		Instance = this;
	}

	public override void ShowAndSetPartnersPhotos(string[] IMGSUrl){
		ChoosePartnerGO.SetActive (true);
		for(int i=0;i<ChooseParternersGOs.Length;i++)
			PlayingFBManager1.Instance.SetFPPhoto (IMGSUrl[i],ChooseParternersGOs[i].GetComponent<Image>());
	}

	public void ShowBidWindow(bool WithoutPass){
		BidWindow.SetActive (true);
		if (WithoutPass) {
			for (int i = 0; i < BidWindow.transform.childCount; i++)
				BidWindow.transform.GetChild (i).gameObject.SetActive (true);
			BidWindow.transform.GetChild(BidWindow.transform.childCount-1).gameObject.SetActive (false);
			return;
		}
		int HighestBid = PlayersManager1.Instance.HighestBid ();
		if (HighestBid < 7)
			return;
		int StartChild = HighestBid - 7;
		for (int i = StartChild; i > -1; i--)
			BidWindow.transform.GetChild (i).gameObject.SetActive (false);
	}

	public void RefreshBidWindow(){
		for (int i = 0; i < BidWindow.transform.childCount; i++)
			BidWindow.transform.GetChild (i).gameObject.SetActive (true);
	}

	public void ShowBidPop(int BidPopNO,string BidValue){
		StartCoroutine (ShowBidCor(BidPopNO,BidValue));
	}

	public void SetTrumpAndBidder(string TrumpType,int Bidding){
		TrumpDetailsWindow.SetActive (true);
		TrumpDetailsWindow.transform.GetChild(1).GetComponent<Image>().sprite = 
			Resources.Load<Sprite> ("Playing/"+TrumpType);
		TrumpDetailsWindow.transform.GetChild (2).GetComponent<Text> ().text = Bidding.ToString ();
	}

	private IEnumerator ShowBidCor(int BidPopNO,string BidValue){
		BidsPops [BidPopNO].SetActive (true);
		BidsPops [BidPopNO].transform.GetChild (0).GetComponent<Text> ().text = BidValueTranslated(BidValue);
		yield return new WaitForSeconds (2);
		BidsPops [BidPopNO].SetActive (false);
	}

	string BidValueTranslated (string TranslfatedBidTxt)
	{
		if (LanguangeManagerFather.ParentInstance.IsArabic) {
			if (TranslfatedBidTxt == "Pass")
				TranslfatedBidTxt =ArabicFixer.Fix("باص");
		}
		return TranslfatedBidTxt;
	}

	public override void StopTimer(){
		try{
		base.StopTimer ();
		for (int i = 0; i < BidsPops.Length; i++)
				BidsPops [i].SetActive (false);}catch{
		}
	}

	protected override IEnumerator StartThisTimer(int PlayerNO,float WaitingTime,int FunToDOAfterEnd,bool JustCreatorDoIT){
		PlayersTimer [PlayerNO].SetActive (true);
		WaitingTime = WaitingTime*SliderSmothnessMultiplier;
		float ActualTimerTikValue = TimerTikValue*SliderSmothnessMultiplier;
		PlayersTimer [PlayerNO].GetComponent<Slider> ().maxValue = WaitingTime;
		PlayersTimer [PlayerNO].GetComponent<Slider> ().value = WaitingTime;
		while (WaitingTime > 0) {
			PlayersTimer [PlayerNO].GetComponent<Slider> ().value -= ActualTimerTikValue;
			WaitingTime-=ActualTimerTikValue;
			yield return new WaitForSeconds (TimerTikValue*0.1f);
		}
		PlayersTimer [PlayerNO].SetActive (false);
		if (JustCreatorDoIT && Player.Instance.GameCreator)
			PlayersManager1.Instance.MasterTakeControl (FunToDOAfterEnd);
		else if(!JustCreatorDoIT)
			PlayersManager1.Instance.MasterTakeControl (FunToDOAfterEnd);
	}

	public void OnDestroy(){
		StopAllCoroutines ();
	}
}