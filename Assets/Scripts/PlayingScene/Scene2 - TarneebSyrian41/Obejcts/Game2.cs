﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Game2:GameOBParent{
	public Game2(){
		PlayersBids = new string[4]{"0","0","0","0"};
		PlayersScores = new int[4]{0,0,0,0};
		Cards = new PlayersCardsOB1 ();
		CurrentRound = new Round1 ();
	}
}