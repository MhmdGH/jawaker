﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class PlayersManager2 : PlayersManagerParent
{
	public static PlayersManager2 Instance;
	public GameObject CardGO;

	public override void Awake ()
	{
		base.Awake ();
		Instance = this;
	}

	public void UnChooseAllOtherCards(){
		for (int i = 0; i < TarneebManager2.Instance.MyPlayerCardsGO.Length; i++)
			try{TarneebManager2.Instance.MyPlayerCardsGO[i].GetComponent<Card2> ().OnEndDragFun (true);}catch{
		}
	}

	public override void Start ()
	{
		TempCurrenRoom = new JsonRoomO ("Default", new string[4]{ "0", "0", "0", "0" }, new Game2 ());
		base.Start ();
		CardsListGO = PlayersCardsGO [0].transform.GetChild (1).GetChild (0).gameObject;
		SceneDoneLoading = false;
	}

	//Main Reciever Fun
	public override void UpdateRoom (JsonRoomO CurrentRoomJSON)
	{
		base.UpdateRoom (CurrentRoomJSON);
		if (TempCurrenRoom.NoMaster)
			AssignNewMasterOrRemoveRoom ();
		if (TempCurrenRoom.Update == 0) {
			PlayingFireBaseManager2.Instance.SetOnDissconnectHandle ();
			return;
		}
		PlayingWindowsManager2.Instance.StopTimer ();
		switch (TempCurrenRoom.ActionID) {
		case -1:
			UpdateRoomPhaseMinus1 ();
			break;
		case 0:
			UpdateRoomPhase0 ();
			break;
		case 2:
			UpdateRoomPhase2 ();
			break;
		case 3:
			UpdateRoomPhase3 ();
			break;
		case 4:
			UpdateRoomPhase4 ();
			break;
		case 5:
			UpdateRoomPhase5 ();
			break;
		case 6:
			UpdateRoomPhase6 ();
			break;
		case 7:
			UpdateRoomPhase7 ();
			break;
		case 8:
			UpdateRoomPhase8 ();
			break;
		case 9:
			UpdateRoomPhase9 ();
			break;
		case 10:
			UpdateRoomPhase10 ();
			break;
		case 12:
			UpdateRoomPhase12 ();
			break;
		case 13:
			UpdateRoomPhase13 ();
			break;
		case 14:
			UpdateRoomPhase14 ();
			break;
		case 15:
			UpdateRoomPhase15 ();
			break;
		}
	}

	void UpdateRoomPhase0 ()
	{
		if (TempCurrenRoom.ReloadScene)
			ReloadScene ();
	}

	void ReloadScene ()
	{
		TempCurrenRoom.ReloadScene = false;
		TempCurrenRoom.ActionID = 3;
		SceneDoneLoading = true;
		if (Player.Instance.GameCreator)
			PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);

	}

	public void MasterTakeControl (int funToDOAfterEnd)
	{
		if (funToDOAfterEnd == 1)
			SetBid ();
		else if (funToDOAfterEnd == 3)
			MasterOrPlayerThrowCardNOW ();
	}

	void MasterOrPlayerThrowCardNOW ()
	{
		//If Player Is Here
		int PlayerSeatStatus = TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay];
		//If Player Is Here
		if (MyTurnToPlay () && PlayerSeatStatus == 1) {
			if (PlayersManagerParent.PlayerHoldingCard) {
				GameObject CardHolded = CanvasGO.GetComponent<RectTransform> ().GetChild (CanvasGO.GetComponent<RectTransform> ().childCount - 1).gameObject;
				CardHolded.GetComponent<Card2> ().ReturnCardBack ();
				CardHolded.GetComponent<Card2> ().SendCard ();
				CheckPlayerKick ();
				return;
			} else {
				IfPlayerHoldingCardGetItBack ();
				List<GameObject> AvailbeCardsToThrow = new List<GameObject> ();
				for (int i = 0; i < CardsListGO.transform.childCount; i++)
					if (CardsListGO.transform.GetChild (i).GetComponent<Image> ().color.r == 1)
						AvailbeCardsToThrow.Add (CardsListGO.transform.GetChild (i).gameObject);
				AvailbeCardsToThrow [GetBestCardFromThisList (AvailbeCardsToThrow)].GetComponent<Card> ().SendCard ();
				CheckPlayerKick ();
				return;
			}
			//If Player Not Here, Master Do IT
		} else if (Player.Instance.GameCreator && PlayerSeatStatus == 0) {
			TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay] = 0;
			ThrowCardNow (FirstAvailabeCardOnOtherPlayer ());
		}
	}

	int GetBestCardFromThisList (List<GameObject> availbeCardsToThrow)
	{
		List<string> availbeCardsToThrowIDsStrings = new List<string> ();
		for (int i = 0; i < availbeCardsToThrow.Count; i++)
			availbeCardsToThrowIDsStrings.Add (availbeCardsToThrow[i].GetComponent<Card>().CardID);
		//ThisIsThePlayerWhoStartsTheRound
		if (ThisIsThePlayerWhoStartsTheRound ())
			return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,true);
		//Not the one who starts the round, tow main conditions

		//1.If i have the played card type
		if(IHaveThisCardType(PlayedType(),availbeCardsToThrowIDsStrings)){
			//A.If there is a high played card,I play small one
			if(GetCardNO(HighestThrowdCard (PlayedType()))>=11)
				return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,false);
			else //B.If there is not a high played card,I play high one
				return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,true);
		}else{ //2.If i havent the played card type
			//A.If i have tarneeb card and my partner not played high card
			if (IHaveThisCardType (TarneebType (), availbeCardsToThrowIDsStrings)) {
				if (PartnerThrowdCard()!="0" && GetCardNO(PartnerThrowdCard())<=11)
					return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,true,TarneebType(),false);
				else if(PartnerThrowdCard()=="0") //A.2.If i have tarneeb card and my partner not played yet
					return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,false,TarneebType(),false);
				else
					return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,false);
			}else{
				//B.If i havent tarneeb card, i play smallest card i have
				return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,false);
			}
		}
	}

	string FirstAvailabeCardOnOtherPlayer ()
	{
		List<string> ThisPlayerCardsList = new List<string> ();
		ThisPlayerCardsList = TempCurrenRoom.GameOB.
			Cards.PlayersCards [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay].PlayerCardsList;

		//ThisIsThePlayerWhoStartsTheRound
		if (ThisIsThePlayerWhoStartsTheRound())
			return ThisPlayerCardsList[GetHighestOrSmallestCardIFromThisList (ThisPlayerCardsList,true)];

		List<string> availbeCardsToThrowIDsStrings = new List<string> ();
		if (ThisPlayerHaveThisCardType (ThisPlayerCardsList, PlayedType ())) {
			for (int i = 0; i < ThisPlayerCardsList.Count; i++) {
				if (ThisPlayerCardsList [i].StartsWith (PlayedType ()))
					availbeCardsToThrowIDsStrings.Add (ThisPlayerCardsList [i]);
				else
					availbeCardsToThrowIDsStrings.Add ("0");
			}
		} else
			availbeCardsToThrowIDsStrings = ThisPlayerCardsList;

		//1.If i have the played card type
		if(ThisPlayerHaveThisCardType(availbeCardsToThrowIDsStrings,PlayedType())){
			//A.If there is a high played card,I play small one
			if(GetCardNO(HighestThrowdCard (PlayedType()))>=11)
				return availbeCardsToThrowIDsStrings [GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings, false)];
			else //B.If there is not a high played card,I play high one
				return availbeCardsToThrowIDsStrings[GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,true)];
		}else{ //2.If i havent the played card type
			//A.1.If i have tarneeb card and my partner not played high card
			if (ThisPlayerHaveThisCardType (availbeCardsToThrowIDsStrings,TarneebType ())) {
				if (PartnerThrowdCard()!="0" && GetCardNO(PartnerThrowdCard())<11)
					return availbeCardsToThrowIDsStrings[GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,true,TarneebType(),false)];
				else if(PartnerThrowdCard()=="0") //A.2.If i have tarneeb card and my partner not played yet
					return availbeCardsToThrowIDsStrings[GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,false,TarneebType(),false)];
				else
					return availbeCardsToThrowIDsStrings[GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,false)];
			}else{
				//B.If i havent tarneeb card, i play smallest card i have
				return availbeCardsToThrowIDsStrings[GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,false)];
			}
		}
	}

	int PlayerIDTurn (){
		return TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
	}

	string PartnerThrowdCard(){
		return TempCurrenRoom.GameOB.CurrentRound.PlayersCards[MyPartnerID(PlayerIDTurn())];
	}

	string HighestThrowdCard(string CardType){
		List<string> ThisPlayersThrowdCardsList = GetPlayersThrowdCardsList ();
		ThisPlayersThrowdCardsList.Sort ();
		for (int i = 3; i >= 0; i--) {
			if (ThisPlayersThrowdCardsList [i].Length < 2)
				continue;
			if (GetCardType (ThisPlayersThrowdCardsList [i]) == CardType)
				return ThisPlayersThrowdCardsList [i];
		}
		for (int i = 0; i < 4; i++) {
			if (ThisPlayersThrowdCardsList [i].Length < 2)
				continue;
			return ThisPlayersThrowdCardsList [i];
		}
		return ThisPlayersThrowdCardsList [3];
	}

	private List<string> GetPlayersThrowdCardsList(){
		List<string> NewPlayersCardsList = new List<string> ();
		for (int i = 0; i < 4; i++)
			NewPlayersCardsList.Add (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i]);
		return NewPlayersCardsList;
	}

	private string PlayedType(){
		return GetChar (TempCurrenRoom.GameOB.CurrentRound.
			PlayersCards [TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound],0);
	}

	private string TarneebType(){
		return TempCurrenRoom.GameOB.TrumpType;
	}

	private bool HaveTarneebCard(List<GameObject> availbeCardsToThrow){
		List<string> availbeCardsToThrowIDsStrings = new List<string> ();
		for (int i = 0; i < availbeCardsToThrow.Count; i++)
			availbeCardsToThrowIDsStrings.Add (availbeCardsToThrow[i].GetComponent<Card>().CardID);
		for (int i = 0; i < availbeCardsToThrowIDsStrings.Count; i++)
			if (GetChar (availbeCardsToThrowIDsStrings [i], 0) == TempCurrenRoom.GameOB.TrumpCard)
				return true;
		return false;
	}

	//Creator Fun
	void SetBid ()
	{
		PlayingWindowsManager2.Instance.BidWindow.SetActive (false);
		int PlayerSeatStatus = TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.PlayerBidTurn];
		if (IsThisMyBidTurn () && PlayerSeatStatus == 1)
			PlayerSetBid ("3", TempCurrenRoom.GameOB.PlayerBidTurn);
		else if (Player.Instance.GameCreator && PlayerSeatStatus == 0)
			PlayerSetBid ("3", TempCurrenRoom.GameOB.PlayerBidTurn);
	}

	public void RestartGame ()
	{
		PlayingWindowsManager2.Instance.WinsWindow.SetActive (false);
		Game2 NewGameOB = new Game2 ();
		for (int i = 0; i < NewGameOB.PlayersScores.Length; i++)
			NewGameOB.PlayersScores [i] = 0;
		TempCurrenRoom.GameOB = NewGameOB;
		TempCurrenRoom.ReloadScene = true;
		TempCurrenRoom.ActionID = 0;
		PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase15 ()
	{
		//One Of The teams wins//Game End
		AddWinsPointsForPlayers ();
		if (MainFireBaseManager.Instance.IsACompition)
			InnerSetCompData ();
		PlayingWindowsManager2.Instance.ShowWindWindow (WinnerNO);
	}

	private void InnerSetCompData ()
	{
		if (Player.Instance.GameCreator) {
			for (int i = 0; i < TempCurrenRoom.DatabasePlayersIDs.Length; i++)
				if (TempCurrenRoom.DatabasePlayersIDs [i].Length < 4)
					return;
			if (WinnerNO == 0) {
				SetCompData (TempCurrenRoom.DatabasePlayersIDs [0], TempCurrenRoom.DatabasePlayersIDs [2],
					TempCurrenRoom.PlayersFBIDs [0], TempCurrenRoom.PlayersFBIDs [2]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [1]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [3]);
			} else {
				SetCompData (TempCurrenRoom.DatabasePlayersIDs [1], TempCurrenRoom.DatabasePlayersIDs [3],
					TempCurrenRoom.PlayersFBIDs [1], TempCurrenRoom.PlayersFBIDs [3]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [0]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [2]);
			}
		}
	}

	private void AddWinsPointsForPlayers ()
	{
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (WinnerNO == 0 && (PlayerID == 0 || PlayerID == 2)) {
			Player.Instance.AddXPsAndCLubPoints (XPsValue);
			Player.Instance.Coins += ScoreValue;
		} else if (WinnerNO == 1 && (PlayerID == 1 || PlayerID == 3)) {
			Player.Instance.AddXPsAndCLubPoints (XPsValue);
			Player.Instance.Coins += ScoreValue;
		}
	}

	void UpdateRoomPhase14 ()
	{
		if (!Player.Instance.GameCreator)
			return;
		if (OneOfTeamsWins ()) {
			TempCurrenRoom.ActionID = 15;
			PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
		} else
			NewRound ();
	}

	private void NewRound ()
	{
		for (int i = 0; i < 4; i++)
			PlayingWindowsManager2.Instance.TeamsScores [i].GetComponent<Text> ().text = TempCurrenRoom.GameOB.PlayersScores [i].ToString ();
		Game2 NewGameOB = new Game2 ();
		for (int i = 0; i < NewGameOB.PlayersScores.Length; i++)
			NewGameOB.PlayersScores [i] = TempCurrenRoom.GameOB.PlayersScores [i];
		TempCurrenRoom.GameOB = NewGameOB;
		TempCurrenRoom.ReloadScene = true;
		TempCurrenRoom.ActionID = 0;
		PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	int WinnerNO;

	bool OneOfTeamsWins ()
	{
		for (int i = 0; i < 4; i++) {
			if (TempCurrenRoom.GameOB.PlayersScores [i] >= 41) {
				if (i == 0 || i == 2)
					WinnerNO = 0;
				else
					WinnerNO = 1;
				return true;
			}
		}
		return false;
	}

	void UpdateRoomPhase13 ()
	{
		//Full Round Ends
		for (int i = 0; i < 4; i++)
			PlayingWindowsManager2.Instance.PlayersScoresGO [i].GetComponent<Text> ().text = "0";

		if (!Player.Instance.GameCreator)
			return;
		for (int i = 0; i < 4; i++) {
			int CurrentRoundPlayerScore = TempCurrenRoom.GameOB.CurrentRound.PlayersScores [i];
			int PlayerComulativeScore = TempCurrenRoom.GameOB.PlayersScores [i];
			int PlayerBid = int.Parse (TempCurrenRoom.GameOB.PlayersBids [i]);
			if (CurrentRoundPlayerScore >= PlayerBid)
				PlayerComulativeScore += CurrentRoundPlayerScore;
			else
				PlayerComulativeScore -= PlayerBid;
			TempCurrenRoom.GameOB.PlayersScores [i] = PlayerComulativeScore;
		}
		TempCurrenRoom.ActionID = 14;
		PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase12 ()
	{
		//COllect Cards Here And Throw it to winner Player
		StartCoroutine (CollectionCor ());
	}

	private IEnumerator CollectionCor ()
	{
		ReturnsMyCardsAvailibity ();
		SetAvailableCardsOnce = false;
		int LastWinnerID = TempCurrenRoom.GameOB.CollectorID;
		yield return new WaitForSeconds (1);
		CardsThrowerMangaer.Instance.CollectCardsToThisPlayer (FromOnlineToLocal (LastWinnerID));
		PlayingWindowsManager2.Instance.PlayersScoresGO [FromOnlineToLocal (LastWinnerID)].GetComponent<Text> ().text =
			TempCurrenRoom.GameOB.CurrentRound.PlayersScores [LastWinnerID].ToString ();
		yield return new WaitForSeconds (1);
		if (Player.Instance.GameCreator) {
			SetPlayersGreenOrRed ();
			if (FullRoundEnds ()) {
				TempCurrenRoom.ActionID = 13;
				PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
			} else {
				TempCurrenRoom.ActionID = 10;
				TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound = LastWinnerID;
				TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = LastWinnerID;
				PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
			}
		}
	}

	void SetPlayersGreenOrRed ()
	{
		for (int i = 0; i < TempCurrenRoom.GreenOrRed.Length; i++) {
			if (TempCurrenRoom.GreenOrRed [i] != 0)
				continue;
			//if win with my bid/**/
			if (TempCurrenRoom.GameOB.PlayersScores [i] >= int.Parse (TempCurrenRoom.GameOB.PlayersBids [i])) {
				TempCurrenRoom.GreenOrRed [i] = 1;
				continue;
			}
			//if lose with my bid
			if (13-TempCurrenRoom.GameOB.CurrentRound.RoundNO < int.Parse (TempCurrenRoom.GameOB.PlayersBids [i])
				&&TempCurrenRoom.GreenOrRed[i]==0) {
				TempCurrenRoom.GreenOrRed [i] = 2;
				continue;
			}
		}
	}

	#region implemented abstract members of PlayersManagerParent

	public override void SetLastRoundCards ()
	{
		string[] LastRoundCards = new string[4];
		for (int i = 0; i < 4; i++)
			LastRoundCards [i] = TempCurrenRoom.GameOB.CurrentRound.PlayersCards [FromOnlineToLocal (i)];
		LastRoundGO.GetComponent<LastRound> ().SetCards (LastRoundCards);
	}

	#endregion

	bool FullRoundEnds ()
	{
		if (TempCurrenRoom.GameOB.CurrentRound.RoundNO >= 13)
			return true;
		else
			return false;
	}

	private void UpdateRoomPhase11 ()
	{
		//Just a fukin Holder
	}

	private void UpdateRoomPhase10 ()
	{
		//GameStart
		PlayingWindowsManager2.Instance.SetTimer (FromOnlineToLocal (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay),
			TimerWaitingTime(TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay), 3,false);
		SetAvailableCards ();
		int PrevPlayerID = PreviosTurn (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay);
		if (MyTurnToPlay ()) {
			if (IsThisFuckerTrulyTurnToPlay()) {
				StartCoroutine (UpdateRoomPhase10Cor());
			} else {
				IfPlayerHoldingCardGetItBack ();
				TarneebManager2.Instance.SetAndArrangeMyCards ();
			}
		}
		if (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [PrevPlayerID].Length < 2)
			return;
		ThrowThisCard (PrevPlayerID, TempCurrenRoom.GameOB.CurrentRound.PlayersCards [PrevPlayerID]);
	}

	private IEnumerator UpdateRoomPhase10Cor ()
	{
		yield return new WaitForSeconds (1);
		try {
			GameObject CardHolded = CanvasGO.GetComponent<RectTransform> ().GetChild (CanvasGO.GetComponent<RectTransform> ().childCount - 1).gameObject;
			CardHolded.GetComponent<Card2> ().ReturnCardBack ();
			CardHolded.GetComponent<Card2> ().SendCard ();
		} catch {
		}
	}


	private bool SetAvailableCardsOnce;
	void SetAvailableCards ()
	{
		if (SetAvailableCardsOnce)
			return;
		//Watcher
		if (Player.Instance.OnlinePlayerID == -1)
			return;
		List<string> PlayedCards = new List<string> ();
		IfPlayerHoldingCardGetItBack ();
		int Counter = 0;
		int PlayedPlayerID = 0;
		for (int i = 0; i < 4; i++)
			PlayedCards.Add (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i]);
		for (int i = 0; i < 4; i++) {
			if (PlayedCards [i].Length > 1) {
				Counter++;
				PlayedPlayerID = i;
			}
		}
		if (Counter != 1 || PreviosTurn (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay) == Player.Instance.OnlinePlayerID)
			return;
		string CardType = GetChar (PlayedCards [PlayedPlayerID], 0);
		if (IHaveThisCardType (CardType)) {
			for (int i = 0; i < CardsListGO.transform.childCount; i++) {
				if (!CardsListGO.transform.GetChild (i).GetComponent<Card2> ().CardID.StartsWith (CardType)) {
					CardAvailability (CardsListGO.transform.GetChild (i).gameObject, false);
					SetAvailableCardsOnce = true;
				}
			}
		}
	}

	bool IHaveThisCardType (string Cardtype)
	{
		for (int i = 0; i < CardsListGO.transform.childCount; i++) {
			if (CardsListGO.transform.GetChild (i).GetComponent<Card2> ().CardID.StartsWith (Cardtype))
				return true;
		}
		return false;
	}

	bool IHaveThisCardType (string Cardtype,List<string> ThisCardsList)
	{
		for (int i = 0; i < ThisCardsList.Count; i++) {
			if (ThisCardsList[i].StartsWith(Cardtype))
				return true;
		}
		return false;
	}

	void ThrowThisCard (int PlayerThrowerID, string ThrowdCardID)
	{
		int PlayerIDOnMyDevice = FromOnlineToLocal (PlayerThrowerID);
		CardsThrowerMangaer.Instance.ThrowThisCard (ThrowdCardID, PlayerIDOnMyDevice);
		
		if (PlayerIDOnMyDevice != 0 || Player.Instance.OnlinePlayerID == -1)
			PlayersCardsGO [PlayerIDOnMyDevice].transform.GetChild (1).GetChild (0).
			GetChild (TempCurrenRoom.GameOB.CurrentRound.RoundNO).gameObject.SetActive (false);
		if (RoundEnds ()) {
			SetLastRoundCards ();
			if (Player.Instance.GameCreator)
				CloseRound ();
		}
	}


	public void ThrowCard (GameObject Cardjo)
	{
		if (MyTurnToPlay () && TempCurrenRoom.ActionID == 10 && CanThrowCard)
			StartCoroutine (RemoveOrderly (Cardjo));
		else
			Cardjo.GetComponent<Card> ().ReturnCardBack ();
	}

	private bool CanThrowCard = true;

	private IEnumerator RemoveOrderly (GameObject Cardjo)
	{
		CanThrowCard = false;
		ThrowCardNow (Cardjo.GetComponent<Card> ().CardID);
		Destroy (Cardjo);
		yield return new WaitForSeconds (0.03f);
		try{TarneebManager2.Instance.SetAndArrangeMyCards ();
			}catch{
		}
		yield return new WaitForSeconds (2f);
		CanThrowCard = true;
		yield return null;
	}

	void ThrowCardNow (string CardID)
	{
		string CardIDString = CardID;
		int PlayerTurn = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		TempCurrenRoom.GameOB.CurrentRound.PlayersCards [PlayerTurn] = CardIDString;
		RemoveThisCardFromOnlineCardsList (PlayerTurn, CardIDString);
		int NextPlayerID = PlayerTurn;
		NextPlayerID++;
		if (NextPlayerID >= 4)
			NextPlayerID = 0;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = NextPlayerID;
		PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private void RemoveThisCardFromOnlineCardsList (int PlayerNO, string CardID)
	{
		for (int i = 0; i < TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList.Count; i++) {
			if (TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList [i] == CardID) {
				TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList [i] = "0";
				return;
			}
		}
	}

	private bool RoundEnds ()
	{
		int Over1Counter = 0;
		for (int i = 0; i < 4; i++) {
			if (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i].Length > 1)
				Over1Counter++;
		}
		if (Over1Counter == 4)
			return true;
		else
			return false;
	}

	private List<string> PlayersCardsList;

	void CloseRound ()
	{
		int LastWinnderID = GetWinnerID ();
		TempCurrenRoom.GameOB.CurrentRound.PlayersScores [LastWinnderID]++;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = LastWinnderID;
		TempCurrenRoom.GameOB.CollectorID = LastWinnderID;
		TempCurrenRoom.GameOB.CurrentRound.RoundNO++;
		for (int i = 0; i < 4; i++)
			TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i] = "0";
		TempCurrenRoom.ActionID = 12;
		PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private int GetWinnerID ()
	{
		TempCurrenRoom.ActionID = 11;
		PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
		PlayersCardsList = new List<string> ();
		for (int i = 0; i < 4; i++)
			PlayersCardsList.Add (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i]);
		if (ThereIsTrumpCard ())
			return GetHighestFromThisType (TempCurrenRoom.GameOB.TrumpType);
		else
			return GetHighestFromThisType (GetChar (PlayersCardsList [TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound], 0));
	}

	private int GetHighestFromThisType (string CardType)
	{
		List<string> NewCardsList = new List<string> ();
		for (int i = 0; i < 4; i++) {
			if (PlayersCardsList [i].StartsWith (CardType))
				NewCardsList.Add (PlayersCardsList [i]);
		}
		//After List Ready
		NewCardsList.Sort ();
		for (int i = 0; i < 4; i++) {
			if (NewCardsList [NewCardsList.Count - 1] == PlayersCardsList [i])
				return i;
		}
		return 0;
	}

	private bool ThereIsTrumpCard ()
	{
		for (int i = 0; i < 4; i++) {
			if (PlayersCardsList [i].StartsWith (TempCurrenRoom.GameOB.TrumpType))
				return true;
		}
		return false;
	}


	void UpdateRoomPhase9 ()
	{
		
		StartCoroutine (CallStartGameAfterASec ());
		PlayingWindowsManager2.Instance.SetTrumpAndBidder (TempCurrenRoom.GameOB.TrumpType);
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.ActionID = 10;
		PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private IEnumerator CallStartGameAfterASec ()
	{
		yield return new WaitForSeconds (1);
		AudioManager.Instance.PlaySound (1);
		AudioManager.Instance.PlaySound (TrumpTypeSound (TempCurrenRoom.GameOB.TrumpType));
	}

	void UpdateRoomPhase8 ()
	{
		PlayingWindowsManager2.Instance.BidWindow.SetActive (false);
		//Trump Choosed
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.ActionID = 9;
		PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private void UpdateRoomPhase7 ()
	{
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = 0;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound = 0;
		TempCurrenRoom.ActionID = 8;
		PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	//Bidding Phase
	void UpdateRoomPhase6 ()
	{
		PlayingWindowsManager2.Instance.SetTimer (FromOnlineToLocal (TempCurrenRoom.GameOB.PlayerBidTurn),
			TimerWaitingTime (TempCurrenRoom.GameOB.PlayerBidTurn), 1, true);
		int Turn = TempCurrenRoom.GameOB.PlayerBidTurn;
		int LastBid = int.Parse (TempCurrenRoom.GameOB.PlayersBids [PreviosTurn (Turn)].ToString ());
		if (LastBid >= 2) {
			AudioManager.Instance.PlaySound (LastBid + 2);
			PlayingWindowsManager2.Instance.ShowBidPop (FromOnlineToLocal (PreviosTurn (Turn)), LastBid.ToString ());
		}
		if (Player.Instance.GameCreator && CheckBidsEnds (4, false)) {
			if (BidsOver11 ()) {
				TempCurrenRoom.ActionID = 7;
				PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
			} else
				NewRound ();
		}
		if (Turn == Player.Instance.OnlinePlayerID && !CheckBidsEnds (4, false))
			PlayingWindowsManager2.Instance.BidWindow.SetActive (true);
		else
			PlayingWindowsManager2.Instance.BidWindow.SetActive (false);
	}

	private bool BidsOver11 ()
	{
		int BidsCounter = 0;
		for (int i = 0; i < 4; i++)
			BidsCounter += int.Parse (TempCurrenRoom.GameOB.PlayersBids [i]);
		if (BidsCounter >= 11)
			return true;
		else
			return false;
	}

	private bool CheckBidsEnds (int CounterTarget, bool CheckTurn)
	{
		if (CheckTurn && !IsThisMyBidTurn ())
			return false;
		int PassCounter = 0;
		for (int i = 0; i < 4; i++) {
			int CurrentBid = int.Parse (TempCurrenRoom.GameOB.PlayersBids [i]);
			if (CurrentBid >= 2)
				PassCounter++;
		}
		if (PassCounter == CounterTarget)
			return true;
		else
			return false;
	}

	bool IsThisMyBidTurn ()
	{
		if (Player.Instance.OnlinePlayerID == TempCurrenRoom.GameOB.PlayerBidTurn)
			return true;
		else
			return false;
	}

	//FirstBid
	void UpdateRoomPhase5 ()
	{
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.ActionID = 6;
		TempCurrenRoom.GameOB.PlayerBidTurn = 1;
		PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private bool SceneDoneLoading;

	void UpdateRoomPhase4 ()
	{
		if (Player.Instance.OnlinePlayerID == -1)
			return;
		SceneDoneLoading = true;
		PlayingWindowsManager2.Instance.SetTrumpCard (TempCurrenRoom.GameOB.TrumpCard);
		TarneebManager2.Instance.Cards.PlayersCards [Player.Instance.OnlinePlayerID].PlayerCardsList =
			TempCurrenRoom.GameOB.Cards.PlayersCards [Player.Instance.OnlinePlayerID].PlayerCardsList;
		TarneebManager2.Instance.SetAndArrangeMyCards ();
		ReturnsMyCardsAvailibity ();
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.ActionID = 5;
		PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase3 ()
	{
		if (SceneDoneLoading)
			return;
		if (IsAllPlayersReady () && Player.Instance.GameCreator) {
			TempCurrenRoom.ActionID = -1;
			PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
			return;
		}
		UpdateRoomPhase2 ();
		if (TempCurrenRoom.PlayersReady [Player.Instance.OnlinePlayerID])
			return;
		SetPlayersScores ();
		PlayingFireBaseManager2.Instance.SetOnDissconnectHandle ();
		TarneebManager2.Instance.AssignPlayerCardsAndActivateOthersCards ();
		PlayingWindowsManager2.Instance.WinsWindow.SetActive (false);
		TempCurrenRoom.PlayersReady [Player.Instance.OnlinePlayerID] = true;
		if (Player.Instance.GameCreator)
			SetReadyForComputer ();
		PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void SetReadyForComputer ()
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.SeatsStatus [i] == 0)
				TempCurrenRoom.PlayersReady [i] = true;
	}

	bool IsAllPlayersReady ()
	{
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++)
			if (!TempCurrenRoom.PlayersReady [i])
				return false;
		return true;
	}

	void UpdateRoomPhaseMinus1 ()
	{
		AudioManager.Instance.PlaySound (0);
		if (!Player.Instance.GameCreator)
			return;
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++)
			TempCurrenRoom.PlayersReady [i] = false;
		SetTrumpCard ();
		TarneebManager2.Instance.SetCardsForPlayers ();
		TempCurrenRoom.ActionID = 4;
		TempCurrenRoom.Available = false;
		for (int i = 0; i < 4; i++)
			TempCurrenRoom.GameOB.Cards.PlayersCards [i].PlayerCardsList = TarneebManager2.Instance.Cards.PlayersCards [i].PlayerCardsList;
		PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void SetPlayersScores ()
	{
		for (int i = 0; i < 4; i++)
			PlayingWindowsManager2.Instance.TeamsScores [i].GetComponent<Text> ().text = TempCurrenRoom.GameOB.PlayersScores [i].ToString ();
	}

	public void PlayerSetBid (string BidValue, int PlayerID)
	{
		TempCurrenRoom.GameOB.PlayerBidTurn++;
		if (TempCurrenRoom.GameOB.PlayerBidTurn > 3)
			TempCurrenRoom.GameOB.PlayerBidTurn = 0;
		TempCurrenRoom.ActionID = 6;
		TempCurrenRoom.GameOB.PlayersBids [PlayerID] = BidValue;
		PlayingFireBaseManager2.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void SetTrumpCard ()
	{
		CardsInfos1 NewCardsInfos1 = new CardsInfos1 ();
		int RandomNO = UnityEngine.Random.Range (0, 13);
		int RandomNO2 = UnityEngine.Random.Range (0, 4);
		string TrumpCardID = NewCardsInfos1.CardTypes [RandomNO2] + NewCardsInfos1.CardNumpers [RandomNO];
		TempCurrenRoom.GameOB.TrumpType = NewCardsInfos1.CardTypes [NegativeTrumpType (RandomNO2)];
		TempCurrenRoom.GameOB.TrumpCard = TrumpCardID;
		PlayingWindowsManager2.Instance.SetTrumpAndBidder (TempCurrenRoom.GameOB.TrumpType);
	}

	private int NegativeTrumpType (int ThisTrumpCardID)
	{
		if (ThisTrumpCardID == 0)
			return 1;
		else if (ThisTrumpCardID == 1)
			return 0;
		else if (ThisTrumpCardID == 2)
			return 3;
		else if (ThisTrumpCardID == 3)
			return 2;
		return 1;
	}

	#region implemented abstract members of PlayersManagerParent

	public override void OnApplicationPause (bool pauseStatus)
	{
		if (TempCurrenRoom.ActionID < 4)
			return;
		try {
			if (pauseStatus) {
				if (Player.Instance.GameCreator && NoOfPlayersInTheRoom () > 1) {
					TempCurrenRoom.NoMaster = true;
					Player.Instance.GameCreator = false;
				}
				TempCurrenRoom.SeatsStatus [Player.Instance.OnlinePlayerID] = 0;
				PlayingFireBaseFather.ParentInstance.UploudThisJson (TempCurrenRoom, false);			
			} else
				StartCoroutine (SetandarrangeCor (pauseStatus));
		} catch (Exception e) {
			Debug.Log ("OnApplicationPause\t" + e.Message);
		}
	}

	#endregion

	private IEnumerator SetandarrangeCor (bool pauseStatus)
	{
		ShowBackBTN ();
		yield return new WaitForSeconds (2.5f);
		try {
			TarneebManager2.Instance.SetAndArrangeMyCards ();
		} catch (Exception e) {
			Debug.Log (e.Message);
		}
	}


	#region implemented abstract members of PlayersManagerParent

	public override void IamBack ()
	{
		TempCurrenRoom.SeatsStatus [Player.Instance.OnlinePlayerID] = 1;
		PlayerIsBack (true);
		PlayingFireBaseFather.ParentInstance.UploudThisJson (TempCurrenRoom, false);
	}

	#endregion

	private void OnDestroy ()
	{
		StopAllCoroutines ();
	}
}