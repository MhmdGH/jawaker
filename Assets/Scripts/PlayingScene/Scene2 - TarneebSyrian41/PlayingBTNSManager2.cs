﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayingBTNSManager2 : PlayingBTNSManagerFather {
	
	public static PlayingBTNSManager2 Instance;

	protected override void Awake(){
		base.Awake ();
		Instance = this;
	}

	public override void BTNSFunction(){
		base.BTNSFunction ();
		if (BTNName.Contains ("BidBTN")) 
			SetBid (GetChar (BTNName, 0) + GetChar (BTNName, 1));
		else if (BTNName.Contains ("RestartBTN"))
			PlayersManager2.Instance.RestartGame ();
	}

	public void SetBid (string BidValue)
	{
		if (BidValue.Contains ("B"))
			BidValue = GetChar (BidValue,0);
		PlayersManager2.Instance.PlayerSetBid (BidValue,Player.Instance.OnlinePlayerID);
		PlayingWindowsManager2.Instance.BidWindow.SetActive (false);
	}
}