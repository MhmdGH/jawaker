﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Database;
using Firebase;
using Firebase.Unity.Editor;

public class PlayingFireBaseManager2 : PlayingFireBaseFather
{
	#region implemented abstract members of PlayingFireBaseFather
	protected override void SetInitials ()
	{
		TempRoomToUpload = new JsonRoomO ("", new string[4]{ "1", "2", "3", "4" },new Game2());
		CurrentRoomUpdate = TempRoomToUpload;
	}
	#endregion

	public static PlayingFireBaseManager2 Instance;

	public override void Awake ()
	{
		base.Awake ();
		Instance = this;
	}

	public override void UpdateRoomNow (string UpdatedRoomJson)
	{
		//Default Values
		JsonUtility.FromJsonOverwrite (UpdatedRoomJson, CurrentRoomUpdate);
		PlayersManager2.Instance.UpdateRoom (CurrentRoomUpdate);
	}
}