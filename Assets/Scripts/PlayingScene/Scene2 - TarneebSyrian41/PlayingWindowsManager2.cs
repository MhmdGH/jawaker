﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayingWindowsManager2 : PlayingWindowsManagerFather {

	public static PlayingWindowsManager2 Instance;
	public GameObject[] BidsPops;
	public GameObject BidWindow,TrumpDetailsWindow,TrumpCard;

	public override void Awake(){
		base.Awake ();
		Instance = this;
	}

	public override void ShowAndSetPartnersPhotos(string[] IMGSUrl){
		OnStartBTNGO.SetActive (false);
		ChoosePartnerGO.SetActive (true);
		for(int i=0;i<ChooseParternersGOs.Length;i++)
			PlayingFBManager2.Instance.SetFPPhoto (IMGSUrl[i],ChooseParternersGOs[i].GetComponent<Image>());
	}

	public void ShowBidWindow(){
		BidWindow.SetActive (true);
	}

	public void ShowBidPop(int BidPopNO,string BidValue){
		StartCoroutine (ShowBidCor(BidPopNO,BidValue));
	}

	public void SetTrumpAndBidder(string TrumpType){
		TrumpDetailsWindow.SetActive (true);
		TrumpDetailsWindow.transform.GetChild(1).GetComponent<Image>().sprite = 
			Resources.Load<Sprite> ("Playing/"+TrumpType);
	}

	private IEnumerator ShowBidCor(int BidPopNO,string BidValue){
		BidsPops [BidPopNO].SetActive (true);
		BidsPops [BidPopNO].transform.GetChild (0).GetComponent<Text> ().text = BidValue;
		yield return new WaitForSeconds (2);
		BidsPops [BidPopNO].SetActive (false);
	}

	public override void StopTimer(){
//		try{
		base.StopTimer ();
//		for (int i = 0; i < BidsPops.Length; i++)
//				BidsPops [i].SetActive (false);}catch{
//		}
	}

	protected override IEnumerator StartThisTimer(int PlayerNO,float WaitingTime,int FunToDOAfterEnd,bool JustCreatorDoIT){
		PlayersTimer [PlayerNO].SetActive (true);
		WaitingTime = WaitingTime*SliderSmothnessMultiplier;
		float ActualTimerTikValue = TimerTikValue*SliderSmothnessMultiplier;
		PlayersTimer [PlayerNO].GetComponent<Slider> ().maxValue = WaitingTime;
		PlayersTimer [PlayerNO].GetComponent<Slider> ().value = WaitingTime;
		while (WaitingTime > 0) {
			PlayersTimer [PlayerNO].GetComponent<Slider> ().value -= ActualTimerTikValue;
			WaitingTime-=ActualTimerTikValue;
			yield return new WaitForSeconds (TimerTikValue*0.1f);
		}
		PlayersTimer [PlayerNO].SetActive (false);
		if (JustCreatorDoIT && Player.Instance.GameCreator)
			PlayersManager2.Instance.MasterTakeControl (FunToDOAfterEnd);
		else if(!JustCreatorDoIT)
			PlayersManager2.Instance.MasterTakeControl (FunToDOAfterEnd);
	}

	public void SetTrumpCard(string CardID){
		StartCoroutine (ShowTrumpCardCor(CardID));
	}

	private IEnumerator ShowTrumpCardCor(string CardID){
		TrumpCard.SetActive (true);
		TrumpCard.GetComponent<Card2> ().CardID = CardID;
		TrumpCard.GetComponent<Card2> ().SetCardCoverOrFace ();
		yield return new WaitForSeconds (2);
		TrumpCard.SetActive (false);
	}

	private void OnDestroy(){
		StopAllCoroutines ();
	}
}