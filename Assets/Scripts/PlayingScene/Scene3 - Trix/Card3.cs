﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Card3 : Card,IPointerDownHandler,IPointerUpHandler{
	#region IPointerUpHandler implementation

	public void OnPointerUp (PointerEventData eventData)
	{
		return;
	}

	#endregion

	#region IPointerDownHandler implementation
	public void OnPointerDown (PointerEventData eventData)
	{
		if (GetComponent<Image> ().color.r < 0.9f)
			return;
		if (CardIsUp) {
			OnEndDragFun ();
			return;
		}
		else {
			PlayersManager3.Instance.UnChooseAllOtherCards ();
			OnBeginDragFun ();
			StopAllCoroutines ();
			StartCoroutine (CardUp ());
		}
	}
	#endregion

	#region implemented abstract members of Card
	public override void ThroughIt (int StartPos)
	{
		GetComponent<Animator> ().SetInteger ("Thorugh",StartPos);
	}
	#endregion

	public void ThroughItTrix (string CardType,int StartPos)
	{
		StartCoroutine (ThroughItTrixCor(CardType,StartPos));
	}

	private IEnumerator ThroughItTrixCor (string CardType, int StartPos)
	{
		GetComponent<Animator> ().SetInteger ("Thorugh",StartPos);
		yield return new WaitForSeconds (0.8f);
		GetComponent<Animator> ().enabled = false;
		GetComponent<RectTransform> ().localScale = new Vector3 (0.6f,0.9f,1);
		GetComponent<RectTransform> ().localRotation = Quaternion.Euler (0,0,0);
		GetComponent<RectTransform>().SetParent(PlayersManager3.Instance.TrixLists [TargetedList(CardType)].transform);
		GetComponent<RectTransform> ().SetSiblingIndex (SetSiblingIndexOrder());
		yield return StartCoroutine (PlayersManager3.Instance.SetAndArrangeAvailableTrixCardsCor ());
	}

	private int SetSiblingIndexOrder ()
	{
		int ThisCardNO = int.Parse (name);
		if (ThisCardNO > int.Parse (transform.parent.transform.GetChild (0).name))
			return 0;
		else
			return transform.GetSiblingIndex ();
	}

	private int TargetedList(string ThrowdCardID){
		if (ThrowdCardID == "H")
			return 0;
		else if (ThrowdCardID == "D")
			return 1;
		else if (ThrowdCardID == "S")
			return 2;
		else
			return 3;
	}

	#region implemented abstract members of Card
	public override void SendCard ()
	{
		PlayersManager3.Instance.ThrowCard (this.gameObject);
	}
	#endregion
}