﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public class LanguangeManager3 : LanguangeManagerFather {

	public Text[] Game3Txts; 

	public override void Start(){
		base.Start ();try{
		if(IsArabic)
				SetTheFukinArabicLang ();}catch{Debug.Log ("////////////////////////////////");
		}
	}

	public override void SetTheFukinArabicLang ()
	{
		base.SetTheFukinArabicLang ();
		SetFontForThisTxts (Game3Txts);
		Game3Txts [0].text = ArabicFixer.Fix ("اختر نوع اللعب");
		Game3Txts [1].text = ArabicFixer.Fix ("اختر أوراق الدبل");
		Game3Txts [2].text = ArabicFixer.Fix ("تم");
	}
}
