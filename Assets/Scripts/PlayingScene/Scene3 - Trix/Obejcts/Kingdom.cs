using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Kingdom
{
	public int KingdomPlayerID;
	// 0 King, 1 Queens, 2 Ltoosh, 3 Diamonds, 4 Trix,5 Complex
	public int CurrentKingdomTypeID;
	public int[] PlayerUnselectedKingdomTypes,IsReady,Counters;
	public bool FirstGame;
	public PlayersDoubles[] CurrentplayerDoubles;
	public TrixOClass Trix;
	public bool IsComplex,IsPartners;
	public Kingdom(){
		PlayerUnselectedKingdomTypes = new int[6]{1,1,1,1,1,0};
		IsReady = new int[4]{0,0,0,0};
		CurrentKingdomTypeID = -1;
		KingdomPlayerID = -1;
		CurrentplayerDoubles = new PlayersDoubles[4];
		for (int i = 0; i < CurrentplayerDoubles.Length; i++)
			CurrentplayerDoubles [i] = new PlayersDoubles ();
		FirstGame = true;
		//0 Queens Counter, 1 Diamonds Counter, 2 KingdomEndsCounter
		Counters = new int[3]{0,0,0};
		Trix = new TrixOClass ();
		IsComplex = false;
		IsPartners = false;
	}
}