using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PlayersDoubles
{
	public int[] PlayerDoubles;
	public PlayersDoubles(){
		PlayerDoubles = new int[5]{0,0,0,0,0};
	}
}