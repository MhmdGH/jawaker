﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class PlayersManager3 : PlayersManagerParent
{
	public static PlayersManager3 Instance;
	public GameObject CardGO;
	public GameObject[] TrixLists;
	public GameObject[] OtherPlayersCardsGO;
	private string[] NextTrixCardList;

	public override void Awake ()
	{
		base.Awake ();
		Instance = this;
	}

	public override void Start ()
	{
		TempCurrenRoom = new JsonRoomO ("Default", new string[4]{ "0", "0", "0", "0" }, new Game3 ());
		base.Start ();
		CardsListGO = PlayersCardsGO [0].transform.GetChild (1).GetChild (0).gameObject;
		NextTrixCardList = new string[8]{ "", "", "", "", "", "", "", "" };
		SceneDoneLoading = false;
	}

	public void UnChooseAllOtherCards(){
		for (int i = 0; i < TrixManager3.Instance.MyPlayerCardsGO.Length; i++)
			try{TrixManager3.Instance.MyPlayerCardsGO[i].GetComponent<Card2> ().OnEndDragFun (true);}catch{
		}
	}

	//Main Reciever Fun
	public override void UpdateRoom (JsonRoomO CurrentRoomJSON)
	{
		base.UpdateRoom (CurrentRoomJSON);
		if (TempCurrenRoom.NoMaster)
			AssignNewMasterOrRemoveRoom ();
		if (TempCurrenRoom.Update == 0) {
			PlayingFireBaseManager3.Instance.SetOnDissconnectHandle ();
			return;
		}
		PlayingWindowsManager3.Instance.StopTimer ();
		switch (TempCurrenRoom.ActionID) {
		case -1:
			UpdateRoomPhaseMinus1 ();
			break;
		case 0:
			UpdateRoomPhase0 ();
			break;
		case 2:
			UpdateRoomPhase2 ();
			break;
		case 3:
			UpdateRoomPhase3 ();
			break;
		case 4:
			UpdateRoomPhase4 ();
			break;
		case 5:
			UpdateRoomPhase5 ();
			break;
		case 6:
			UpdateRoomPhase6 ();
			break;
		case 7:
			UpdateRoomPhase7 ();
			break;
		case 8:
			UpdateRoomPhase8 ();
			break;
		case 9:
			UpdateRoomPhase9 ();
			break;
		case 10:
			UpdateRoomPhase10 ();
			break;
		case 11:
			UpdateRoomPhase11 ();
			break;
		case 12:
			UpdateRoomPhase12 ();
			break;
		case 13:
			UpdateRoomPhase13 ();
			break;
		case 14:
			UpdateRoomPhase14 ();
			break;
		case 15:
			UpdateRoomPhase15 ();
			break;
		case 16:
			UpdateRoomPhase16 ();
			break;
		}
	}

	void UpdateRoomPhaseMinus1 ()
	{
		AudioManager.Instance.PlaySound (0);
		if (!Player.Instance.GameCreator)
			return;
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++)
			TempCurrenRoom.PlayersReady [i] = false;
		SetPartnerAndComplex ();

		TrixManager3.Instance.SetCardsForPlayers ();
		TempCurrenRoom.Available = false;
		for (int i = 0; i < 4; i++)
			TempCurrenRoom.GameOB.Cards.PlayersCards [i].PlayerCardsList = TrixManager3.Instance.Cards.PlayersCards [i].PlayerCardsList;
		if (TempCurrenRoom.GameOB.KingdomsDetails.FirstGame)
			FirstGameConfigurations ();
		SetComplexUnSelected ();
		TempCurrenRoom.ActionID = 4;
		PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase0 ()
	{
		if (TempCurrenRoom.ReloadScene)
			ReloadScene ();
	}

	void ReloadScene ()
	{
		TempCurrenRoom.ReloadScene = false;
		TempCurrenRoom.ActionID = 3;
		SceneDoneLoading = true;
		if (Player.Instance.GameCreator)
			PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}

	public void MasterTakeControl (int funToDOAfterEnd)
	{
		if (funToDOAfterEnd == 1)
			AutoChooseKingdom ();
		if (funToDOAfterEnd == 2)
			AutoSetDoubleReady ();
		else if (funToDOAfterEnd == 3)
			MasterOrPlayerThrowCardNOW ();
	}

	void AutoSetDoubleReady ()
	{
		//Check Player Is Here
		int SeatStatus = TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.PlayerBidTurn];
		if (SeatStatus == 1	&& TempCurrenRoom.GameOB.PlayerBidTurn == Player.Instance.OnlinePlayerID)
			PlayingBTNSManager3.Instance.PlayerIsReady ();
		else if (SeatStatus == 0 &&	Player.Instance.GameCreator)
			PlayerIsReady (TempCurrenRoom.GameOB.PlayerBidTurn);
	}

	private void AutoChooseKingdom ()
	{
		int SeatStatus = TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.KingdomsDetails.KingdomPlayerID];
		if (TempCurrenRoom.GameOB.KingdomsDetails.KingdomPlayerID == Player.Instance.OnlinePlayerID && SeatStatus == 1) {
			PlayingWindowsManager3.Instance.ChooseKingdomTypeGO.transform.parent.gameObject.SetActive (false);
			AutoChooseKingdomNOW ();
			return;
		} else if (Player.Instance.GameCreator && SeatStatus == 0)
			AutoChooseKingdomNOW ();
	}

	private void AutoChooseKingdomNOW ()
	{
		for (int i = 0; i < 6; i++) {
			if (TempCurrenRoom.GameOB.KingdomsDetails.PlayerUnselectedKingdomTypes [i] == 1) {
				PlayerChooseKingdomType (i);
				return;
			}
		}
	}

	void MasterOrPlayerThrowCardNOW ()
	{
		int PlayerSeatStatus = TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay];
		//If Player Is Here
		if (MyTurnToPlay () && PlayerSeatStatus == 1) {
			if (PlayersManagerParent.PlayerHoldingCard) {
				GameObject CardHolded = CanvasGO.GetComponent<RectTransform> ().GetChild (CanvasGO.GetComponent<RectTransform> ().childCount - 1).gameObject;
				CardHolded.GetComponent<Card3> ().ReturnCardBack ();
				CardHolded.GetComponent<Card3> ().SendCard ();
				CheckPlayerKick ();
				return;	 
			} else {
				IfPlayerHoldingCardGetItBack ();
				List<GameObject> AvailbeCardsToThrow = new List<GameObject> ();
				for (int i = 0; i < CardsListGO.transform.childCount; i++)
					if (CardsListGO.transform.GetChild (i).GetComponent<Image> ().color.r == 1)
						AvailbeCardsToThrow.Add (CardsListGO.transform.GetChild (i).gameObject);
				if (AvailbeCardsToThrow.Count > 0) {
					AvailbeCardsToThrow [GetBestCardFromThisList (AvailbeCardsToThrow)].GetComponent<Card> ().SendCard ();
					CheckPlayerKick ();
					return;
				}
			}
			ThrowCardNow (FirstAvailabeCardOnOtherPlayerTRIX ());
			//If Player Not Here, Master Do IT
		} else if (Player.Instance.GameCreator && PlayerSeatStatus == 0) {
			TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay] = 0;
			if (TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID != 4)
				ThrowCardNow (FirstAvailabeCardOnOtherPlayer ());
			else
				ThrowCardNow (FirstAvailabeCardOnOtherPlayerTRIX ());
		}
	}

	int GetBestCardFromThisList (List<GameObject> availbeCardsToThrow)
	{
		int PlayerIDTurn = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		List<string> availbeCardsToThrowIDsStrings = new List<string> ();
		for (int i = 0; i < availbeCardsToThrow.Count; i++)
			availbeCardsToThrowIDsStrings.Add (availbeCardsToThrow [i].GetComponent<Card> ().CardID);

		if (ThisIsThePlayerWhoStartsTheRound ())
				return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings, false);

		if (!IsPartners() || (IsPartners () && MyPartenerCardIsTheLowestOne())) {
			if (IsComplex ()) {//Is Complex()
				if (HaveTheKingCard (availbeCardsToThrowIDsStrings)) {
					if (PlayedType () == "H")
					if (GetCardNO (HighestThrowdCard (PlayedType ())) > 13)
						return  ThrowKingCard (availbeCardsToThrowIDsStrings, true);
					if (!ThisPlayerHaveThisCardType (availbeCardsToThrowIDsStrings, PlayedType ()))
						return  ThrowKingCard (availbeCardsToThrowIDsStrings, true);
				} else if (HaveQueenCard (availbeCardsToThrowIDsStrings)) {
					if (GetCardNO (HighestThrowdCard (PlayedType ())) >= 13)
						return ThrowqueenCard (availbeCardsToThrowIDsStrings, true);
					if (!ThisPlayerHaveThisCardType (availbeCardsToThrowIDsStrings, PlayedType ()))
						return ThrowqueenCard (availbeCardsToThrowIDsStrings, true);
				} else {
					if (!ThisPlayerHaveThisCardType (availbeCardsToThrowIDsStrings, PlayedType ()))
						return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings, true);
					else
						return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings, false);
				}
			} else {//Is Not Complex()
				if (IsAKingType ()&&HaveTheKingCard(availbeCardsToThrowIDsStrings)) {
					if (PlayedType () == "H")
					if (GetCardNO (HighestThrowdCard (PlayedType ())) > 13)
						return ThrowKingCard (availbeCardsToThrowIDsStrings, true);
					if (!ThisPlayerHaveThisCardType (availbeCardsToThrowIDsStrings, PlayedType ()))
						return ThrowKingCard (availbeCardsToThrowIDsStrings, true);
				} else if (IsAQueenType ()&&HaveQueenCard(availbeCardsToThrowIDsStrings)) {
					if (GetCardNO (HighestThrowdCard (PlayedType ())) >= 13)
						return ThrowqueenCard (availbeCardsToThrowIDsStrings, true);
					if (!ThisPlayerHaveThisCardType (availbeCardsToThrowIDsStrings, PlayedType ()))
						return ThrowqueenCard (availbeCardsToThrowIDsStrings, true);
				} else {
					if (!ThisPlayerHaveThisCardType (availbeCardsToThrowIDsStrings, PlayedType ()))
						return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings, true);
					else
						return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings, false);
				}
			}
		}
		Debug.Log ("Heeeeeeeeeeeeeeeeeeere");
			return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings, false);

		if (IsAKingType () || IsAQueenType () || ISComplexType ()) {
			//Player Not Starting The Round
			//Not the one who starts the round, tow conditions
			string PartnerThrowdCard = TempCurrenRoom.GameOB.CurrentRound.PlayersCards [MyPartnerID (PlayerIDTurn)];
			//1.my partner played high card
			if ((IsAKingType () || ISComplexType ()) && HaveTheKingCard (availbeCardsToThrowIDsStrings))
				return ThrowKingCard (availbeCardsToThrowIDsStrings, MyPartenerCardIsTheLowestOne ());
			else if ((IsAQueenType () || ISComplexType ()) && HaveQueenCard (availbeCardsToThrowIDsStrings))
				return ThrowqueenCard (availbeCardsToThrowIDsStrings, MyPartenerCardIsTheLowestOne ());
		}
		return 0;
	}

	private string FirstAvailabeCardOnOtherPlayer ()
	{
		List<string> ThisPlayerCardsList = new List<string> ();
		ThisPlayerCardsList = TempCurrenRoom.GameOB.Cards.
			PlayersCards [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay].PlayerCardsList;

		if (ThisIsThePlayerWhoStartsTheRound ())
			return ThisPlayerCardsList[GetHighestOrSmallestCardIFromThisList (ThisPlayerCardsList, false)];

		List<string> availbeCardsToThrowIDsStrings = new List<string> ();
		if (ThisPlayerHaveThisCardType (ThisPlayerCardsList, PlayedType ())) {
			for (int i = 0; i < ThisPlayerCardsList.Count; i++)
				if (ThisPlayerCardsList [i].StartsWith (PlayedType ()))
					availbeCardsToThrowIDsStrings.Add (ThisPlayerCardsList [i]);
		} else
			availbeCardsToThrowIDsStrings = ThisPlayerCardsList;

		int FirstAvailbleITerator = 0;
		for (int i = 0; i < ThisPlayerCardsList.Count; i++)
			if (ThisPlayerCardsList [i].Length > 2) {
				FirstAvailbleITerator = i;
				break;
			}

		if (!IsPartners() || (IsPartners () && MyPartenerCardIsTheLowestOne())) {
			if (IsComplex ()) {//Is Complex()
				if (HaveTheKingCard (availbeCardsToThrowIDsStrings)) {
					if (PlayedType () == "H")
					if (GetCardNO (HighestThrowdCard (PlayedType ())) > 13)
						return availbeCardsToThrowIDsStrings[ThrowKingCard (availbeCardsToThrowIDsStrings, true)];
					if (!ThisPlayerHaveThisCardType (availbeCardsToThrowIDsStrings, PlayedType ()))
						return availbeCardsToThrowIDsStrings[ThrowKingCard (availbeCardsToThrowIDsStrings, true)];
				} else if (HaveQueenCard (availbeCardsToThrowIDsStrings)) {
					if (GetCardNO (HighestThrowdCard (PlayedType ())) >= 13)
						return availbeCardsToThrowIDsStrings[ThrowqueenCard (availbeCardsToThrowIDsStrings, true)];
					if (!ThisPlayerHaveThisCardType (availbeCardsToThrowIDsStrings, PlayedType ()))
						return availbeCardsToThrowIDsStrings[ThrowqueenCard (availbeCardsToThrowIDsStrings, true)];
				} else {
					if (!ThisPlayerHaveThisCardType (availbeCardsToThrowIDsStrings, PlayedType ()))
						return availbeCardsToThrowIDsStrings[GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings, true)];
					else
						return availbeCardsToThrowIDsStrings[GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings, false)];
				}
			} else {//Is Not Complex()
				if (IsAKingType ()&&HaveTheKingCard(availbeCardsToThrowIDsStrings)) {
					if (PlayedType () == "H")
					if (GetCardNO (HighestThrowdCard (PlayedType ())) > 13)
						return availbeCardsToThrowIDsStrings[ThrowKingCard (availbeCardsToThrowIDsStrings, true)];
					if (!ThisPlayerHaveThisCardType (availbeCardsToThrowIDsStrings, PlayedType ()))
						return availbeCardsToThrowIDsStrings[ThrowKingCard (availbeCardsToThrowIDsStrings, true)];
				} else if (IsAQueenType ()&&HaveQueenCard(availbeCardsToThrowIDsStrings)) {
					if (GetCardNO (HighestThrowdCard (PlayedType ())) >= 13)
						return availbeCardsToThrowIDsStrings[ThrowqueenCard (availbeCardsToThrowIDsStrings, true)];
					if (!ThisPlayerHaveThisCardType (availbeCardsToThrowIDsStrings, PlayedType ()))
						return availbeCardsToThrowIDsStrings[ThrowqueenCard (availbeCardsToThrowIDsStrings, true)];
				} else {
					if (!ThisPlayerHaveThisCardType (availbeCardsToThrowIDsStrings, PlayedType ()))
						return availbeCardsToThrowIDsStrings[GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings, true)];
					else
						return availbeCardsToThrowIDsStrings[GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings, false)];
				}
			}
		}
		Debug.Log ("Heeeeeeeeeeeeeeeeeeere");
		return availbeCardsToThrowIDsStrings [GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings, false)];
		int PlayerIDTurn = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		string PartnerThrowdCard = TempCurrenRoom.GameOB.CurrentRound.PlayersCards [MyPartnerID (PlayerIDTurn)];
		//Not the one who starts the round, tow conditions
		//return first availble That match playedtype card
		if (ThisPlayerHaveThisCardType (ThisPlayerCardsList, PlayedType ())) {
			for (int i = 0; i < ThisPlayerCardsList.Count; i++)
				if (ThisPlayerCardsList [i].StartsWith (PlayedType ()))
					return ThisPlayerCardsList [i];
		} else {
			if (((IsAKingType () || ISComplexType ()) && HaveTheKingCard (availbeCardsToThrowIDsStrings)) || ((IsAQueenType () || ISComplexType ()) && HaveQueenCard (availbeCardsToThrowIDsStrings))) {
				if (IsAKingType () || ISComplexType ())
					return availbeCardsToThrowIDsStrings [ThrowKingCard (availbeCardsToThrowIDsStrings, MyPartenerCardIsTheLowestOne ())];
				else if (IsAQueenType () || ISComplexType ())
					return availbeCardsToThrowIDsStrings [ThrowqueenCard (availbeCardsToThrowIDsStrings, MyPartenerCardIsTheLowestOne ())];
			}
		}
		return ThisPlayerCardsList [FirstAvailbleITerator];
	}

	private List<string> GetPlayersThrowdCardsList ()
	{
		List<string> NewPlayersCardsList = new List<string> ();
		for (int i = 0; i < 4; i++)
			NewPlayersCardsList.Add (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i]);
		return NewPlayersCardsList;
	}

	private string PlayedType ()
	{
		return GetChar (TempCurrenRoom.GameOB.CurrentRound.
			PlayersCards [TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound], 0);
	}

	string HighestThrowdCard (string CardType)
	{
		List<string> ThisPlayersThrowdCardsList = GetPlayersThrowdCardsList ();
		ThisPlayersThrowdCardsList.Sort ();
		for (int i = 3; i >= 0; i--) {
			if (ThisPlayersThrowdCardsList [i].Length < 2)
				continue;
			if (GetCardType (ThisPlayersThrowdCardsList [i]) == CardType)
				return ThisPlayersThrowdCardsList [i];
		}
		for (int i = 0; i < 4; i++) {
			if (ThisPlayersThrowdCardsList [i].Length < 2)
				continue;
			return ThisPlayersThrowdCardsList [i];
		}
		return ThisPlayersThrowdCardsList [3];
	}


	int ThrowqueenCard (List<string> availbeCardsToThrowIDsStrings, bool ThrowQueen)
	{
		Debug.Log ("ThrowQueen:"+ThrowQueen);
		int AvailableCardsNo = 0, AvaibleIterator = 0, QueenIterator = -1, OtherThanQueenIterator = 0;
		for (int i = 0; i < availbeCardsToThrowIDsStrings.Count; i++)
			if (availbeCardsToThrowIDsStrings [i].Length > 2) {
				if (availbeCardsToThrowIDsStrings [i].Contains ("12"))
					QueenIterator = i;
				else
					OtherThanQueenIterator = i;
				AvaibleIterator = i;
				AvailableCardsNo++;
			}

		if (AvailableCardsNo == 1)
			return AvaibleIterator;

		if (QueenIterator == -1)
			QueenIterator = AvaibleIterator;

		if (ThrowQueen)
			return QueenIterator;

		return OtherThanQueenIterator;
	}

	private bool IsAKingType ()
	{
		return TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID == 0;
	}

	private bool ISComplexType ()
	{
		return TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID == 5;
	}

	private bool IsAQueenType ()
	{
		return TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID == 1;
	}

	private bool HaveTheKingCard (List<string> ThisPlayerCardsList)
	{
		for (int i = 0; i < ThisPlayerCardsList.Count; i++)
			if (ThisPlayerCardsList [i] == "H13")
				return true;
		return false;
	}

	private bool HaveQueenCard (List<string> ThisPlayerCardsList)
	{
		for (int i = 0; i < ThisPlayerCardsList.Count; i++) {
			if (ThisPlayerCardsList [i].Length < 2)
				continue;
			if (GetCardNO (ThisPlayerCardsList [i]) == 12)
				return true;
		}
		return false;
	}

	int ThrowKingCard (List<string> availbeCardsToThrowIDsStrings, bool ThrowKing)
	{
		Debug.Log ("ThrowKing:"+ThrowKing);
		int AvailableCardsNo = 0, AvaibleIterator = -1, KingIterator = -1, OtherThanKingIterator = -1;
		for (int i = 0; i < availbeCardsToThrowIDsStrings.Count; i++)
			if (availbeCardsToThrowIDsStrings [i].Length > 2) {
				if (availbeCardsToThrowIDsStrings [i] == "H13")
					KingIterator = i;
				else {
					if (OtherThanKingIterator == -1)
						OtherThanKingIterator = i;
				}
				if (AvaibleIterator == -1)
					AvaibleIterator = i;
				AvailableCardsNo++;
			}

		if (AvailableCardsNo == 1)
			return AvaibleIterator;

		if (KingIterator == -1)
			KingIterator = AvaibleIterator;

		if (ThrowKing)
			return KingIterator;

		return OtherThanKingIterator;

	}

	private string FirstAvailabeCardOnOtherPlayerTRIX ()
	{
		List<string> ThisPlayerCardsList = new List<string> ();
		for (int i = 0; i < 13; i++)
			ThisPlayerCardsList = TempCurrenRoom.GameOB.
				Cards.PlayersCards [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay].PlayerCardsList;
		for (int i = 0; i < ThisPlayerCardsList.Count; i++) {
			for (int u = 0; u < 8; u++) {
				if (ThisPlayerCardsList [i] == TempCurrenRoom.GameOB.KingdomsDetails.Trix.AvailableCardPlace [u] || ThisPlayerCardsList [i].Contains ("11"))
					return ThisPlayerCardsList [i];
			}
		}
		return "0";
	}

	public void RestartGame ()
	{
		PlayingWindowsManager3.Instance.WinsWindow.SetActive (false);
		ResetCurrentKingdomData ();
		Game3 NewGameOB = new Game3 ();
		for (int i = 0; i < NewGameOB.PlayersScores.Length; i++)
			NewGameOB.PlayersScores [i] = 0;
		TempCurrenRoom.GameOB = NewGameOB;
		TempCurrenRoom.ReloadScene = true;
		TempCurrenRoom.ActionID = 0;
		PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private bool IsPartners ()
	{
		return TempCurrenRoom.GameOB.KingdomsDetails.IsPartners;
	}

	private bool IsComplex ()
	{
		return TempCurrenRoom.GameOB.KingdomsDetails.IsComplex;
	}

	private int ThisWinnerID;

	void UpdateRoomPhase16 ()
	{
		//Game End, Show Winner
		List<int> PlayersScores = new List<int> ();
		for (int i = 0; i < 4; i++)
			PlayersScores.Add (TempCurrenRoom.GameOB.PlayersScores [i]);
		PlayersScores.Sort ();
		int WinnerID = 0;
		for (int i = 0; i < 4; i++) {
			if (TempCurrenRoom.GameOB.PlayersScores [i] == PlayersScores [3]) {
				if (TempCurrenRoom.GameOB.KingdomsDetails.IsPartners) {
					if (i == 0 || i == 2)
						WinnerID = 1;
					else
						WinnerID = 2;
				} else {
					WinnerID = i;
				}
			}
		}
		if (WinnerID == 1)
			ThisWinnerID = 0;
		else
			ThisWinnerID = 1;
		AddWinsPointsForPlayers (WinnerID);
		if (MainFireBaseManager.Instance.IsACompition)
			InnerSetCompData ();
		PlayingWindowsManager3.Instance.ShowWindWindow (ThisWinnerID);
	}

	private void InnerSetCompData ()
	{
		if (Player.Instance.GameCreator) {
			for (int i = 0; i < TempCurrenRoom.DatabasePlayersIDs.Length; i++)
				if (TempCurrenRoom.DatabasePlayersIDs [i].Length < 4)
					return;
			if (ThisWinnerID == 0) {
				SetCompData (TempCurrenRoom.DatabasePlayersIDs [0], TempCurrenRoom.DatabasePlayersIDs [2],
					TempCurrenRoom.PlayersFBIDs [0], TempCurrenRoom.PlayersFBIDs [2]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [1]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [3]);
			} else {
				SetCompData (TempCurrenRoom.DatabasePlayersIDs [1], TempCurrenRoom.DatabasePlayersIDs [3],
					TempCurrenRoom.PlayersFBIDs [1], TempCurrenRoom.PlayersFBIDs [3]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [0]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [2]);
			}
		}
	}

	private void AddWinsPointsForPlayers (int WinnerID)
	{
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (TempCurrenRoom.GameOB.KingdomsDetails.IsPartners) {
			if (WinnerID == 0 && (PlayerID == 0 || PlayerID == 2)) {
				Player.Instance.AddXPsAndCLubPoints (XPsValue);
				Player.Instance.Coins += ScoreValue;
			} else if (WinnerID == 1 && (PlayerID == 1 || PlayerID == 3)) {
				Player.Instance.AddXPsAndCLubPoints (XPsValue);
				Player.Instance.Coins += ScoreValue;
			}
		} else {
			if (PlayerID == WinnerID) {
				Player.Instance.AddXPsAndCLubPoints (XPsValue);
				Player.Instance.Coins += ScoreValue;
			}
		}
	}

	void UpdateRoomPhase15 ()
	{
		//PlayerKingdomEnds
		if (!Player.Instance.GameCreator)
			return;
		StartCoroutine (UpdateRoomPhase15Cor ());
	}

	private IEnumerator UpdateRoomPhase15Cor ()
	{
		TempCurrenRoom.GameOB.KingdomsDetails.Counters [2]++;
		if (TempCurrenRoom.GameOB.KingdomsDetails.Counters [2] >= 4) {
			TempCurrenRoom.ActionID = 16;
			PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
		} else {
			Kingdom NewKingdom = new Kingdom ();
			NewKingdom.IsPartners = TempCurrenRoom.GameOB.KingdomsDetails.IsPartners;
			NewKingdom.IsComplex = TempCurrenRoom.GameOB.KingdomsDetails.IsComplex;
			if (NewKingdom.IsComplex)
				NewKingdom.PlayerUnselectedKingdomTypes = new int[6]{ 0, 0, 0, 0, 1, 1 };
			NewKingdom.KingdomPlayerID = NextPlayerKingdomID ();
			NewKingdom.FirstGame = false;
			NewKingdom.Counters [2] = TempCurrenRoom.GameOB.KingdomsDetails.Counters [2];
			TempCurrenRoom.GameOB.KingdomsDetails = NewKingdom;
			NewRound ();
		}
		yield return null;
	}

	int NextPlayerKingdomID ()
	{
		int PlayerKingdomID = TempCurrenRoom.GameOB.KingdomsDetails.KingdomPlayerID;
		PlayerKingdomID++;
		if (PlayerKingdomID >= 4)
			PlayerKingdomID = 0;
		return PlayerKingdomID;
	}

	void UpdateRoomPhase14 ()
	{
		//FullRound Ends
		if (Player.Instance.OnlinePlayerID != -1)
			OnRoundEndsResultAndResets ();
		if (!Player.Instance.GameCreator)
			return;
		if (PlayerKingdomEnds ()) {
			TempCurrenRoom.ActionID = 15;
			PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
		} else
			NewRound ();
	}

	bool PlayerKingdomEnds ()
	{
		int NOofUnselected = 5;
		if (TempCurrenRoom.GameOB.KingdomsDetails.IsComplex)
			NOofUnselected = 6;
		for (int i = 0; i < NOofUnselected; i++)
			if (TempCurrenRoom.GameOB.KingdomsDetails.PlayerUnselectedKingdomTypes [i] == 1)
				return false;
		return true;
	}

	private void NewRound ()
	{
		//New Game In Same Kingdom
		Game3 NewGameOB = new Game3 ();
		for (int i = 0; i < NewGameOB.PlayersScores.Length; i++)
			NewGameOB.PlayersScores [i] = TempCurrenRoom.GameOB.PlayersScores [i];
		NewGameOB.KingdomsDetails = TempCurrenRoom.GameOB.KingdomsDetails;
		TempCurrenRoom.GameOB = NewGameOB;
		TempCurrenRoom.ReloadScene = true;
		TempCurrenRoom.ActionID = 0;
		PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void OnRoundEndsResultAndResets ()
	{
		for (int i = 0; i < 4; i++)
			PlayingWindowsManager3.Instance.TeamsScores [FromOnlineToLocal (i)].GetComponent<Text> ().text = TempCurrenRoom.GameOB.PlayersScores [i].ToString ();
		ResetCurrentKingdomData ();
	}

	private void ResetCurrentKingdomData ()
	{
		for (int o = 0; o < 4; o++) {
			if (TrixLists [o].transform.childCount > 0)
				for (int i = 0; i < TrixLists [o].transform.childCount; i++)
					Destroy (TrixLists [o].transform.GetChild (i).gameObject);
		}
		if (!Player.Instance.GameCreator)
			return;
		for (int i = 0; i < 4; i++) {
			TempCurrenRoom.GameOB.KingdomsDetails.IsReady [i] = 0;
			for (int u = 0; u < 5; u++) {
				TempCurrenRoom.GameOB.KingdomsDetails.CurrentplayerDoubles [i].PlayerDoubles [u] = 0;
			}
		}
		TempCurrenRoom.GameOB.KingdomsDetails.Trix = new TrixOClass ();
	}

	void UpdateRoomPhase13 ()
	{
		//Full Round Ends
		for (int i = 0; i < 4; i++)
			PlayingWindowsManager3.Instance.PlayersScoresGO [i].GetComponent<Text> ().text = "0";

		if (!Player.Instance.GameCreator)
			return;

		if (TempCurrenRoom.GameOB.KingdomsDetails.IsPartners) {
			int Team1Score = TempCurrenRoom.GameOB.CurrentRound.PlayersScores [0] + TempCurrenRoom.GameOB.CurrentRound.PlayersScores [2];
			int Team2Score = TempCurrenRoom.GameOB.CurrentRound.PlayersScores [1] + TempCurrenRoom.GameOB.CurrentRound.PlayersScores [3];
			TempCurrenRoom.GameOB.CurrentRound.PlayersScores [0] = Team1Score;
			TempCurrenRoom.GameOB.CurrentRound.PlayersScores [2] = Team1Score;
			TempCurrenRoom.GameOB.CurrentRound.PlayersScores [1] = Team2Score;
			TempCurrenRoom.GameOB.CurrentRound.PlayersScores [3] = Team2Score;
		}

		for (int i = 0; i < 4; i++) {
			int CurrentRoundPlayerScore = TempCurrenRoom.GameOB.CurrentRound.PlayersScores [i];
			TempCurrenRoom.GameOB.PlayersScores [i] += CurrentRoundPlayerScore;
		}
		TempCurrenRoom.ActionID = 14;
		PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase12 ()
	{
		//COllect Cards Here And Throw it to winner Player
		if (TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID != 4)
			StartCoroutine (CollectionCor ());
		else {
			if (!Player.Instance.GameCreator)
				return;
			SetPlayersGreenOrRed ();
			TempCurrenRoom.GameOB.CurrentRound.PlayersScores [TempCurrenRoom.GameOB.KingdomsDetails.Trix.PlayersWinnersOrder [0]] += 200;
			TempCurrenRoom.GameOB.CurrentRound.PlayersScores [TempCurrenRoom.GameOB.KingdomsDetails.Trix.PlayersWinnersOrder [1]] += 150;
			TempCurrenRoom.GameOB.CurrentRound.PlayersScores [TempCurrenRoom.GameOB.KingdomsDetails.Trix.PlayersWinnersOrder [2]] += 100;
			TempCurrenRoom.GameOB.CurrentRound.PlayersScores [TempCurrenRoom.GameOB.KingdomsDetails.Trix.PlayersWinnersOrder [3]] += 50;
			TempCurrenRoom.ActionID = 13;
			PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
		}
	}

	void SetPlayersGreenOrRed ()
	{
		for (int i = 0; i < TempCurrenRoom.GreenOrRed.Length; i++) {
			if (TempCurrenRoom.GameOB.CurrentRound.PlayersScores [i] > 0)
				TempCurrenRoom.GreenOrRed [i] = 1;
			else if (TempCurrenRoom.GameOB.CurrentRound.PlayersScores [i] < 0)
				TempCurrenRoom.GreenOrRed [i] = 2;
		}
	}

	private IEnumerator CollectionCor ()
	{
		RefreshDoublesCard ();
		SetAvailableCardsOnce = false;
		ReturnsMyCardsAvailibity ();
		int LastWinnerID = TempCurrenRoom.GameOB.CollectorID;
		for (int i = 0; i < 4; i++)
			PlayingWindowsManager3.Instance.PlayersScoresGO [FromOnlineToLocal (i)].GetComponent<Text> ().text =
				TempCurrenRoom.GameOB.CurrentRound.PlayersScores [i].ToString ();
		yield return new WaitForSeconds (1f);
		CardsThrowerMangaer.Instance.CollectCardsToThisPlayer (FromOnlineToLocal (LastWinnerID));
		yield return new WaitForSeconds (1f);
		if (Player.Instance.GameCreator) {
			if (FullRoundEnds ()) {
				TempCurrenRoom.ActionID = 13;
				PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
			} else {
				TempCurrenRoom.ActionID = 10;
				TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound = LastWinnerID;
				TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = LastWinnerID;
				PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
			}
		}
	}

	void RefreshDoublesCard ()
	{
		Kingdom CurrentKingdomsDetails = TempCurrenRoom.GameOB.KingdomsDetails;
		for (int i = 0; i < 4; i++)
			PlayingWindowsManager3.Instance.RefreshDoublesGO (FromOnlineToLocal (i), CurrentKingdomsDetails.CurrentplayerDoubles [i]);	
	}

	#region implemented abstract members of PlayersManagerParent

	public override void SetLastRoundCards ()
	{
		string[] LastRoundCards = new string[4];
		for (int i = 0; i < 4; i++)
			LastRoundCards [i] = TempCurrenRoom.GameOB.CurrentRound.PlayersCards [FromOnlineToLocal (i)];
		LastRoundGO.GetComponent<LastRound> ().SetCards (LastRoundCards);
	}

	#endregion

	bool FullRoundEnds ()
	{
		if (TempCurrenRoom.GameOB.CurrentRound.RoundNO >= 13)
			return true;
		else
			return false;
	}

	private void UpdateRoomPhase11 ()
	{
		//Just A Hanger
		if (TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID != 4)
			return;
	}

	private void UpdateRoomPhase10 ()
	{

		if (TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID != 4 && !RoundClosed ()) {
			PlayingWindowsManager3.Instance.SetTimer (FromOnlineToLocal (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay),
				TimerWaitingTime (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay), 3, false);
			SetAvailableCards ();
		}
		int PrevPlayerID = PreviosTurn (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay);
		if (MyTurnToPlay ()) {
			if (IsThisFuckerTrulyTurnToPlay()) {
				StartCoroutine (UpdateRoomPhase10Cor());
			} else {
				IfPlayerHoldingCardGetItBack ();
				TrixManager3.Instance.SetAndArrangeMyCards ();
			}
		}
		if (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [PrevPlayerID].Length > 2)
			ThrowThisCard (PrevPlayerID, TempCurrenRoom.GameOB.CurrentRound.PlayersCards [PrevPlayerID]);
		else if (TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID == 4) {
			if (!RoundClosed ())
				PlayingWindowsManager3.Instance.SetTimer (FromOnlineToLocal (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay),
					TimerWaitingTime (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay), 3, false);
			if (MyTurnToPlay () && !ThereisCardAvailable ())
				TrixNextTurn ();
		}
	}

	private IEnumerator UpdateRoomPhase10Cor(){
		yield return new WaitForSeconds (1);
		try{GameObject CardHolded = CanvasGO.GetComponent<RectTransform> ().GetChild (CanvasGO.GetComponent<RectTransform> ().childCount - 1).gameObject;
			CardHolded.GetComponent<Card3> ().ReturnCardBack ();
			CardHolded.GetComponent<Card3> ().SendCard ();}catch{
		}
	}

	private bool RoundClosed ()
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i] == "0")
				return false;
		return true;
	}

	private bool FirstTurnInTrix ()
	{
		for (int i = 0; i < NextTrixCardList.Length; i++)
			if (NextTrixCardList [i].Length > 1)
				return false;
		return true;
	}

	void SetTrixAvailableCards ()
	{
		//Fukin NightWatcher
		if (Player.Instance.OnlinePlayerID == -1)
			return;

		IfPlayerHoldingCardGetItBack ();
		for (int i = 0; i < CardsListGO.transform.childCount; i++) {
			for (int u = 0; u < 8; u++) {
				CardAvailability (CardsListGO.transform.GetChild (i).gameObject, false);
				if (CardsListGO.transform.GetChild (i).GetComponent<Card3> ().CardID ==
					TempCurrenRoom.GameOB.KingdomsDetails.Trix.AvailableCardPlace [u] ||
					CardsListGO.transform.GetChild (i).GetComponent<Card3> ().CardID.Contains ("11")) {
					CardAvailability (CardsListGO.transform.GetChild (i).gameObject, true);
					break;
				}
			}
		}
	}

	bool ThereisCardAvailable ()
	{

		for (int i = 0; i < CardsListGO.transform.childCount; i++)
			if (CardsListGO.transform.GetChild (i).GetComponent<Image> ().color.r == 1)
				return true;
		return false;
	}

	private bool SetAvailableCardsOnce;
	void SetAvailableCards ()
	{
		//Watcher
		if (Player.Instance.OnlinePlayerID == -1)
			return;

		if (SetAvailableCardsOnce)
			return;
		List<string> PlayedCards = new List<string> ();
		IfPlayerHoldingCardGetItBack ();
		int Counter = 0;
		int PlayedPlayerID = 0;
		for (int i = 0; i < 4; i++)
			PlayedCards.Add (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i]);
		for (int i = 0; i < 4; i++) {
			if (PlayedCards [i].Length > 1) {
				Counter++;
				PlayedPlayerID = i;
			}
		}
		if (Counter != 1 || PreviosTurn (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay) == Player.Instance.OnlinePlayerID)
			return;
		string CardType = GetChar (PlayedCards [PlayedPlayerID], 0);
		if (IHaveThisCardType (CardType)) {
			for (int i = 0; i < CardsListGO.transform.childCount; i++) {
				if (!CardsListGO.transform.GetChild (i).GetComponent<Card3> ().CardID.StartsWith (CardType)) {
					CardAvailability (CardsListGO.transform.GetChild (i).gameObject, false);
					SetAvailableCardsOnce = true;
				}
			}
		}
	}


	bool IHaveThisCardType (string Cardtype)
	{
		for (int i = 0; i < CardsListGO.transform.childCount; i++) {
			if (CardsListGO.transform.GetChild (i).GetComponent<Card3> ().CardID.StartsWith (Cardtype))
				return true;
		}
		return false;
	}

	void ThrowThisCard (int PlayerThrowerID, string ThrowdCardID)
	{
		int CurrentKingdomTypeID = TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID;
		int PlayerIDOnMyDevice = FromOnlineToLocal (PlayerThrowerID);
		if (CurrentKingdomTypeID != 4)
			CardsThrowerMangaer.Instance.ThrowThisCard (ThrowdCardID, PlayerIDOnMyDevice);
		else {
			GameObject NewCardGO = Instantiate (CardGO, TargetVect (PlayerIDOnMyDevice), Quaternion.identity) as GameObject;
			NewCardGO.transform.SetParent (PlayersCardsGO [PlayerIDOnMyDevice].transform.parent.parent);
			NewCardGO.GetComponent<Card3> ().CardID = ThrowdCardID;
			NewCardGO.GetComponent<Card3> ().SetCardCoverOrFace ();
			NewCardGO.GetComponent<Card3> ().ThroughItTrix (GetChar (ThrowdCardID, 0), PlayerIDOnMyDevice);
			Vector3 PrevCardVect = NewCardGO.GetComponent<RectTransform> ().localPosition;
			NewCardGO.GetComponent<RectTransform> ().localPosition = new Vector3 (PrevCardVect.x, PrevCardVect.y, 0);
		}
		DeActiveCardFromOtherPlayer (PlayerThrowerID, PlayerIDOnMyDevice);
		if (CurrentKingdomTypeID != 4) {
			if (RoundEnds ()) {
				SetLastRoundCards ();
				if (Player.Instance.GameCreator)
					CloseRound ();
			}
		} //Trix Close Round is related to the end of card animation with a courotine yield
	}

	void CheckTrixWinners (int ThorwerOnlineID)
	{
		if (!Player.Instance.GameCreator)
			return;
		//Check Me is winner
		int PlayerIDOnMyDevice = FromOnlineToLocal (ThorwerOnlineID);
		if (PlayerIDOnMyDevice == 0) {
			if (CardsListGO.transform.childCount == 0)
				AddWinnerToTrixWinnerOrderList (ThorwerOnlineID);
		} else {
			int ActiveChildrenCounter = 0;
			for (int i = 0; i < OtherPlayersCardsGO [PlayerIDOnMyDevice].transform.childCount; i++) {
				if (OtherPlayersCardsGO [PlayerIDOnMyDevice].transform.GetChild (i).gameObject.activeSelf) {
					return;
				} else
					ActiveChildrenCounter++;
			}
			if (ActiveChildrenCounter == 13)
				AddWinnerToTrixWinnerOrderList (ThorwerOnlineID);
		}
	}

	//Add trix winner in this
	void DeActiveCardFromOtherPlayer (int ThorwerOnlineID, int PlayerIDOnMyDevice)
	{
		if (PlayerIDOnMyDevice != 0 || Player.Instance.OnlinePlayerID == -1) {
			if (TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID != 4)
				OtherPlayersCardsGO [PlayerIDOnMyDevice].transform.
				GetChild (TempCurrenRoom.GameOB.CurrentRound.RoundNO).gameObject.SetActive (false);
			else {
				for (int i = 0; i < OtherPlayersCardsGO [PlayerIDOnMyDevice].transform.childCount; i++)
					if (OtherPlayersCardsGO [PlayerIDOnMyDevice].transform.
						GetChild (i).gameObject.activeSelf) {
						OtherPlayersCardsGO [PlayerIDOnMyDevice].transform.
						GetChild (i).gameObject.SetActive (false);
						break;
					}
			}
		}
	}

	void AddWinnerToTrixWinnerOrderList (int ThorwerOnlineID)
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.KingdomsDetails.Trix.PlayersWinnersOrder [i] == -1) {
				TempCurrenRoom.GameOB.KingdomsDetails.Trix.PlayersWinnersOrder [i] = ThorwerOnlineID;
				return;
			}
	}

	public IEnumerator SetAndArrangeAvailableTrixCardsCor ()
	{
		PlayingWindowsManager3.Instance.SetTimer (FromOnlineToLocal (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay),
			TimerWaitingTime (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay), 3, false);
		if (Player.Instance.GameCreator) {
			if (TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID == 4)
				CheckTrixWinners (PreviosTurn (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay));
			SetAndArrangeAvailableTrix ();
		}
		SetTrixAvailableCards ();
		if (MyTurnToPlay () && !ThereisCardAvailable ())
			TrixNextTurn ();
		yield return null;
	}

	private void SetAndArrangeAvailableTrix ()
	{
		TempCurrenRoom.GameOB.KingdomsDetails.Trix.TrixCounter++;
		NextTrixCardList = TempCurrenRoom.GameOB.KingdomsDetails.Trix.AvailableCardPlace;
		for (int i = 0; i < 4; i++) {
			if (TrixLists [i].transform.childCount == 0)
				continue;
			NextTrixCardList [SpecificIFirst (i)] = TrixLists [i].transform.GetChild (0).GetComponent<Card3> ().CardID;
			NextTrixCardList [SpecificISocend (i)] = TrixLists [i].transform.GetChild (TrixLists [i].transform.childCount - 1).GetComponent<Card3> ().CardID;
		}

		for (int i = 0; i < 8; i += 2) {
			if (NextTrixCardList [i].Length > 1) {
				int ItemInNextTrixCardList = int.Parse (GetChar (NextTrixCardList [i], 1) + GetChar (NextTrixCardList [i], 2));
				ItemInNextTrixCardList++;
				NextTrixCardList [i] = GetChar (NextTrixCardList [i], 0) + ItemInNextTrixCardList;
			}
		}

		for (int i = 1; i < 8; i += 2) {
			if (NextTrixCardList [i].Length > 1) {
				int ItemInNextTrixCardList = int.Parse (GetChar (NextTrixCardList [i], 1) + GetChar (NextTrixCardList [i], 2));
				ItemInNextTrixCardList--;
				NextTrixCardList [i] = GetChar (NextTrixCardList [i], 0) + ItemInNextTrixCardList;
			}
		}

		for (int i = 0; i < 8; i++) {
			if (NextTrixCardList [i].Length == 2)
				NextTrixCardList [i] = GetChar (NextTrixCardList [i], 0) + "0" + GetChar (NextTrixCardList [i], 1);
			TempCurrenRoom.GameOB.KingdomsDetails.Trix.AvailableCardPlace [i] = NextTrixCardList [i];
		}

		if (TempCurrenRoom.GameOB.KingdomsDetails.Trix.TrixCounter >= 52) {
			TempCurrenRoom.ActionID = 12;
			PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
		} else {
			TempCurrenRoom.ActionID = 11;
			PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, false);
		}
	}

	private int SpecificIFirst (int I)
	{
		if (I == 0)
			return 0;
		else if (I == 1)
			return 2;
		else if (I == 2)
			return 4;
		else
			return 6;
	}

	private int SpecificISocend (int I)
	{
		if (I == 0)
			return 1;
		else if (I == 1)
			return 3;
		else if (I == 2)
			return 5;
		else
			return 7;
	}


	private bool CanThrowCard = true;

	public void ThrowCard (GameObject Cardjo)
	{
		if ((TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID != 4 && MyTurnToPlay () && TempCurrenRoom.ActionID == 10 && CanThrowCard) ||
			(TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID == 4 && MyTurnToPlay () && CanThrowCard)) {
			StartCoroutine (RemoveOrderly (Cardjo));
		} else
			Cardjo.GetComponent<Card3> ().ReturnCardBack ();
	}

	private IEnumerator RemoveOrderly (GameObject Cardjo)
	{
		CanThrowCard = false;
		ThrowCardNow (Cardjo.GetComponent<Card3> ().CardID);
		Destroy (Cardjo);
		yield return new WaitForSeconds (0.03f);try{
			TrixManager3.Instance.SetAndArrangeMyCards ();}catch{
		}
		yield return new WaitForSeconds (2f);
		CanThrowCard = true;
		yield return null;
	}

	void ThrowCardNow (string CardID)
	{
		if (CardID == "0") {
			TrixNextTurn ();
			return;
		}
		string CardIDString = CardID;
		int PlayerTurn = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		TempCurrenRoom.GameOB.CurrentRound.PlayersCards [PlayerTurn] = CardIDString;
		RemoveThisCardFromOnlineCardsList (PlayerTurn, CardIDString);
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = NextID ();
		TempCurrenRoom.ActionID = 10;
		PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private void RemoveThisCardFromOnlineCardsList (int PlayerNO, string CardID)
	{
		for (int i = 0; i < TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList.Count; i++) {
			if (TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList [i] == CardID) {
				TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList [i] = "0";
				return;
			}
		}
	}

	private bool RoundEnds ()
	{
		int Over1Counter = 0;
		for (int i = 0; i < 4; i++) {
			if (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i].Length > 1)
				Over1Counter++;
		}
		if (Over1Counter == 4)
			return true;
		else
			return false;
	}

	private List<string> PlayersCardsList;
	private int CurrentCollectorID;

	void CloseRound ()
	{
		TempCurrenRoom.ActionID = 11;
		PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
		CurrentCollectorID = GetCollectorID ();
		TempCurrenRoom.GameOB.CollectorID = CurrentCollectorID;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = CurrentCollectorID;
		switch (TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID) {
		case 0:
			KingCloseRound ();
			break;
		case 1:
			QueensCloseRound ();
			break;
		case 2:
			DiamondsCloseRound ();
			break;
		case 3:
			LtooshCloseRound ();
			break;
		case 5:
			ComplexCloseRound ();
			break;
		}
		for (int i = 0; i < 4; i++)
			TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i] = "0";
		TempCurrenRoom.ActionID = 12;
		PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void ComplexCloseRound ()
	{
		KingCloseRound ();
		QueensCloseRound ();
		DiamondsCloseRound ();
		LtooshCloseRound ();
		TempCurrenRoom.GameOB.CurrentRound.RoundNO++;
	}

	void TrixNextTurn ()
	{
		for (int i = 0; i < 4; i++)
			TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i] = "0";
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = NextID ();
		TempCurrenRoom.ActionID = 10;
		PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private int NextID ()
	{
		int ThisPlayerID = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		ThisPlayerID++;
		if (ThisPlayerID >= 4)
			ThisPlayerID = 0;
		return ThisPlayerID;
	}

	void DiamondsCloseRound ()
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i].StartsWith ("D")) {
				TempCurrenRoom.GameOB.CurrentRound.PlayersScores [CurrentCollectorID] -= 10;
				TempCurrenRoom.GameOB.KingdomsDetails.Counters [1]++;
			}
		if (TempCurrenRoom.GameOB.KingdomsDetails.IsComplex)
			return;
		TempCurrenRoom.GameOB.CurrentRound.RoundNO++;
		if (TempCurrenRoom.GameOB.KingdomsDetails.Counters [1] >= 13)
			TempCurrenRoom.GameOB.CurrentRound.RoundNO = 13;
	}

	void LtooshCloseRound ()
	{
		TempCurrenRoom.GameOB.CurrentRound.PlayersScores [CurrentCollectorID] -= 15;
		if (TempCurrenRoom.GameOB.KingdomsDetails.IsComplex)
			return;
		TempCurrenRoom.GameOB.CurrentRound.RoundNO++;
	}

	private void QueensCloseRound ()
	{
		for (int o = 1; o < 5; o++) {
			for (int i = 0; i < 4; i++) {
				if (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i] == CurrentQueentType (o)) {
					if (IsItDoubled (o)) {
						TempCurrenRoom.GameOB.CurrentRound.PlayersScores [CurrentCollectorID] -= 50;
						SetPlusBonus (i, CurrentCollectorID, false);
						TempCurrenRoom.GameOB.KingdomsDetails.CurrentplayerDoubles [i].PlayerDoubles [o] = 0;
						TempCurrenRoom.GameOB.KingdomsDetails.Counters [0]++;
					}//Not Doubled
					else {
						TempCurrenRoom.GameOB.CurrentRound.PlayersScores [CurrentCollectorID] -= 25;
						TempCurrenRoom.GameOB.KingdomsDetails.Counters [0]++;
					}
				}
			}
		}
		if (TempCurrenRoom.GameOB.KingdomsDetails.IsComplex)
			return;
		TempCurrenRoom.GameOB.CurrentRound.RoundNO++;
		if (TempCurrenRoom.GameOB.KingdomsDetails.Counters [0] == 4)
			TempCurrenRoom.GameOB.CurrentRound.RoundNO = 13;
	}

	private void KingCloseRound ()
	{
		bool KingThrowd = false;
		for (int i = 0; i < 4; i++) {
			if (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i] == "H13") {
				if (IsItDoubled (0)) {
					TempCurrenRoom.GameOB.CurrentRound.PlayersScores [CurrentCollectorID] -= 150;
					SetPlusBonus (i, CurrentCollectorID, true);
					TempCurrenRoom.GameOB.KingdomsDetails.CurrentplayerDoubles [i].PlayerDoubles [0] = 0;
					KingThrowd = true;
					break;
				}//Not Doubled
				else {
					TempCurrenRoom.GameOB.CurrentRound.PlayersScores [CurrentCollectorID] -= 75;
					KingThrowd = true;
					break;
				}
			}
		}
		if (TempCurrenRoom.GameOB.KingdomsDetails.IsComplex)
			return;
		TempCurrenRoom.GameOB.CurrentRound.RoundNO++;
		if (KingThrowd)
			TempCurrenRoom.GameOB.CurrentRound.RoundNO = 13;
	}

	void SetPlusBonus (int ThrowerOnlineID, int CollectorID, bool KingOrQueens)
	{
		if (TempCurrenRoom.GameOB.KingdomsDetails.IsPartners) {
			//Partner
			if (ThrowerOnlineID != CollectorID && ThrowerOnlineID != MyPartnerID (CollectorID)) {
				if (KingOrQueens)
					TempCurrenRoom.GameOB.CurrentRound.PlayersScores [ThrowerOnlineID] += 75;
				else
					TempCurrenRoom.GameOB.CurrentRound.PlayersScores [ThrowerOnlineID] += 25;
			}
		} else {
			//Not Partners
			if (ThrowerOnlineID != CollectorID) {
				if (KingOrQueens)
					TempCurrenRoom.GameOB.CurrentRound.PlayersScores [ThrowerOnlineID] += 75;
				else
					TempCurrenRoom.GameOB.CurrentRound.PlayersScores [ThrowerOnlineID] += 25;
			}

		}
	}

	private string CurrentQueentType (int QueenID)
	{
		if (QueenID == 1)
			return "H12";
		else if (QueenID == 2)
			return "D12";
		else if (QueenID == 3)
			return "S12";
		else if (QueenID == 4)
			return "C12";
		//UnReachable
		return "H12";
	}

	private bool IsItDoubled (int CardID)
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.KingdomsDetails.CurrentplayerDoubles [i].PlayerDoubles [CardID] == 1)
				return true;
		return false;
	}

	private int GetCollectorID ()
	{
		PlayersCardsList = new List<string> ();
		for (int i = 0; i < 4; i++)
			PlayersCardsList.Add (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i]);
		return GetHighestFromThisType (GetChar (PlayersCardsList [TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound], 0));
	}

	private int GetHighestFromThisType (string CardType)
	{
		List<string> NewCardsList = new List<string> ();
		for (int i = 0; i < 4; i++) {
			if (PlayersCardsList [i].StartsWith (CardType))
				NewCardsList.Add (PlayersCardsList [i]);
		}
		//After List Ready
		NewCardsList.Sort ();
		for (int i = 0; i < 4; i++) {
			if (NewCardsList [NewCardsList.Count - 1] == PlayersCardsList [i])
				return i;
		}
		return 0;
	}



	void UpdateRoomPhase9 ()
	{
		AudioManager.Instance.PlaySound (1);
		PlayingWindowsManager3.Instance.SetTrumpAndBidder (TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID);
		if (Player.Instance.OnlinePlayerID != -1 && TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID == 4)
			SetTrixAvailableCards ();
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.ActionID = 10;
		PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase8 ()
	{
		PlayingWindowsManager3.Instance.DoublesGO.SetActive (false);
		for (int i = 0; i < 4; i++) {
			int[] DoublesCards = new int[5];
			for (int u = 0; u < 5; u++)
				DoublesCards [u] = TempCurrenRoom.GameOB.KingdomsDetails.CurrentplayerDoubles [i].PlayerDoubles [u];
			PlayingWindowsManager3.Instance.ShowDoublesCards (FromOnlineToLocal (i), DoublesCards);
		}
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.ActionID = 9;
		PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private void UpdateRoomPhase7 ()
	{
		if (TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID <= 1 ||
			TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID == 5) {
			int DoublerID = TempCurrenRoom.GameOB.PlayerBidTurn;
			if (DoublerID >= 0)
				PlayingWindowsManager3.Instance.SetTimer (FromOnlineToLocal (DoublerID),
					TimerWaitingTime (DoublerID), 2, false);
			if (MyBidTurn ()) {
				if (TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID == 0)
					SetPlayerReadyifnodoublesKing (Player.Instance.OnlinePlayerID, true, false);
				else if (TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID == 1)
					SetPlayerReadyifnodoublesQueens (Player.Instance.OnlinePlayerID, true, false, null);
				else {
					SetPlayerReadyifnodoublesKing (Player.Instance.OnlinePlayerID, true, true);
					SetPlayerReadyifnodoublesQueens (Player.Instance.OnlinePlayerID, true, true, ComplexAvailableDoubles);
				}
			} else
				PlayingWindowsManager3.Instance.DoublesGO.SetActive (false);
		}
		if (!Player.Instance.GameCreator)
			return;
		int PlayersReadyCounter = 0;
		if (TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID <= 1 ||
			TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID == 5) {
			for (int i = 0; i < 4; i++)
				if (TempCurrenRoom.GameOB.KingdomsDetails.IsReady [i] == 1)
					PlayersReadyCounter++;
		} else
			PlayersReadyCounter = 4;
		if (PlayersReadyCounter == 4) {
			TempCurrenRoom.ActionID = 8;
			PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
		}
	}

	private bool MyBidTurn ()
	{
		if (Player.Instance.OnlinePlayerID == TempCurrenRoom.GameOB.PlayerBidTurn)
			return true;
		else
			return false;
	}


	private bool CallKingdomTypeOnce;
	//Bidding Phase
	void UpdateRoomPhase6 ()
	{
		if (!CallKingdomTypeOnce) {
			AudioManager.Instance.PlaySound (GameTypeToSound (TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID));
			CallKingdomTypeOnce = true;
		}
		if (Player.Instance.OnlinePlayerID == -1)
			return;
		if (!Player.Instance.GameCreator)
			return;
		switch (TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID) {
		case 0:
			CheckKing ();
			break;
		case 1:
			CheckQueens ();
			break;
		case 5://Complex
			CheckKingAndQueens ();
			break;
		default:
			TempCurrenRoom.ActionID = 7;
			PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
			break;
		}
	}

	private int GameTypeToSound (int currentKingdomTypeID)
	{
		if (currentKingdomTypeID == 0)
			return 29;
		else if (currentKingdomTypeID == 1)
			return 30;
		else if (currentKingdomTypeID == 2)
			return 31;
		else if (currentKingdomTypeID == 3)
			return 18;
		else if (currentKingdomTypeID == 4)
			return 32;
		else if (currentKingdomTypeID == 5)
			return 33;
		else
			return 29;
	}

	void CheckKingAndQueens ()
	{
		for (int i = 0; i < 4; i++) {
			if (TempCurrenRoom.SeatsStatus [i] == 0)
				TempCurrenRoom.GameOB.KingdomsDetails.IsReady [i] = 1;
			else {
				SetPlayerReadyifnodoublesKing (i, false, true);
				SetPlayerReadyifnodoublesQueens (i, false, true, ComplexAvailableDoubles);
			}
		}

		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.KingdomsDetails.IsReady [i] == 0) {
				TempCurrenRoom.GameOB.PlayerBidTurn = i;
				break;
			}
		TempCurrenRoom.ActionID = 7;
		PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private int[] ComplexAvailableDoubles;

	void CheckKing ()
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.SeatsStatus [i] == 0)
				TempCurrenRoom.GameOB.KingdomsDetails.IsReady [i] = 1;
			else
				SetPlayerReadyifnodoublesKing (i, false, false);
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.KingdomsDetails.IsReady [i] == 0) {
				TempCurrenRoom.GameOB.PlayerBidTurn = i;
				break;
			}
		TempCurrenRoom.ActionID = 7;
		PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void CheckQueens ()
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.SeatsStatus [i] == 0)
				TempCurrenRoom.GameOB.KingdomsDetails.IsReady [i] = 1;
			else
				SetPlayerReadyifnodoublesQueens (i, false, false, null);
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.KingdomsDetails.IsReady [i] == 0) {
				TempCurrenRoom.GameOB.PlayerBidTurn = i;
				break;
			}
		TempCurrenRoom.ActionID = 7;
		PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private void SetPlayerReadyifnodoublesKing (int PlayerID, bool ShowDoubles, bool IsComplex)
	{
		int[] AvailableDoubles = new int[5]{ 0, 0, 0, 0, 0 };
		bool ThereIsDoubles = false;
		string WantedQueenType = "H13";
		for (int u = 0; u < 13; u++) {
			if (TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerID].PlayerCardsList [u] == WantedQueenType) {
				AvailableDoubles [0] = 1;
				ThereIsDoubles = true;
				if (IsComplex)
					ThereisKingDouble = ThereIsDoubles;
				break;
			}
		}
		if (IsComplex)
			ComplexAvailableDoubles = AvailableDoubles;
		if (ThereIsDoubles) {
			if (ShowDoubles && !IsComplex) {
				PlayingWindowsManager3.Instance.DoublesGO.SetActive (true);
				PlayingWindowsManager3.Instance.ShowAvailableDoublesGO (AvailableDoubles);
			}
		} else if (!IsComplex)
			TempCurrenRoom.GameOB.KingdomsDetails.IsReady [PlayerID] = 1;
	}

	private bool ThereisKingDouble;

	private void SetPlayerReadyifnodoublesQueens (int PlayerID, bool ShowDoubles, bool IsComplex, int[] ComplexAvailabeDoubles)
	{
		int[] AvailableDoubles = new int[5]{ 0, 0, 0, 0, 0 };
		if (IsComplex)
			AvailableDoubles = ComplexAvailabeDoubles;
		bool ThereIsDoubles = false;
		for (int i = 1; i < 5; i++) {
			string WantedQueenType = "";
			if (i == 1)
				WantedQueenType = "H12";
			else if (i == 2)
				WantedQueenType = "D12";
			else if (i == 3)
				WantedQueenType = "S12";
			else if (i == 4)
				WantedQueenType = "C12";
			for (int u = 0; u < 13; u++) {
				if (TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerID].PlayerCardsList [u] == WantedQueenType) {
					AvailableDoubles [i] = 1;
					ThereIsDoubles = true;
				}
			}
		}
		if (ThereIsDoubles || ThereisKingDouble) {
			if (ShowDoubles) {
				PlayingWindowsManager3.Instance.DoublesGO.SetActive (true);
				PlayingWindowsManager3.Instance.ShowAvailableDoublesGO (AvailableDoubles);
			}
		} else
			TempCurrenRoom.GameOB.KingdomsDetails.IsReady [PlayerID] = 1;
	}

	public void SetDouble (int DoubleName, bool AddOrRemove)
	{
		if (AddOrRemove)
			TempCurrenRoom.GameOB.KingdomsDetails.CurrentplayerDoubles [Player.Instance.OnlinePlayerID].PlayerDoubles [DoubleName] = 1;
		else
			TempCurrenRoom.GameOB.KingdomsDetails.CurrentplayerDoubles [Player.Instance.OnlinePlayerID].PlayerDoubles [DoubleName] = 0;
	}

	public void PlayerIsReady (int PlayerID)
	{
		TempCurrenRoom.GameOB.KingdomsDetails.IsReady [PlayerID] = 1;
		TempCurrenRoom.GameOB.PlayerBidTurn = -2;
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.KingdomsDetails.IsReady [i] == 0) {
				TempCurrenRoom.GameOB.PlayerBidTurn = i;
				break;
			}
		PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	//Player Choose Kingdom Type
	void UpdateRoomPhase5 ()
	{
		PlayingWindowsManager3.Instance.SetTimer (FromOnlineToLocal (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay),
			TimerWaitingTime (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay), 1, false);
		if (Player.Instance.GameCreator) {
			TempCurrenRoom.GameOB.KingdomsDetails.FirstGame = false;
			PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, false);
		}
		if (Player.Instance.OnlinePlayerID != TempCurrenRoom.GameOB.KingdomsDetails.KingdomPlayerID)
			return;
		PlayingWindowsManager3.Instance.ShowChooseKingdomTypeWindow (TempCurrenRoom.GameOB.KingdomsDetails.PlayerUnselectedKingdomTypes);
	}

	public void PlayerChooseKingdomType (int KingDomType)
	{
		TempCurrenRoom.GameOB.KingdomsDetails.CurrentKingdomTypeID = KingDomType;
		TempCurrenRoom.GameOB.KingdomsDetails.PlayerUnselectedKingdomTypes [KingDomType] = 0;
		TempCurrenRoom.ActionID = 6;
		PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private bool SceneDoneLoading;

	void UpdateRoomPhase4 ()
	{
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (PlayerID == -1)
			return;
		SceneDoneLoading = true;
		if (TempCurrenRoom.GameOB.KingdomsDetails.FirstGame) {
			PlayingWindowsManager3.Instance.Set7OfHearts (FromOnlineToLocal (PlayerIDStartsGame ()));
			for (int i = 0; i < 4; i++)
				PlayingWindowsManager3.Instance.TeamsScores [i].GetComponent<Text> ().text = "0";
		}
		TrixManager3.Instance.Cards.PlayersCards [PlayerID].PlayerCardsList =
			TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerID].PlayerCardsList;
		TrixManager3.Instance.SetAndArrangeMyCards ();
		ReturnsMyCardsAvailibity ();
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound = TempCurrenRoom.GameOB.KingdomsDetails.KingdomPlayerID;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = TempCurrenRoom.GameOB.KingdomsDetails.KingdomPlayerID;
		TempCurrenRoom.ActionID = 5;
		PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase3 ()
	{
		if (SceneDoneLoading)
			return;
		if (IsAllPlayersReady () && Player.Instance.GameCreator) {
			TempCurrenRoom.ActionID = -1;
			PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);
			return;
		}
		UpdateRoomPhase2 ();
		if (TempCurrenRoom.PlayersReady [Player.Instance.OnlinePlayerID])
			return;
		PlayingWindowsManager3.Instance.ResetSomeBTNSAndWindows ();
		SetPlayersScores ();
		ResetCurrentKingdomData ();
		PlayingFireBaseManager3.Instance.SetOnDissconnectHandle ();
		TrixManager3.Instance.AssignPlayerCardsAndActivateOthersCards ();
		PlayingWindowsManager3.Instance.WinsWindow.SetActive (false);
		TempCurrenRoom.PlayersReady [Player.Instance.OnlinePlayerID] = true;
		if (Player.Instance.GameCreator)
			SetReadyForComputer ();
		PlayingFireBaseManager3.Instance.UploudThisJson (TempCurrenRoom, true);

	}

	void SetReadyForComputer ()
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.SeatsStatus [i] == 0)
				TempCurrenRoom.PlayersReady [i] = true;
	}

	bool IsAllPlayersReady ()
	{
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++)
			if (!TempCurrenRoom.PlayersReady [i])
				return false;
		return true;
	}

	void SetPlayersScores ()
	{
		for (int i = 0; i < 4; i++)
			PlayingWindowsManager3.Instance.TeamsScores [i].GetComponent<Text> ().text = TempCurrenRoom.GameOB.PlayersScores [i].ToString ();
	}

	void SetComplexUnSelected ()
	{
		if (TempCurrenRoom.GameOB.KingdomsDetails.IsComplex) {
			int[] ComplexUnSelected = TempCurrenRoom.GameOB.KingdomsDetails.PlayerUnselectedKingdomTypes;
			for (int i = 0; i < 4; i++)
				ComplexUnSelected [i] = 0;
			TempCurrenRoom.GameOB.KingdomsDetails.PlayerUnselectedKingdomTypes = ComplexUnSelected;
		}
	}

	void SetPartnerAndComplex ()
	{
		if (MainFireBaseManager.GameType.Contains ("Partner"))
			TempCurrenRoom.GameOB.KingdomsDetails.IsPartners = true;
		if (MainFireBaseManager.GameType.Contains ("Complex"))
			TempCurrenRoom.GameOB.KingdomsDetails.IsComplex = true;
	}

	void FirstGameConfigurations ()
	{
		int StartPlayerID = PlayerIDStartsGame ();
		TempCurrenRoom.GameOB.KingdomsDetails.KingdomPlayerID = StartPlayerID;
	}

	private int PlayerIDStartsGame ()
	{
		for (int i = 0; i < 4; i++) {
			for (int u = 0; u < 13; u++) {
				if (TempCurrenRoom.GameOB.Cards.PlayersCards [i].PlayerCardsList [u] == "H07") {
					return i;
				}
			}
		}
		//Unreachable
		return 0;
	}

	#region implemented abstract members of PlayersManagerParent

	public override void OnApplicationPause (bool pauseStatus)
	{
		if (TempCurrenRoom.ActionID < 4)
			return;
		try {
			if (pauseStatus) {
				TempCurrenRoom.SeatsStatus [Player.Instance.OnlinePlayerID] = 0;
				if (Player.Instance.GameCreator && NoOfPlayersInTheRoom () > 0) {
					TempCurrenRoom.NoMaster = true;
					Player.Instance.GameCreator = false;
					for (int i = 0; i < 4; i++) {
						if (TempCurrenRoom.SeatsStatus [i] == 1) {
							TempCurrenRoom.MasterID = i;
							break;
						}
					}
				}
				PlayingFireBaseFather.ParentInstance.UploudThisJson (TempCurrenRoom, TempCurrenRoom.NoMaster);			
			} else
				StartCoroutine (SetandarrangeCor (pauseStatus));
		} catch {
		}
	}

	#endregion

	private IEnumerator SetandarrangeCor (bool pauseStatus)
	{
		ShowBackBTN ();
		yield return new WaitForSeconds (2.5f);
		try {
			TrixManager3.Instance.SetAndArrangeMyCards ();
		} catch {
		}
	}


	#region implemented abstract members of PlayersManagerParent

	public override void IamBack ()
	{
		TempCurrenRoom.SeatsStatus [Player.Instance.OnlinePlayerID] = 1;
		PlayerIsBack (true);
		PlayingFireBaseFather.ParentInstance.UploudThisJson (TempCurrenRoom, false);
	}

	#endregion

	private void OnDestroy ()
	{
		StopAllCoroutines ();
	}
}