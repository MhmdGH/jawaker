﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayingBTNSManager3 : PlayingBTNSManagerFather {
	
	public static PlayingBTNSManager3 Instance;

	protected override void Awake(){
		base.Awake ();
		Instance = this;
	}

	public override void BTNSFunction(){
		base.BTNSFunction ();
		if (BTNName.Contains ("RestartBTN"))
			PlayersManager3.Instance.RestartGame ();
		else if (BTNName.Contains ("KingdomType"))
			ChooseKingdomType (GetChar (BTNName, 0));
		 else if (BTNName.EndsWith ("Double"))
			DoubleChoosed (GetChar (BTNName, 0) + GetChar (BTNName, 1), CurrentBTNGO);
		else if (BTNName == "DoubleReady")
			PlayerIsReady ();
	}

	public void PlayerIsReady ()
	{
		PlayingWindowsManager3.Instance.DoublesGO.SetActive (false);
		PlayersManager3.Instance.PlayerIsReady (Player.Instance.OnlinePlayerID);
	}

	void DoubleChoosed (string DoubleName, GameObject currentBTNGO)
	{
		if (currentBTNGO.GetComponent<Image> ().color.a < 1) {
			currentBTNGO.GetComponent<Image> ().color = new Color (1, 1, 1, 1);
			PlayersManager3.Instance.SetDouble (DoubleNameToInt(DoubleName), true);
		} else {
			currentBTNGO.GetComponent<Image> ().color = new Color (1,1,1,0.7f);
			PlayersManager3.Instance.SetDouble (DoubleNameToInt(DoubleName),false);
		}
	}

	int DoubleNameToInt (string doubleName)
	{
		if (doubleName == "King")
			return 0;
		else if (doubleName == "QH")
			return 1;
		else if (doubleName == "QD")
			return 2;
		else if (doubleName == "QS")
			return 3;
		else if (doubleName == "QC")
			return 4;
		else
			return 0;
	}

	void ChooseKingdomType (string KingdomType)
	{
		PlayingWindowsManager3.Instance.ChooseKingdomTypeGO.transform.parent.transform.gameObject.SetActive (false);
		PlayersManager3.Instance.PlayerChooseKingdomType (int.Parse(KingdomType));
	}
}