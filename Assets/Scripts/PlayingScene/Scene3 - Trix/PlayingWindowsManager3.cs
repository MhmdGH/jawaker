﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public class PlayingWindowsManager3 : PlayingWindowsManagerFather
{

	public static PlayingWindowsManager3 Instance;
	public GameObject[] BidsPops, PlayersDoublesGO;
	public GameObject BidWindow, KingdomDetailsWindow, SevenOfHearts, ChooseKingdomTypeGO, DoublesGO;

	public override void Awake ()
	{
		base.Awake ();
		Instance = this;
	}

	public override void ShowAndSetPartnersPhotos (string[] IMGSUrl)
	{
		OnStartBTNGO.SetActive (false);
		ChoosePartnerGO.SetActive (true);
		for(int i=0;i<ChooseParternersGOs.Length;i++)
			PlayingFBManager3.Instance.SetFPPhoto (IMGSUrl[i],ChooseParternersGOs[i].GetComponent<Image>());
	}

	public void SetTrumpAndBidder (int TrumpType)
	{
		KingdomDetailsWindow.transform.GetChild (1).gameObject.SetActive (false);
		KingdomDetailsWindow.SetActive (true);
		GameObject BiddingTxtGO = KingdomDetailsWindow.transform.GetChild (2).gameObject;
		if (TrumpType == 0)
			BiddingTxtGO.GetComponent<Text> ().text = "K";
		else if (TrumpType == 1)
			BiddingTxtGO.GetComponent<Text> ().text = "Q";
		else if (TrumpType == 2) {
			KingdomDetailsWindow.transform.GetChild (1).gameObject.SetActive (true);
			BiddingTxtGO.GetComponent<Text> ().text = "";
		} else if (TrumpType == 3)
			BiddingTxtGO.GetComponent<Text> ().text = "L";
		else if (TrumpType == 4)
			BiddingTxtGO.GetComponent<Text> ().text = "T";
		else if (TrumpType == 5)
			BiddingTxtGO.GetComponent<Text> ().text = "C";
	}

	private IEnumerator ShowBidCor (int BidPopNO, string BidValue)
	{
		BidsPops [BidPopNO].SetActive (true);
		BidsPops [BidPopNO].transform.GetChild (0).GetComponent<Text> ().text = BidValue;
		yield return new WaitForSeconds (2);
		BidsPops [BidPopNO].SetActive (false);
	}

	public override void StopTimer ()
	{
		base.StopTimer ();
	}

	protected override IEnumerator StartThisTimer (int PlayerNO, float WaitingTime, int FunToDOAfterEnd, bool JustCreatorDoIT)
	{
		PlayersTimer [PlayerNO].SetActive (true);
		WaitingTime = WaitingTime * SliderSmothnessMultiplier;
		float ActualTimerTikValue = TimerTikValue * SliderSmothnessMultiplier;
		PlayersTimer [PlayerNO].GetComponent<Slider> ().maxValue = WaitingTime;
		PlayersTimer [PlayerNO].GetComponent<Slider> ().value = WaitingTime;
		while (WaitingTime > 0) {
			PlayersTimer [PlayerNO].GetComponent<Slider> ().value -= ActualTimerTikValue;
			WaitingTime -= ActualTimerTikValue;
			yield return new WaitForSeconds (TimerTikValue * 0.1f);
		}
		PlayersTimer [PlayerNO].SetActive (false);
		if (JustCreatorDoIT && Player.Instance.GameCreator)
			PlayersManager3.Instance.MasterTakeControl (FunToDOAfterEnd);
		else if (!JustCreatorDoIT)
			PlayersManager3.Instance.MasterTakeControl (FunToDOAfterEnd);
	}

	public void Set7OfHearts (int Pos)
	{
		StartCoroutine (Set7OfHeartsCor (Pos));
	}

	private IEnumerator Set7OfHeartsCor (int Pos)
	{
		SevenOfHearts.SetActive (true);
		SevenOfHearts.GetComponent<RectTransform> ().anchoredPosition = TargetPos (Pos);
		yield return new WaitForSeconds (2);
		SevenOfHearts.SetActive (false);
	}

	private Vector3 TargetPos (int Pos)
	{
		if (Pos == 0)
			return new Vector3 (0, -300, 0);
		else if (Pos == 1)
			return new Vector3 (250, 0, 0);
		else if (Pos == 2)
			return new Vector3 (0, 300, 0);
		else if (Pos == 3)
			return new Vector3 (-250, 0, 0);
		else
			return new Vector3 (0, -300, 0);
	}

	public void ShowAvailableDoublesGO (int[] AvailableDoubles)
	{
		DoublesGO.SetActive (true);
		for (int i = 0; i < AvailableDoubles.Length; i++)
				DoublesGO.transform.GetChild (1).GetChild (i).gameObject.SetActive (AvailableDoubles [i] == 1);
	}

	public void ShowDoublesCards (int PlayerNO, int[] DoublesCards)
	{
		for (int i = 0; i < DoublesCards.Length; i++)
			PlayersDoublesGO [PlayerNO].transform.GetChild (i).gameObject.SetActive (DoublesCards [i] == 1);
	}

	public void RefreshDoublesGO (int PlayerNO, PlayersDoubles ThisPlayersDoubles)
	{
		for (int i = 0; i < 5; i++) {
			PlayingWindowsManager3.Instance.PlayersDoublesGO [PlayerNO].
			transform.GetChild (i).gameObject.SetActive (ThisPlayersDoubles.PlayerDoubles [i] == 1);
		}
	}

	public void ResetSomeBTNSAndWindows(){
		//PlayersDoublesGO
		PlayingWindowsManager3.Instance.KingdomDetailsWindow.SetActive(false);
		for (int i = 0; i < 4; i++)
			for (int u = 0; u < 5; u++)
				PlayersDoublesGO [i].transform.GetChild (u).gameObject.SetActive(false);
	}

	public override void ShowWindWindow(int WinnerTeamNO){
		string WinsTxt = "";
		if (PlayersManager3.Instance.TempCurrenRoom.GameOB.KingdomsDetails.IsPartners) {
			if (LanguangeManagerFather.ParentInstance.IsArabic)
				WinsTxt = ArabicFixer.Fix ("الفائز " + WinnerTeamNO + " فريق");
			else
				WinsTxt = "Team " + WinnerTeamNO + " Wins";
		} else {
			if(LanguangeManagerFather.ParentInstance.IsArabic)
				WinsTxt = ArabicFixer.Fix("الفائز "+WinnerTeamNO+" اللاعب");
			else
				WinsTxt = "player "+WinnerTeamNO+" Wins";
		}
		WinsWindow.transform.GetChild (1).GetComponent<Text> ().text = WinsTxt;
		WinsWindow.SetActive (true);
		WinsWindow.GetComponent<RectTransform> ().GetChild (4).gameObject.SetActive (!MainFireBaseManager.Instance.IsACompition);
	}

	public void ShowChooseKingdomTypeWindow(int[] PlayerUnselectedKingdomType){
		for (int i = 0; i < 6; i++) {
			ChooseKingdomTypeGO.transform.GetChild (i).gameObject.SetActive (true);
			if (PlayerUnselectedKingdomType [i] == 0)
				ChooseKingdomTypeGO.transform.GetChild (i).gameObject.SetActive (false);
		}
		ChooseKingdomTypeGO.transform.parent.transform.gameObject.SetActive (true);
	}

	private void OnDestroy ()
	{
		StopAllCoroutines ();
	}
}