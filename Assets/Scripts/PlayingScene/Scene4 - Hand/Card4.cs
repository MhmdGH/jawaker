﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Card4 : Card
{
	public bool CanDraw;
	public GameObject MeldSign;

	#region implemented abstract members of Card

	public override void ThroughIt (int StartPos)
	{
		StartCoroutine (ThroughItCor (StartPos));
	}

	private IEnumerator ThroughItCor (int StartPos)
	{
		GetComponent<Animator> ().SetInteger ("HandThrough", StartPos);
		yield return new WaitForSeconds (1f);
		GetComponent<Animator> ().SetInteger ("HandThrough", -1);
	}

	#endregion


	public void DrawIT (int StartPos)
	{
		throw new System.NotImplementedException ();
	}

	#region implemented abstract members of Card

	public override void SendCard ()
	{
		PlayersManager4.Instance.ThrowCard (this.gameObject);
	}

	#endregion

	protected override bool PutCardOnBoard ()
	{
		if (MyRectTransform.localPosition.y > -40)
			return true;
		else
			return false;
	}

	public override void OnEndDrag (PointerEventData eventData)
	{
		if (!CanDrag)
			return;
		GetComponent<Image> ().color = new Color (1, 1, 1, 1);
		PlayersManagerParent.PlayerHoldingCard = false;
		GetComponent<Animator> ().enabled = true;
		if (!PlayersManager4.Instance.MyTurnToPlay ())
			ReturnCardBack ();
		else if (PutCardOnDownMeld ())
			AddCardToMeld ();
		else if (PutCardOnBoard ())
			SendCard ();
		else
			ChangeCardPos ();
	}

	void AddCardToMeld ()
	{
		int ActualDownMeldNO = int.Parse (GetChar (MeldParentGO.name, 4)) - 1;
		PlayersManager4.Instance.CardAddedToDownedMeld (CardID, ActualDownMeldNO, MeldAddedSiblingNO, Replace);
		Destroy (this.gameObject);
	}
		
	private GameObject MeldParentGO;

	private bool PutCardOnDownMeld ()
	{
		if (PlayersManager4.Instance.TempCurrenRoom.ActionID != 7)
			return false;
		PointerEventData pointerData = new PointerEventData (EventSystem.current);
		pointerData.position = Input.mousePosition;
		List<RaycastResult> results = new List<RaycastResult> ();
		EventSystem.current.RaycastAll (pointerData, results);
		for (int i = 0; i < results.Count; i++)
			if (results [i].gameObject.layer == LayerMask.NameToLayer ("MeldLayerMask")) {
				if (CardCanBeAdded (results [i].gameObject.transform.parent.gameObject)) {
					MeldParentGO = results [i].gameObject.transform.parent.gameObject;
					return true;
				} else
					return false;
			}
		return false;
	}

	private bool ISASequenceMeld (GameObject ThisMeldListGO)
	{

		string MeldFirstCardNO = ThisMeldListGO.transform.GetChild (0).GetComponent<Card> ().CardID;
		string MeldSocendCardNO = ThisMeldListGO.transform.GetChild (1).GetComponent<Card> ().CardID;
		string MeldThirdCardNO = ThisMeldListGO.transform.GetChild (2).GetComponent<Card> ().CardID;
		if (!PlayersManager4.Instance.AreEqual (MeldFirstCardNO, MeldSocendCardNO) &&
		    !PlayersManager4.Instance.AreEqual (MeldSocendCardNO, MeldThirdCardNO))//This is a not same suit meld
			return true;
		else
			return false;
	}

	private int MeldAddedSiblingNO;

	bool CardCanBeAdded (GameObject MeldsListGO)
	{
		if (ThereISAJokerInMeld (MeldsListGO)) {
			if (CanReplaceWithAJoker (MeldsListGO))
				return true;
		}

		if (GetCardNO (CardID) == 15) {
			if (!ISASequenceMeld (MeldsListGO))
			if (MeldsListGO.transform.childCount <= 3) {
				MeldAddedSiblingNO = MeldsListGO.transform.childCount;
				return true;
			}
		}

		//Add Card To sequeance Down Meld
		if (ISASequenceMeld (MeldsListGO))
			for (int i = 0; i < MeldsListGO.transform.childCount; i++) {
				string OtherCardID = MeldsListGO.transform.GetChild (i).GetComponent<Card> ().CardID;
				int OtherCardNO = int.Parse (GetChar (OtherCardID, 1) + GetChar (OtherCardID, 2));
				int ThisCardNO = int.Parse (GetChar (CardID, 1) + GetChar (CardID, 2));
				if (OtherCardNO == 14 || OtherCardNO == 2)
					continue;
				
				if (ThisCardNO == 15) {
					
					if (GetCardNO (MeldsListGO.transform.GetChild (0).GetComponent<Card> ().CardID) != 14) {
						MeldAddedSiblingNO = 0;
						return true;
					} else if (GetCardNO (MeldsListGO.transform.GetChild (MeldsListGO.transform.childCount - 1).GetComponent<Card> ().CardID) != 14) {
						MeldAddedSiblingNO = MeldsListGO.transform.childCount;
						return true;
					}
				}
				if (i == 0) {
					if (PlayersManager4.Instance.AreInARow (CardID, OtherCardID) &&
					    PlayersManager4.Instance.AreTheSameSuite (CardID, OtherCardID) &&
					    (PlayersManager4.Instance.AreTheSameSuite (CardID, MeldsListGO.transform.GetChild (1).GetComponent<Card> ().CardID) ||
					    PlayersManager4.Instance.AreTheSameSuite (CardID, MeldsListGO.transform.GetChild (2).GetComponent<Card> ().CardID))) {
						
						if (GetCardNO (MeldsListGO.transform.GetChild (i + 1).GetComponent<Card> ().CardID) != 15 &&
						    ThisCardNO != GetCardNO (MeldsListGO.transform.GetChild (i + 1).GetComponent<Card> ().CardID) - 2)
							continue;
						
						if (GetCardNO (MeldsListGO.transform.GetChild (i + 2).GetComponent<Card> ().CardID) != 15 &&
						    ThisCardNO != GetCardNO (MeldsListGO.transform.GetChild (i + 2).GetComponent<Card> ().CardID) - 3)
							continue;
						
						MeldAddedSiblingNO = i;
						return true;
					}
				} else if (i == MeldsListGO.transform.childCount - 1) {
					if (PlayersManager4.Instance.AreTheSameSuite (CardID, OtherCardID)
					    && (PlayersManager4.Instance.AreTheSameSuite (CardID, MeldsListGO.transform.GetChild (0).GetComponent<Card> ().CardID) || PlayersManager4.Instance.AreTheSameSuite (CardID, MeldsListGO.transform.GetChild (1).GetComponent<Card> ().CardID))) {
						if (GetCardNO (MeldsListGO.transform.GetChild (i - 2).GetComponent<Card> ().CardID) != 15 &&
						    ThisCardNO != GetCardNO (MeldsListGO.transform.GetChild (i - 2).GetComponent<Card> ().CardID) + 3)
							continue;
						if (GetCardNO (MeldsListGO.transform.GetChild (i - 1).GetComponent<Card> ().CardID) != 15 &&
						    ThisCardNO != GetCardNO (MeldsListGO.transform.GetChild (i - 1).GetComponent<Card> ().CardID) + 2)
							continue;
						if (GetCardNO (MeldsListGO.transform.GetChild (i).GetComponent<Card> ().CardID) != 15 &&
						    ThisCardNO != GetCardNO (MeldsListGO.transform.GetChild (i).GetComponent<Card> ().CardID) + 1)
							continue;
						MeldAddedSiblingNO = i + 1;
						return true;
					}
				}
			}
		//Add Card To same suits Down Meld

		if (CardCanBeAddedToSameNOMeld (MeldsListGO))
			return true;

		return false;
	}

	private bool Replace;

	bool CanReplaceWithAJoker (GameObject MeldsListGO)
	{//Joker Replacment
		Replace = false;
		for (int i = 0; i < MeldsListGO.transform.childCount; i++)
			if (MeldsListGO.transform.GetChild (i).GetComponent<Card> ().CardID.Contains ("15")) {
				int ThisCardNO = GetCardNO (CardID);
				int[] CardsNO = new int[MeldsListGO.transform.childCount];
				string[] CardsIDs = new string[MeldsListGO.transform.childCount];
				for (int u = 0; u < CardsNO.Length; u++) {
					CardsNO [u] = GetCardNO (MeldsListGO.transform.GetChild (u).GetComponent<Card> ().CardID);
					CardsIDs [u] = MeldsListGO.transform.GetChild (u).GetComponent<Card> ().CardID;
				}

				if (i == 0) {//Joker IS In The first

					//Check Sequense Meld
					if (PlayersManager4.Instance.AreInARow (CardID, CardsIDs [1])) //A row with 2 card
					if (PlayersManager4.Instance.AreTheSameSuite (CardID, CardsIDs [1])//Same Suit With 2 or 3 card
					    || PlayersManager4.Instance.AreTheSameSuite (CardID, CardsIDs [2])) {
						if (CardsNO [2] != 15 && ThisCardNO != CardsNO [2] - 2) //A row with 3 card
							continue;
						Replace = true;
						MeldAddedSiblingNO = i;
						return true;
					}

					//Check Suits Melds
					if (PlayersManager4.Instance.AreEqual (CardID, CardsIDs [1]) && PlayersManager4.Instance.AreEqual (CardID, CardsIDs [2])) //Equal with 2 card
					if (PlayersManager4.Instance.AreNotTheSameSuite (CardID, CardsIDs [1])//Same Suit With 2 or 3 card
					    && PlayersManager4.Instance.AreNotTheSameSuite (CardID, CardsIDs [2])) {
						if (MeldsListGO.transform.childCount <= 3)
							continue;
						Replace = true;
						MeldAddedSiblingNO = i;
						return true;
					}
				} else if (i == MeldsListGO.transform.childCount - 1) {//Last Card Joker

					//Check Sequense Meld
					if (PlayersManager4.Instance.AreTheSameSuite (CardID, CardsIDs [i - 2])
					    && PlayersManager4.Instance.AreTheSameSuite (CardID, CardsIDs [i - 1])) {//Same suits
						if (CardsNO [i - 2] != 15 && ThisCardNO != CardsNO [i - 2] + 2)
							continue;
						if (CardsNO [i - 1] != 15 && ThisCardNO != CardsNO [i - 1] + 1)
							continue;
						Replace = true;
						MeldAddedSiblingNO = i;
						return true;
					}

					//Check Suits Melds
					if (PlayersManager4.Instance.AreEqual (CardID, CardsIDs [1]) && PlayersManager4.Instance.AreEqual (CardID, CardsIDs [2])) //Equal with 2 card
					if (PlayersManager4.Instance.AreNotTheSameSuite (CardID, CardsIDs [1])//Same Suit With 2 or 3 card
					    && PlayersManager4.Instance.AreNotTheSameSuite (CardID, CardsIDs [2])) {
						if (MeldsListGO.transform.childCount <= 3)
							continue;
						Replace = true;
						MeldAddedSiblingNO = i;
						return true;
					}

				} else {//Joker IS Somewhere in the meddle
					//Check Sequense Meld
					if (PlayersManager4.Instance.AreTheSameSuite (CardID, CardsIDs [i + 1])
					    && PlayersManager4.Instance.AreTheSameSuite (CardID, CardsIDs [i - 1])) {//Same suits
						if (CardsNO [i + 1] != 15 && ThisCardNO != CardsNO [i + 1] - 1)
							continue;
						if (CardsNO [i - 1] != 15 && ThisCardNO != CardsNO [i - 1] + 1)
							continue;
						Replace = true;
						MeldAddedSiblingNO = i;
						return true;
					}

					//Check Suits Melds
					if (PlayersManager4.Instance.AreEqual (CardID, CardsIDs [0]) && PlayersManager4.Instance.AreEqual (CardID, CardsIDs [2])) //Equal with 2 card
					if (PlayersManager4.Instance.AreNotTheSameSuite (CardID, CardsIDs [0])//Same Suit With 2 or 3 card
					    && PlayersManager4.Instance.AreNotTheSameSuite (CardID, CardsIDs [2])) {
						if (MeldsListGO.transform.childCount <= 3)
							continue;
						Replace = true;
						MeldAddedSiblingNO = i;
						return true;
					}
				}
			}
		return false;
	}

	bool ThereISAJokerInMeld (GameObject MeldsListGO)
	{
		for (int i = 0; i < MeldsListGO.transform.childCount; i++)
			if (MeldsListGO.transform.GetChild (i).GetComponent<Card> ().CardID.Contains ("15"))
				return true;
		return false;
	}

	bool CardCanBeAddedToSameNOMeld (GameObject MeldsListGO)
	{
		if (MeldsListGO.transform.childCount == 4)
			return false;
		for (int i = 0; i < MeldsListGO.transform.childCount; i++)
			if (!PlayersManager4.Instance.AreNotTheSameSuite (CardID, MeldsListGO.transform.GetChild (i).GetComponent<Card> ().CardID) ||
			    !PlayersManager4.Instance.AreEqual (CardID, MeldsListGO.transform.GetChild (i).GetComponent<Card> ().CardID))
				return false;
		MeldAddedSiblingNO = 3;
		return true;
	}

	public void DrawMe ()
	{
		PlayersManager4.Instance.DrawCardBTN (false);
	}

	void ChangeCardPos ()
	{
		PointerEventData pointerData = new PointerEventData (EventSystem.current);
		pointerData.position = Input.mousePosition;
		List<RaycastResult> results = new List<RaycastResult> ();
		EventSystem.current.RaycastAll (pointerData, results);
		for (int i = 0; i < results.Count; i++)
			if (results [i].gameObject.layer == LayerMask.NameToLayer ("CardLayerMask")) {
				if (results [i].gameObject.GetComponent<Card> ().CardID != CardID) {
					PlayersManagerParent.PlayerHoldingCard = false;
					MyRectTransform.SetParent (MyContentParent);
					int TargetSiblingIndex = results [i].gameObject.GetComponent<RectTransform> ().GetSiblingIndex ();
					MyRectTransform.SetSiblingIndex (TargetSiblingIndex + 1);
					PlayersManager4.Instance.CardsListFromLocalToOnline ();
					return;
				}
			}
		ReturnCardBack ();
	}
}