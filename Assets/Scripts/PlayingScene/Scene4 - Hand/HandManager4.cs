﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandManager4 : MonoBehaviour
{

	public static HandManager4 Instance;
	public GameObject[] PlayersCardsContentGO;
	public GameObject CardGO;
	[HideInInspector]
	public PlayersCardsOB1 Cards;
	private List<string> ModifeidCardList;

	void Awake ()
	{
		Instance = this;
	}

	void Start ()
	{
		ModifeidCardList = new List<string> ();
	}

	public void SetAndArrangeMyCards ()
	{
		SetAndArrangeMyCardsCor();
	}

	private void SetAndArrangeMyCardsCor(){
		PlayersManager4.Instance.IfPlayerHoldingCardGetItBack ();
		int PlayerID = Player.Instance.OnlinePlayerID;
		Cards.PlayersCards [PlayerID].PlayerCardsList = PlayersManager4.Instance.TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerID].PlayerCardsList;
		if (Cards.PlayersCards [PlayerID].PlayerCardsList.Count != PlayersCardsContentGO[0].transform.childCount)
			CreateAndAssignMyCards ();
		//ForWatcher
		if (PlayerID == -1) {
			for (int i = 0; i < PlayersCardsContentGO [0].transform.childCount; i++)
				PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card> ().CardID = Cards.PlayersCards [4].PlayerCardsList [i];
			return;
		} else {
			ModifeidCardList.Clear ();
			for (int i = 0; i < Cards.PlayersCards [PlayerID].PlayerCardsList.Count; i++)
				if (!Cards.PlayersCards [PlayerID].PlayerCardsList [i].StartsWith ("0"))
					ModifeidCardList.Add (Cards.PlayersCards [PlayerID].PlayerCardsList [i]);
			for (int i = 0; i < ModifeidCardList.Count; i++)
				try{PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card> ().CardID = ModifeidCardList [i];}catch{
				}
		}
		for (int i = 0; i < ModifeidCardList.Count; i++) {
			if (PlayersManager4.Instance.TempCurrenRoom.SeatsStatus [Player.Instance.OnlinePlayerID] == 1)
				PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card4> ().CanDrag = true;
			PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card4> ().SetCardCoverOrFace ();
		}
		PlayersManager4.Instance.RefreshMyMelds ();
		return;
	}

	public void AssignPlayerCardsAndActivateOthersCards ()
	{
		StartCoroutine (AssignPlayerCardsAndActivateOthersCardsCor ());
	}

	private IEnumerator AssignPlayerCardsAndActivateOthersCardsCor ()
	{
		Cards = new PlayersCardsOB1 ();
		CreateAndAssignMyCards ();
		for (int i = 0; i < 14; i++)
			for (int u = 1; u < 4; u++)
				PlayersCardsContentGO [u].transform.GetChild (i).gameObject.SetActive (true);
		yield return null;
	}

	private void CreateAndAssignMyCards ()
	{
		while (Cards.PlayersCards[Player.Instance.OnlinePlayerID].PlayerCardsList.Count>PlayersCardsContentGO [0].transform.childCount) {
			GameObject NewCard = Instantiate (CardGO, PlayersCardsContentGO [0].transform.position,
				Quaternion.identity, PlayersCardsContentGO [0].transform) as GameObject;
			NewCard.GetComponent<Card4> ().CanDrag = false;
			NewCard.GetComponent<Card4> ().IsCover = false;
		}
		while (Cards.PlayersCards[Player.Instance.OnlinePlayerID].PlayerCardsList.Count<PlayersCardsContentGO [0].transform.childCount) {
			DestroyImmediate (PlayersCardsContentGO [0].transform.GetChild(PlayersCardsContentGO [0].transform.childCount-1).gameObject);
		}
	}

	public void SetCardsForPlayers ()
	{
		int PlayersNO = PlayersManager4.Instance.TempCurrenRoom.GameOB.PlayersNO;
		CardsInfos4 NewCardsInfo = new CardsInfos4 ();
		for (int u = 0; u < PlayersNO; u++) {
			for (int i = 0; i < 14; i++) {
				int RandomNO = Random.Range (0, NewCardsInfo.FullCards.Count);
				Cards.PlayersCards [u].PlayerCardsList.Add (NewCardsInfo.FullCards [RandomNO]);
				NewCardsInfo.FullCards.RemoveAt (RandomNO);
			}
		}
		int RandomX = Random.Range (0, PlayersNO);
		int NewRandomNO = Random.Range (0, NewCardsInfo.FullCards.Count);
		Cards.PlayersCards [RandomX].PlayerCardsList.Add (NewCardsInfo.FullCards [NewRandomNO]);
		NewCardsInfo.FullCards.RemoveAt (NewRandomNO);
		PlayersManager4.Instance.TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = RandomX;
		PlayersManager4.Instance.ActivateCardOnThisPlayer (RandomX);
		PlayersManager4.Instance.TempCurrenRoom.GameOB.HandDetails.DrawCards = NewCardsInfo.FullCards;
	}

	private void OnDestroy ()
	{
		StopAllCoroutines ();
	}
}