﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public class LanguangeManager4 : LanguangeManagerFather {

	public Text[] Game4Txts; 

	public override void Start(){
		base.Start ();
		try{
		if(IsArabic)
				SetTheFukinArabicLang ();}catch{Debug.Log ("////////////////////////////////");
		}
	}

	public override void SetTheFukinArabicLang ()
	{
		base.SetTheFukinArabicLang ();
		SetFontForThisTxts (Game4Txts);
		Game4Txts [0].text = ArabicFixer.Fix ("اختر النزول");
		Game4Txts [1].text = ArabicFixer.Fix ("انزل الأن");
		Game4Txts [2].text = ArabicFixer.Fix ("انزل الأن");
	}

}
