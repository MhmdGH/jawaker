﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardsInfos4{
	
	public string[] CardTypes,CardNumpers;
	public List<string> FullCards;
	public CardsInfos4(){
		CardTypes = new string[4]{ "D","H","S","C"};
		CardNumpers = new string[]{"02","03","04","05","06","07","08","09","10","11","12","13","14","15"};
		FullCards = new List<string> ();
		for (int i = 0; i < 13; i++) {
			for (int u = 0; u < 4; u++) {
				FullCards.Add (CardTypes [u] + "" + CardNumpers [i]);
				FullCards.Add (CardTypes [u] + "" + CardNumpers [i]);
			}
		}
		FullCards.Add ("J" + CardNumpers [13]);
		FullCards.Add ("J" + CardNumpers [13]);
		for (int i = 0; i < FullCards.Count; i++) {
			string temp = FullCards[i];
			int randomIndex = Random.Range(i, FullCards.Count);
			FullCards[i] = FullCards[randomIndex];
			FullCards[randomIndex] = temp;
		}
	}
}