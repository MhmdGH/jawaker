﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Game4:GameOBParent{
	public Game4(int ThisPlayersNO){
		PlayersScores = new int[4]{0,0,0,0};
		Cards = new PlayersCardsOB1 ();
		CurrentRound = new Round1 ();
		PlayersNO = ThisPlayersNO;
	}
}