using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Hand
{
	public int GameEndsCounter,RoundWinnerID,LowestAcceptedSum;
	public bool FirstGame,IsPartners,IsSaudi;
	public List<string> DrawCards, ThrowdCards;
	public string LastDrawdCardFromDrawDeck,LastDrawdCardFromThrowDeck, LastThrowdCard;
	public PlayerMelds[] PlayersMeldsData;
	public int LastMeldNOAdded;
	public bool[] AddCardsToDownedMelds;
	public Hand(){
		AddCardsToDownedMelds = new bool[4]{false,false,false,false};
		LowestAcceptedSum = 0;
		RoundWinnerID = 0;
		GameEndsCounter = 0;
		IsPartners = false;
		IsSaudi = false;
		LowestAcceptedSum = 51;
		DrawCards = new List<string> ();
		PlayersMeldsData = new PlayerMelds[4];
		for (int i = 0; i < PlayersMeldsData.Length; i++)
			PlayersMeldsData[i] = new PlayerMelds ();
	}
}