using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class MeldsLists
{
	public List<string> Melds;
	public MeldsLists(){
		Melds = new List<string> ();
		for (int i = 0; i < 5; i++)
			Melds.Add ("0");
	}
}
