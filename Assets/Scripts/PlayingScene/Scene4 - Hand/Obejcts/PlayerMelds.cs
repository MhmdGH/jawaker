using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PlayerMelds
{
	public MeldsLists[] Melds;
	public PlayerMelds(){
		Melds = new MeldsLists[5];
		for (int i = 0; i < Melds.Length; i++)
			Melds [i] = new MeldsLists ();
	}
}