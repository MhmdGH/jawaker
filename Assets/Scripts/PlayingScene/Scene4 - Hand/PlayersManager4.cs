﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class PlayersManager4 : PlayersManagerParent
{
	public static PlayersManager4 Instance;
	public GameObject CardGO, DrawCardsDeckGO, ThrowdCardDeckGO, CanDrawDrawCardsDeckGO, CanDrowThrowCardDeckGO;
	public GameObject DrawAnimateCard;
	public GameObject[] OtherPlayersCardsGO;
	private List<string> PlayersCardsList;
	private List<string>[] LocalPlayersMedls;
	public GameObject[] PlayersMeldsGO;

	public override void Awake ()
	{
		base.Awake ();
		Instance = this;
	}

	public override void Start ()
	{
		TempCurrenRoom = new JsonRoomO ("Default", new string[4]{ "0", "0", "0", "0" }, new Game4 (0));
		base.Start ();
		CardsListGO = PlayersCardsGO [0].transform.GetChild (1).GetChild (0).gameObject;
		PlayersCardsList = new List<string> ();
		LocalPlayersMedls = new List<string>[5];
		for (int i = 0; i < LocalPlayersMedls.Length; i++)
			LocalPlayersMedls [i] = new List<string> ();
		SceneDoneLoading = false;
	}

	//Main Reciever Fun
	public override void UpdateRoom (JsonRoomO CurrentRoomJSON)
	{
		base.UpdateRoom (CurrentRoomJSON);
		
		if (TempCurrenRoom.NoMaster)
			AssignNewMasterOrRemoveRoom ();
		if (TempCurrenRoom.Update == 0) {
			PlayingFireBaseManager4.Instance.SetOnDissconnectHandle ();
			return;
		}
		PlayingWindowsManager4.Instance.StopTimer ();
		switch (TempCurrenRoom.ActionID) {
		case -1:
			UpdateRoomPhaseMinus1();
			break;
		case 0:
			UpdateRoomPhase0 ();
			break;
		case 2:
			UpdateRoomPhase2 ();
			break;
		case 3:
			UpdateRoomPhase3 ();
			break;
		case 4:
			UpdateRoomPhase4 ();
			break;
		case 5:
			UpdateRoomPhase5 ();
			break;
		case 6:
			UpdateRoomPhase6 ();
			break;
		case 7:
			UpdateRoomPhase7 ();
			break;
		case 8:
			UpdateRoomPhase8 ();
			break;
		case 9:
			UpdateRoomPhase9 ();
			break;
		case 10:
			UpdateRoomPhase10 ();
			break;
		case 11:
			UpdateRoomPhase11 ();
			break;
		case 12:
			UpdateRoomPhase12 ();
			break;
		case 13:
			UpdateRoomPhase13 ();
			break;
		case 14:
			UpdateRoomPhase14 ();
			break;
		case 15:
			UpdateRoomPhase15 ();
			break;
		case 16:
			UpdateRoomPhase16 ();
			break;
		}
	}

	void UpdateRoomPhaseMinus1 ()
	{
		AudioManager.Instance.PlaySound (0);
		if (!Player.Instance.GameCreator)
			return;
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++)
			TempCurrenRoom.PlayersReady [i] = false;
		HandManager4.Instance.SetCardsForPlayers ();
		TempCurrenRoom.Available = false;
		for (int i = 0; i < 4; i++)
			TempCurrenRoom.GameOB.Cards.PlayersCards [i].PlayerCardsList = HandManager4.Instance.Cards.PlayersCards [i].PlayerCardsList;
		TempCurrenRoom.ActionID = 4;
		PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase0 ()
	{
		if (TempCurrenRoom.ReloadScene)
			ReloadScene ();
	}

	void ReloadScene ()
	{
		TempCurrenRoom.ReloadScene = false;
		TempCurrenRoom.ActionID = 3;
		SceneDoneLoading = true;
		if (Player.Instance.GameCreator)
			PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}

	public void MasterTakeControl (int funToDOAfterEnd)
	{
		if (funToDOAfterEnd == 1)//Draw
			AutoDraw ();
		else if (funToDOAfterEnd == 2)//ThrowCard
			AutoThrowCard (false);
		else if (funToDOAfterEnd == 3)//throw drawd card from throwd deck and draw card from draw deck
			AutoThrowCard (true);
	}

	void AutoDraw ()
	{
		int PlayerSeatStatus = TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay];
		//If Player Is Here
		if (MyTurnToPlay () && PlayerSeatStatus == 1) {//If Player Here
			DrawCardBTN (true);
			return;
		} else if (Player.Instance.GameCreator && PlayerSeatStatus == 0) {//If Player Not Here, Master Do IT
			AutoDrawNow ();
		}
	}

	void AutoDrawNow ()
	{
		TempCurrenRoom.GameOB.HandDetails.LastDrawdCardFromDrawDeck = "";
		TempCurrenRoom.GameOB.HandDetails.LastDrawdCardFromThrowDeck = "";
		TempCurrenRoom.GameOB.HandDetails.LastThrowdCard = "";
		TempCurrenRoom.GameOB.HandDetails.LastDrawdCardFromDrawDeck = TempCurrenRoom.GameOB.HandDetails.DrawCards [0];
		TempCurrenRoom.GameOB.Cards.PlayersCards [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay].PlayerCardsList.Add (TempCurrenRoom.GameOB.HandDetails.DrawCards [0]);
		TempCurrenRoom.GameOB.HandDetails.DrawCards.RemoveAt (0);
		TempCurrenRoom.ActionID = 6;
		PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void AutoThrowCard (bool ReturnDrawdCardFromThrowList)
	{
		int PlayerSeatStatus = TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay];
		//If Player Is Here
		if (MyTurnToPlay () && PlayerSeatStatus == 1) {
			IfPlayerHoldingCardGetItBack ();
			for (int i = 0; i < CardsListGO.transform.childCount; i++) {
				string ThisCardID = CardsListGO.transform.GetChild (i).GetComponent<Card> ().CardID;
				if (GetCardNO (ThisCardID) != 15) {
					if (ReturnDrawdCardFromThrowList) {
						if (ThisCardID == TempCurrenRoom.GameOB.HandDetails.LastDrawdCardFromThrowDeck) {
							CardsListGO.transform.GetChild (i).GetComponent<Card4> ().SendCard ();
							CheckPlayerKick ();
							return;
						}
					} else {
						CardsListGO.transform.GetChild (i).GetComponent<Card4> ().SendCard ();
						CheckPlayerKick ();
						return;
					}
				}
			}
			//If Player Not Here, Master Do IT
		} else if (Player.Instance.GameCreator && PlayerSeatStatus == 0) {
			TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay] = 0;
			if (ReturnDrawdCardFromThrowList)
				ThrowCardNow (TempCurrenRoom.GameOB.HandDetails.LastDrawdCardFromThrowDeck);
			else
				ThrowCardNow (FirstAvailabeCardOnOtherPlayer ());
		}
	}

	private string FirstAvailabeCardOnOtherPlayer ()
	{
		List<string> ThisPlayerCardsList = new List<string> ();
		for (int i = 0; i < 14; i++)
			ThisPlayerCardsList = TempCurrenRoom.GameOB.
				Cards.PlayersCards [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay].PlayerCardsList;
		for (int i = 0; i < ThisPlayerCardsList.Count; i++)
			if (ThisPlayerCardsList [i].Length > 2 && GetCardNO (ThisPlayerCardsList [i]) != 15) {
				return ThisPlayerCardsList [i];
			}
		//Not Reachable
		return "0";
	}

	public void RestartGame ()
	{
		PlayingWindowsManager4.Instance.WinsWindow.SetActive (false);
		Game4 NewGameOB = new Game4 (TempCurrenRoom.GameOB.PlayersNO);
		for (int i = 0; i < NewGameOB.PlayersScores.Length; i++)
			NewGameOB.PlayersScores [i] = 0;
		TempCurrenRoom.GameOB = NewGameOB;
		TempCurrenRoom.ReloadScene = true;
		TempCurrenRoom.ActionID = 0;
		PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private int ThisWinnerID;
	void UpdateRoomPhase16 ()
	{
		//Game End, Show Winner
		int WinnerID = 0;
		List<int> PlayersScores = new List<int> ();
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++)
			PlayersScores.Add (TempCurrenRoom.GameOB.PlayersScores [i]);
		PlayersScores.Sort ();
		for (int i = 0; i < 4; i++) {
			if (TempCurrenRoom.GameOB.PlayersScores [i] == PlayersScores [3]) {
				if (TempCurrenRoom.GameOB.HandDetails.IsPartners) {
					if (i == 0 || i == 2)
						WinnerID = 1;
					else
						WinnerID = 2;
				} else
					WinnerID = i;
			}
		}
		if (WinnerID == 1)
			ThisWinnerID = 0;
		else
			ThisWinnerID = 1;
		AddWinsPointsForPlayers (WinnerID);
		if (MainFireBaseManager.Instance.IsACompition)
			InnerSetCompData ();
		PlayingWindowsManager4.Instance.ShowWindWindow (ThisWinnerID);
	}

	private void InnerSetCompData ()
	{
		if (Player.Instance.GameCreator) {
			for (int i = 0; i < TempCurrenRoom.DatabasePlayersIDs.Length; i++)
				if (TempCurrenRoom.DatabasePlayersIDs [i].Length < 4)
					return;
			if (ThisWinnerID == 0) {
				SetCompData (TempCurrenRoom.DatabasePlayersIDs [0], TempCurrenRoom.DatabasePlayersIDs [2],
					TempCurrenRoom.PlayersFBIDs [0], TempCurrenRoom.PlayersFBIDs [2]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [1]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [3]);
			} else {
				SetCompData (TempCurrenRoom.DatabasePlayersIDs [1], TempCurrenRoom.DatabasePlayersIDs [3],
					TempCurrenRoom.PlayersFBIDs [1], TempCurrenRoom.PlayersFBIDs [3]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [0]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [2]);
			}
		}
	}

	private void AddWinsPointsForPlayers(int WinnerID){
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (TempCurrenRoom.GameOB.HandDetails.IsPartners) {
			if (WinnerID == 0 && (PlayerID == 0 || PlayerID == 2)) {
				Player.Instance.AddXPsAndCLubPoints (XPsValue);
				Player.Instance.Coins += ScoreValue;
			} else if (WinnerID == 1 && (PlayerID == 1 || PlayerID == 3)) {
				Player.Instance.AddXPsAndCLubPoints (XPsValue);
				Player.Instance.Coins += ScoreValue;
			}
		} else {
			if (PlayerID == WinnerID) {
				Player.Instance.AddXPsAndCLubPoints (XPsValue);
				Player.Instance.Coins += ScoreValue;
			}
		}
	}

	void UpdateRoomPhase15 ()
	{
		if (!Player.Instance.GameCreator)
			return;
	}

	void UpdateRoomPhase14 ()
	{
		//FullRound Ends
		if (Player.Instance.OnlinePlayerID != -1)
			OnRoundEndsResultAndResets ();
		if (!Player.Instance.GameCreator)
			return;
		if (TempCurrenRoom.GameOB.HandDetails.GameEndsCounter >= 5) {
			TempCurrenRoom.ActionID = 16;
			PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
		} else
			NewRound ();
	}

	private void NewRound ()
	{
		//New Round
		//Save the PlaresNO,PlayersScores,GameEndsCounter
		Game4 NewGameOB = new Game4 (TempCurrenRoom.GameOB.PlayersNO);
		for (int i = 0; i < NewGameOB.PlayersScores.Length; i++)
			NewGameOB.PlayersScores [i] = TempCurrenRoom.GameOB.PlayersScores [i];
		NewGameOB.HandDetails = new Hand ();
		NewGameOB.HandDetails.GameEndsCounter = TempCurrenRoom.GameOB.HandDetails.GameEndsCounter;
		NewGameOB.HandDetails.IsPartners = TempCurrenRoom.GameOB.HandDetails.IsPartners;
		NewGameOB.HandDetails.IsSaudi = TempCurrenRoom.GameOB.HandDetails.IsSaudi;
		TempCurrenRoom.GameOB = NewGameOB;
		TempCurrenRoom.ReloadScene = true;
		TempCurrenRoom.ActionID = 0;
		PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void OnRoundEndsResultAndResets ()
	{
		for (int i = 0; i < 4; i++)
			PlayingWindowsManager4.Instance.TeamsScores [FromOnlineToLocal (i)].GetComponent<Text> ().text = TempCurrenRoom.GameOB.PlayersScores [i].ToString ();
	}

	void UpdateRoomPhase13 ()
	{
		//Full Round Ends
		for (int i = 0; i < 4; i++)
			PlayingWindowsManager4.Instance.PlayersScoresGO [i].GetComponent<Text> ().text = "0";

		if (!Player.Instance.GameCreator)
			return;

		if (TempCurrenRoom.GameOB.HandDetails.IsPartners) {
			int Team1Score = TempCurrenRoom.GameOB.CurrentRound.PlayersScores [0] + TempCurrenRoom.GameOB.CurrentRound.PlayersScores [2];
			int Team2Score = TempCurrenRoom.GameOB.CurrentRound.PlayersScores [1] + TempCurrenRoom.GameOB.CurrentRound.PlayersScores [3];
			TempCurrenRoom.GameOB.CurrentRound.PlayersScores [0] = Team1Score;
			TempCurrenRoom.GameOB.CurrentRound.PlayersScores [2] = Team1Score;
			TempCurrenRoom.GameOB.CurrentRound.PlayersScores [1] = Team2Score;
			TempCurrenRoom.GameOB.CurrentRound.PlayersScores [3] = Team2Score;
		}

		for (int i = 0; i < 4; i++) {
			int CurrentRoundPlayerScore = TempCurrenRoom.GameOB.CurrentRound.PlayersScores [i];
			TempCurrenRoom.GameOB.PlayersScores [i] += CurrentRoundPlayerScore;
		}
		TempCurrenRoom.ActionID = 14;
		PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase12 ()
	{
	}

	private bool DoTheScoringOnce;

	private void UpdateRoomPhase11 ()
	{
		if (DoTheScoringOnce && Player.Instance.GameCreator)
			StartCoroutine (DoTheScoring ());
	}

	private IEnumerator DoTheScoring ()
	{
		yield return new WaitForSeconds (2);
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++) {
			if (i == TempCurrenRoom.GameOB.HandDetails.RoundWinnerID) {//Round Winner Here
				if (TempCurrenRoom.GameOB.HandDetails.AddCardsToDownedMelds [i])
					TempCurrenRoom.GameOB.CurrentRound.PlayersScores [i] -= 30;
				else
					TempCurrenRoom.GameOB.CurrentRound.PlayersScores [i] -= 60;
			} else {//Fukin Lossers
				int DoubleValue = 1;
				if (!TempCurrenRoom.GameOB.HandDetails.AddCardsToDownedMelds [TempCurrenRoom.GameOB.HandDetails.RoundWinnerID])
					DoubleValue = 2;

				if (TempCurrenRoom.GameOB.HandDetails.PlayersMeldsData [i].Melds [0].Melds [0].Length <= 1) {//This Player Not melded
					TempCurrenRoom.GameOB.CurrentRound.PlayersScores [i] += 100 * DoubleValue;
					continue;
				}

				for (int u = 0; u < TempCurrenRoom.GameOB.Cards.PlayersCards [i].PlayerCardsList.Count; u++) {//Loop for all players cards
					string ThisCardID = TempCurrenRoom.GameOB.Cards.PlayersCards [i].PlayerCardsList [u];
					TempCurrenRoom.GameOB.CurrentRound.PlayersScores [i] += GetActualValueOfThisCard (ThisCardID) * DoubleValue;
				}
			}
		}
		TempCurrenRoom.ActionID = 13;
		PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private int GetActualValueOfThisCard (string thisCardID)
	{
		int CardValue = GetCardNO (thisCardID);
		if (CardValue == 15)
			return 15;
		else if (CardValue == 14)
			return 11;
		else if (CardValue >= 10 && CardValue <= 13)
			return 10;
		else
			return CardValue;
	}

	private void UpdateRoomPhase10 ()
	{
		int PlayerIDTurn = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		TempCurrenRoom.GameOB.HandDetails.AddCardsToDownedMelds [PlayerIDTurn] = true;
		if (MyTurnToPlay ())
			HandManager4.Instance.SetAndArrangeMyCards ();
		RefreshThisMeld (PlayerIDTurn);
	}

	void RefreshThisMeld (int playerIDTurn)
	{
		//Remove All List Cards,and recreate it from online meld list
		int LastMeldNO = TempCurrenRoom.GameOB.HandDetails.LastMeldNOAdded;
		List<string> TargetedMeldList = TempCurrenRoom.GameOB.HandDetails.PlayersMeldsData [playerIDTurn].Melds [LastMeldNO].Melds;
		for (int i = 0; i < TargetedMeldList.Count; i++) {
			//If There is a card, Change it ID
			if (i < PlayersMeldsGO [FromOnlineToLocal (playerIDTurn)].transform.GetChild (LastMeldNO).childCount) {
				GameObject ThisCardGO = PlayersMeldsGO [FromOnlineToLocal (playerIDTurn)].transform.GetChild (LastMeldNO).GetChild (i).gameObject;
				ThisCardGO.GetComponent<Card4> ().CardID = TargetedMeldList [i];
				ThisCardGO.GetComponent<Card4> ().SetCardCoverOrFace ();
			} else {
				if (TargetedMeldList [i].Length < 2)
					break;
				GameObject NewCardGO = Instantiate (CardGO, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
				NewCardGO.GetComponent<RectTransform> ().SetParent (PlayersMeldsGO [FromOnlineToLocal (playerIDTurn)].transform.GetChild (LastMeldNO).GetComponent<RectTransform> ());
				NewCardGO.GetComponent<Card4> ().CardID = TargetedMeldList [i];
				NewCardGO.GetComponent<Card4> ().IsCover = false;
				NewCardGO.GetComponent<Card4> ().SetCardCoverOrFace ();
			}
		}
		TempCurrenRoom.ActionID = 7;
		PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	public void CardAddedToDownedMeld (string ThisCardID, int MeldNO, int SiblingNO, bool Replace)
	{
		int PlayerIDTurn = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;

		for (int i = 0; i < TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerIDTurn].PlayerCardsList.Count; i++)
			if (TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerIDTurn].PlayerCardsList [i] == ThisCardID) {
				TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerIDTurn].PlayerCardsList.RemoveAt (i);
				break;
			}
		if (Replace)
			TempCurrenRoom.GameOB.HandDetails.PlayersMeldsData [PlayerIDTurn].Melds [MeldNO].Melds [SiblingNO] = ThisCardID;
		else
			TempCurrenRoom.GameOB.HandDetails.PlayersMeldsData [PlayerIDTurn].Melds [MeldNO].Melds.Insert (SiblingNO, ThisCardID);
		if (Replace)
			TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerIDTurn].PlayerCardsList.Add ("H15");
		GoToPhase10 (MeldNO);
		return;
	}

	private void GoToPhase10 (int MeldNO)
	{
		TempCurrenRoom.GameOB.HandDetails.LastMeldNOAdded = MeldNO;
		TempCurrenRoom.ActionID = 10;
		PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void RecieveThrowThisCard (int PlayerThrowerID, string ThrowdCardID, bool NextTurn)
	{
		AudioManager.Instance.PlaySound (28);
		int PlayerIDOnMyDevice = FromOnlineToLocal (PlayerThrowerID);
		GameObject NewCardGO = Instantiate (CardGO, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
		NewCardGO.GetComponent<RectTransform> ().SetParent (ThrowdCardDeckGO.GetComponent<RectTransform> ());
		if (ThrowdCardDeckGO.transform.childCount >= 4)
			Destroy (ThrowdCardDeckGO.transform.GetChild (1).gameObject);
		NewCardGO.GetComponent<Card4> ().CardID = ThrowdCardID;
		NewCardGO.GetComponent<Card4> ().IsCover = false;
		NewCardGO.GetComponent<Card4> ().SetCardCoverOrFace ();
		NewCardGO.GetComponent<Card4> ().ThroughIt (AnimationFromLocalToActual (PlayerIDOnMyDevice));
		NewCardGO.GetComponent<Card4> ().CanDraw = true;
		ThrowdCardDeckGO.GetComponent<Button> ().interactable = true;
		Vector3 PrevCardVect = NewCardGO.GetComponent<RectTransform> ().localPosition;
		NewCardGO.GetComponent<RectTransform> ().localPosition = new Vector3 (PrevCardVect.x, PrevCardVect.y, 0);
		DeActiveCardFromOtherPlayer (PlayerIDOnMyDevice);
		if (Player.Instance.GameCreator) {
			if (IsRoundEnds ())
				RoundsEnds ();
			else
				StartCoroutine (GoToPhaseCor (NextTurn));
		}
	}

	bool IsRoundEnds ()
	{
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++) {
			if (TempCurrenRoom.GameOB.Cards.PlayersCards [i].PlayerCardsList.Count == 0) {
				TempCurrenRoom.GameOB.HandDetails.RoundWinnerID = i;
				return true;
			}
		}
		return false;
	}

	void RoundsEnds ()
	{
		TempCurrenRoom.GameOB.HandDetails.GameEndsCounter++;
		DoTheScoringOnce = true;
		TempCurrenRoom.ActionID = 11;
		PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private int AnimationFromLocalToActual (int FromOnlineToLocalPlayerID)
	{

		int ActualNO = FromOnlineToLocalPlayerID;
		if (TempCurrenRoom.GameOB.PlayersNO == 2) {
			if (ActualNO == 1)
				ActualNO = 2;
		} else if (TempCurrenRoom.GameOB.PlayersNO == 3) {
			if (ActualNO == 2)
				ActualNO = 3;
		}
		return ActualNO;
	}

	private IEnumerator GoToPhaseCor (bool NextTurn)
	{
		yield return new WaitForSeconds (0.5f);
		if (NextTurn)
			TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = NextID ();
		TempCurrenRoom.ActionID = 5;
		PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void DeActiveCardFromOtherPlayer (int PlayerIDOnMyDevice)
	{
		if (PlayerIDOnMyDevice != 0 || Player.Instance.OnlinePlayerID == -1) {
			OtherPlayersCardsGO [PlayerIDOnMyDevice].transform.
			GetChild (TempCurrenRoom.GameOB.CurrentRound.RoundNO).gameObject.SetActive (false);
		}
	}

	private bool CanThrowCard = true;

	public void ThrowCard (GameObject Cardjo)
	{
		if (GetCardNO (Cardjo.GetComponent<Card> ().CardID) == 15)
			Cardjo.GetComponent<Card4> ().ReturnCardBack ();
		else if (MyTurnToPlay () && TempCurrenRoom.ActionID == 7 && CanThrowCard)
			StartCoroutine (RemoveOrderly (Cardjo));
		else if (MyTurnToPlay () && TempCurrenRoom.ActionID == 8 && PlayerMeldedWithTheDrawdCard () && CanThrowCard)
			StartCoroutine (RemoveOrderly (Cardjo));
		else if (MyTurnToPlay () && TempCurrenRoom.ActionID == 8
		         && Cardjo.GetComponent<Card> ().CardID == TempCurrenRoom.GameOB.HandDetails.LastDrawdCardFromThrowDeck && CanThrowCard)
			StartCoroutine (RemoveOrderly (Cardjo));
		else
			Cardjo.GetComponent<Card4> ().ReturnCardBack ();
	}

	bool PlayerMeldedWithTheDrawdCard ()
	{
		string LastDrawdCard = TempCurrenRoom.GameOB.HandDetails.LastDrawdCardFromThrowDeck;
		MeldsLists[] ThisMeldsDataLists = TempCurrenRoom.GameOB.HandDetails.PlayersMeldsData [Player.Instance.OnlinePlayerID].Melds;
		for (int i = 0; i < ThisMeldsDataLists.Length; i++) {
			for (int u = 0; u < ThisMeldsDataLists [i].Melds.Count; u++) {
				if (ThisMeldsDataLists [i].Melds [u] == LastDrawdCard)
					return true;
			}
		}
		return false;
	}

	private IEnumerator RemoveOrderly (GameObject Cardjo)
	{
		CanThrowCard = false;
		ThrowCardNow (Cardjo.GetComponent<Card4> ().CardID);
		DestroyImmediate (Cardjo);
		yield return new WaitForSeconds (0.03f);
		HandManager4.Instance.SetAndArrangeMyCards ();
		yield return new WaitForSeconds (2f);
		CanThrowCard = true;
		yield return null;
	}

	void ThrowCardNow (string CardID)
	{
		string CardIDString = CardID;
		TempCurrenRoom.GameOB.HandDetails.LastThrowdCard = CardID;
		int PlayerTurn = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		TempCurrenRoom.GameOB.HandDetails.ThrowdCards.Add (CardID);
		if (TempCurrenRoom.GameOB.HandDetails.ThrowdCards.Count >= 3)
			AddFromThrowToDrawDecks ();
		RemoveThisCardFromOnlineCardsList (PlayerTurn, CardIDString);
		PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void AddFromThrowToDrawDecks ()
	{
		TempCurrenRoom.GameOB.HandDetails.DrawCards.Add (TempCurrenRoom.GameOB.HandDetails.ThrowdCards [0]);
		TempCurrenRoom.GameOB.HandDetails.ThrowdCards.RemoveAt (0);
	}

	private void RemoveThisCardFromOnlineCardsList (int PlayerNO, string CardID)
	{
		for (int i = 0; i < TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList.Count; i++) {
			if (TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList [i] == CardID) {
				TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList.Remove (CardID);
				return;
			}
		}
	}

	private int CurrentCollectorID;

	void CloseRound ()
	{
	}

	private int NextID ()
	{
		int ThisPlayerID = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		ThisPlayerID++;
		if (ThisPlayerID >= TempCurrenRoom.GameOB.PlayersNO)
			ThisPlayerID = 0;
		return ThisPlayerID;
	}



	void UpdateRoomPhase9 ()
	{
		//Player Melds Down
		HandManager4.Instance.SetAndArrangeMyCards ();
		int MeldsPlayerIDTurn = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		AddNewMeldsToThisPlayerMelds (MeldsPlayerIDTurn);				
	}

	private void AddNewMeldsToThisPlayerMelds (int ThisPlayerNO)
	{
		for (int i = 0; i < 5; i++)
			if (PlayersMeldsGO [FromOnlineToLocal (ThisPlayerNO)].transform.GetChild (i).childCount == 0 && //Empty local Meld List
			    TempCurrenRoom.GameOB.HandDetails.PlayersMeldsData [ThisPlayerNO].Melds [i].Melds [0].Length > 2) {
				for (int u = 0; u < TempCurrenRoom.GameOB.HandDetails.PlayersMeldsData [ThisPlayerNO].Melds [i].Melds.Count; u++) {
					if (TempCurrenRoom.GameOB.HandDetails.PlayersMeldsData [ThisPlayerNO].Melds [i].Melds [u].Length <= 1)
						continue;
					GameObject NewCardGO = Instantiate (CardGO, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
					NewCardGO.GetComponent<RectTransform> ().SetParent (PlayersMeldsGO [FromOnlineToLocal (ThisPlayerNO)].transform.GetChild (i).
						GetComponent<RectTransform> ());
					NewCardGO.GetComponent<Card4> ().CardID = TempCurrenRoom.GameOB.HandDetails.PlayersMeldsData [ThisPlayerNO].Melds [i].Melds [u];
					NewCardGO.GetComponent<Card4> ().IsCover = false;
					NewCardGO.GetComponent<Card4> ().SetCardCoverOrFace ();
					NewCardGO.GetComponent<Card4> ().CanDraw = false;
					NewCardGO.gameObject.layer = 9;
				}
			}
		if (!Player.Instance.GameCreator)
			return;
		if (TempCurrenRoom.GameOB.HandDetails.LastDrawdCardFromThrowDeck.Length > 1)
			TempCurrenRoom.ActionID = 8;
		else
			TempCurrenRoom.ActionID = 7;
		PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	//Reciev Card Drawded From Throwd Cards ,So Must Meld Turn
	void UpdateRoomPhase8 ()
	{
		HideMeldsStuff ();
		if (MyTurnToPlay ()) {
			HandManager4.Instance.SetAndArrangeMyCards ();
		} 
		int ThrowerID = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;

		PlayingWindowsManager4.Instance.SetTimer (FromOnlineToLocal (ThrowerID),
			TimerWaitingTime (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay), 3, false);

		if (TempCurrenRoom.GameOB.HandDetails.LastThrowdCard.Length > 1) {
			if (ReturnTheDrawdCard ())//Return DrawdCardfromThreowddeck
				RecieveThrowThisCard (ThrowerID, TempCurrenRoom.GameOB.HandDetails.LastThrowdCard, false);
			else//Melded with the throwd card and throwd another one
				RecieveThrowThisCard (ThrowerID, TempCurrenRoom.GameOB.HandDetails.LastThrowdCard, true);
		}
	}

	public void GoDownNowBTN ()
	{
		if (TempCurrenRoom.GameOB.HandDetails.IsSaudi && IsFirstMeld ())
			TempCurrenRoom.GameOB.HandDetails.LowestAcceptedSum = ComulativeSum + 1;
		PlayingWindowsManager4.Instance.MeldsWindowGO.SetActive (false);
		PlayingWindowsManager4.Instance.GoDownBTN.SetActive (false);
		for (int i = 0; i < 5; i++) {//Search for empty online melds lists
			if (TempCurrenRoom.GameOB.HandDetails.PlayersMeldsData [Player.Instance.OnlinePlayerID].Melds [i].Melds [0].Length <= 1) {
				for (int u = 0; u < LocalPlayersMedls.Length; u++)
					if (LocalPlayersMedls [u].Count > 1) {//Search for full local melds lists
						if (PlayingWindowsManager4.Instance.CardsMeldsGO [u].GetComponent<Image> ().color.a <= 0.5f)
							continue;
						for (int y = 0; y < LocalPlayersMedls [u].Count; y++) {//loop for this meld list cards
							TempCurrenRoom.GameOB.HandDetails.PlayersMeldsData [Player.Instance.OnlinePlayerID].
							Melds [i].Melds [y] = (LocalPlayersMedls [u] [y]);
							string MeldCardID = LocalPlayersMedls [u] [y];
							RemoveThisCardFromOnlineCardsList (Player.Instance.OnlinePlayerID, MeldCardID);
						}
						LocalPlayersMedls [u].Clear ();
						break;
					}
			}
		}
		TempCurrenRoom.ActionID = 9;
		PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private int ComulativeSum;

	public void CheckCanGoDownNowBTN (int ThisComulativeSum)
	{
		ComulativeSum = ThisComulativeSum;
		if (!IsFirstMeld ())//Melded Before
			PlayingWindowsManager4.Instance.GoDownNowBTN.SetActive (true);
		else if (ComulativeSum >= TempCurrenRoom.GameOB.HandDetails.LowestAcceptedSum)//First meld
			PlayingWindowsManager4.Instance.GoDownNowBTN.SetActive (true);
	}

	private bool IsFirstMeld ()
	{
		if (TempCurrenRoom.GameOB.HandDetails.PlayersMeldsData [Player.Instance.OnlinePlayerID].Melds [0].Melds [0].Length > 1)//Melded Before
			return false;
		else
			return true;
	}

	private List<string> MyCardList;

	public void RefreshMyMelds ()
	{
		PlayingWindowsManager4.Instance.GoDownBTN.SetActive (false);
		if (CardsListGO.transform.childCount <= 3)
			return;
		for (int i = 0; i < CardsListGO.transform.childCount; i++)
			CardsListGO.transform.GetChild (i).GetComponent<Card4> ().MeldSign.SetActive (false);
		for (int i = 0; i < 5; i++)
			LocalPlayersMedls [i].Clear ();
		MyCardList = TempCurrenRoom.GameOB.Cards.PlayersCards [Player.Instance.OnlinePlayerID].PlayerCardsList;
		//Check Continues
		for (int i = 0; i < MyCardList.Count; i++) {
			if (i > MyCardList.Count - 3 || i > CardsListGO.transform.childCount - 3)
				break;
			List<string> MyCards = new List<string> (5);
			MyCards.Add (MyCardList [i]);
			MyCards.Add (MyCardList [i + 1]);
			MyCards.Add (MyCardList [i + 2]);
			if (i + 3 < MyCardList.Count)
				MyCards.Add (MyCardList [i + 3]);
			if (i + 4 < MyCardList.Count)
				MyCards.Add (MyCardList [i + 4]);
			if (CardsListGO.transform.GetChild (i).GetComponent<Card4> ().MeldSign.activeSelf ||
			    CardsListGO.transform.GetChild (i + 1).GetComponent<Card4> ().MeldSign.activeSelf ||
			    CardsListGO.transform.GetChild (i + 2).GetComponent<Card4> ().MeldSign.activeSelf)
				continue;
			if (AreTheSameSuite (MyCards [0], MyCards [1])) {
				//2 Cards are have same suits
				if (AreInARow (MyCards [0], MyCards [1])) {
					//Tow Cards in a row
					if (AreTheSameSuite (MyCards [1], MyCards [2])) {
						//2 Cards are have same suits
						if (AreInARow (MyCards [1], MyCards [2]) && //Socend And Third Card in a row&& (0 card &&2 card are in a row//
						    ((GetCardNO (MyCards [0]) == (GetCardNO (MyCards [2])) - 2) || ((GetCardNO (MyCards [0]) == 15) || (GetCardNO (MyCards [2]) == 15) ||
						    (GetCardNO (MyCards [0]) == 14 && GetCardNO (MyCards [2]) == 3)))) {
							//Tow Cards in a row
							if ((GetCardNO (MyCards [0]) == 13) || ((GetCardNO (MyCards [1]) == 14)))
								continue;
							if (GetCardNO (MyCards [0]) != 15 && (!AreTheSameSuite (MyCards [0], MyCards [1]) || !AreTheSameSuite (MyCards [0], MyCards [2])))
								continue;
							CardsListGO.transform.GetChild (i).GetComponent<Card4> ().MeldSign.SetActive (true);
							CardsListGO.transform.GetChild (i + 1).GetComponent<Card4> ().MeldSign.SetActive (true);
							CardsListGO.transform.GetChild (i + 2).GetComponent<Card4> ().MeldSign.SetActive (true);
							List<string> MeldList = new List<string> ();
							for (int u = 0; u < 3; u++)
								MeldList.Add (MyCards [u]);
							if (GetCardNO (MyCards [1]) == 15 && GetCardNO (MyCards [2]) == 15) {
								AddThisMeld (MeldList, true);
								continue;
							}
							if (MyCards.Count > 3 && MyCards [3].Length > 1) {
								if (GetCardNO (MyCards [2]) == 15 && GetCardNO (MyCards [3]) == 15)
									continue;
								if (AreTheSameSuite (MyCards [2], MyCards [3])) {
									if (AreInARow (MyCards [2], MyCards [3]) &&
									    ((ISItAJoker (MyCards [1]) || ISItAJoker (MyCards [3])) || (GetCardNO (MyCards [1]) == GetCardNO (MyCards [3]) - 2))) {
										if ((GetCardNO (MyCards [1]) == 13) || ((GetCardNO (MyCards [2]) == 14))) {
											AddThisMeld (MeldList, true);
											continue;
										}
										if (CardsListGO.transform.GetChild (i + 3).GetComponent<Card4> ().MeldSign.activeSelf)
											continue;
										CardsListGO.transform.GetChild (i + 3).GetComponent<Card4> ().MeldSign.SetActive (true);
										MeldList.Add (MyCards [3]);
										if (MyCards.Count > 4 && MyCards [4].Length > 1) {
											if (GetCardNO (MyCards [3]) == 15 && GetCardNO (MyCards [4]) == 15)
												continue;
											if (AreTheSameSuite (MyCards [3], MyCards [4])) {
												if (AreInARow (MyCards [3], MyCards [4]) &&
												    ((ISItAJoker (MyCards [2]) || ISItAJoker (MyCards [4])) || (GetCardNO (MyCards [2]) == GetCardNO (MyCards [4]) - 2))) {
													if ((GetCardNO (MyCards [2]) == 13) || ((GetCardNO (MyCards [3]) == 14)))
														continue;
													if (CardsListGO.transform.GetChild (i + 4).GetComponent<Card4> ().MeldSign.activeSelf)
														continue;
													CardsListGO.transform.GetChild (i + 4).GetComponent<Card4> ().MeldSign.SetActive (true);
													MeldList.Add (MyCards [4]);
												}
											}
										}
									}
								}
							}
							AddThisMeld (MeldList, true);
						}
					}
				}
			}
		}

		//Check Same number,Different Suits

		for (int i = 0; i < MyCardList.Count; i++) {
			if (i > MyCardList.Count - 3 || i > CardsListGO.transform.childCount - 3)
				break;
			List<string> MyCards = new List<string> (4);
			MyCards.Add (MyCardList [i]);
			MyCards.Add (MyCardList [i + 1]);
			MyCards.Add (MyCardList [i + 2]);
			if (i + 3 < MyCardList.Count)
				MyCards.Add (MyCardList [i + 3]);
			if (CardsListGO.transform.GetChild (i).GetComponent<Card4> ().MeldSign.activeSelf ||
			    CardsListGO.transform.GetChild (i + 1).GetComponent<Card4> ().MeldSign.activeSelf ||
			    CardsListGO.transform.GetChild (i + 2).GetComponent<Card4> ().MeldSign.activeSelf)
				continue;
			if (AreNotTheSameSuite (MyCards [0], MyCards [1])) {//2 Cards are not have same suits
				if (AreEqual (MyCards [0], MyCards [1])) {//Tow Cards Equal
					if (AreNotTheSameSuite (MyCards [1], MyCards [2]) && AreNotTheSameSuite (MyCards [0], MyCards [2])) {//2 Cards are not have same suits
						if (AreEqual (MyCards [1], MyCards [2])) {//Tow Cards Equals
							if (!AreEqual (MyCards [0], MyCards [2]))
								continue;
							List<string> MeldList = new List<string> ();
							for (int u = 0; u < 3; u++) {
								CardsListGO.transform.GetChild (i + u).GetComponent<Card4> ().MeldSign.SetActive (true);
								MeldList.Add (MyCards [u]);
							}
							if (MyCards.Count > 3 && MyCards [3].Length > 1) {
								if (AreNotTheSameSuite (MyCards [2], MyCards [3]) && AreNotTheSameSuite (MyCards [1], MyCards [3]) && AreNotTheSameSuite (MyCards [0], MyCards [3])) {//2 Cards are not have same suits
									if (AreEqual (MyCards [2], MyCards [3])) {//Tow Cards in a row
										if (!AreEqual (MyCards [0], MyCards [3]) || !AreEqual (MyCards [1], MyCards [3]))
											continue;
										CardsListGO.transform.GetChild (i + 3).GetComponent<Card4> ().MeldSign.SetActive (true);
										MeldList.Add (MyCards [3]);
									}
								}
							}
							AddThisMeld (MeldList, false);
						}
					}
				}
			}
		}
	}

	public bool AreNotTheSameSuite (string FirstCardID, string SocendCardID)
	{
		if (GetChar (FirstCardID, 0) != GetChar (SocendCardID, 0) || ISItAJoker (FirstCardID) || ISItAJoker (SocendCardID))
			return true;
		else
			return false;
	}

	void AddThisMeld (List<string> MeldList, bool RowOrSuits)
	{
		List<string> ModifiedMeldListJoker = MeldList;//CheckSetTheFukinJoker (MeldList, RowOrSuits);

		for (int i = 0; i < 5; i++)
			if (LocalPlayersMedls [i].Count <= 1) {
				LocalPlayersMedls [i] = ModifiedMeldListJoker;
				break;
			}
		if (TempCurrenRoom.ActionID == 7 || TempCurrenRoom.ActionID == 8) {
			if (NoOfGreenSign () == 0)
				return;
			if (TempCurrenRoom.SeatsStatus [Player.Instance.OnlinePlayerID] == 0)
				return;
			if (!IsFirstMeld ())
				PlayingWindowsManager4.Instance.GoDownBTN.SetActive (true);
			else if (GetSumOfThisPlayerMelds () >= TempCurrenRoom.GameOB.HandDetails.LowestAcceptedSum)
				PlayingWindowsManager4.Instance.GoDownBTN.SetActive (true);
		}
	}

	int NoOfGreenSign ()
	{
		int SignsCounter = 0;
		for (int i = 0; i < CardsListGO.transform.childCount; i++)
			if (CardsListGO.transform.GetChild (i).GetComponent<Card4> ().MeldSign.activeSelf)
				SignsCounter++;
		return SignsCounter;
	}

	List<string> CheckSetTheFukinJoker (List<string> meldList, bool RowOrSuits)
	{
		List<string> NewModifiedList = meldList;
		for (int i = 0; i < NewModifiedList.Count; i++)//Loop for all elenmts of the meld
			if (NewModifiedList [i].Contains ("15")) {//There is a fukin joker here
				if (i < NewModifiedList.Count - 1) {//Joker Is not the last
					if (RowOrSuits) {//RowTypeMeld
						NewModifiedList [i] = GetChar (NewModifiedList [NewModifiedList.Count - 1], 0) + "15";
						break;
					} else {//SuitsTypeMeld
						List<string> AddedSuitsType = new List<string> (4);
						AddedSuitsType.Add ("D");
						AddedSuitsType.Add ("H");
						AddedSuitsType.Add ("S");
						AddedSuitsType.Add ("C");
						for (int u = 0; u < AddedSuitsType.Count; u++)
							try {
								AddedSuitsType.Remove (NewModifiedList [u]);
							} catch {
							}
						NewModifiedList [i] = AddedSuitsType [0] + "15";
						break;
					}
				} else {//Joker IS The Last

					if (RowOrSuits) {//RowTypeMeldd
						NewModifiedList [i] = GetChar (NewModifiedList [0], 0) + "15";
						break;
					} else {//SuitsTypeMeld
						List<string> AddedSuitsType = new List<string> (4);
						AddedSuitsType.Add ("D");
						AddedSuitsType.Add ("H");
						AddedSuitsType.Add ("S");
						AddedSuitsType.Add ("C");
						for (int u = 0; u < AddedSuitsType.Count; u++) {
							try {
								AddedSuitsType.Remove (NewModifiedList [u]);
							} catch {
							}
						}
						NewModifiedList [i] = AddedSuitsType [0] + "15";
						break;
					}
				}
			}
		return NewModifiedList;
	}

	public void GoDownBTN ()
	{
		PlayingWindowsManager4.Instance.MeldsData = LocalPlayersMedls;
		PlayingWindowsManager4.Instance.ShowMeldsWindow ();
	}

	private int GetSumOfThisPlayerMelds ()
	{
		int ComulativeSum = 0;
		for (int i = 0; i < 5; i++)//All Melds List
			if (LocalPlayersMedls [i].Count > 1)//Check if There is a meld list
				ComulativeSum += GetSumOfThisMeldList (LocalPlayersMedls [i]);
		return ComulativeSum;
	}

	public int GetSumOfThisMeldList (List<string> list)
	{
		int SumOfThisMeldList = 0;
		for (int u = 0; u < 5; u++)//All cards in this meld list
			if (u < list.Count) {//if there is card in this list
				int ThisCardNO = GetCardNO (list [u]);
				if (ThisCardNO == 14) {//fukin ace
					if (u == 0) {
						if (list [u + 1].Contains ("02"))
							ThisCardNO = 1;
					}
				}
				SumOfThisMeldList += ThisCardNO;
			}
		return SumOfThisMeldList;
	}

	public bool AreTheSameSuite (string FirstCardID, string SocendCardID)
	{
		if (GetChar (FirstCardID, 0) == GetChar (SocendCardID, 0) || ISItAJoker (FirstCardID) || ISItAJoker (SocendCardID))
			return true;
		else
			return false;
	}

	public bool AreEqual (string FirstCardID, string SocendCardID)
	{
		int FirstCardIDNO = GetCardNO (FirstCardID);
		int SocendCardIDNO = GetCardNO (SocendCardID);
		if ((FirstCardIDNO == SocendCardIDNO) || ISItAJoker (FirstCardID) || ISItAJoker (SocendCardID))
			return true;
		else
			return false;
	}

	public bool AreInARow (string FirstCardID, string SocendCardID)
	{
		bool ThereIsAndAce = false;
		int FirstCardIDNO = GetCardNO (FirstCardID);
		int SocendCardIDNO = GetCardNO (SocendCardID);
		if (FirstCardIDNO == 14)
			ThereIsAndAce = true;
		if (ISItAJoker (FirstCardID) || ISItAJoker (SocendCardID)) {
			return true;
		} else if (!ThereIsAndAce && FirstCardIDNO == SocendCardIDNO - 1)
			return true;
		else if (ThereIsAndAce) {
			FirstCardIDNO = 1;
			if (FirstCardIDNO == SocendCardIDNO - 1)
				return true;
		}
		return false;
	}

	private bool ISItAJoker (string FirstCardID)
	{
		if (GetCardNO (FirstCardID) == 15)
			return true;
		else
			return false;
	}

	private bool ReturnTheDrawdCard ()
	{
		if (TempCurrenRoom.GameOB.HandDetails.LastThrowdCard == TempCurrenRoom.GameOB.HandDetails.LastDrawdCardFromThrowDeck)
			return true;
		else
			return false;
	}

	//Recieve THrowd Card
	private void UpdateRoomPhase7 ()
	{
		HideMeldsStuff ();
		//GameStart
		if (MyTurnToPlay ()) {
			PlayerIsBack (true);
			IfPlayerHoldingCardGetItBack ();
			HandManager4.Instance.SetAndArrangeMyCards ();
		}
		int ThrowerID = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		if (TempCurrenRoom.GameOB.HandDetails.LastThrowdCard.Length < 1)
			PlayingWindowsManager4.Instance.SetTimer (FromOnlineToLocal (ThrowerID),
				TimerWaitingTime (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay), 2, false);
		else
			RecieveThrowThisCard (ThrowerID, TempCurrenRoom.GameOB.HandDetails.LastThrowdCard, true);
	}

	//Draw Card Animate
	void UpdateRoomPhase6 ()
	{
		HideMeldsStuff ();
		CanDrawDrawCardsDeckGO.SetActive (false);
		CanDrowThrowCardDeckGO.SetActive (false);
		StartCoroutine (DrawCardAnimateCor (FromOnlineToLocal (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay)));
	}

	private void HideMeldsStuff ()
	{
		PlayingWindowsManager4.Instance.GoDownBTN.SetActive (false);
		PlayingWindowsManager4.Instance.MeldsWindowGO.SetActive (false);
	}

	private GameObject TargetedDrawCard;

	private IEnumerator DrawCardAnimateCor (int DrawerID)
	{
		bool CardFromDrawCardsDeck = TempCurrenRoom.GameOB.HandDetails.LastDrawdCardFromDrawDeck.Length > 1;
		if (CardFromDrawCardsDeck)
			TargetedDrawCard = DrawAnimateCard;
		else
			TargetedDrawCard = ThrowdCardDeckGO.transform.GetChild (ThrowdCardDeckGO.transform.childCount - 1).gameObject;
		TargetedDrawCard.GetComponent<Animator> ().SetInteger ("Draw", AnimationFromLocalToActual (DrawerID));
		yield return new WaitForSeconds (0.5f);
		if (CardFromDrawCardsDeck)
			TargetedDrawCard.GetComponent<Animator> ().SetInteger ("Draw", -1);
		yield return new WaitForSeconds (0.25f);
		if (!CardFromDrawCardsDeck)
			Destroy (TargetedDrawCard);
		if (!MyTurnToPlay ())
			ActivateCardOnThisPlayer (DrawerID);			

		if (Player.Instance.GameCreator) {
			if (CardFromDrawCardsDeck)
				TempCurrenRoom.ActionID = 7;
			else
				TempCurrenRoom.ActionID = 8;
			PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
		}
	}

	public void ActivateCardOnThisPlayer (int PlayerNO)
	{
		if (PlayerNO == 0)
			return;
		for (int i = 0; i < OtherPlayersCardsGO [PlayerNO].transform.childCount; i++)
			if (!OtherPlayersCardsGO [PlayerNO].transform.GetChild (i).gameObject.activeSelf) {
				OtherPlayersCardsGO [PlayerNO].transform.GetChild (i).gameObject.SetActive (true);
				break;
			}
	}

	//Draw Phase
	void UpdateRoomPhase5 ()
	{
		HideMeldsStuff ();
		if (MyTurnToPlay ()) {
			if (TempCurrenRoom.GameOB.HandDetails.LastDrawdCardFromThrowDeck == TempCurrenRoom.GameOB.HandDetails.LastThrowdCard)
				DrawCardBTN (true);
			else {
				CanDrawDrawCardsDeckGO.SetActive (true);
				if (ThrowdCardDeckGO.transform.childCount > 1)
					CanDrowThrowCardDeckGO.SetActive (true);
			}
		}
		PlayingWindowsManager4.Instance.SetTimer (FromOnlineToLocal (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay),
			TimerWaitingTime (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay), 1, false);
	}

	public void DrawCardBTN (bool DrawDeckOrThrowDeck)
	{
		if (MyTurnToPlay () && TempCurrenRoom.ActionID == 5)
			DrawCard (Player.Instance.OnlinePlayerID, DrawDeckOrThrowDeck);
	}

	//Draw
	private void DrawCard (int PlayerNO, bool DrawDeckOrThrowDeck)
	{
		PlayerIsBack (false);
		TempCurrenRoom.GameOB.HandDetails.LastDrawdCardFromDrawDeck = "";
		TempCurrenRoom.GameOB.HandDetails.LastDrawdCardFromThrowDeck = "";
		TempCurrenRoom.GameOB.HandDetails.LastThrowdCard = "";
		List<string> TargetedDeck = new List<string> ();
		if (DrawDeckOrThrowDeck)
			TargetedDeck = TempCurrenRoom.GameOB.HandDetails.DrawCards;
		else
			TargetedDeck = TempCurrenRoom.GameOB.HandDetails.ThrowdCards;
		MyCardList = TempCurrenRoom.GameOB.Cards.PlayersCards [Player.Instance.OnlinePlayerID].PlayerCardsList;
		if (DrawDeckOrThrowDeck)
			MyCardList.Add (TargetedDeck [0]);
		else
			MyCardList.Add (TargetedDeck [TargetedDeck.Count - 1]);
		if (DrawDeckOrThrowDeck) {
			TempCurrenRoom.GameOB.HandDetails.LastDrawdCardFromDrawDeck = TargetedDeck [0];
			TargetedDeck.RemoveAt (0);
		} else {
			TempCurrenRoom.GameOB.HandDetails.LastDrawdCardFromThrowDeck = TargetedDeck [TargetedDeck.Count - 1];
			TargetedDeck.RemoveAt (TargetedDeck.Count - 1);
		}
		TempCurrenRoom.GameOB.Cards.PlayersCards [Player.Instance.OnlinePlayerID].PlayerCardsList = MyCardList;
		if (DrawDeckOrThrowDeck)
			TempCurrenRoom.GameOB.HandDetails.DrawCards = TargetedDeck;
		else
			TempCurrenRoom.GameOB.HandDetails.ThrowdCards = TargetedDeck;
		TempCurrenRoom.ActionID = 6;
		PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private bool SceneDoneLoading;
	void UpdateRoomPhase4 ()
	{
		DrawCardsDeckGO.SetActive (true);
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (PlayerID == -1)
			return;
		SceneDoneLoading = true;
		HandManager4.Instance.Cards.PlayersCards [PlayerID].PlayerCardsList =
			TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerID].PlayerCardsList;
		HandManager4.Instance.SetAndArrangeMyCards ();
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.ActionID = 7;
		PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase3 ()
	{
		if (Player.Instance.OnlinePlayerID == -1)
			return;
		if (SceneDoneLoading)
			return;
		if (IsAllPlayersReady () && Player.Instance.GameCreator) {
			TempCurrenRoom.ActionID = -1;
			PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
			return;
		}
		UpdateRoomPhase2 ();
		if (TempCurrenRoom.PlayersReady [Player.Instance.OnlinePlayerID])
			return;
		SetPlayersScores ();
		PlayingWindowsManager4.Instance.SetPlayersGO (TempCurrenRoom.GameOB.PlayersNO);
		ClearOldGameStuff ();
		PlayingFireBaseManager4.Instance.SetOnDissconnectHandle ();
		HandManager4.Instance.AssignPlayerCardsAndActivateOthersCards ();
		TempCurrenRoom.PlayersReady [Player.Instance.OnlinePlayerID] = true;
		PlayingWindowsManager4.Instance.WinsWindow.SetActive (false);
		if (Player.Instance.GameCreator)
			SetReadyForComputer ();
		PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void SetReadyForComputer ()
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.SeatsStatus [i] == 0)
				TempCurrenRoom.PlayersReady [i] = true;
	}

	bool IsAllPlayersReady ()
	{
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++)
			if (!TempCurrenRoom.PlayersReady [i])
				return false;
		return true;
	}


	void SetPlayersScores ()
	{
		for (int i = 0; i < 4; i++)
			PlayingWindowsManager4.Instance.TeamsScores [i].GetComponent<Text> ().text = TempCurrenRoom.GameOB.PlayersScores [i].ToString ();
	}

	void ClearOldGameStuff ()
	{
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++) {//Loop For Al Players
			for (int u = 0; u < PlayersMeldsGO [i].transform.childCount; u++) {//Loop for player Melds lists
				if (PlayersMeldsGO [i].transform.GetChild (u).transform.childCount > 0) {//If Player meld containt cards
					for (int y = 0; y < PlayersMeldsGO [i].transform.GetChild (u).childCount; y++)//loop for this player meld list cards.
						Destroy (PlayersMeldsGO [i].transform.GetChild (u).transform.GetChild (y).gameObject);
				}
			}
		}
		if (ThrowdCardDeckGO.transform.childCount > 1)
			for (int i = 1; i < ThrowdCardDeckGO.transform.childCount; i++)
				Destroy (ThrowdCardDeckGO.transform.GetChild (i).gameObject);
	}

	public void CardsListFromLocalToOnline ()
	{
		PlayersCardsList.Clear ();
		for (int i = 0; i < CardsListGO.transform.childCount; i++)
			PlayersCardsList.Add (CardsListGO.transform.GetChild (i).GetComponent<Card4> ().CardID);
		TempCurrenRoom.GameOB.Cards.PlayersCards [Player.Instance.OnlinePlayerID].PlayerCardsList = PlayersCardsList;
		RefreshMyMelds ();
		PlayingFireBaseManager4.Instance.UploudThisJson (TempCurrenRoom, false);
	}

	#region implemented abstract members of PlayersManagerParent

	public override void OnApplicationPause (bool pauseStatus)
	{
		if (TempCurrenRoom.ActionID < 4)
			return;
		try {
			if (pauseStatus) {
				TempCurrenRoom.SeatsStatus [Player.Instance.OnlinePlayerID] = 0;
				if (Player.Instance.GameCreator && NoOfPlayersInTheRoom () > 0) {
					TempCurrenRoom.NoMaster = true;
					Player.Instance.GameCreator = false;
					for (int i = 0; i < 4; i++) {
						if (TempCurrenRoom.SeatsStatus [i] == 1) {
							TempCurrenRoom.MasterID = i;
							break;
						}
					}
				}
				PlayingFireBaseFather.ParentInstance.UploudThisJson (TempCurrenRoom, TempCurrenRoom.NoMaster);			
			} else
				StartCoroutine (SetandarrangeCor (pauseStatus));
		} catch (Exception e) {
			Debug.Log ("OnApplicationPause\t" + e.Message);
		}
	}

	#endregion

	private IEnumerator SetandarrangeCor (bool pauseStatus)
	{
		ShowBackBTN ();
		yield return new WaitForSeconds (2.5f);
		try {
			HandManager4.Instance.SetAndArrangeMyCards ();
		} catch  {
		}
	}

	#region implemented abstract members of PlayersManagerParent

	public override void IamBack ()
	{
		TempCurrenRoom.SeatsStatus [Player.Instance.OnlinePlayerID] = 1;
		for (int i = 0; i < CardsListGO.transform.childCount; i++)
			CardsListGO.transform.GetChild (i).GetComponent<Card4> ().CanDrag = true;
		PlayingFireBaseFather.ParentInstance.UploudThisJson (TempCurrenRoom, false);
	}

	#endregion

	#region implemented abstract members of PlayersManagerParent

	public override void SetLastRoundCards ()
	{
		return;
	}

	#endregion

	private void OnDestroy ()
	{
		StopAllCoroutines ();
	}
}