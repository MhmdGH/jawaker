﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayingBTNSManager4 : PlayingBTNSManagerFather {
	
	public static PlayingBTNSManager4 Instance;

	protected override void Awake(){
		base.Awake ();
		Instance = this;
	}

	public override void BTNSFunction(){
		base.BTNSFunction ();
		if (BTNName.Contains ("RestartBTN"))
			PlayersManager4.Instance.RestartGame ();
		else if (BTNName.Contains ("DrawCardDeck"))
			PlayersManager4.Instance.DrawCardBTN (true);
		else if (BTNName.Contains ("ThrowCardDeck"))
			PlayersManager4.Instance.DrawCardBTN (false);
		else if (BTNName == ("GoDownBTN"))
			PlayersManager4.Instance.GoDownBTN ();
		else if (BTNName == ("MeldsCloseBTN"))
			PlayingWindowsManager4.Instance.MeldsWindowGO.SetActive (false);
		else if (BTNName == ("GoDownNowBTN"))
			PlayersManager4.Instance.GoDownNowBTN ();
		else if (BTNName.StartsWith ("Meld"))
			PlayingWindowsManager4.Instance.MeldChoosed (int.Parse(GetChar(BTNName,4)));
	}
}