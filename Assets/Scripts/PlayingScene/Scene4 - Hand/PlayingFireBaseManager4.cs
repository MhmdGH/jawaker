﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Database;
using Firebase;
using Firebase.Unity.Editor;

public class PlayingFireBaseManager4 : PlayingFireBaseFather
{
	
	public static PlayingFireBaseManager4 Instance;

	public override void Awake ()
	{
		base.Awake ();
		Instance = this;
	}

	public override void StartGame ()
	{	
		if (MainFireBaseManager.GameType.Contains ("Saudi"))
			TempRoomToUpload.GameOB.HandDetails.IsSaudi = true;
		if (MainFireBaseManager.GameType.Contains ("Partner")) {
			TempRoomToUpload.GameOB.HandDetails.IsPartners= true;
			base.StartGame ();
		}
		else {
			TempRoomToUpload = CurrentRoomUpdate;
			TempRoomToUpload.ActionID = 3;
			UploudThisJson (TempRoomToUpload, true);
		}
	}

	public override void UpdateRoomNow (string UpdatedRoomJson)
	{
		//Default Values
		JsonUtility.FromJsonOverwrite (UpdatedRoomJson, CurrentRoomUpdate);
		PlayersManager4.Instance.UpdateRoom (CurrentRoomUpdate);
	}
	
	#region implemented abstract members of PlayingFireBaseFather
	protected override void SetInitials ()
	{
		TempRoomToUpload = new JsonRoomO ("", new string[4]{ "1", "2", "3", "4" },new Game4(3));
		CurrentRoomUpdate = TempRoomToUpload;
	}
	#endregion

}