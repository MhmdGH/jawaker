﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using ArabicSupport;

public class PlayingWindowsManager4 : PlayingWindowsManagerFather
{
	public static PlayingWindowsManager4 Instance;
	public GameObject[] PlayersGO, PlayersGOPos, CardsMeldsGO;
	public GameObject GoDownBTN, MeldsWindowGO, GoDownNowBTN;

	public override void Awake ()
	{
		base.Awake ();
		Instance = this;
		Screen.orientation = ScreenOrientation.Landscape;
	}

	public override void ShowAndSetPartnersPhotos (string[] IMGSUrl)
	{
		OnStartBTNGO.SetActive (false);
		ChoosePartnerGO.SetActive (true);
		for (int i = 0; i < ChooseParternersGOs.Length; i++)
			PlayingFBManager4.Instance.SetFPPhoto (IMGSUrl [i], ChooseParternersGOs [i].GetComponent<Image> ());
	}

	public override void StopTimer ()
	{
		base.StopTimer ();
	}

	protected override IEnumerator StartThisTimer (int PlayerNO, float WaitingTime, int FunToDOAfterEnd, bool JustCreatorDoIT)
	{
		PlayersTimer [PlayerNO].SetActive (true);
		WaitingTime = WaitingTime * SliderSmothnessMultiplier;
		float ActualTimerTikValue = TimerTikValue * SliderSmothnessMultiplier;
		PlayersTimer [PlayerNO].GetComponent<Slider> ().maxValue = WaitingTime;
		PlayersTimer [PlayerNO].GetComponent<Slider> ().value = WaitingTime;
		while (WaitingTime > 0) {
			PlayersTimer [PlayerNO].GetComponent<Slider> ().value -= ActualTimerTikValue;
			WaitingTime -= ActualTimerTikValue;
			yield return new WaitForSeconds (TimerTikValue * 0.1f);
		}
		PlayersTimer [PlayerNO].SetActive (false);
		if (JustCreatorDoIT && Player.Instance.GameCreator)
			PlayersManager4.Instance.MasterTakeControl (FunToDOAfterEnd);
		else if (!JustCreatorDoIT)
			PlayersManager4.Instance.MasterTakeControl (FunToDOAfterEnd);
	}

	private Vector3 TargetPos (int Pos)
	{
		if (Pos == 0)
			return new Vector3 (0, -300, 0);
		else if (Pos == 1)
			return new Vector3 (250, 0, 0);
		else if (Pos == 2)
			return new Vector3 (0, 300, 0);
		else if (Pos == 3)
			return new Vector3 (-250, 0, 0);
		else
			return new Vector3 (0, -300, 0);
	}

	public override void ShowWindWindow (int WinnerTeamNO)
	{
		string WinsTxt = "";
		if (PlayersManager4.Instance.TempCurrenRoom.GameOB.HandDetails.IsPartners) {
			if (LanguangeManagerFather.ParentInstance.IsArabic)
				WinsTxt = ArabicFixer.Fix ("الفائز " + WinnerTeamNO + " فريق");
			else
				WinsTxt = "Team " + WinnerTeamNO + " Wins";
		} else {
			if(LanguangeManagerFather.ParentInstance.IsArabic)
				WinsTxt = ArabicFixer.Fix("الفائز "+WinnerTeamNO+" اللاعب");
			else
				WinsTxt = "player "+WinnerTeamNO+" Wins";
		}
		WinsWindow.transform.GetChild (1).GetComponent<Text> ().text = WinsTxt;
		WinsWindow.SetActive (true);
		WinsWindow.GetComponent<RectTransform> ().GetChild (4).gameObject.SetActive (!MainFireBaseManager.Instance.IsACompition);
	}

	public void SetPlayersGO (int NoOfPlayers)
	{
		if (NoOfPlayers == 2) {
			PlayersGO [2].SetActive (false);
			PlayersGO [3].SetActive (false);
			PlayersGO [1].GetComponent<RectTransform> ().SetPositionAndRotation (PlayersGOPos [1].GetComponent<RectTransform> ().position, Quaternion.identity);
			PlayersGO [1].GetComponent<RectTransform> ().GetChild (0).GetComponent<RectTransform> ().localRotation = Quaternion.Euler (0, 0, 90);
		} else if (NoOfPlayers == 3) {
			PlayersGO [3].SetActive (false);
			PlayersGO [2].GetComponent<RectTransform> ().SetPositionAndRotation (PlayersGOPos [2].GetComponent<RectTransform> ().position, Quaternion.identity);
			PlayersGO [2].GetComponent<RectTransform> ().GetChild (0).GetComponent<RectTransform> ().localRotation = Quaternion.Euler (0, 0, 90);
		}
	}

	[HideInInspector]
	public List<string>[] MeldsData;
	public void ShowMeldsWindow ()
	{
		GoDownNowBTN.SetActive (false);
		MeldsWindowGO.SetActive (true);
		for (int i = 0; i < MeldsData.Length; i++) {
			CardsMeldsGO [i].GetComponent<Image> ().color = new Color (1, 1, 1, 0.3f);
			if (MeldsData [i].Count < 1)
				CardsMeldsGO [i].SetActive (false);
			else {
				CardsMeldsGO [i].SetActive (true);
				int ThisMeldListCount = MeldsData [i].Count;
				if (ThisMeldListCount < 4) {
					CardsMeldsGO [i].transform.GetChild (3).gameObject.SetActive (false);
					CardsMeldsGO [i].transform.GetChild (4).gameObject.SetActive (false);
				}else if(ThisMeldListCount <5)
					CardsMeldsGO [i].transform.GetChild (4).gameObject.SetActive (false);
				for (int u = 0; u < MeldsData [i].Count; u++) {
					CardsMeldsGO [i].transform.GetChild (u).gameObject.SetActive (true);
					CardsMeldsGO [i].transform.GetChild (u).GetComponent<Card> ().CardID = MeldsData [i] [u];
					CardsMeldsGO [i].transform.GetChild (u).GetComponent<Card> ().SetCardCoverOrFace ();
				}
			}
		}
	}

	public void MeldChoosed (int MeldNO)
	{
		GoDownNowBTN.SetActive (false);
		if (CardsMeldsGO [MeldNO - 1].GetComponent<Image> ().color.a > 0.5f)
			CardsMeldsGO [MeldNO - 1].GetComponent<Image> ().color = new Color (1, 1, 1, 0.3f);
		else
			CardsMeldsGO [MeldNO - 1].GetComponent<Image> ().color = new Color (1, 1, 1, 1);
		RefreshComulativeMeldsSum ();
	}

	void RefreshComulativeMeldsSum ()
	{
		int ComulativeScore = 0;
		for (int i = 0; i < CardsMeldsGO.Length; i++)
			if (CardsMeldsGO [i].GetComponent<Image> ().color.a > 0.5f)
				ComulativeScore += PlayersManager4.Instance.GetSumOfThisMeldList (MeldsData[i]);
		PlayersManager4.Instance.CheckCanGoDownNowBTN (ComulativeScore);
	}

	private void OnDestroy ()
	{
		StopAllCoroutines ();
	}
}