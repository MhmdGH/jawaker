﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Card5 : Card,IPointerDownHandler,IPointerUpHandler{

	#region IPointerUpHandler implementation

	public void OnPointerUp (PointerEventData eventData)
	{
		return;
	}

	#endregion

	#region IPointerDownHandler implementation
	public void OnPointerDown (PointerEventData eventData)
	{
		if (GetComponent<Image> ().color.r < 0.9f)
			return;
		if (CardIsUp) {
			OnEndDragFun ();
			return;
		}
		else {
			PlayersManager5.Instance.UnChooseAllOtherCards ();
			OnBeginDragFun ();
			StopAllCoroutines ();
			StartCoroutine (CardUp ());
		}
	}
	#endregion

	#region implemented abstract members of Card

	public override void ThroughIt (int StartPos)
	{
		GetComponent<Animator> ().SetInteger ("Thorugh",StartPos);
	}

	#endregion

	#region implemented abstract members of Card

	public override void SendCard ()
	{
		PlayersManager5.Instance.ThrowCard (this.gameObject);
	}

	#endregion
}
