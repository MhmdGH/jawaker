﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstimationManager5 : MonoBehaviour
{

	public static EstimationManager5 Instance;
	public GameObject[] MyPlayerCardsGO, PlayersCardsContentGO;
	public GameObject CardGO;
	[HideInInspector]
	public PlayersCardsOB1 Cards;
	private List<string> ModifeidCardList;

	void Awake ()
	{
		Instance = this;
	}

	void Start ()
	{
		ModifeidCardList = new List<string> ();
	}

	public void SetAndArrangeMyCards ()
	{
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (Cards.PlayersCards [PlayerID].PlayerCardsList.Count > MyPlayerCardsGO.Length)
			CreateAndAssignMyCards ();

		for (int i = 0; i < PlayersCardsContentGO [0].transform.childCount; i++)
			PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card5> ().CardID = "0";
		//ForWatcher
		if (PlayerID == -1) {
			for (int i = 0; i < PlayersCardsContentGO [0].transform.childCount; i++)
				PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card5> ().CardID = Cards.PlayersCards [4].PlayerCardsList [i];
			return;
		} else {
			List<string> CardsList = Cards.PlayersCards [PlayerID].PlayerCardsList;
			ModifeidCardList.Clear ();

			for (int i = 0; i < CardsList.Count; i++)
				if (!CardsList [i].StartsWith ("0"))
					ModifeidCardList.Add (CardsList [i]);
			for (int i = 0; i < ModifeidCardList.Count; i++)
				PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card5> ().CardID = ModifeidCardList [i];
			for (int i = 0; i < PlayersCardsContentGO [0].transform.childCount; i++)
				if (PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card5> ().CardID.StartsWith ("0"))
					Destroy (PlayersCardsContentGO [0].transform.GetChild (i).gameObject);
		}
		List<string> DCards = new List<string> ();
		List<string> SCards = new List<string> ();
		List<string> HCards = new List<string> ();
		List<string> CCards = new List<string> ();
		for (int i = 0; i < ModifeidCardList.Count; i++) {
			if (ModifeidCardList [i].StartsWith ("D"))
				DCards.Add (ModifeidCardList [i]);
			else if (ModifeidCardList [i].StartsWith ("S"))
				SCards.Add (ModifeidCardList [i]);
			else if (ModifeidCardList [i].StartsWith ("H"))
				HCards.Add (ModifeidCardList [i]);
			else if (ModifeidCardList [i].StartsWith ("C"))
				CCards.Add (ModifeidCardList [i]);
		}
		DCards.Sort ();
		SCards.Sort ();
		HCards.Sort ();
		CCards.Sort ();
		int Counter = 0;
		for (int i = 0; i < DCards.Count; i++) {
			PlayersCardsContentGO [0].transform.GetChild (Counter).GetComponent<Card5> ().CardID = DCards [i];
			Counter++;
		}
		for (int i = 0; i < SCards.Count; i++) {
			PlayersCardsContentGO [0].transform.GetChild (Counter).GetComponent<Card5> ().CardID = SCards [i];
			Counter++;
		}
		for (int i = 0; i < HCards.Count; i++) {
			PlayersCardsContentGO [0].transform.GetChild (Counter).GetComponent<Card5> ().CardID = HCards [i];
			Counter++;
		}
		for (int i = 0; i < CCards.Count; i++) {
			PlayersCardsContentGO [0].transform.GetChild (Counter).GetComponent<Card5> ().CardID = CCards [i];
			Counter++;
		}
		for (int i = 0; i < PlayersCardsContentGO [0].transform.childCount; i++)
			PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card5> ().SetCardCoverOrFace ();
	}

	public void AssignPlayerCardsAndActivateOthersCards ()
	{
		StartCoroutine (AssignPlayerCardsAndActivateOthersCardsCor ());
	}

	private IEnumerator AssignPlayerCardsAndActivateOthersCardsCor ()
	{
		Cards = new PlayersCardsOB1 ();
		CreateAndAssignMyCards ();
		for (int i = 0; i < 13; i++)
			for (int u = 1; u < 4; u++)
				PlayersCardsContentGO [u].transform.GetChild (i).gameObject.SetActive (true);
		yield return null;
	}

	private void CreateAndAssignMyCards ()
	{
		for (int i = 0; i < MyPlayerCardsGO.Length; i++)
			Destroy (MyPlayerCardsGO [i].gameObject);			
		MyPlayerCardsGO = new GameObject[13];
		for (int i = 0; i < 13; i++) {
			GameObject NewCard = Instantiate (CardGO, PlayersCardsContentGO [0].transform.position,
				Quaternion.identity, PlayersCardsContentGO [0].transform) as GameObject;
			NewCard.GetComponent<Card5> ().CanDrag = false;
			NewCard.GetComponent<Card5> ().IsCover = false;
			MyPlayerCardsGO [i] = NewCard;
		}
	}

	public void SetCardsForPlayers ()
	{
		CardsInfos1 NewCardsInfo = new CardsInfos1 ();
		for (int u = 0; u < 4; u++) {
			for (int i = 0; i < 13; i++) {
				int RandomNO = Random.Range (0, NewCardsInfo.HalfOfCards.Count);
				Cards.PlayersCards [u].PlayerCardsList.Add (NewCardsInfo.HalfOfCards [RandomNO]);
				NewCardsInfo.HalfOfCards.RemoveAt (RandomNO);
			}
		}
	}

	private void OnDestroy ()
	{
		StopAllCoroutines ();
	}
}