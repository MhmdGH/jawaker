﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public class LanguangeManager5 : LanguangeManagerFather {

	public Text[] Game5Txts; 

	public override void Start(){
		base.Start ();
		try{
		if(IsArabic)
				SetTheFukinArabicLang ();}catch{Debug.Log ("////////////////////////////////");
		}
	}

	public override void SetTheFukinArabicLang ()
	{
		base.SetTheFukinArabicLang ();
		SetFontForThisTxts (Game5Txts);
		Game5Txts [0].text = ArabicFixer.Fix ("إعادة طلب");
		Game5Txts [1].text = ArabicFixer.Fix ("باص");
		Game5Txts [2].text = ArabicFixer.Fix ("اختر طلبتك");
		Game5Txts [3].text = ArabicFixer.Fix ("طلب");
		Game5Txts [4].text = ArabicFixer.Fix ("داش كول");
		Game5Txts [5].text = ArabicFixer.Fix ("اختر طلبتك");
		Game5Txts [6].text = ArabicFixer.Fix ("اختر طلبتك");
		Game5Txts [7].text = ArabicFixer.Fix ("طلب");
		Game5Txts [8].text = ArabicFixer.Fix ("باص");
	}
}
