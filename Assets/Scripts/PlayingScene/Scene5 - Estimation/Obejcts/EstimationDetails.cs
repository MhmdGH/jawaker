﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class EstimationDetails
{
	public int GamesCounter,PlayerWhoStartBids,LastPlayerID,BidsWinnerID,BidRound;
	//Pass1 => Dash Call
	public bool[] Avoid, Bid1,Bid2,Pass1,Pass2, Risk, DoubleRisk, With, Pass3Round2;
	public bool DoubleRound,FirstGame,FastBidding;
	public int[] PlayersBid;
	public string[] PlayerTrump;
	public string CurrentPopString;

	public EstimationDetails ()
	{
		FastBidding = false;
		BidRound = 1;
		BidsWinnerID = 0;
		FirstGame = true;
		GamesCounter = 1;
		LastPlayerID = 3;
		PlayerWhoStartBids = 0;
		Avoid = new bool[]{ false, false, false, false };
		Risk = new bool[]{ false, false, false, false };
		DoubleRisk = new bool[]{ false, false, false, false };
		With = new bool[]{ false, false, false, false };
		Bid1 = new bool[]{ false, false, false, false };
		Bid2 = new bool[]{ false, false, false, false };
		Pass1 = new bool[]{ false, false, false, false };
		Pass2 = new bool[]{ false, false, false, false };
		Pass3Round2 = new bool[]{ false, false, false, false };
		DoubleRound = false;
		PlayersBid = new int[]{ 0, 0, 0, 0 };//-2 => not bid yet, -1=> Pass;
		PlayerTrump = new string[4]{"NT","NT","NT","NT"};
		CurrentPopString="";
	}
}