﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class PlayersManager5 : PlayersManagerParent
{
	public static PlayersManager5 Instance;
	public GameObject CardGO;
	private List<GameObject> ThrowdCardsList;

	public override void Awake ()
	{
		base.Awake ();
		Instance = this;
	}

	public override void Start ()
	{
		TempCurrenRoom = new JsonRoomO ("Default", new string[4]{ "0", "0", "0", "0" }, new Game5 ());
		base.Start ();
		ThrowdCardsList = new List<GameObject> ();
		CardsListGO = PlayersCardsGO [0].transform.GetChild (1).GetChild (0).gameObject;
		SceneDoneLoading = false;
	}

	//Main Reciever Fun
	public override void UpdateRoom (JsonRoomO CurrentRoomJSON)
	{
		base.UpdateRoom (CurrentRoomJSON);
		
		if (TempCurrenRoom.NoMaster)
			AssignNewMasterOrRemoveRoom ();
		if (TempCurrenRoom.Update == 0) {
			PlayingFireBaseManager5.Instance.SetOnDissconnectHandle ();
			return;
		}
		PlayingWindowsManager5.Instance.StopTimer ();
		switch (TempCurrenRoom.ActionID) {
		case -1:
			UpdateRoomPhaseMinus1 ();
			break;
		case 0:
			UpdateRoomPhase0 ();
			break;
		case 2:
			UpdateRoomPhase2 ();
			break;
		case 3:
			UpdateRoomPhase3 ();
			break;
		case 4:
			UpdateRoomPhase4 ();
			break;
		case 5:
			UpdateRoomPhase5 ();
			break;
		case 6:
			UpdateRoomPhase6 ();
			break;
		case 7:
			UpdateRoomPhase7 ();
			break;
		case 8:
			UpdateRoomPhase8 ();
			break;
		case 9:
			UpdateRoomPhase9 ();
			break;
		case 10:
			UpdateRoomPhase10 ();
			break;
		case 12:
			UpdateRoomPhase12 ();
			break;
		case 13:
			UpdateRoomPhase13 ();
			break;
		case 14:
			UpdateRoomPhase14 ();
			break;
		case 15:
			UpdateRoomPhase15 ();
			break;
		}
	}

	void UpdateRoomPhaseMinus1 ()
	{
		AudioManager.Instance.PlaySound (0);
		if (!Player.Instance.GameCreator)
			return;
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++)
			TempCurrenRoom.PlayersReady [i] = false;
		if (TempCurrenRoom.GameOB.EstimationDetailsOB.FirstGame) {
			TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = 0;
			TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound = 0;
			TempCurrenRoom.GameOB.EstimationDetailsOB.PlayerWhoStartBids = 0;
			TempCurrenRoom.GameOB.EstimationDetailsOB.FirstGame = false;
			TempCurrenRoom.GameOB.PlayerBidTurn = 0;
		}
		if (TempCurrenRoom.GameOB.EstimationDetailsOB.GamesCounter >= 14)
			TempCurrenRoom.GameOB.EstimationDetailsOB.FastBidding = true;
		else
			TempCurrenRoom.GameOB.EstimationDetailsOB.FastBidding = false;
		EstimationManager5.Instance.SetCardsForPlayers ();
		TempCurrenRoom.ActionID = 4;
		TempCurrenRoom.Available = false;
		for (int i = 0; i < 4; i++)
			TempCurrenRoom.GameOB.Cards.PlayersCards [i].PlayerCardsList = EstimationManager5.Instance.Cards.PlayersCards [i].PlayerCardsList;
		PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase0 ()
	{
		if (TempCurrenRoom.ReloadScene)
			ReloadScene ();
	}

	public void UnChooseAllOtherCards ()
	{
		for (int i = 0; i < EstimationManager5.Instance.MyPlayerCardsGO.Length; i++)
			try{EstimationManager5.Instance.MyPlayerCardsGO[i].GetComponent<Card5> ().OnEndDragFun (true);}catch{
		}
	}

	void ReloadScene ()
	{
		TempCurrenRoom.ReloadScene = false;
		TempCurrenRoom.ActionID = 3;
		SceneDoneLoading = true;
		if (Player.Instance.GameCreator)
			PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}

	public void MasterTakeControl (int funToDOAfterEnd)
	{
		if (funToDOAfterEnd == 1)
			AutoBid ();
		else if (funToDOAfterEnd == 2)
			MasterOrPlayerThrowCardNOW ();
	}

	void AutoBid ()
	{
		//Player IS Here
		int PlayerID = Player.Instance.OnlinePlayerID;
		//Player Is Here
		if (IsThisMyBidTurn () && TempCurrenRoom.SeatsStatus [PlayerID] == 1) {
			IfPlayerHoldingCardGetItBack ();
			if (OneOfTypesMissing () && !TempCurrenRoom.GameOB.EstimationDetailsOB.Avoid [Player.Instance.OnlinePlayerID])
				RedealOrAvoid (false);
			else if (!TempCurrenRoom.GameOB.EstimationDetailsOB.Pass1 [PlayerID]
			         && !TempCurrenRoom.GameOB.EstimationDetailsOB.Bid1 [PlayerID])
				BidAndPassBTN (true);
			else {
				if (OneOrMorePlayerSetBids ()) {
					string[] TrumpTypes = new string[5]{ "NT", "D", "H", "C", "S" };
					string RandomType = TrumpTypes [UnityEngine.Random.Range (0, TrumpTypes.Length)];
					DetailsBidBTN (GetAcceptableBid (), RandomType);
				} else
					DetailsPassBTN ();
			}
		} else if (Player.Instance.GameCreator && TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.PlayerBidTurn] == 0) {
			PlayerID = TempCurrenRoom.GameOB.PlayerBidTurn;
			if (!TempCurrenRoom.GameOB.EstimationDetailsOB.Pass1 [PlayerID]
			    && !TempCurrenRoom.GameOB.EstimationDetailsOB.Bid1 [PlayerID])
				BidAndPassBTN (true);
			else {
				if (OneOrMorePlayerSetBids ()) {
					//Debug.Log ("auto OneOrMorePlayerSetBids");
					string[] TrumpTypes = new string[5]{ "NT", "D", "H", "C", "S" };
					string RandomType = TrumpTypes [UnityEngine.Random.Range (0, TrumpTypes.Length)];
					DetailsBidBTN (GetAcceptableBid (), RandomType);
				} else
					DetailsPassBTN ();
			}
		}
	}

	private bool NoMoreDashs ()
	{
		int Pass3Counter = 0;
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.EstimationDetailsOB.Pass3Round2 [i])
				Pass3Counter++;
		return Pass3Counter >= 2;
	}

	void MasterOrPlayerThrowCardNOW ()
	{
		int PlayerSeatStatus = TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay];
		//If Player Is Here
		if (MyTurnToPlay () && PlayerSeatStatus == 1) {
			if (PlayersManagerParent.PlayerHoldingCard) {
				GameObject CardHolded = CanvasGO.GetComponent<RectTransform> ().GetChild (CanvasGO.GetComponent<RectTransform> ().childCount - 1).gameObject;
				CardHolded.GetComponent<Card5> ().ReturnCardBack ();
				CardHolded.GetComponent<Card5> ().SendCard ();
				CheckPlayerKick ();
				return;
			} else {
				IfPlayerHoldingCardGetItBack ();
				for (int i = 0; i < CardsListGO.transform.childCount; i++) {
					if (CardsListGO.transform.GetChild (i).GetComponent<Image> ().color.r == 1) {
						CardsListGO.transform.GetChild (i).GetComponent<Card5> ().SendCard ();
						CheckPlayerKick ();
						return;
					}
				}
			}
			//If Player Not Here, Master Do IT
		} else if (Player.Instance.GameCreator && PlayerSeatStatus == 0) {
			TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay] = 0;
			ThrowCardNow (FirstAvailabeCardOnOtherPlayer ());
		}
	}

	private int GetAcceptableBid ()
	{
		int BidValue = UnityEngine.Random.Range (4, 7);
		if (TempCurrenRoom.GameOB.EstimationDetailsOB.BidRound == 1 && TempCurrenRoom.GameOB.EstimationDetailsOB.LastPlayerID == TempCurrenRoom.GameOB.PlayerBidTurn) {
			if (BidValue + BidsSum () < 13) {
				int DifferenceValue = (13 - BidsSum ()) - BidValue;
				PrivateLowestAccepted = 13 - BidsSum ();
				SetRiskKeyForThisPlayer (DifferenceValue);
				BidValue = PrivateLowestAccepted;
			}
		} else if (TempCurrenRoom.GameOB.EstimationDetailsOB.BidRound == 2)
			BidValue = UnityEngine.Random.Range (NoMoreDashsInt (), TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [TempCurrenRoom.GameOB.EstimationDetailsOB.BidsWinnerID] + 1);
		//Debug.Log ("BidValue" + BidValue);
		return BidValue;
	}

	int NoMoreDashsInt ()
	{
		if (NoMoreDashs ())
			return 1;
		else
			return 0;
	}

	void SetRiskKeyForThisPlayer (int ThisDifferenceValue)
	{
		//Debug.Log ("privateLowestAccepted" + ThisDifferenceValue);
		if (ThisDifferenceValue == 2 || ThisDifferenceValue == 3)
			TempCurrenRoom.GameOB.EstimationDetailsOB.Risk [TempCurrenRoom.GameOB.PlayerBidTurn] = true;
		else if (ThisDifferenceValue >= 4)
			TempCurrenRoom.GameOB.EstimationDetailsOB.DoubleRisk [TempCurrenRoom.GameOB.PlayerBidTurn] = true;
	}

	private int BidsSum ()
	{
		int BidsComulativeSum = 0;
		for (int i = 0; i < 4; i++)
			BidsComulativeSum += TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [i];
		//Debug.Log ("BidsSum:" + BidsComulativeSum);
		return BidsComulativeSum;
	}

	private bool OneOrMorePlayerSetBids ()
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [i] > 0)
				return true;
		return false;
	}

	string FirstAvailabeCardOnOtherPlayer ()
	{
		List<string> ThisPlayerCardsList = new List<string> ();
		for (int i = 0; i < 13; i++)
			ThisPlayerCardsList = TempCurrenRoom.GameOB.
				Cards.PlayersCards [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay].PlayerCardsList;
		//if this Player STart the round
		if (TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound == TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay) {
			for (int i = 0; i < ThisPlayerCardsList.Count; i++) {
				if (ThisPlayerCardsList [i].Length > 2)
					return ThisPlayerCardsList [i];
			}
		}

		string PlayedType = GetChar (TempCurrenRoom.GameOB.CurrentRound.
			PlayersCards [TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound], 0);

		for (int i = 0; i < ThisPlayerCardsList.Count; i++)
			if (ThisPlayerCardsList [i].StartsWith (PlayedType))
				return ThisPlayerCardsList [i];
		for (int i = 0; i < ThisPlayerCardsList.Count; i++)
			if (!ThisPlayerCardsList [i].StartsWith ("0"))
				return ThisPlayerCardsList [i];
		//Not Reachable
		return "0";
	}

	public void RestartGame ()
	{
		PlayingWindowsManager5.Instance.WinsWindow.SetActive (false);
		Game5 NewGameOB = new Game5 ();
		for (int i = 0; i < NewGameOB.PlayersScores.Length; i++)
			NewGameOB.PlayersScores [i] = 0;
		TempCurrenRoom.GameOB = NewGameOB;
		TempCurrenRoom.ReloadScene = true;
		TempCurrenRoom.ActionID = 0;
		PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase15 ()
	{
		//Game Ends, Full 18 rounds ends
		int WinnerID = 0;
		List<int> PlayersScores = new List<int> ();
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++)
			PlayersScores.Add (TempCurrenRoom.GameOB.PlayersScores [i]);
		PlayersScores.Sort ();
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.PlayersScores [i] == PlayersScores [3])
				WinnerID = i;
		AddWinsPointsForPlayers (WinnerID);
		if (WinnerID == 0 || WinnerID == 2)
			ThisWinnerID = 0;
		else
			ThisWinnerID = 1;
		if (MainFireBaseManager.Instance.IsACompition)
			InnerSetCompData ();
		PlayingWindowsManager5.Instance.ShowWindWindow (WinnerID);
	}

	private int ThisWinnerID;
	private void InnerSetCompData ()
	{
		if (Player.Instance.GameCreator) {
			for (int i = 0; i < TempCurrenRoom.DatabasePlayersIDs.Length; i++)
				if (TempCurrenRoom.DatabasePlayersIDs [i].Length < 4)
					return;
			if (ThisWinnerID == 0) {
				SetCompData (TempCurrenRoom.DatabasePlayersIDs [0], TempCurrenRoom.DatabasePlayersIDs [2],
					TempCurrenRoom.PlayersFBIDs [0], TempCurrenRoom.PlayersFBIDs [2]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [1]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [3]);
			} else {
				SetCompData (TempCurrenRoom.DatabasePlayersIDs [1], TempCurrenRoom.DatabasePlayersIDs [3],
					TempCurrenRoom.PlayersFBIDs [1], TempCurrenRoom.PlayersFBIDs [3]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [0]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [2]);
			}
		}
	}

	private void AddWinsPointsForPlayers (int WinnerID)
	{
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (PlayerID == WinnerID) {
			Player.Instance.AddXPsAndCLubPoints (XPsValue);
			Player.Instance.Coins += ScoreValue;
		}
	}

	void UpdateRoomPhase14 ()
	{
		//Debug.Log ("UpdateRoomPhase14");
		//Full Round Ends
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.GameOB.EstimationDetailsOB.GamesCounter++;
		if (TempCurrenRoom.GameOB.EstimationDetailsOB.GamesCounter >= 19) {//Game Counter in estimation details
			TempCurrenRoom.ActionID = 15;
			PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
		} else //New Round Here
			NewRound (true);
	}

	private void NewRound (bool PlusGamesCounter)
	{
		Game5 NewGameOB = new Game5 ();
		if (PlusGamesCounter) {
			if (!ThereIsWinners ())
				NewGameOB.EstimationDetailsOB.DoubleRound = true;
			else
				NewGameOB.EstimationDetailsOB.DoubleRound = false;
		} else
			NewGameOB.EstimationDetailsOB.DoubleRound = TempCurrenRoom.GameOB.EstimationDetailsOB.DoubleRound;
		NewGameOB.EstimationDetailsOB.GamesCounter = TempCurrenRoom.GameOB.EstimationDetailsOB.GamesCounter;
		for (int i = 0; i < 4; i++)
			NewGameOB.PlayersScores [i] = TempCurrenRoom.GameOB.PlayersScores [i];
		NewGameOB.EstimationDetailsOB.FirstGame = false;
		TempCurrenRoom.GameOB = NewGameOB;
		if (PlusGamesCounter) {
			TempCurrenRoom.GameOB.EstimationDetailsOB.LastPlayerID = NextNO (TempCurrenRoom.GameOB.EstimationDetailsOB.LastPlayerID);
			TempCurrenRoom.GameOB.EstimationDetailsOB.PlayerWhoStartBids = NextNO (TempCurrenRoom.GameOB.EstimationDetailsOB.PlayerWhoStartBids);
		}
		TempCurrenRoom.ReloadScene = true;
		TempCurrenRoom.ActionID = 0;
		PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase13 ()
	{
		//Debug.Log ("UpdateRoomPhase13");
		//Full Round Ends
		for (int i = 0; i < 4; i++)
			PlayingWindowsManager5.Instance.PlayersScoresGO [i].GetComponent<Text> ().text = "0/0";
		if (!Player.Instance.GameCreator)
			return;
		if (TempCurrenRoom.GameOB.EstimationDetailsOB.GamesCounter < 18 && !ThereIsWinners ()) {
			NewRound (true);
			return;
		} else {
			SetPlayersScores ();
			TempCurrenRoom.ActionID = 14;
			PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
		}
	}

	void SetPlayersScores ()
	{
		for (int i = 0; i < 4; i++) {
			int CurrentRoundPlayerScore = TempCurrenRoom.GameOB.CurrentRound.PlayersScores [i];
			TempCurrenRoom.GameOB.PlayersScores [i] += PlayerCalculatedScore (i, CurrentRoundPlayerScore); 
		}
	}

	int PlayerCalculatedScore (int ThisPlayerID, int ThisPlayerScore)
	{
		int ThisPlayerBid = TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [ThisPlayerID];
		bool PlayerWin = (ThisPlayerBid == ThisPlayerScore);
		int X, Y, U, L;
		X = ThisPlayerScore;
		U = 1;
		if (ThisPlayerBid >= 8)
			U = 2;
		L = 1;
		if (TempCurrenRoom.GameOB.EstimationDetailsOB.DoubleRisk [ThisPlayerID])
			L = 2;
		///////////////
		Y = 0;
		if (ThisPlayerID == TempCurrenRoom.GameOB.EstimationDetailsOB.BidsWinnerID)
		if (PlayerWin)
			Y += 20;
		else
			Y -= 10;
		//Debug.Log ("Y:" + ThisPlayerID + "||" + Y);
		if (ThisPlayerBid == 0)
		if (PlayerWin)
			Y += 23;
		else
			Y -= 23;
		//Debug.Log ("Y:" + ThisPlayerID + "||" + Y);
		if (RegularPlayer (ThisPlayerID)) {//No Risks or with or double risk or dashcall
			if (PlayerWin)
				Y += 10;
			//Debug.Log ("Y:" + ThisPlayerID + "||" + Y);
		} else {
			if (TempCurrenRoom.GameOB.EstimationDetailsOB.With [ThisPlayerID] &&
			    TempCurrenRoom.GameOB.EstimationDetailsOB.DoubleRisk [ThisPlayerID]) {
				if (PlayerWin)
					Y += 40;
				else
					Y -= 30;
				//Debug.Log ("Y:" + ThisPlayerID + "||" + Y);
			} else if (TempCurrenRoom.GameOB.EstimationDetailsOB.With [ThisPlayerID] &&
			           TempCurrenRoom.GameOB.EstimationDetailsOB.Risk [ThisPlayerID]) {
				if (PlayerWin)
					Y += 30;
				else
					Y -= 20;
				//Debug.Log ("Y:" + ThisPlayerID + "||" + Y);
			} else {
				if (TempCurrenRoom.GameOB.EstimationDetailsOB.With [ThisPlayerID]) {
					if (PlayerWin)
						Y += 20;
					else
						Y -= 10;
					//Debug.Log ("Y:" + ThisPlayerID + "||" + Y);
				}
				if (TempCurrenRoom.GameOB.EstimationDetailsOB.Risk [ThisPlayerID]) {
					if (PlayerWin)
						Y += 20;
					else
						Y -= 10;
					//Debug.Log ("Y:" + ThisPlayerID + "||" + Y);
				} else if (TempCurrenRoom.GameOB.EstimationDetailsOB.DoubleRisk [ThisPlayerID]) {
					if (PlayerWin)
						Y += 30;
					else
						Y -= 20;
					//Debug.Log ("Y:" + ThisPlayerID + "||" + Y);
				}
			}
		}

		if (ThisPlayerIsTheOnlyWinner (ThisPlayerID))
			Y += 10;
		else if (ThisPlayerIsTheOnlyLosser (ThisPlayerID))
			Y -= 10;
		//Debug.Log ("Y:" + ThisPlayerID + "||" + Y);
		if (TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [ThisPlayerID] == 0 && BidsSum () < 13)
		if (PlayerWin)
			Y += 10;
		else
			Y -= 10;
		//Debug.Log ("Final Y:" + ThisPlayerID + "||" + Y);

		////////////////////////////////////
		return ((X + Y) * U) + ((X + Y) * (1 - L)) + ((X + Y) * (U - 1) * (L - 1));
	}

	bool ThisPlayerIsTheOnlyLosser (int thisPlayerID)
	{
		for (int i = 0; i < TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid.Length; i++) {//Loop return if other player wins
			if (i == thisPlayerID)
				continue;
			if (TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [i] != TempCurrenRoom.GameOB.CurrentRound.PlayersScores [i])
				return false;
		}
		return true;
	}

	bool ThisPlayerIsTheOnlyWinner (int thisPlayerID)
	{
		for (int i = 0; i < TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid.Length; i++) {//Loop return if other player wins
			if (i == thisPlayerID)
				continue;
			if (TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [i] == TempCurrenRoom.GameOB.CurrentRound.PlayersScores [i])
				return false;
		}
		return true;
	}

	bool RegularPlayer (int ThisPlayerID)
	{
		if (TempCurrenRoom.GameOB.EstimationDetailsOB.DoubleRisk [ThisPlayerID])
			return false;
		else if (TempCurrenRoom.GameOB.EstimationDetailsOB.Risk [ThisPlayerID])
			return false;
		else if (TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [ThisPlayerID] == 0)
			return false;
		else if (TempCurrenRoom.GameOB.EstimationDetailsOB.With [ThisPlayerID])
			return false;
		else
			return true;
	}

	bool ThereIsWinners ()
	{
		for (int i = 0; i < 4; i++) {
			//Debug.Log (TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [i] + "||" + TempCurrenRoom.GameOB.CurrentRound.PlayersScores [i]);
			if (TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [i] == TempCurrenRoom.GameOB.CurrentRound.PlayersScores [i])
				return true;
		}
		return false;
	}

	void UpdateRoomPhase12 ()
	{
		//COllect Cards Here And Throw it to winner Player
		StartCoroutine (CollectionCor ());
	}

	private IEnumerator CollectionCor ()
	{
		ReturnsMyCardsAvailibity ();
		SetAvailableCardsOnce = false;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = -2;
		int LastWinnerID = TempCurrenRoom.GameOB.CollectorID;
		yield return new WaitForSeconds (1);
		CardsThrowerMangaer.Instance.CollectCardsToThisPlayer (FromOnlineToLocal (LastWinnerID));
		PlayingWindowsManager5.Instance.PlayersScoresGO [FromOnlineToLocal (LastWinnerID)].GetComponent<Text> ().text =
			TempCurrenRoom.GameOB.CurrentRound.PlayersScores [LastWinnerID] + "/" +
		TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [LastWinnerID];
		yield return new WaitForSeconds (1);
		for (int i = 0; i < ThrowdCardsList.Count; i++)
			Destroy (ThrowdCardsList [i].gameObject);
		ThrowdCardsList.Clear ();
		if (Player.Instance.GameCreator) {
			SetPlayersGreenOrRed ();
			if (FullRoundEnds ()) {
				TempCurrenRoom.ActionID = 13;
				PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
			} else {
				TempCurrenRoom.ActionID = 10;
				TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound = LastWinnerID;
				TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = LastWinnerID;
				PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
			}
		}
	}

	void SetPlayersGreenOrRed ()
	{
		for (int i = 0; i < TempCurrenRoom.GreenOrRed.Length; i++) {

			if (TempCurrenRoom.GameOB.PlayersScores [i] == int.Parse(TempCurrenRoom.GameOB.PlayersBids [i]))
				TempCurrenRoom.GreenOrRed [i] = 1;
			
			if (TempCurrenRoom.GameOB.PlayersScores [i] > int.Parse(TempCurrenRoom.GameOB.PlayersBids [i]))
				TempCurrenRoom.GreenOrRed [i] = 2;

			if (13 - TempCurrenRoom.GameOB.CurrentRound.RoundNO < int.Parse (TempCurrenRoom.GameOB.PlayersBids [i])
				&&TempCurrenRoom.GreenOrRed[i]==0)
				TempCurrenRoom.GreenOrRed [i] = 2;
		}
	}

	#region implemented abstract members of PlayersManagerParent

	public override void SetLastRoundCards ()
	{
		string[] LastRoundCards = new string[4];
		for (int i = 0; i < 4; i++)
			LastRoundCards [i] = TempCurrenRoom.GameOB.CurrentRound.PlayersCards [FromOnlineToLocal (i)];
		LastRoundGO.GetComponent<LastRound> ().SetCards (LastRoundCards);
	}

	#endregion

	bool FullRoundEnds ()
	{
		if (TempCurrenRoom.GameOB.CurrentRound.RoundNO >= 13)
			return true;
		else
			return false;
	}

	private void UpdateRoomPhase11 ()
	{
		//Just a fukin Holder
	}

	private void UpdateRoomPhase10 ()
	{
		//GameStart
		PlayingWindowsManager5.Instance.SetTimer (FromOnlineToLocal (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay),
			TimerWaitingTime (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay), 2, false);
		SetAvailableCards ();
		int PrevPlayerID = PreviosTurn (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay);
		if (MyTurnToPlay ()) {
			if (IsThisFuckerTrulyTurnToPlay()) {
				StartCoroutine (UpdateRoomPhase10Cor());
			} else {
				IfPlayerHoldingCardGetItBack ();
				EstimationManager5.Instance.SetAndArrangeMyCards ();
			}
		}
		if (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [PrevPlayerID].Length < 2)
			return;
		ThrowThisCard (PrevPlayerID, TempCurrenRoom.GameOB.CurrentRound.PlayersCards [PrevPlayerID]);
	}

	private IEnumerator UpdateRoomPhase10Cor(){
		yield return new WaitForSeconds (1);
		try{GameObject CardHolded = CanvasGO.GetComponent<RectTransform> ().GetChild (CanvasGO.GetComponent<RectTransform> ().childCount - 1).gameObject;
			CardHolded.GetComponent<Card5> ().ReturnCardBack ();
			CardHolded.GetComponent<Card5> ().SendCard ();}catch{
		}
	}

	private bool SetAvailableCardsOnce;
	void SetAvailableCards ()
	{
		//Watcher
		if (Player.Instance.OnlinePlayerID == -1)
			return;

		if (SetAvailableCardsOnce)
			return;
		List<string> PlayedCards = new List<string> ();
		IfPlayerHoldingCardGetItBack ();
		int Counter = 0;
		int PlayedPlayerID = 0;
		for (int i = 0; i < 4; i++)
			PlayedCards.Add (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i]);
		for (int i = 0; i < 4; i++) {
			if (PlayedCards [i].Length > 1) {
				Counter++;
				PlayedPlayerID = i;
			}
		}
		if (Counter != 1 || PreviosTurn (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay) == Player.Instance.OnlinePlayerID)
			return;
		string CardType = GetChar (PlayedCards [PlayedPlayerID], 0);
		if (IHaveThisCardType (CardType)) {
			for (int i = 0; i < CardsListGO.transform.childCount; i++) {
				if (!CardsListGO.transform.GetChild (i).GetComponent<Card5> ().CardID.StartsWith (CardType)) {
					CardAvailability (CardsListGO.transform.GetChild (i).gameObject, false);
					SetAvailableCardsOnce = true;
				}
			}
		}
	}

	bool IHaveThisCardType (string Cardtype)
	{
		for (int i = 0; i < CardsListGO.transform.childCount; i++) {
			if (CardsListGO.transform.GetChild (i).GetComponent<Card5> ().CardID.StartsWith (Cardtype))
				return true;
		}
		return false;
	}

	void ThrowThisCard (int PlayerThrowerID, string ThrowdCardID)
	{
		int PlayerIDOnMyDevice = FromOnlineToLocal (PlayerThrowerID);
		CardsThrowerMangaer.Instance.ThrowThisCard (ThrowdCardID, PlayerIDOnMyDevice);
		if (PlayerIDOnMyDevice != 0 || Player.Instance.OnlinePlayerID == -1)
			PlayersCardsGO [PlayerIDOnMyDevice].transform.GetChild (1).GetChild (0).
			GetChild (TempCurrenRoom.GameOB.CurrentRound.RoundNO).gameObject.SetActive (false);
		if (RoundEnds ()) {
			SetLastRoundCards ();
			if (Player.Instance.GameCreator)
				CloseRound ();
		}
	}

	public void ThrowCard (GameObject Cardjo)
	{
		if (MyTurnToPlay () && TempCurrenRoom.ActionID == 10 && CanThrowCard)
			StartCoroutine (RemoveOrderly (Cardjo));
		else
			Cardjo.GetComponent<Card5> ().ReturnCardBack ();
	}

	private bool CanThrowCard = true;

	private IEnumerator RemoveOrderly (GameObject Cardjo)
	{
		CanThrowCard = false;
		ThrowCardNow (Cardjo.GetComponent<Card5> ().CardID);
		Destroy (Cardjo);
		yield return new WaitForSeconds (0.03f);try{
			EstimationManager5.Instance.SetAndArrangeMyCards ();}catch{
		}
		yield return new WaitForSeconds (2f);
		CanThrowCard = true;
		yield return null;
	}

	void ThrowCardNow (string CardID)
	{
		string CardIDString = CardID;
		int PlayerTurn = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		TempCurrenRoom.GameOB.CurrentRound.PlayersCards [PlayerTurn] = CardIDString;
		RemoveThisCardFromOnlineCardsList (PlayerTurn, CardIDString);
		int NextPlayerID = PlayerTurn;
		NextPlayerID++;
		if (NextPlayerID >= 4)
			NextPlayerID = 0;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = NextPlayerID;
		PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private void RemoveThisCardFromOnlineCardsList (int PlayerNO, string CardID)
	{
		for (int i = 0; i < TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList.Count; i++) {
			if (TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList [i] == CardID) {
				TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList [i] = "0";
				return;
			}
		}
	}

	private bool RoundEnds ()
	{
		int Over1Counter = 0;
		for (int i = 0; i < 4; i++) {
			if (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i].Length > 1)
				Over1Counter++;
		}
		if (Over1Counter == 4)
			return true;
		else
			return false;
	}

	private List<string> PlayersCardsList;

	void CloseRound ()
	{
		int LastWinnderID = GetWinnerID ();
		TempCurrenRoom.GameOB.CurrentRound.PlayersScores [LastWinnderID]++;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = LastWinnderID;
		TempCurrenRoom.GameOB.CollectorID = LastWinnderID;
		TempCurrenRoom.GameOB.CurrentRound.RoundNO++;
		for (int i = 0; i < 4; i++)
			TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i] = "0";
		TempCurrenRoom.ActionID = 12;
		PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private int GetWinnerID ()
	{
		TempCurrenRoom.ActionID = 11;
		PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
		PlayersCardsList = new List<string> ();
		for (int i = 0; i < 4; i++)
			PlayersCardsList.Add (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i]);
		if (ThereIsTrumpCard ())
			return GetHighestFromThisType (TempCurrenRoom.GameOB.TrumpType);
		else
			return GetHighestFromThisType (GetChar (PlayersCardsList [TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound], 0));
	}

	private int GetHighestFromThisType (string CardType)
	{
		List<string> NewCardsList = new List<string> ();
		for (int i = 0; i < 4; i++) {
			if (PlayersCardsList [i].StartsWith (CardType))
				NewCardsList.Add (PlayersCardsList [i]);
		}
		//After List Ready
		NewCardsList.Sort ();
		for (int i = 0; i < 4; i++) {
			if (NewCardsList [NewCardsList.Count - 1] == PlayersCardsList [i])
				return i;
		}
		return 0;
	}

	private bool ThereIsTrumpCard ()
	{
		for (int i = 0; i < 4; i++) {
			if (PlayersCardsList [i].StartsWith (TempCurrenRoom.GameOB.TrumpType))
				return true;
		}
		return false;
	}


	void UpdateRoomPhase9 ()
	{
		//Debug.Log ("UpdateRoomPhase9");
		AudioManager.Instance.PlaySound (1);
		AudioManager.Instance.PlaySound (TrumpTypeSound(TempCurrenRoom.GameOB.TrumpType));
		PlayingWindowsManager5.Instance.SetTrumpAndBidder (TempCurrenRoom.GameOB.TrumpType, TempCurrenRoom.GameOB.HighestBid);
		SetPlayersBidsTxts ();
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.ActionID = 10;
		PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void SetPlayersBidsTxts ()
	{
		//Debug.Log ("SetPlayersBidsTxts");
		for (int i = 0; i < 4; i++) {
			//Debug.Log (FromOnlineToLocal (i));
			PlayingWindowsManager5.Instance.PlayersScoresGO [FromOnlineToLocal (i)].GetComponent<Text> ().text = "0/" +
			TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [i];
		}
	}

	void UpdateRoomPhase8 ()
	{
		//Here Trump Player Chooses TrumpType
		//Debug.Log ("UpdateRoomPhase8");
	}

	private void UpdateRoomPhase7 ()
	{
		//Debug.Log ("UpdateRoomPhase7");
		if (TempCurrenRoom.GameOB.EstimationDetailsOB.CurrentPopString.Length > 0)
			PlayingWindowsManager5.Instance.ShowBidPop (FromOnlineToLocal (TempCurrenRoom.GameOB.PlayerBidTurn),
				TempCurrenRoom.GameOB.EstimationDetailsOB.CurrentPopString);
		RefreshPlayersEstimationsDetails ();
		if (!Player.Instance.GameCreator)
			return;
		if (BidsTurnEndsRound2 ()) {
			int ThisWinnerID = TempCurrenRoom.GameOB.EstimationDetailsOB.BidsWinnerID;
			TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound = ThisWinnerID;
			TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = ThisWinnerID;
			TempCurrenRoom.ActionID = 9;
		} else {
			TempCurrenRoom.GameOB.PlayerBidTurn = NextNO (TempCurrenRoom.GameOB.PlayerBidTurn);
			TempCurrenRoom.ActionID = 5;
		}
		PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	bool BidsTurnEndsRound2 ()
	{
		int Bids2Counter = 0;
		if (TempCurrenRoom.GameOB.EstimationDetailsOB.BidsWinnerID == NextNO (TempCurrenRoom.GameOB.PlayerBidTurn)) {
			for (int i = 0; i < 4; i++)
				if (TempCurrenRoom.GameOB.EstimationDetailsOB.Bid2 [i])
					Bids2Counter++;
		}
		if (Bids2Counter >= 3)
			return true;
		return false;
	}

	//Bidding Phase
	void UpdateRoomPhase6 ()
	{
		//Debug.Log ("UpdateRoomPhase6");
		string LastBid = TempCurrenRoom.GameOB.EstimationDetailsOB.CurrentPopString;
		PlayingWindowsManager5.Instance.ShowBidPop (FromOnlineToLocal (TempCurrenRoom.GameOB.PlayerBidTurn),
				LastBid);
		if (!Player.Instance.GameCreator)
			return;
		if (NoOfPasses () == 4) {
			if (Player.Instance.GameCreator)
				NewRound (false);
		} else {
			if (BidsTurnEndsRound1 ()) {
				int ThisWinnerID = GetBidWinnerID ();
				TempCurrenRoom.GameOB.EstimationDetailsOB.BidRound = 2;
				TempCurrenRoom.GameOB.EstimationDetailsOB.BidsWinnerID = ThisWinnerID;
				TempCurrenRoom.GameOB.PlayerBidTurn = ThisWinnerID;
				TempCurrenRoom.GameOB.EstimationDetailsOB.CurrentPopString = "";
				TempCurrenRoom.GameOB.HighestBid = TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [ThisWinnerID];
				if (!TempCurrenRoom.GameOB.EstimationDetailsOB.FastBidding)
					TempCurrenRoom.GameOB.TrumpType = TempCurrenRoom.GameOB.EstimationDetailsOB.PlayerTrump [ThisWinnerID];
				TempCurrenRoom.ActionID = 7;
			} else {
				TempCurrenRoom.ActionID = 5;
				if (TempCurrenRoom.GameOB.EstimationDetailsOB.CurrentPopString != "Avoid")
					TempCurrenRoom.GameOB.PlayerBidTurn = NextBidder (TempCurrenRoom.GameOB.PlayerBidTurn);
			}
			PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
		}
	}

	int GetBidWinnerID ()
	{
		//Debug.Log ("GetBidWinnerID");
		int ThisHighestBid = 0;
		int InitialWinner = 0;
		int ThisWinnerID = 0;
		List<int> BidsList = new List<int> (4);
		for (int i = 0; i < TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid.Length; i++)
			BidsList.Add (TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [i]);
		BidsList.Sort ();
		ThisHighestBid = BidsList [3];
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [i] == ThisHighestBid) {
				InitialWinner = i;
				break;
			}
		ThisWinnerID = InitialWinner;
		if (TempCurrenRoom.GameOB.EstimationDetailsOB.FastBidding)
			return ThisWinnerID;
		if (BidsList [2] == BidsList [3])
			for (int i = 0; i < 4; i++) {
				if (i == InitialWinner)
					continue;
				else if (TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [i] == BidsList [2]) {
					ThisWinnerID = GetHighestFromTheseTow (i, InitialWinner);
					break;
				}
			}
		return ThisWinnerID;
	}

	int GetHighestFromTheseTow (int FirstWinnerID, int SocendWinnerID)
	{
		//Debug.Log (FirstWinnerID + "||" + SocendWinnerID);
		string[] TrumpTypes = new string[5]{ "NT", "S", "H", "D", "C" };
		for (int i = 0; i < TrumpTypes.Length; i++) {
			if (TempCurrenRoom.GameOB.EstimationDetailsOB.PlayerTrump [FirstWinnerID] == TrumpTypes [i])
				return FirstWinnerID;
			else if (TempCurrenRoom.GameOB.EstimationDetailsOB.PlayerTrump [SocendWinnerID] == TrumpTypes [i])
				return SocendWinnerID;
		}
		return FirstWinnerID;
	}

	bool BidsTurnEndsRound1 ()
	{
		if (TempCurrenRoom.GameOB.EstimationDetailsOB.LastPlayerID == TempCurrenRoom.GameOB.PlayerBidTurn)
		if (BidsSum () > 0)
			return true;
		return false;
	}

	private int NoOfFirstBis ()
	{
		int FirstBisCounter = 0;
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.EstimationDetailsOB.Bid1 [i])
				FirstBisCounter++;
		//Debug.Log ("NoOfFirstBis:" + FirstBisCounter);
		return FirstBisCounter;
	}

	int NextBidder (int playerBidTurn)
	{
		playerBidTurn = NextNO (playerBidTurn);
		for (int i = playerBidTurn; i < 4; i++)
			if (!TempCurrenRoom.GameOB.EstimationDetailsOB.Pass1 [playerBidTurn]) {
				playerBidTurn = i;
				break;
			} else
				playerBidTurn = NextNO (playerBidTurn);		
		return playerBidTurn;
	}

	void RefreshPlayersEstimationsDetails ()
	{
		for (int i = 0; i < 4; i++) {
			PlayingWindowsManager5.Instance.PlayerEstimationDetails [FromOnlineToLocal (i)].GetComponent<Text> ().text = "";
			if (TempCurrenRoom.GameOB.EstimationDetailsOB.Pass3Round2 [i])
				PlayingWindowsManager5.Instance.PlayerEstimationDetails [FromOnlineToLocal (i)].GetComponent<Text> ().text = "DC";
			else {
				if (TempCurrenRoom.GameOB.EstimationDetailsOB.With [i])
					PlayingWindowsManager5.Instance.PlayerEstimationDetails [FromOnlineToLocal (i)].GetComponent<Text> ().text = "W";
				if (TempCurrenRoom.GameOB.EstimationDetailsOB.Risk [i])
					PlayingWindowsManager5.Instance.PlayerEstimationDetails [FromOnlineToLocal (i)].GetComponent<Text> ().text += "R";
				else if (TempCurrenRoom.GameOB.EstimationDetailsOB.DoubleRisk [i])
					PlayingWindowsManager5.Instance.PlayerEstimationDetails [FromOnlineToLocal (i)].GetComponent<Text> ().text += "RR";
			}
		}
	}

	public void DetailsBidBTN (int SelectedBid, string SelectedTrumpType)
	{
		bool IsRound2 = TempCurrenRoom.GameOB.EstimationDetailsOB.BidRound == 2;
		int PlayerBidTurn = TempCurrenRoom.GameOB.PlayerBidTurn;
		PlayingWindowsManager5.Instance.BidDetailsWindowGO.SetActive (false);
		TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [PlayerBidTurn] = SelectedBid;
		TempCurrenRoom.GameOB.EstimationDetailsOB.PlayerTrump [PlayerBidTurn] = SelectedTrumpType;
		if (IsRound2)
			TempCurrenRoom.GameOB.EstimationDetailsOB.Bid2 [PlayerBidTurn] = true;
		if (!IsRound2)
			TempCurrenRoom.GameOB.EstimationDetailsOB.CurrentPopString = SelectedBid + " - " + SelectedTrumpType;
		else
			TempCurrenRoom.GameOB.EstimationDetailsOB.CurrentPopString = SelectedBid + "";
		if (IsRound2) {
			if (SelectedBid == 0)
				TempCurrenRoom.GameOB.EstimationDetailsOB.Pass3Round2 [PlayerBidTurn] = true;
			else if (SelectedBid == TempCurrenRoom.GameOB.HighestBid)
				TempCurrenRoom.GameOB.EstimationDetailsOB.With [PlayerBidTurn] = true;
		}

		if (!IsRound2)
			TempCurrenRoom.ActionID = 6;
		else
			TempCurrenRoom.ActionID = 7;
		PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	public void DetailsPassBTN ()
	{
		int PlayerID = TempCurrenRoom.GameOB.PlayerBidTurn;
		PlayingWindowsManager5.Instance.BidDetailsWindowGO.SetActive (false);
		TempCurrenRoom.GameOB.EstimationDetailsOB.Pass2 [PlayerID] = true;
		TempCurrenRoom.GameOB.EstimationDetailsOB.CurrentPopString = "Pass";
		TempCurrenRoom.ActionID = 6;
		PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	public void RedealOrAvoid (bool RedealOrAvoidKey)
	{
		PlayingWindowsManager5.Instance.AvoidWindowGO.SetActive (false);
		if (RedealOrAvoidKey)
			NewRound (false);
		else {
			TempCurrenRoom.GameOB.EstimationDetailsOB.Avoid [TempCurrenRoom.GameOB.PlayerBidTurn] = true;
			TempCurrenRoom.GameOB.EstimationDetailsOB.CurrentPopString = "Avoid";
			TempCurrenRoom.ActionID = 6;
			PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
		}
	}

	private int PrivateLowestAccepted = 4;
	//Player Bid Turn
	void UpdateRoomPhase5 ()
	{
		//Debug.Log ("UpdateRoomPhase5");
		PlayingWindowsManager5.Instance.SetTimer (FromOnlineToLocal (TempCurrenRoom.GameOB.PlayerBidTurn),
			TimerWaitingTime (TempCurrenRoom.GameOB.PlayerBidTurn), 1, false);
		
		if (!IsThisMyBidTurn ())
			return;

		if (TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.PlayerBidTurn] == 0)
			return;
		
		if (OneOfTypesMissing () && !TempCurrenRoom.GameOB.EstimationDetailsOB.Avoid [Player.Instance.OnlinePlayerID])
			PlayingWindowsManager5.Instance.AvoidWindowGO.SetActive (true);
		else if (NoOfDashCalls () < 2 &&
		         (!TempCurrenRoom.GameOB.EstimationDetailsOB.Bid1 [Player.Instance.OnlinePlayerID] &&
		         !TempCurrenRoom.GameOB.EstimationDetailsOB.Pass1 [Player.Instance.OnlinePlayerID]))
			PlayingWindowsManager5.Instance.BidOrPassWindowGO.SetActive (true);
		else if (TempCurrenRoom.GameOB.EstimationDetailsOB.BidRound == 1)
			PlayingWindowsManager5.Instance.ShowBidDetailsFun (PrivateLowestAccepted, LastPlayer () && BidsSum () < 13);
		else if (TempCurrenRoom.GameOB.EstimationDetailsOB.BidRound == 2)
			PlayingWindowsManager5.Instance.ShowBidDetailsFun2 
			(TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [TempCurrenRoom.GameOB.EstimationDetailsOB.BidsWinnerID], NoMoreDashs ());
	}

	private bool LastPlayer ()
	{
		if (Player.Instance.OnlinePlayerID == TempCurrenRoom.GameOB.EstimationDetailsOB.LastPlayerID)
			return true;
		else
			return false;
	}

	private int NoOfDashCalls ()
	{
		int DashCallsCounter = 0;
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.EstimationDetailsOB.Pass1 [i])
				DashCallsCounter++;
		return DashCallsCounter;
	}

	private int NoOfPasses ()
	{
		int NoOfPassesCounter = 0;
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.EstimationDetailsOB.Pass1 [i] || TempCurrenRoom.GameOB.EstimationDetailsOB.Pass2 [i])
				NoOfPassesCounter++;
		//Debug.Log ("NoOfPassesCounter:" + NoOfPassesCounter);
		return NoOfPassesCounter;
	}

	public void BidAndPassBTN (bool BidOrPass)
	{
//		//Debug.Log ("BidAndPassBTN");
		PlayingWindowsManager5.Instance.BidOrPassWindowGO.SetActive (false);
		int PlayerID = TempCurrenRoom.GameOB.PlayerBidTurn;
		if (BidOrPass) {
			TempCurrenRoom.GameOB.EstimationDetailsOB.CurrentPopString = "Bid";
			TempCurrenRoom.GameOB.EstimationDetailsOB.Bid1 [PlayerID] = true;
		} else {
			TempCurrenRoom.GameOB.EstimationDetailsOB.CurrentPopString = "Dash Call";
			TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [PlayerID] = 0;
			TempCurrenRoom.GameOB.EstimationDetailsOB.PlayerTrump [PlayerID] = "";
			TempCurrenRoom.GameOB.EstimationDetailsOB.Pass1 [TempCurrenRoom.GameOB.PlayerBidTurn] = true;
		}
		TempCurrenRoom.ActionID = 6;
		PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
	}


	bool IsThisMyBidTurn ()
	{
		if (Player.Instance.OnlinePlayerID == TempCurrenRoom.GameOB.PlayerBidTurn)
			return true;
		else
			return false;
	}

	bool OneOfTypesMissing ()
	{
		List<string> MyCardsList = TempCurrenRoom.GameOB.Cards.PlayersCards [Player.Instance.OnlinePlayerID].PlayerCardsList;
		int TypesCounter = 0;
		bool[] TypeCounted = new bool[4];
		string[] CardsTypes = new string[4]{ "S", "C", "H", "D" };
		for (int i = 0; i < MyCardsList.Count; i++) {
			for (int u = 0; u < 4; u++)
				if (!TypeCounted [u] && GetChar (MyCardsList [i], 0) == CardsTypes [u]) {
					TypesCounter++;
					TypeCounted [u] = true;
					continue;
				}
		}
		if (TypesCounter != 4)
			return true;
		else
			return false;
	}

	private bool SceneDoneLoading;

	void UpdateRoomPhase4 ()
	{
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (PlayerID == -1)
			return;
		SceneDoneLoading = true;
		EstimationManager5.Instance.Cards.PlayersCards [PlayerID].PlayerCardsList =
			TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerID].PlayerCardsList;
		EstimationManager5.Instance.SetAndArrangeMyCards ();
		ResetData ();
		ReturnsMyCardsAvailibity ();
		if (!Player.Instance.GameCreator)
			return;
		if (TempCurrenRoom.GameOB.EstimationDetailsOB.FastBidding)
			TempCurrenRoom.GameOB.TrumpType = GetTrump ();
		TempCurrenRoom.GameOB.PlayerBidTurn = TempCurrenRoom.GameOB.EstimationDetailsOB.PlayerWhoStartBids;
		TempCurrenRoom.ActionID = 5;
		PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	string GetTrump ()
	{
		int GameCounter = TempCurrenRoom.GameOB.EstimationDetailsOB.GamesCounter;
		if (GameCounter == 14)
			return "NT";
		else if (GameCounter == 15)
			return "S";
		else if (GameCounter == 16)
			return "H";
		else if (GameCounter == 17)
			return "D";
		else
			return "C";
		
	}

	void ResetData ()
	{
		for (int i = 0; i < 4; i++)
			PlayingWindowsManager5.Instance.PlayerEstimationDetails [i].GetComponent<Text> ().text = "";
		PlayingWindowsManager5.Instance.ResetWindows ();
	}

	void UpdateRoomPhase3 ()
	{
		if (Player.Instance.OnlinePlayerID == -1)
			return;
		if (SceneDoneLoading)
			return;
		if (IsAllPlayersReady () && Player.Instance.GameCreator) {
			TempCurrenRoom.ActionID = -1;
			PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
			return;
		}
		UpdateRoomPhase2 ();
		if (TempCurrenRoom.PlayersReady [Player.Instance.OnlinePlayerID])
			return;
		SetPlayersScoresTxts ();
		IfPlayerHoldingCardGetItBack ();
		PlayingWindowsManager5.Instance.TrumpDetailsWindow.SetActive (false);
		PlayingFireBaseManager5.Instance.SetOnDissconnectHandle ();
		EstimationManager5.Instance.AssignPlayerCardsAndActivateOthersCards ();
		PlayingWindowsManager5.Instance.WinsWindow.SetActive (false);
		TempCurrenRoom.PlayersReady [Player.Instance.OnlinePlayerID] = true;
		if (Player.Instance.GameCreator)
			SetReadyForComputer ();
		PlayingFireBaseManager5.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void SetReadyForComputer ()
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.SeatsStatus [i] == 0)
				TempCurrenRoom.PlayersReady [i] = true;
	}

	bool IsAllPlayersReady ()
	{
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++)
			if (!TempCurrenRoom.PlayersReady [i])
				return false;
		return true;
	}

	void SetPlayersScroesTxt ()
	{
		
	}

	void SetPlayersScoresTxts ()
	{
		for (int i = 0; i < 4; i++)
			PlayingWindowsManager5.Instance.TeamsScores [FromOnlineToLocal (i)].GetComponent<Text> ().text = TempCurrenRoom.GameOB.PlayersScores [i].ToString ();
	}

	private int NextNO (int NO)
	{
		NO++;
		if (NO >= 4)
			NO = 0;
		return NO;
	}

	public int HighestBid ()
	{
		List<int> BidsList = new List<int> ();
		for (int i = 0; i < 4; i++)
			BidsList.Add (TempCurrenRoom.GameOB.EstimationDetailsOB.PlayersBid [i]);
		BidsList.Sort ();
		return BidsList [3];
	}

	#region implemented abstract members of PlayersManagerParent

	public override void OnApplicationPause (bool pauseStatus)
	{
		if (TempCurrenRoom.ActionID < 4)
			return;
		try {
			if (pauseStatus)
				CreatorPauseOrExit (false);
			else
				StartCoroutine (SetandarrangeCor (pauseStatus));
		} catch {
			//Debug.Log ("OnApplicationPause\t" + e.Message);
		}
	}

	#endregion

	private IEnumerator SetandarrangeCor (bool pauseStatus)
	{
		ShowBackBTN ();
		yield return new WaitForSeconds (2.5f);
		try {
			EstimationManager5.Instance.SetAndArrangeMyCards ();
		} catch {
		}
	}


	#region implemented abstract members of PlayersManagerParent

	public override void IamBack ()
	{
		TempCurrenRoom.SeatsStatus [Player.Instance.OnlinePlayerID] = 1;
		PlayerIsBack (true);
		PlayingFireBaseFather.ParentInstance.UploudThisJson (TempCurrenRoom, false);
	}

	#endregion

	private void OnDestroy ()
	{
		StopAllCoroutines ();
	}
}