﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayingBTNSManager5 : PlayingBTNSManagerFather {
	
	public static PlayingBTNSManager5 Instance;

	protected override void Awake(){
		base.Awake ();
		Instance = this;
	}

	public override void BTNSFunction(){
		base.BTNSFunction ();
		if (BTNName.Contains ("RestartBTN"))
			PlayersManager5.Instance.RestartGame ();
		else if (BTNName.Contains ("AvoidBTN")) {
			PlayingWindowsManager5.Instance.AvoidWindowGO.SetActive (false);
			PlayersManager5.Instance.RedealOrAvoid (false);
		} else if (BTNName.Contains ("RedealBTN"))
			PlayersManager5.Instance.RedealOrAvoid (true);
		else if (BTNName.Contains ("FirstBidBTN"))
			PlayersManager5.Instance.BidAndPassBTN (true);
		else if (BTNName.Contains ("DashCallBTN"))
			PlayersManager5.Instance.BidAndPassBTN (false);
		else if (BTNName.Contains ("BidNOBTN"))
			PlayingWindowsManager5.Instance.SetSelectedBidNO (CurrentBTNGO.transform.GetSiblingIndex ());
		else if (CurrentBTNGO.transform.parent.name == "ChooseTrumpFather")
			SetActualSelectedTrumpType (BTNName, CurrentBTNGO);
		else if (BTNName == "DetailsBid")
			PlayersManager5.Instance.DetailsBidBTN (PlayingWindowsManager5.Instance.SelectedBidNO,PlayingWindowsManager5.Instance.SelectedTrumpType);
		else if (BTNName== "DetailsPass")
			PlayersManager5.Instance.DetailsPassBTN ();
	}

	void SetActualSelectedTrumpType (string BTNName,GameObject BTNGO)
	{
		BTNName = GetChar (BTNName, 0) + GetChar (BTNName, 1);
		if (BTNName != "NT")
			BTNName = GetChar (BTNName, 0);
		PlayingWindowsManager5.Instance.SetSelectedTrumpType (BTNName,BTNGO);
	}
}