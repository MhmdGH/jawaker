﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public class PlayingWindowsManager5 : PlayingWindowsManagerFather {

	public static PlayingWindowsManager5 Instance;
	public GameObject[] BidsPops,PlayerEstimationDetails,BidsNOGO,SelectionBidsNOGO,TrumpTypesGO,SelectedTrumpTypesGO;
	public GameObject AvoidWindowGO,BidOrPassWindowGO,BidDetailsWindowGO,TrumpDetailsWindow,DetailsPassBidGO;
	[HideInInspector]
	public int SelectedBidNO=4;
	[HideInInspector]
	public string SelectedTrumpType="NT";

	public override void Awake(){
		base.Awake ();
		Instance = this;
		SelectedBidNO = 4;
		SelectedTrumpType="NT";
	}

	public void ShowBidDetailsFun (int LowesetAcceptedBidNO,bool HidePassBTN)
	{
		if(HidePassBTN)
			DetailsPassBidGO.SetActive (false);
		SetSelectedBidNO (LowesetAcceptedBidNO);
		for (int i = 0; i < BidsNOGO.Length; i++) {
			if (i< LowesetAcceptedBidNO)
				BidsNOGO [i].SetActive (false);
			else
				BidsNOGO [i].SetActive (true);
		}
		if(PlayersManager5.Instance.TempCurrenRoom.GameOB.EstimationDetailsOB.FastBidding)
			for (int i = 0; i < TrumpTypesGO.Length; i++)
				TrumpTypesGO [i].SetActive (false);
		BidDetailsWindowGO.SetActive (true);
	}

	public void ShowBidDetailsFun2 (int HighestAccepted,bool NoMoreDashs)
	{
		DetailsPassBidGO.SetActive (false);
		SetSelectedBidNO (1);
		for (int i = 0; i < BidsNOGO.Length; i++)
			if (i > HighestAccepted)
				BidsNOGO [i].SetActive (false);
			else
				BidsNOGO [i].SetActive (true);
		for (int i = 0; i < TrumpTypesGO.Length; i++)
			TrumpTypesGO [i].SetActive (false);
			BidsNOGO [0].SetActive (!NoMoreDashs);
		if(PlayersManager5.Instance.TempCurrenRoom.GameOB.EstimationDetailsOB.FastBidding)
			for (int i = 0; i < TrumpTypesGO.Length; i++)
				TrumpTypesGO [i].SetActive (false);
		BidDetailsWindowGO.SetActive (true);
	}

	public void SetSelectedBidNO(int BidNO){
		for (int i = 0; i < SelectionBidsNOGO.Length; i++)
			SelectionBidsNOGO [i].SetActive (false);
		SelectionBidsNOGO [BidNO].SetActive (true);
		SelectedBidNO = BidNO;
	}

	public override void ShowWindWindow(int WinnerTeamNO){
		string WinsTxt = "";
		if(LanguangeManagerFather.ParentInstance.IsArabic)
			WinsTxt = ArabicFixer.Fix("الفائز "+WinnerTeamNO+" اللاعب");
		else
			WinsTxt = "player "+WinnerTeamNO+" Wins";
		WinsWindow.transform.GetChild (1).GetComponent<Text> ().text = WinsTxt;
		WinsWindow.SetActive (true);
		WinsWindow.GetComponent<RectTransform> ().GetChild (4).gameObject.SetActive (!MainFireBaseManager.Instance.IsACompition);
	}

	public void SetSelectedTrumpType(string TrumpType,GameObject BTNGO){
		for (int i = 0; i < SelectedTrumpTypesGO.Length; i++)
			SelectedTrumpTypesGO [i].SetActive (false);
		SelectedTrumpTypesGO [BTNGO.transform.GetSiblingIndex()].SetActive (true);
		SelectedTrumpType = TrumpType;
	}

	public void ShowBidPop(int BidPopNO,string BidValue){
		StartCoroutine (ShowBidCor(BidPopNO,BidValue));
	}

	public void SetTrumpAndBidder(string TrumpType,int Bidding){
		TrumpDetailsWindow.transform.GetChild (2).GetComponent<Text> ().text = "";
		TrumpDetailsWindow.SetActive (true);
		TrumpDetailsWindow.transform.GetChild (1).gameObject.SetActive (true);
		if (TrumpType == "NT") {
			TrumpDetailsWindow.transform.GetChild (1).gameObject.SetActive (false);
			TrumpDetailsWindow.transform.GetChild (2).GetComponent<Text> ().text+="NT ";
		}
		else
		TrumpDetailsWindow.transform.GetChild(1).GetComponent<Image>().sprite = 
			Resources.Load<Sprite> ("Playing/"+TrumpType);
		TrumpDetailsWindow.transform.GetChild (2).GetComponent<Text> ().text += Bidding.ToString ();
	}

	private IEnumerator ShowBidCor(int BidPopNO,string BidValue){
		if (BidValue == "Pass")
			AudioManager.Instance.PlaySound (16);
		else if(BidValue=="Avoid")
			AudioManager.Instance.PlaySound (35);
		else if(BidValue=="Bid")
			AudioManager.Instance.PlaySound (23);
		else if (BidValue == "Dash Call")
			AudioManager.Instance.PlaySound (34);
		BidsPops [BidPopNO].SetActive (true);
		BidsPops [BidPopNO].transform.GetChild (0).GetComponent<Text> ().text = BidValueTranslated(BidValue);
		yield return new WaitForSeconds (2);
		BidsPops [BidPopNO].SetActive (false);
	}

	string BidValueTranslated (string TranslfatedBidTxt)
	{
		if (LanguangeManagerFather.ParentInstance.IsArabic) {
			if (TranslfatedBidTxt == "Pass")
				TranslfatedBidTxt =ArabicFixer.Fix("باص");
			else if (TranslfatedBidTxt == "Avoid")
				TranslfatedBidTxt =ArabicFixer.Fix("أمتنع");
			else if (TranslfatedBidTxt == "Bid")
				TranslfatedBidTxt =ArabicFixer.Fix("طلب");
			else if (TranslfatedBidTxt == "Dash Call")
				TranslfatedBidTxt =ArabicFixer.Fix("داش كول");
		}
		return TranslfatedBidTxt;
	}


	public override void StopTimer(){
		try{
		base.StopTimer ();
		}catch{
		}
	}

	protected override IEnumerator StartThisTimer(int PlayerNO,float WaitingTime,int FunToDOAfterEnd,bool JustCreatorDoIT){
		PlayersTimer [PlayerNO].SetActive (true);
		WaitingTime = WaitingTime*SliderSmothnessMultiplier;
		float ActualTimerTikValue = TimerTikValue*SliderSmothnessMultiplier;
		PlayersTimer [PlayerNO].GetComponent<Slider> ().maxValue = WaitingTime;
		PlayersTimer [PlayerNO].GetComponent<Slider> ().value = WaitingTime;
		while (WaitingTime > 0) {
			PlayersTimer [PlayerNO].GetComponent<Slider> ().value -= ActualTimerTikValue;
			WaitingTime-=ActualTimerTikValue;
			yield return new WaitForSeconds (TimerTikValue*0.1f);
		}
		PlayersTimer [PlayerNO].SetActive (false);
		if (JustCreatorDoIT && Player.Instance.GameCreator)
			PlayersManager5.Instance.MasterTakeControl (FunToDOAfterEnd);
		else if(!JustCreatorDoIT)
			PlayersManager5.Instance.MasterTakeControl (FunToDOAfterEnd);
	}

	public void ShowBidDetails ()
	{
		BidDetailsWindowGO.SetActive (true);
	}

	public void ResetWindows(){
		DetailsPassBidGO.SetActive (true);
		for (int i = 0; i < BidsNOGO.Length; i++)
			BidsNOGO [i].SetActive (true);
		for (int i = 0; i < TrumpTypesGO.Length; i++)
			TrumpTypesGO [i].SetActive (true);
	}

	private void OnDestroy(){
		StopAllCoroutines ();
	}
}