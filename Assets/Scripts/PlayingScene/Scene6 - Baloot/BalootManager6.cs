﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalootManager6 : MonoBehaviour
{

	public static BalootManager6 Instance;
	public GameObject[] MyPlayerCardsGO, PlayersCardsContentGO;
	public GameObject CardGO;
	[HideInInspector]
	public PlayersCardsOB1 Cards;
	private List<string> ModifeidCardList;

	void Awake ()
	{
		Instance = this;
	}

	void Start ()
	{
		ModifeidCardList = new List<string> ();
	}

	public void SetAndArrangeMyCards ()
	{
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (Cards.PlayersCards [PlayerID].PlayerCardsList.Count > MyPlayerCardsGO.Length)
			CreateAndAssignMyCards ();

		for (int i = 0; i < PlayersCardsContentGO [0].transform.childCount; i++)
			PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card6> ().CardID = "0";
		//ForWatcher
		if (PlayerID == -1) {
			for (int i = 0; i < PlayersCardsContentGO [0].transform.childCount; i++)
				PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card6> ().CardID = Cards.PlayersCards [4].PlayerCardsList [i];
			return;
		} else {
			List<string> CardsList = Cards.PlayersCards [PlayerID].PlayerCardsList;
			ModifeidCardList.Clear ();

			for (int i = 0; i < CardsList.Count; i++)
				if (!CardsList [i].StartsWith ("0"))
					ModifeidCardList.Add (CardsList [i]);
			for (int i = 0; i < ModifeidCardList.Count; i++)
				PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card6> ().CardID = ModifeidCardList [i];
			for (int i = 0; i < PlayersCardsContentGO [0].transform.childCount; i++)
				if (PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card6> ().CardID.StartsWith ("0"))
					Destroy (PlayersCardsContentGO [0].transform.GetChild (i).gameObject);
		}
		List<string> DCards = new List<string> ();
		List<string> SCards = new List<string> ();
		List<string> HCards = new List<string> ();
		List<string> CCards = new List<string> ();
		for (int i = 0; i < ModifeidCardList.Count; i++) {
			if (ModifeidCardList [i].StartsWith ("D"))
				DCards.Add (ModifeidCardList [i]);
			else if (ModifeidCardList [i].StartsWith ("S"))
				SCards.Add (ModifeidCardList [i]);
			else if (ModifeidCardList [i].StartsWith ("H"))
				HCards.Add (ModifeidCardList [i]);
			else if (ModifeidCardList [i].StartsWith ("C"))
				CCards.Add (ModifeidCardList [i]);
		}
		DCards.Sort ();
		SCards.Sort ();
		HCards.Sort ();
		CCards.Sort ();
		int Counter = 0;
		for (int i = 0; i < DCards.Count; i++) {
			PlayersCardsContentGO [0].transform.GetChild (Counter).GetComponent<Card6> ().CardID = DCards [i];
			Counter++;
		}
		for (int i = 0; i < SCards.Count; i++) {
			PlayersCardsContentGO [0].transform.GetChild (Counter).GetComponent<Card6> ().CardID = SCards [i];
			Counter++;
		}
		for (int i = 0; i < HCards.Count; i++) {
			PlayersCardsContentGO [0].transform.GetChild (Counter).GetComponent<Card6> ().CardID = HCards [i];
			Counter++;
		}
		for (int i = 0; i < CCards.Count; i++) {
			PlayersCardsContentGO [0].transform.GetChild (Counter).GetComponent<Card6> ().CardID = CCards [i];
			Counter++;
		}
		for (int i = 0; i < PlayersCardsContentGO [0].transform.childCount; i++) {
			PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card> ().CanDrag = true;
			PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card6> ().SetCardCoverOrFace ();
		}
	}

	public void AssignPlayerCardsAndActivateOthersCards (bool FirstOrSocend)
	{
		StartCoroutine (AssignPlayerCardsAndActivateOthersCardsCor (FirstOrSocend));
	}

	private IEnumerator AssignPlayerCardsAndActivateOthersCardsCor (bool FirstOrSocend)
	{
		int TargetedActiveCardsNO = 8;
		if (FirstOrSocend) {
			Cards = new PlayersCardsOB1 ();
			TargetedActiveCardsNO = 5;
		}
		CreateAndAssignMyCards ();
		for (int i = 0; i < TargetedActiveCardsNO; i++)
			for (int u = 1; u < 4; u++)
				PlayersCardsContentGO [u].transform.GetChild (i).gameObject.SetActive (true);
		yield return null;
	}

	private void CreateAndAssignMyCards ()
	{
		for (int i = 0; i < MyPlayerCardsGO.Length; i++)
			Destroy (MyPlayerCardsGO [i].gameObject);			
		MyPlayerCardsGO = new GameObject[8];
		for (int i = 0; i < 8; i++) {
			GameObject NewCard = Instantiate (CardGO, PlayersCardsContentGO [0].transform.position,
				Quaternion.identity, PlayersCardsContentGO [0].transform) as GameObject;
			NewCard.GetComponent<Card6> ().CanDrag = false;
			NewCard.GetComponent<Card6> ().IsCover = false;
			MyPlayerCardsGO [i] = NewCard;
		}
	}

	CardsInfos6 NewCardsInfo;
	public void SetCardsForPlayers (bool FirstOrSocend)
	{
		Debug.Log ("SetCardsForPlayers "+FirstOrSocend);
		int AddThisNoOfCards = 3;
		if (FirstOrSocend) {
			NewCardsInfo = new CardsInfos6 ();
			AddThisNoOfCards = 5;
		}
		for (int u = 0; u < 4; u++) {
			for (int i = 0; i < AddThisNoOfCards; i++) {
				if (DontAddCardBecouseHeGotTheExposedOne (u, i, AddThisNoOfCards))
					continue;
				int RandomNO = Random.Range (0, NewCardsInfo.HalfOfCards.Count);
				Cards.PlayersCards [u].PlayerCardsList.Add (NewCardsInfo.HalfOfCards [RandomNO]);
				NewCardsInfo.HalfOfCards.RemoveAt (RandomNO);
			}
		}
		if (FirstOrSocend) {
			int RandomNO = Random.Range (0, NewCardsInfo.HalfOfCards.Count);
			PlayersManager6.Instance.TempCurrenRoom.GameOB.BalootDetailsOB.ExposedCard = NewCardsInfo.HalfOfCards [RandomNO];
			NewCardsInfo.HalfOfCards.RemoveAt (RandomNO);
		}
	}

	bool DontAddCardBecouseHeGotTheExposedOne (int PlayerID,int IteratorNO,int AddThisNoOfCards)
	{
		if (PlayerID == PlayersManager6.Instance.TempCurrenRoom.GameOB.BalootDetailsOB.PlayerWhoTakesExposedCard
		    && AddThisNoOfCards == 3 && IteratorNO == 2) {
			Cards.PlayersCards [PlayerID].PlayerCardsList.Add (PlayersManager6.Instance.TempCurrenRoom.GameOB.BalootDetailsOB.ExposedCard);
			NewCardsInfo.HalfOfCards.Remove (PlayersManager6.Instance.TempCurrenRoom.GameOB.BalootDetailsOB.ExposedCard);
			return true;
		}
		else
			return false;
	}

	private void OnDestroy ()
	{
		StopAllCoroutines ();
	}
}