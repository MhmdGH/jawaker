using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardsInfos6{

	public string[] CardTypes,CardNumpers;
	public List<string> HalfOfCards;
	public CardsInfos6(){
		CardTypes = new string[4]{ "D","H","S","C"};
		CardNumpers = new string[]{"07","08","09","10","11","12","13","14"};
		HalfOfCards = new List<string> ();
		for (int i = 0; i < 8; i++) {
			HalfOfCards.Add (CardTypes[0]+""+CardNumpers[i]);
			HalfOfCards.Add (CardTypes[1]+""+CardNumpers[i]);
			HalfOfCards.Add (CardTypes[2]+""+CardNumpers[i]);
			HalfOfCards.Add (CardTypes[3]+""+CardNumpers[i]);
		}
	}
}