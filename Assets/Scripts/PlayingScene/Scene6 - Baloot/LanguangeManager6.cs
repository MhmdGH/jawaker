﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public class LanguangeManager6 : LanguangeManagerFather {

	public Text[] Game6Txts,BiddingWindow1,BiddingWindow2;

	public override void Start(){
		base.Start ();
		try{
		if(IsArabic)
				SetTheFukinArabicLang ();}catch{Debug.Log ("////////////////////////////////");
		}
	}

	public override void SetTheFukinArabicLang ()
	{
		base.SetTheFukinArabicLang ();
		SetFontForThisTxts (Game6Txts);
		Game6Txts [0].text = ArabicFixer.Fix ("دبل الجولة");
		Game6Txts [1].text = ArabicFixer.Fix ("دبل");
		Game6Txts [2].text = ArabicFixer.Fix ("باص");
		Game6Txts [3].text = ArabicFixer.Fix ("التدبيل الحالي");
		Game6Txts [4].text = ArabicFixer.Fix ("اختر الطرنيب");
		Game6Txts [5].text = ArabicFixer.Fix ("تم");
		Game6Txts [6].text = ArabicFixer.Fix ("سيرا");
		BiddingWindow1 [0].text = ArabicFixer.Fix ("صن");
		BiddingWindow1 [1].text = ArabicFixer.Fix ("حكم");
		BiddingWindow1 [2].text = ArabicFixer.Fix ("تأكيد الحكم");
		BiddingWindow1 [3].text = ArabicFixer.Fix ("باص");
		BiddingWindow1 [4].text = ArabicFixer.Fix ("أشكال");

		BiddingWindow2 [0].text = ArabicFixer.Fix ("صن");
		BiddingWindow2 [1].text = ArabicFixer.Fix ("حكم");
		BiddingWindow2 [2].text = ArabicFixer.Fix ("تأكيد الحكم");
		BiddingWindow2 [3].text = ArabicFixer.Fix ("باص");
		BiddingWindow2 [4].text = ArabicFixer.Fix ("أشكال");
	}

}
