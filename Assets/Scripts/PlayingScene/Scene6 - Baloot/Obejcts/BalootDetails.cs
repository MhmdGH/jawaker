﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class BalootDetails
{
	//GameType => 1 Sun, 2 Hokum,3 Ashkl
	public int PlayerWhoStartBids,BidsWinnerID,BidRound,LastBidderID,PlayerBidTurn,GameType,PlayerWhoTakesExposedCard
	,RoundDoubleLevel,OppisiteDoublerID;
	//Pass1 => Dash Call
	public bool[] SunChoosed,Hokum,HokumConfirmed,Wala,Bas,AshkalChoosed,IsCollectOnce;
	public bool Gahwa,Locked,BiddingEnds;
	public string TrumpType,ExposedCard;
	public string CurrentPopString;
	public BalootProjects Projects;

	public BalootDetails ()
	{
		TrumpType="NT";
		OppisiteDoublerID = 0;
		BiddingEnds = false;
		RoundDoubleLevel = 1;
		PlayerWhoTakesExposedCard = 0;
		LastBidderID = 0;
		PlayerBidTurn = 0;
		CurrentPopString = "";
		Projects = new BalootProjects ();
		BidRound = 1;
		BidsWinnerID = 0;
		PlayerWhoStartBids = 0;
		CurrentPopString="";
		ExposedCard = "";
		Gahwa = false;
		Locked= false;
		IsCollectOnce= new bool[]{ false, false};
		AshkalChoosed= new bool[]{ false, false, false, false };
		SunChoosed = new bool[]{ false, false, false, false };
		Hokum = new bool[]{ false, false, false, false };
		HokumConfirmed = new bool[]{ false, false, false, false };
		Wala = new bool[]{ false, false, false, false };
		Bas = new bool[]{ false, false, false, false };
	}
}