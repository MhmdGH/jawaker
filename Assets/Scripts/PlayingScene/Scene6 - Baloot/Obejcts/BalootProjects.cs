using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class BalootProjects
{
	public int[] Sira, Fifty, Hundred, FourHundred, Bloot;
	public StringList[] PlayersProjectsGroup1,PlayersProjectsGroup2;
	public int[] SubmitedProject1, SubmitedProject2;

	public BalootProjects(){
		PlayersProjectsGroup1 = new StringList[4];
		PlayersProjectsGroup2 = new StringList[4];
		for (int i = 0; i < PlayersProjectsGroup1.Length; i++) {
			PlayersProjectsGroup1 [i] = new StringList();
			PlayersProjectsGroup2 [i] = new StringList();
		}
		SubmitedProject1 = new int[4]{0,0,0,0};
		SubmitedProject2 = new int[4]{0,0,0,0};
		Sira = new int[4]{0,0,0,0};
		Fifty = new int[4]{0,0,0,0};
		Hundred = new int[4]{0,0,0,0};
		FourHundred = new int[4]{0,0,0,0};
		Bloot = new int[4]{0,0,0,0};
	}
}
