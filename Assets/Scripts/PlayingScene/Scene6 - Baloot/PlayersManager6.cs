﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class PlayersManager6 : PlayersManagerParent
{
	public static PlayersManager6 Instance;
	public GameObject CardGO;
	private List<GameObject> ThrowdCardsList;

	public override void Awake ()
	{
		base.Awake ();
		Instance = this;
	}

	public override void Start ()
	{
		CardsListGO = PlayersCardsGO [0].transform.GetChild (1).GetChild (0).gameObject;
		TempCurrenRoom = new JsonRoomO ("Default", new string[4]{ "0", "0", "0", "0" }, new Game6 ());
		base.Start ();
		ThrowdCardsList = new List<GameObject> ();
		CardsListGO = PlayersCardsGO [0].transform.GetChild (1).GetChild (0).gameObject;
		SceneNotReady = false;
	}

	//Main Reciever Fun
	public override void UpdateRoom (JsonRoomO CurrentRoomJSON)
	{
		base.UpdateRoom (CurrentRoomJSON);
		
		if (TempCurrenRoom.NoMaster)
			AssignNewMasterOrRemoveRoom ();
		if (TempCurrenRoom.Update == 0) {
			PlayingFireBaseManager6.Instance.SetOnDissconnectHandle ();
			return;
		}
		PlayingWindowsManager6.Instance.StopTimer ();
		switch (TempCurrenRoom.ActionID) {
		case -1:
			UpdateRoomPhaseMinus1 ();
			break;
		case 0:
			UpdateRoomPhase0 ();
			break;
		case 2:
			UpdateRoomPhase2 ();
			break;
		case 3:
			UpdateRoomPhase3 ();
			break;
		case 4:
			UpdateRoomPhase4 ();
			break;
		case 5:
			UpdateRoomPhase5 ();
			break;
		case 6:
			UpdateRoomPhase6 ();
			break;
		case 7:
			UpdateRoomPhase7 ();
			break;
		case 8:
			UpdateRoomPhase8 ();
			break;
		case 9:
			UpdateRoomPhase9 ();
			break;
		case 10:
			UpdateRoomPhase10 ();
			break;
		case 12:
			UpdateRoomPhase12 ();
			break;
		case 13:
			UpdateRoomPhase13 ();
			break;
		case 14:
			UpdateRoomPhase14 ();
			break;
		case 15:
			UpdateRoomPhase15 ();
			break;
		}
	}

	void UpdateRoomPhaseMinus1 ()
	{
		AudioManager.Instance.PlaySound (0);
		if (!Player.Instance.GameCreator)
			return;
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++)
			TempCurrenRoom.PlayersReady [i] = false;
		BalootManager6.Instance.SetCardsForPlayers (true);
		TempCurrenRoom.ActionID = 4;
		TempCurrenRoom.Available = false;
		for (int i = 0; i < 4; i++)
			TempCurrenRoom.GameOB.Cards.PlayersCards [i].PlayerCardsList = BalootManager6.Instance.Cards.PlayersCards [i].PlayerCardsList;
		PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase0 ()
	{
		if (TempCurrenRoom.ReloadScene)
			ReloadScene ();
	}

	void ReloadScene ()
	{
		TempCurrenRoom.ReloadScene = false;
		TempCurrenRoom.ActionID = 3;
		SceneNotReady = true;
		if (Player.Instance.GameCreator)
			PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}

	public void MasterTakeControl (int funToDOAfterEnd)
	{
		if (funToDOAfterEnd == 1)
			AutoBid ();
		else if (funToDOAfterEnd == 2)
			AutoPassDouble ();
		else if (funToDOAfterEnd == 3)
			MasterOrPlayerThrowCardNOW ();
		else if (funToDOAfterEnd == 4)
			AutoChooseTrump ();
	}

	void AutoChooseTrump ()
	{
		Debug.Log ("AutoPassDouble");
		//Player IS Here
		int PlayerID = Player.Instance.OnlinePlayerID;
		//Player Is Here
		if (IsThisMyBidTurn () && TempCurrenRoom.SeatsStatus [PlayerID] == 1)
			TrumSelected (SelectAvailableTrump ());
		else if (Player.Instance.GameCreator && TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn] == 0)
			TrumSelected (SelectAvailableTrump ());
	}

	public void UnChooseAllOtherCards ()
	{
		for (int i = 0; i < BalootManager6.Instance.MyPlayerCardsGO.Length; i++)
			try{BalootManager6.Instance.MyPlayerCardsGO[i].GetComponent<Card6> ().OnEndDragFun (true);}catch{
		}
	}

	string SelectAvailableTrump ()
	{
		string ExposedCardType = GetChar (TempCurrenRoom.GameOB.BalootDetailsOB.ExposedCard, 0);
		if (ExposedCardType == "D")
			return "H";
		else if (ExposedCardType == "H")
			return "C";
		else if (ExposedCardType == "C")
			return "D";
		else//S
			return "H";
	}

	void AutoPassDouble ()
	{
		Debug.Log ("AutoPassDouble");
		//Player IS Here
		int PlayerID = Player.Instance.OnlinePlayerID;
		//Player Is Here
		if (IsThisMyBidTurn () && TempCurrenRoom.SeatsStatus [PlayerID] == 1)
			PassDoubleBTN ();
		else if (Player.Instance.GameCreator && TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn] == 0)
			DoubleBTN ();
	}

	void AutoBid ()
	{
		Debug.Log ("AutoBid");
		int PlayerID = Player.Instance.OnlinePlayerID;
		//Player Is Here
		if (IsThisMyBidTurn () && TempCurrenRoom.SeatsStatus [PlayerID] == 1) {
			{
				if (TempCurrenRoom.GameOB.BalootDetailsOB.Hokum [TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn])
					ConfirmHokumBTN (TempCurrenRoom.ActionID == 5);
				else
					AutoPassNow ();}
		} else if (Player.Instance.GameCreator && TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn] == 0) {
			if (TempCurrenRoom.GameOB.BalootDetailsOB.Hokum [TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn])
				ConfirmHokumBTN (TempCurrenRoom.ActionID == 5);
			else
				AutoPassNow ();
		}
	}

	void AutoPassNow ()
	{
		if (TempCurrenRoom.ActionID == 5)
			BasBTN (true);
		else
			BasBTN (false);
	}

	void MasterOrPlayerThrowCardNOW ()
	{
		int PlayerSeatStatus = TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay];
		//If Player Is Here
		if (MyTurnToPlay () && PlayerSeatStatus == 1) {
			if (TempCurrenRoom.GameOB.CurrentRound.RoundNO == 0)
				DoneBTN ();
			if (PlayersManagerParent.PlayerHoldingCard) {
				GameObject CardHolded = CanvasGO.GetComponent<RectTransform> ().GetChild (CanvasGO.GetComponent<RectTransform> ().childCount - 1).gameObject;
				CardHolded.GetComponent<Card6> ().ReturnCardBack ();
				CardHolded.GetComponent<Card6> ().SendCard ();
				CheckPlayerKick ();
				return;
			} else {
				IfPlayerHoldingCardGetItBack ();
				List<GameObject> AvailbeCardsToThrow = new List<GameObject> ();
				for (int i = 0; i < CardsListGO.transform.childCount; i++)
					if (CardsListGO.transform.GetChild (i).GetComponent<Image> ().color.r == 1)
						AvailbeCardsToThrow.Add (CardsListGO.transform.GetChild (i).gameObject);
				AvailbeCardsToThrow[GetBestCardFromThisList(AvailbeCardsToThrow)].GetComponent<Card> ().SendCard ();
				CheckPlayerKick ();
				return;
			}
			//If Player Not Here, Master Do IT
		} else if (Player.Instance.GameCreator && PlayerSeatStatus == 0) {
			TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay] = 0;
			ThrowCardNow (FirstAvailabeCardOnOtherPlayer ());
		}
	}

	int GetBestCardFromThisList (List<GameObject> availbeCardsToThrow)
	{
		List<string> availbeCardsToThrowIDsStrings = new List<string> ();
		for (int i = 0; i < availbeCardsToThrow.Count; i++)
			availbeCardsToThrowIDsStrings.Add (availbeCardsToThrow[i].GetComponent<Card>().CardID);
		return GetHighestOrSmallestCardIFromThisList (availbeCardsToThrowIDsStrings,false);
	}

	private string PlayedType(){
		return GetChar (TempCurrenRoom.GameOB.CurrentRound.
			PlayersCards [TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound],0);
	}

	string FirstAvailabeCardOnOtherPlayer ()
	{

		List<string> ThisPlayerCardsList = new List<string> ();
		ThisPlayerCardsList = TempCurrenRoom.GameOB.
			Cards.PlayersCards [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay].PlayerCardsList;
		//ThisIsThePlayerWhoStartsTheRound
		if (ThisIsThePlayerWhoStartsTheRound())
			return ThisPlayerCardsList[GetHighestOrSmallestCardIFromThisList (ThisPlayerCardsList,true)];

		List<string> availbeCardsToThrowIDsStrings = new List<string> ();
		if (ThisPlayerHaveThisCardType (ThisPlayerCardsList, PlayedType ())) {
			for (int i = 0; i < ThisPlayerCardsList.Count; i++) {
				if (ThisPlayerCardsList [i].StartsWith (PlayedType ()))
					availbeCardsToThrowIDsStrings.Add (ThisPlayerCardsList [i]);
				else
					availbeCardsToThrowIDsStrings.Add ("0");
			}
		} else
			availbeCardsToThrowIDsStrings = ThisPlayerCardsList;

		return ThisPlayerCardsList [GetHighestOrSmallestCardIFromThisList (ThisPlayerCardsList, false)];

		//if this Player STart the round
		if (TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound == TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay) {
			if (TempCurrenRoom.GameOB.BalootDetailsOB.Locked && IHaveCardsOtherThanThisType (TempCurrenRoom.GameOB.BalootDetailsOB.TrumpType)) {
				for (int i = 0; i < ThisPlayerCardsList.Count; i++) {
					if (ThisPlayerCardsList [i].Length > 2 && !ThisPlayerCardsList [i].StartsWith (TempCurrenRoom.GameOB.BalootDetailsOB.TrumpType))
						return ThisPlayerCardsList [i];
				}
			} else {
				for (int i = 0; i < ThisPlayerCardsList.Count; i++) {
					if (ThisPlayerCardsList [i].Length > 2)
						return ThisPlayerCardsList [i];
				}
			}
		}


		for (int i = 0; i < ThisPlayerCardsList.Count; i++)
			if (ThisPlayerCardsList [i].StartsWith (PlayedType()))
				return ThisPlayerCardsList [i];
		for (int i = 0; i < ThisPlayerCardsList.Count; i++)
			if (!ThisPlayerCardsList [i].StartsWith ("0"))
				return ThisPlayerCardsList [i];
		//Not Reachable
		return "0";
	}

	public void RestartGame ()
	{
		PlayingWindowsManager6.Instance.WinsWindow.SetActive (false);
		Debug.Log ("RestartGame");
		Game6 NewGameOB = new Game6 ();
		for (int i = 0; i < NewGameOB.PlayersScores.Length; i++)
			NewGameOB.PlayersScores [i] = 0;
		TempCurrenRoom.GameOB = NewGameOB;
		TempCurrenRoom.ReloadScene = true;
		TempCurrenRoom.ActionID = 0;
		PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase15 ()
	{
		int Team1Score = TeamScore (true);
		int Team2Score = TeamScore (false);
		int Winner = 0;
		if (Team2Score > Team1Score)
			Winner = 1;
		AddWinsPointsForPlayers (Winner);
		WinnerNo = Winner;
		if (MainFireBaseManager.Instance.IsACompition)
			InnerSetCompData ();
		PlayingWindowsManager6.Instance.ShowWindWindow (Winner);
	}
	private int WinnerNo;
	private void InnerSetCompData ()
	{
		if (Player.Instance.GameCreator) {
			for (int i = 0; i < TempCurrenRoom.DatabasePlayersIDs.Length; i++)
				if (TempCurrenRoom.DatabasePlayersIDs [i].Length < 4)
					return;
			if (WinnerNo == 0) {
				SetCompData (TempCurrenRoom.DatabasePlayersIDs [0], TempCurrenRoom.DatabasePlayersIDs [2],
					TempCurrenRoom.PlayersFBIDs [0], TempCurrenRoom.PlayersFBIDs [2]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [1]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [3]);
			} else {
				SetCompData (TempCurrenRoom.DatabasePlayersIDs [1], TempCurrenRoom.DatabasePlayersIDs [3],
					TempCurrenRoom.PlayersFBIDs [1], TempCurrenRoom.PlayersFBIDs [3]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [0]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [2]);
			}
		}
	}

	private void AddWinsPointsForPlayers(int WinnerNO){
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (WinnerNO == 0 && (PlayerID == 0 || PlayerID == 2)) {
			Player.Instance.AddXPsAndCLubPoints (XPsValue);
			Player.Instance.Coins += ScoreValue;
		}else if (WinnerNO == 1 && (PlayerID == 1 || PlayerID == 3)) {
			Player.Instance.AddXPsAndCLubPoints (XPsValue);
			Player.Instance.Coins += ScoreValue;
		}
	}

	void UpdateRoomPhase14 ()
	{
		Debug.Log ("UpdateRoomPhase14");
	}

	private void NewRound ()
	{
		Game6 NewGameOB = new Game6 ();
		for (int i = 0; i < 4; i++)
			NewGameOB.PlayersScores [i] = TempCurrenRoom.GameOB.PlayersScores [i];
		TempCurrenRoom.GameOB = NewGameOB;
		TempCurrenRoom.GameOB.BalootDetailsOB.PlayerWhoStartBids = NextNO (TempCurrenRoom.GameOB.BalootDetailsOB.PlayerWhoStartBids);
		TempCurrenRoom.ReloadScene = true;
		TempCurrenRoom.ActionID = 0;
		PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase13 ()
	{
		Debug.Log ("UpdateRoomPhase13");
		//Full Round Ends
		for (int i = 0; i < 4; i++)
			PlayingWindowsManager6.Instance.PlayersScoresGO [i].GetComponent<Text> ().text = "0";

		if (!Player.Instance.GameCreator)
			return;
		SetPlayersScores ();

		if (FullGameEnds ()) {
			TempCurrenRoom.ActionID = 15;
			PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
		} else
			NewRound ();
	}

	bool FullGameEnds ()
	{
		Debug.Log (TeamScore (true) + "||" + TeamScore (false) + "||" + TempCurrenRoom.GameOB.BalootDetailsOB.Gahwa);
		if (TeamScore (true) >= 152 || TeamScore (false) >= 152 || TempCurrenRoom.GameOB.BalootDetailsOB.Gahwa)
			return false;
		else
			return false;
	}

	private int TeamScore (bool Team1)
	{
		int Team1Score = 0;
		int Team2Score = 0;
		Team1Score = TempCurrenRoom.GameOB.PlayersScores [0];
		Team2Score = TempCurrenRoom.GameOB.PlayersScores [1];
		if (Team1)
			return Team1Score;
		else
			return Team2Score;
	}

	private int OneTeamCollectsAll ()
	{
		if (!TempCurrenRoom.GameOB.BalootDetailsOB.IsCollectOnce [0])
			return 0;
		else if (!TempCurrenRoom.GameOB.BalootDetailsOB.IsCollectOnce [1])
			return 1;
		else
			return -1;
	}

	void SetPlayersScores ()
	{
		int[] ThisRoundTeamsScores = new int[2];
		ThisRoundTeamsScores [0] = 0;
		ThisRoundTeamsScores [1] = 0;
		if (OneTeamCollectsAll () != -1)
				ThisRoundTeamsScores [OneTeamCollectsAll ()] += 44;
		for (int i = 0; i < 4; i++) {
			int CurrentRoundPlayerScore = TempCurrenRoom.GameOB.CurrentRound.PlayersScores [i];
			if (OneTeamCollectsAll () == -1) {
				if (i == 0 || i == 2)
					ThisRoundTeamsScores [0] += PlayerCalculatedScore (i, CurrentRoundPlayerScore);
				else
					ThisRoundTeamsScores [1] += PlayerCalculatedScore (i, CurrentRoundPlayerScore);
			} else {
				if ((i == 0 || i == 2) && OneTeamCollectsAll () == 0)
					ThisRoundTeamsScores [0] += PlayerCalculatedScore (i, CurrentRoundPlayerScore);
				else if (OneTeamCollectsAll () == 1)
					ThisRoundTeamsScores [1] += PlayerCalculatedScore (i, CurrentRoundPlayerScore);
			}
		}

		int BidderWinner = TempCurrenRoom.GameOB.BalootDetailsOB.BidsWinnerID;
		if (BidderWinner == 0 || BidderWinner == 2) {//FirstTeam Bidding winner
			if (TempCurrenRoom.GameOB.BalootDetailsOB.GameType != 2) {//Sun
				if (ThisRoundTeamsScores [0] >= 13)//Team Wins
					BidderWinsOrLose (true, ThisRoundTeamsScores, 0, true);
				else//Team Lose
					BidderWinsOrLose (false, ThisRoundTeamsScores, 0, true);
			} else {//Hokum
				if (ThisRoundTeamsScores [0] >= 8)//Team Wins
					BidderWinsOrLose (true, ThisRoundTeamsScores, 0, false);
				else//Team Lose
					BidderWinsOrLose (false, ThisRoundTeamsScores, 0, false);
			}
		} else {
			if (TempCurrenRoom.GameOB.BalootDetailsOB.GameType != 2) {//Hokum
				if (ThisRoundTeamsScores [1] >= 13)//Team Wins
						BidderWinsOrLose (true, ThisRoundTeamsScores, 1, true);
				else//Team Lose
						BidderWinsOrLose (false, ThisRoundTeamsScores, 1, true);
			} else {//Hokum
				if (ThisRoundTeamsScores [1] >= 8)//Team Wins
						BidderWinsOrLose (true, ThisRoundTeamsScores, 1, false);
				else//Team Lose
						BidderWinsOrLose (false, ThisRoundTeamsScores, 1, false);
			}
		}
	}

	private void BidderWinsOrLose (bool WinsOrLose, int[] ThisRoundTeamsScores, int TeamsWhoWinBidding, bool IsSunGameType)
	{
		Debug.Log ("WinsOrLose" + WinsOrLose + "||" + "ThisRoundTeamsScores" + ThisRoundTeamsScores [0] + "||" + TeamsWhoWinBidding + "||" + IsSunGameType);
		if (WinsOrLose) {//Bidder winner Wins The round
			for (int i = 0; i < 4; i++) {
				if (TempCurrenRoom.GameOB.BalootDetailsOB.Gahwa) {
					if (i == 0 || i == 2)
						TempCurrenRoom.GameOB.PlayersScores [i] = ThisRoundTeamsScores [0];
					else
						TempCurrenRoom.GameOB.PlayersScores [i] = ThisRoundTeamsScores [1];
				} else {
					if (i == 0 || i == 2)
						TempCurrenRoom.GameOB.PlayersScores [i] += ThisRoundTeamsScores [0]*TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel;
					else
						TempCurrenRoom.GameOB.PlayersScores [i] += ThisRoundTeamsScores [1]*TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel;
				}
			}
		} else {//Bidder winner Lose
			if (TeamsWhoWinBidding == 0) {
				if (TempCurrenRoom.GameOB.BalootDetailsOB.Gahwa) {
					if (IsSunGameType) {
						TempCurrenRoom.GameOB.PlayersScores [1] = 26;
						TempCurrenRoom.GameOB.PlayersScores [3] = 26;
					} else {
						TempCurrenRoom.GameOB.PlayersScores [1] = 16;
						TempCurrenRoom.GameOB.PlayersScores [3] = 16;
					}
				} else {
					if (IsSunGameType) {
						TempCurrenRoom.GameOB.PlayersScores [1] += 26*TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel;
						TempCurrenRoom.GameOB.PlayersScores [3] += 26*TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel;
					} else {
						TempCurrenRoom.GameOB.PlayersScores [1] += 16*TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel;
						TempCurrenRoom.GameOB.PlayersScores [3] += 16*TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel;
					}
				}
			} else {
				if (TempCurrenRoom.GameOB.BalootDetailsOB.Gahwa) {
					if (IsSunGameType) {
						TempCurrenRoom.GameOB.PlayersScores [0] = 26;
						TempCurrenRoom.GameOB.PlayersScores [2] = 26;
					} else {
						TempCurrenRoom.GameOB.PlayersScores [0] = 16;
						TempCurrenRoom.GameOB.PlayersScores [2] = 16;
					}
				} else {
					if (IsSunGameType) {
						TempCurrenRoom.GameOB.PlayersScores [0] += 26*TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel;
						TempCurrenRoom.GameOB.PlayersScores [2] += 26*TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel;
					} else {
						TempCurrenRoom.GameOB.PlayersScores [0] += 16*TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel;
						TempCurrenRoom.GameOB.PlayersScores [2] += 16*TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel;
					}
				}
			}
		}
	}

	int PlayerCalculatedScore (int ThisPlayerID, int ThisPlayerScore)
	{
		if (ThisPlayerID == TempCurrenRoom.GameOB.CollectorID)
			ThisPlayerScore += 10;
		if (TempCurrenRoom.GameOB.BalootDetailsOB.GameType != 2) {//Sun Or Ashkal
			ThisPlayerScore += ProjectsScores (ThisPlayerID, true);
			if (ThisPlayerScore % 5 != 0)
				ThisPlayerScore = (int)Math.Round (ThisPlayerScore / 10.0f) * 10;
			return ThisPlayerScore;
		} else {//Hokum
			ThisPlayerScore += ProjectsScores (ThisPlayerID, false);
			float HokumValue = ThisPlayerScore / 10;
			if (HokumValue % 0.5f == 0)
				HokumValue = (int)HokumValue;
			else
				HokumValue = (int)Mathf.Round (ThisPlayerScore);
			ThisPlayerScore = (int)HokumValue;
		}
		return ThisPlayerScore;
	}

	int ProjectsScores (int thisPlayerID, bool IsSun)
	{
		int ThisProjectScores = 0;
		if (IsSun) {
			//Group1
			if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject1 [thisPlayerID] == 1)
				ThisProjectScores += 4;
			else if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject1 [thisPlayerID] == 2)
				ThisProjectScores += 10;
			else if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject1 [thisPlayerID] == 3)
				ThisProjectScores += 20;
			else if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject1 [thisPlayerID] == 4)
				ThisProjectScores += 40;
			//Group2
			if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject2 [thisPlayerID] == 1)
				ThisProjectScores += 4;
			else if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject2 [thisPlayerID] == 2)
				ThisProjectScores += 10;
			else if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject2 [thisPlayerID] == 3)
				ThisProjectScores += 20;
			else if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject2 [thisPlayerID] == 4)
				ThisProjectScores += 40;
		} else {
			//Group1
			if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject1 [thisPlayerID] == 1)
				ThisProjectScores += 2;
			else if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject1 [thisPlayerID] == 2)
				ThisProjectScores += 5;
			else if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject1 [thisPlayerID] == 3)
				ThisProjectScores += 10;
			else if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject1 [thisPlayerID] == 5)
				ThisProjectScores += 2;
			//Group2
			if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject1 [thisPlayerID] == 1)
				ThisProjectScores += 2;
			else if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject1 [thisPlayerID] == 2)
				ThisProjectScores += 5;
			else if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject1 [thisPlayerID] == 3)
				ThisProjectScores += 10;
			else if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject1 [thisPlayerID] == 5)
				ThisProjectScores += 2;
		}
		return ThisProjectScores;
	}

	void UpdateRoomPhase12 ()
	{
		//COllect Cards Here And Throw it to winner Player
		StartCoroutine (CollectionCor ());
	}

	private IEnumerator CollectionCor ()
	{
		ReturnsMyCardsAvailibity ();
		SetAvailableCardsOnce = false;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = -2;
		int LastWinnerID = TempCurrenRoom.GameOB.CollectorID;
		yield return new WaitForSeconds (1);
		CardsThrowerMangaer.Instance.CollectCardsToThisPlayer (FromOnlineToLocal (LastWinnerID));
		PlayingWindowsManager6.Instance.PlayersScoresGO [FromOnlineToLocal (LastWinnerID)].GetComponent<Text> ().text =
			TempCurrenRoom.GameOB.CurrentRound.PlayersScores [LastWinnerID] + "";
		yield return new WaitForSeconds (1);
		for (int i = 0; i < ThrowdCardsList.Count; i++)
			Destroy (ThrowdCardsList [i].gameObject);
		ThrowdCardsList.Clear ();
		if (Player.Instance.GameCreator) {
			if (FullRoundEnds ()) {
				TempCurrenRoom.ActionID = 13;
				PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
			} else {
				TempCurrenRoom.ActionID = 10;
				TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound = LastWinnerID;
				TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = LastWinnerID;
				PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
			}
		}
	}

	#region implemented abstract members of PlayersManagerParent

	public override void SetLastRoundCards ()
	{
		string[] LastRoundCards = new string[4];
		for (int i = 0; i < 4; i++)
			LastRoundCards [i] = TempCurrenRoom.GameOB.CurrentRound.PlayersCards [FromOnlineToLocal (i)];
		LastRoundGO.GetComponent<LastRound> ().SetCards (LastRoundCards);
	}

	#endregion

	bool FullRoundEnds ()
	{
		if (TempCurrenRoom.GameOB.CurrentRound.RoundNO >= 8)
			return true;
		else
			return false;
	}

	private void UpdateRoomPhase11 ()
	{
		//Just a fukin Holder
	}

	private bool ProjectWindowShowed, ProjectsCardsShowed;

	private void UpdateRoomPhase10 ()
	{
		if (!ProjectWindowShowed && TempCurrenRoom.GameOB.CurrentRound.RoundNO == 0) {
			ProjectWindowShowed = true;
			PlayingWindowsManager6.Instance.ProjectsWindowGO.SetActive (true);
		}
		if (!ProjectsCardsShowed && TempCurrenRoom.GameOB.CurrentRound.RoundNO == 1) {
			ProjectsCardsShowed = true;
			PlayingWindowsManager6.Instance.SetAndShowProjectCards (TempCurrenRoom.GameOB.BalootDetailsOB.Projects);
		}
		//GameStart
		if (MyTurnToPlay ()) {
			if (TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound == Player.Instance.OnlinePlayerID) {
				if (TempCurrenRoom.GameOB.BalootDetailsOB.Locked &&
				    IHaveCardsOtherThanThisType (TempCurrenRoom.GameOB.BalootDetailsOB.TrumpType))
					DeActivateThisCardsType (TempCurrenRoom.GameOB.BalootDetailsOB.TrumpType);	
			} else
				SetAvailableCards ();
		}

		if (MyTurnToPlay ()) {
			if (IsThisFuckerTrulyTurnToPlay()) {
				StartCoroutine (UpdateRoomPhase10Cor());
			} else {
				IfPlayerHoldingCardGetItBack ();
				BalootManager6.Instance.SetAndArrangeMyCards ();
			}
		}
		if (!RoundEnds ())
			PlayingWindowsManager6.Instance.SetTimer (FromOnlineToLocal (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay),
				TimerWaitingTime (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay), 3, false);
		int PrevPlayerID = PreviosPlayerID (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay);
		if (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [PrevPlayerID].Length < 2)
			return;
		ThrowThisCard (PrevPlayerID, TempCurrenRoom.GameOB.CurrentRound.PlayersCards [PrevPlayerID]);
	}

	private IEnumerator UpdateRoomPhase10Cor(){
		yield return new WaitForSeconds (1);
		try{GameObject CardHolded = CanvasGO.GetComponent<RectTransform> ().GetChild (CanvasGO.GetComponent<RectTransform> ().childCount - 1).gameObject;
			CardHolded.GetComponent<Card6> ().ReturnCardBack ();
			CardHolded.GetComponent<Card6> ().SendCard ();}catch{
		}
	}

	private bool SetAvailableCardsOnce;

	void DeActivateThisCardsType (string trumpType)
	{
		for (int i = 0; i < CardsListGO.transform.childCount; i++)
			if (CardsListGO.transform.GetChild (i).GetComponent<Card6> ().CardID.StartsWith (trumpType))
				CardAvailability (CardsListGO.transform.GetChild (i).gameObject, false);
	}

	private bool DoneBTNClicked;

	public void DoneBTN ()
	{
		CanThrowCard = true;
		int PlayerID = Player.Instance.OnlinePlayerID;
		PlayingWindowsManager6.Instance.ProjectsWindowGO.SetActive (false);
		CheckBaloot ();//Check Baloot
		if (ProjectTotalValue > 0 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Sira [PlayerID] > 0)
			CheckContinuesCards (3, 1);//Check Sira
		if (ProjectTotalValue > 0 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Fifty [PlayerID] > 0)
			CheckContinuesCards (4, 2);//Check 50
		if (ProjectTotalValue > 0 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Hundred [PlayerID] > 0)
			CheckContinuesCards (5, 3);//Check 100
		if (ProjectTotalValue > 0 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Hundred [PlayerID] > 0)
			Check4SameType (false, 3);//Check 100 too
		if (ProjectTotalValue > 0 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.FourHundred [PlayerID] > 0)
			Check4SameType (true, 4);//Check 400

		if (ProjectTotalValue > 0 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Sira [PlayerID] > 0)
			CheckContinuesCards (3, 1);//Check Sira
		if (ProjectTotalValue > 0 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Fifty [PlayerID] > 0)
			CheckContinuesCards (4, 2);//Check 50
		if (ProjectTotalValue > 0 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Hundred [PlayerID] > 0)
			CheckContinuesCards (5, 3);//Check 100
		if (ProjectTotalValue > 0 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Hundred [PlayerID] > 0)
			Check4SameType (false, 3);//Check 100 too
		if (ProjectTotalValue > 0 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.FourHundred [PlayerID] > 0)
			Check4SameType (true, 4);//Check 400
	}

	public bool AreNotTheSameSuite (string FirstCardID, string SocendCardID)
	{
		if (GetChar (FirstCardID, 0) != GetChar (SocendCardID, 0))
			return true;
		else
			return false;
	}

	void CheckBaloot ()
	{
		List<string> ThisMyCards = new List<string> (4);
		ThisMyCards.Add ("0");
		ThisMyCards.Add ("0");
		string TrumpType = TempCurrenRoom.GameOB.BalootDetailsOB.TrumpType;
		if (TempCurrenRoom.GameOB.BalootDetailsOB.GameType != 2)
			return;
		for (int i = 0; i < CardsListGO.transform.childCount; i++) {
			ThisMyCards [0] = CardsListGO.transform.GetChild (i).GetComponent<Card> ().CardID;
			if (ThisCardIsInAnotherProject (ThisMyCards [0]))
				continue;
			if (GetCardNO (ThisMyCards [0]) == 12 && GetChar (ThisMyCards [0], 0) == TrumpType) {
				for (int u = 0; u < CardsListGO.transform.childCount; u++) {
					ThisMyCards [1] = CardsListGO.transform.GetChild (u).GetComponent<Card> ().CardID;
					if (GetCardNO (ThisMyCards [1]) == 13 && GetChar (ThisMyCards [1], 0) == TrumpType) {
						List<string> MeldList = new List<string> ();
						MeldList.Add (ThisMyCards [0]);
						MeldList.Add (ThisMyCards [1]);
						AddThis (MeldList, 5);
						return;
					}
				}
			}
		}
	}

	bool ThisCardIsInAnotherProject (string CardID)
	{
		List<string> PlayerCardList1 = new List<string> ();
		List<string> PlayerCardList2 = new List<string> ();
		if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.PlayersProjectsGroup1.Length > Player.Instance.OnlinePlayerID) {
			PlayerCardList1 = TempCurrenRoom.GameOB.BalootDetailsOB.Projects.PlayersProjectsGroup1 [Player.Instance.OnlinePlayerID].CardList;
			for (int u = 0; u < PlayerCardList1.Count; u++)
				if (CardID == PlayerCardList1 [u])
					return true;
		}
		if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.PlayersProjectsGroup2.Length > Player.Instance.OnlinePlayerID) {
			PlayerCardList2 = TempCurrenRoom.GameOB.BalootDetailsOB.Projects.PlayersProjectsGroup2 [Player.Instance.OnlinePlayerID].CardList;
			for (int u = 0; u < PlayerCardList2.Count; u++)
				if (CardID == PlayerCardList2 [u])
					return true;
		}
		return false;
	}

	void Check4SameType (bool Check400, int ProjectType)
	{
		List<string> MyCards = new List<string> (4);
		MyCards.Add ("0");
		MyCards.Add ("0");
		MyCards.Add ("0");
		MyCards.Add ("0");
		if (!Check400 && TempCurrenRoom.GameOB.BalootDetailsOB.GameType != 2)//Just Hokum
			return;
		if (Check400 && TempCurrenRoom.GameOB.BalootDetailsOB.GameType == 2)
			return;
		for (int i = 0; i < CardsListGO.transform.childCount; i++) {
			if (i > CardsListGO.transform.childCount - 4)
				break;
			MyCards [0] = CardsListGO.transform.GetChild (i).GetComponent<Card> ().CardID;
			if (ThisCardIsInAnotherProject (MyCards [0]))
				continue;
			if (GetCardNO (MyCards [0]) < 10)//Accept Higher Thank 10
				continue;
			if (Check400 && GetCardNO (MyCards [0]) != 14)
				continue;
			for (int u = i + 1; u < CardsListGO.transform.childCount; u++) {
				MyCards [1] = CardsListGO.transform.GetChild (u).GetComponent<Card> ().CardID;
				if (AreNotTheSameSuite (MyCards [0], MyCards [1])) {//2 Cards are not have same suits
					if (AreEqual (MyCards [0], MyCards [1])) {//Tow Cards Equal
						for (int y = u + 1; y < CardsListGO.transform.childCount; y++) {
							MyCards [2] = CardsListGO.transform.GetChild (y).GetComponent<Card> ().CardID;
							if (AreNotTheSameSuite (MyCards [1], MyCards [2]) && AreNotTheSameSuite (MyCards [0], MyCards [2])) {//2 Cards are not have same suits
								if (AreEqual (MyCards [1], MyCards [2])) {//Tow Cards Equals
									if (!AreEqual (MyCards [0], MyCards [2]))
										continue;
									for (int t = y + 1; t < CardsListGO.transform.childCount; t++) {
										MyCards [3] = CardsListGO.transform.GetChild (t).GetComponent<Card> ().CardID;
										if (AreNotTheSameSuite (MyCards [2], MyCards [3]) && AreNotTheSameSuite (MyCards [1], MyCards [3]) && AreNotTheSameSuite (MyCards [0], MyCards [3])) {//2 Cards are not have same suits
											if (AreEqual (MyCards [2], MyCards [3])) {
												if (!AreEqual (MyCards [0], MyCards [3]) || !AreEqual (MyCards [1], MyCards [3]))
													continue;
												List<string> MeldList = new List<string> ();
												for (int r = 0; r < 4; r++)
													MeldList.Add (MyCards [r]);
												AddThis (MeldList, ProjectType);
												return;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public void CheckContinuesCards (int ContinuesLong, int ProjectType)
	{
		//		Debug.Log ("ProjectTotalValue "+ProjectTotalValue);
		List<string> MyCards = new List<string> (ContinuesLong);
		for (int i = 0; i < CardsListGO.transform.childCount; i++) {
			if (i > CardsListGO.transform.childCount - ContinuesLong)
				break;
			if (i > 0)
				MyCards.Clear ();
			MyCards.Add (CardsListGO.transform.GetChild (i).GetComponent<Card> ().CardID);
			MyCards.Add (CardsListGO.transform.GetChild (i + 1).GetComponent<Card> ().CardID);
			MyCards.Add (CardsListGO.transform.GetChild (i + 2).GetComponent<Card> ().CardID);
			if (ThisCardIsInAnotherProject (MyCards [0]))
				continue;
			if (ContinuesLong > 3)
				MyCards.Add (CardsListGO.transform.GetChild (i + 3).GetComponent<Card> ().CardID);
			if (ContinuesLong > 4)
				MyCards.Add (CardsListGO.transform.GetChild (i + 4).GetComponent<Card> ().CardID);
			if (AreTheSameSuite (MyCards [0], MyCards [1])) {
				//2 Cards are have same suits
				if (AreInARow (MyCards [0], MyCards [1])) {
					//Tow Cards in a row
					if (AreTheSameSuite (MyCards [1], MyCards [2])) {
						//2 Cards are have same suits
						if (AreInARow (MyCards [1], MyCards [2]) && //Socend And Third Card in a row&& (0 card &&2 card are in a row//
						    ((GetCardNO (MyCards [0]) == (GetCardNO (MyCards [2])) - 2))) {
							//Three Cards in a row
							List<string> MeldList = new List<string> ();
							for (int u = 0; u < 3; u++)
								MeldList.Add (MyCards [u]);
							if (ContinuesLong > 3 && MyCards.Count > 3 && MyCards [3].Length > 1) {
								if (AreTheSameSuite (MyCards [2], MyCards [3])) {
									if (AreInARow (MyCards [2], MyCards [3]) &&
									    ((GetCardNO (MyCards [1]) == GetCardNO (MyCards [3]) - 2))) {
										MeldList.Add (MyCards [3]);
										if (ContinuesLong > 4 && MyCards.Count > 4 && MyCards [4].Length > 1) {
											if (AreTheSameSuite (MyCards [3], MyCards [4])) {
												if (AreInARow (MyCards [3], MyCards [4]) &&
												    ((GetCardNO (MyCards [2]) == GetCardNO (MyCards [4]) - 2))) {
													MeldList.Add (MyCards [4]);
												}
											}
										}
									}
								}
							}
							AddThis (MeldList, ProjectType);
							return;
						}
					}
				}
			}
		}
	}

	void AddThis (List<string> MeldList, int ProjectType)
	{
		if (ProjectTotalValue <= 0)
			return;
		if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.
			PlayersProjectsGroup1 [Player.Instance.OnlinePlayerID].CardList.Count == 1) {
			//Here is an empty list
			TempCurrenRoom.GameOB.BalootDetailsOB.Projects.
			PlayersProjectsGroup1 [Player.Instance.OnlinePlayerID].CardList = MeldList;
			SubmitProject (ProjectType);
			if (ProjectType == 0 && ProjectTotalValue == 2)
				ProjectTotalValue -= 1;
		} else if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.
			PlayersProjectsGroup2 [Player.Instance.OnlinePlayerID].CardList.Count == 1) {
			TempCurrenRoom.GameOB.BalootDetailsOB.Projects.
			PlayersProjectsGroup2 [Player.Instance.OnlinePlayerID].CardList = MeldList;
			SubmitProject (ProjectType);
			ProjectTotalValue -= 1;
		}
	}

	void SubmitProject (int projectType)
	{
		Debug.Log ("SubmitProject " + projectType);
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject1 [PlayerID] == 0)
			TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject1 [PlayerID] = projectType;
		else
			TempCurrenRoom.GameOB.BalootDetailsOB.Projects.SubmitedProject2 [PlayerID] = projectType;
		if (projectType == 1)
			TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Sira [PlayerID] -= 1;
		else if (projectType == 2)
			TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Fifty [PlayerID] -= 1;
		else if (projectType == 3)
			TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Hundred [PlayerID] -= 1;
		else if (projectType == 4)
			TempCurrenRoom.GameOB.BalootDetailsOB.Projects.FourHundred [PlayerID] -= 1;
	}

	public bool AreTheSameSuite (string FirstCardID, string SocendCardID)
	{
		if (GetChar (FirstCardID, 0) == GetChar (SocendCardID, 0))
			return true;
		else
			return false;
	}

	public bool AreEqual (string FirstCardID, string SocendCardID)
	{
		int FirstCardIDNO = GetCardNO (FirstCardID);
		int SocendCardIDNO = GetCardNO (SocendCardID);
		if (FirstCardIDNO == SocendCardIDNO)
			return true;
		else
			return false;
	}

	public bool AreInARow (string FirstCardID, string SocendCardID)
	{
		int FirstCardIDNO = GetCardNO (FirstCardID);
		int SocendCardIDNO = GetCardNO (SocendCardID);
		if (FirstCardIDNO == SocendCardIDNO - 1)
			return true;
		return false;
	}

	void SetAvailableCards ()
	{
		//Watcher
		if (Player.Instance.OnlinePlayerID == -1)
			return;
		if (SetAvailableCardsOnce)
			return;
		List<string> PlayedCards = new List<string> ();
		IfPlayerHoldingCardGetItBack ();
		int ThrowdCardsCounter = 0;
		for (int i = 0; i < 4; i++)
			PlayedCards.Add (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i]);
		for (int i = 0; i < 4; i++) {
			if (PlayedCards [i].Length > 1) {
				ThrowdCardsCounter++;
			}
		}
		if (ThrowdCardsCounter == 0)
			return;
		string CardType = GetChar (PlayedCards [TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound], 0);
		if (IHaveThisCardType (CardType)) {
			for (int i = 0; i < CardsListGO.transform.childCount; i++) {
				if (!CardsListGO.transform.GetChild (i).GetComponent<Card6> ().CardID.StartsWith (CardType)) {
					CardAvailability (CardsListGO.transform.GetChild (i).gameObject, false);
					SetAvailableCardsOnce = true;
				}
			}
		}
	}

	private bool IHaveCardsOtherThanThisType (string CardType)
	{
		List<string> ThisCardsList = new List<string> ();
		ThisCardsList = TempCurrenRoom.GameOB.Cards.PlayersCards [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay].PlayerCardsList;
		for (int i = 0; i < ThisCardsList.Count; i++)
			if (ThisCardsList [i].Length > 2 && !ThisCardsList [i].StartsWith (CardType))
				return true;
		return false;
	}


	bool IHaveThisCardType (string Cardtype)
	{
		for (int i = 0; i < CardsListGO.transform.childCount; i++) {
			if (CardsListGO.transform.GetChild (i).GetComponent<Card6> ().CardID.StartsWith (Cardtype))
				return true;
		}
		return false;
	}

	void ThrowThisCard (int PlayerThrowerID, string ThrowdCardID)
	{
		int PlayerIDOnMyDevice = FromOnlineToLocal (PlayerThrowerID);
		CardsThrowerMangaer.Instance.ThrowThisCard (ThrowdCardID, PlayerIDOnMyDevice);
		if (PlayerIDOnMyDevice != 0 || Player.Instance.OnlinePlayerID == -1)
			PlayersCardsGO [PlayerIDOnMyDevice].transform.GetChild (1).GetChild (0).
			GetChild (TempCurrenRoom.GameOB.CurrentRound.RoundNO).gameObject.SetActive (false);
		if (RoundEnds ()) {
			SetLastRoundCards ();
			if (Player.Instance.GameCreator)
				CloseRound ();
		}
	}

	public void ThrowCard (GameObject Cardjo)
	{
		if (MyTurnToPlay () && TempCurrenRoom.ActionID == 10 && CanThrowCard)
			StartCoroutine (RemoveOrderly (Cardjo));
		else
			Cardjo.GetComponent<Card6> ().ReturnCardBack ();
	}

	private bool CanThrowCard = false;

	private IEnumerator RemoveOrderly (GameObject Cardjo)
	{
		CanThrowCard = false;
		ThrowCardNow (Cardjo.GetComponent<Card6> ().CardID);
		Destroy (Cardjo);
		yield return new WaitForSeconds (0.03f);try{
			BalootManager6.Instance.SetAndArrangeMyCards ();}catch{
		}
		yield return new WaitForSeconds (2f);
		CanThrowCard = true;
		yield return null;
	}

	void ThrowCardNow (string CardID)
	{
		string CardIDString = CardID;
		int PlayerTurn = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		TempCurrenRoom.GameOB.CurrentRound.PlayersCards [PlayerTurn] = CardIDString;
		RemoveThisCardFromOnlineCardsList (PlayerTurn, CardIDString);
		int NextPlayerID = PlayerTurn;
		NextPlayerID++;
		if (NextPlayerID >= 4)
			NextPlayerID = 0;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = NextPlayerID;
		PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private void RemoveThisCardFromOnlineCardsList (int PlayerNO, string CardID)
	{
		for (int i = 0; i < TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList.Count; i++) {
			if (TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList [i] == CardID) {
				TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList [i] = "0";
				return;
			}
		}
	}

	private bool RoundEnds ()
	{
		int Over1Counter = 0;
		for (int i = 0; i < 4; i++) {
			if (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i].Length > 1)
				Over1Counter++;
		}
		if (Over1Counter == 4)
			return true;
		else
			return false;
	}

	private List<string> PlayersCardsList;

	void CloseRound ()
	{
		int LastWinnderID = GetWinnerID ();
		TempCurrenRoom.GameOB.CurrentRound.PlayersScores [LastWinnderID] += GetRoundScore ();
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = LastWinnderID;
		TempCurrenRoom.GameOB.CollectorID = LastWinnderID;
		SetOnCollectOnce (LastWinnderID);
		TempCurrenRoom.GameOB.CurrentRound.RoundNO++;
		for (int i = 0; i < 4; i++)
			TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i] = "0";
		TempCurrenRoom.ActionID = 12;
		PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void SetOnCollectOnce (int lastWinnderID)
	{
		if (lastWinnderID == 0 || lastWinnderID == 2)
			lastWinnderID = 0;
		else
			lastWinnderID = 1;
		if (!TempCurrenRoom.GameOB.BalootDetailsOB.IsCollectOnce [lastWinnderID])
			TempCurrenRoom.GameOB.BalootDetailsOB.IsCollectOnce [lastWinnderID] = true;
	}

	int GetRoundScore ()
	{
		int ScoreCounter = 0;
		string[] ThisThrowdCardsList = TempCurrenRoom.GameOB.CurrentRound.PlayersCards;
		for (int i = 0; i < ThisThrowdCardsList.Length; i++)
			ScoreCounter += GetThisCardScore (ThisThrowdCardsList [i], ThisThrowdCardsList [i].StartsWith (TempCurrenRoom.GameOB.BalootDetailsOB.TrumpType));
		return ScoreCounter;
	}

	private int GetThisCardScore (string ThisCardID, bool ItsAfukinHokumCard)
	{
		int CardNO = GetCardNO (ThisCardID);
		if (ItsAfukinHokumCard) {//Fukin Hokum
			if (CardNO == 14)
				return 11;
			else if (CardNO == 13)
				return 4;
			else if (CardNO == 12)
				return 3;
			else if (CardNO == 11)
				return 20;
			else if (CardNO == 10)
				return 10;
			else if (CardNO == 9)
				return 14;
			else
				return 0;
		} else {
			if (CardNO == 14)
				return 11;
			else if (CardNO == 13)
				return 4;
			else if (CardNO == 12)
				return 3;
			else if (CardNO == 11)
				return 2;
			else if (CardNO == 10)
				return 10;
			else
				return 0;
		}
	}

	private int GetWinnerID ()
	{
		TempCurrenRoom.ActionID = 11;
		PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
		PlayersCardsList = new List<string> ();
		for (int i = 0; i < 4; i++)
			PlayersCardsList.Add (TempCurrenRoom.GameOB.CurrentRound.PlayersCards [i]);
		
		if (ThereIsTrumpCard ())
			return GetHighestFromThisType (TempCurrenRoom.GameOB.BalootDetailsOB.TrumpType);
		else
			return GetHighestFromThisType (GetChar (PlayersCardsList [TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound], 0));
	}

	private int GetHighestFromThisType (string CardType)
	{
		List<string> NewCardsList = new List<string> ();
		for (int i = 0; i < 4; i++) {
			if (PlayersCardsList [i].StartsWith (CardType))
				NewCardsList.Add (PlayersCardsList [i]);
		}
		for (int i = 0; i < 4; i++) {
			if (NewCardsList [GetBalootHighestOne (NewCardsList, CardType)] == PlayersCardsList [i])
				return i;
		}
		return 0;
	}

	int GetBalootHighestOne (List<string> NewCardsList, string CardType)
	{
		if (TempCurrenRoom.GameOB.BalootDetailsOB.GameType == 2 &&
		    CardType == TempCurrenRoom.GameOB.BalootDetailsOB.TrumpType) {//Hokum
			for (int i = 0; i < NewCardsList.Count; i++)
				if (GetCardNO (NewCardsList [i]) == 11)
					return i;
			for (int i = 0; i < NewCardsList.Count; i++)
				if (GetCardNO (NewCardsList [i]) == 9)
					return i;
			for (int i = 0; i < NewCardsList.Count; i++)
				if (GetCardNO (NewCardsList [i]) == 14)
					return i;
			for (int i = 0; i < NewCardsList.Count; i++)
				if (GetCardNO (NewCardsList [i]) == 10)
					return i;
			for (int i = 0; i < NewCardsList.Count; i++)
				if (GetCardNO (NewCardsList [i]) == 13)
					return i;
			for (int i = 0; i < NewCardsList.Count; i++)
				if (GetCardNO (NewCardsList [i]) == 12)
					return i;
			for (int i = 0; i < NewCardsList.Count; i++)
				if (GetCardNO (NewCardsList [i]) == 8)
					return i;
			for (int i = 0; i < NewCardsList.Count; i++)
				if (GetCardNO (NewCardsList [i]) == 7)
					return i;
		} else {
			for (int i = 0; i < NewCardsList.Count; i++)
				if (GetCardNO (NewCardsList [i]) == 14)
					return i;
			for (int i = 0; i < NewCardsList.Count; i++)
				if (GetCardNO (NewCardsList [i]) == 10)
					return i;
			for (int i = 0; i < NewCardsList.Count; i++)
				if (GetCardNO (NewCardsList [i]) == 13)
					return i;
			for (int i = 0; i < NewCardsList.Count; i++)
				if (GetCardNO (NewCardsList [i]) == 12)
					return i;
			for (int i = 0; i < NewCardsList.Count; i++)
				if (GetCardNO (NewCardsList [i]) == 11)
					return i;
			for (int i = 0; i < NewCardsList.Count; i++)
				if (GetCardNO (NewCardsList [i]) == 9)
					return i;
			for (int i = 0; i < NewCardsList.Count; i++)
				if (GetCardNO (NewCardsList [i]) == 8)
					return i;
			for (int i = 0; i < NewCardsList.Count; i++)
				if (GetCardNO (NewCardsList [i]) == 7)
					return i;
		}
		//Not Reachable
		return 0;
	}

	private bool ThereIsTrumpCard ()
	{
		for (int i = 0; i < 4; i++) {
			if (PlayersCardsList [i].StartsWith (TempCurrenRoom.GameOB.BalootDetailsOB.TrumpType))
				return true;
		}
		return false;
	}


	private int ProjectTotalValue = 0;

	public void ProjectSelected (GameObject ProjectBTNGO)
	{
		int ProjectID = ProjectBTNGO.transform.GetSiblingIndex ();
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (ProjectTotalValue >= 2) {
			if (ProjectID == 0 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Sira [PlayerID] == 1) {
				TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Sira [PlayerID] = 0;
				ProjectTotalValue -= 1;
			} else if (ProjectID == 1 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Fifty [PlayerID] == 1) {
				TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Fifty [PlayerID] = 0;
				ProjectTotalValue -= 1;
			} else if (ProjectID == 2 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Hundred [PlayerID] == 1) {
				TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Hundred [PlayerID] = 0;
				ProjectTotalValue -= 1;
			} else if (ProjectID == 3 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.FourHundred [PlayerID] == 1) {
				TempCurrenRoom.GameOB.BalootDetailsOB.Projects.FourHundred [PlayerID] = 0;
				ProjectTotalValue -= 1;
			}
			if (ProjectID == 0 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Sira [PlayerID] == 2) {
				TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Sira [PlayerID] = 0;
				ProjectTotalValue -= 2;
			} else if (ProjectID == 1 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Fifty [PlayerID] == 2) {
				TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Fifty [PlayerID] = 0;
				ProjectTotalValue -= 2;
			} else if (ProjectID == 2 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Hundred [PlayerID] == 2) {
				TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Hundred [PlayerID] = 0;
				ProjectTotalValue -= 2;
			} else if (ProjectID == 3 && TempCurrenRoom.GameOB.BalootDetailsOB.Projects.FourHundred [PlayerID] == 2) {
				TempCurrenRoom.GameOB.BalootDetailsOB.Projects.FourHundred [PlayerID] = 0;
				ProjectTotalValue -= 2;
			}
			PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, false);
			ProjectBTNGO.transform.GetComponentInChildren<Text> ().text = PlusOneForThisBTN (ProjectBTNGO);
			return;
		} 
		ProjectTotalValue += 1;
		if (ProjectID == 0)
			TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Sira [PlayerID] += 1;
		else if (ProjectID == 1)
			TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Fifty [PlayerID] += 1;
		else if (ProjectID == 2)
			TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Hundred [PlayerID] += 1;
		else if (ProjectID == 3)
			TempCurrenRoom.GameOB.BalootDetailsOB.Projects.FourHundred [PlayerID] += 1;
		PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, false);
		ProjectBTNGO.transform.GetComponentInChildren<Text> ().text = PlusOneForThisBTN (ProjectBTNGO);
	}

	string PlusOneForThisBTN (GameObject projectBTNGO)
	{
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (projectBTNGO.transform.GetSiblingIndex () == 0)
			return TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Sira [PlayerID].ToString();
		else if (projectBTNGO.transform.GetSiblingIndex () == 1)
			return "50:" + TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Fifty [PlayerID];
		else if (projectBTNGO.transform.GetSiblingIndex () == 2)
			return "100:" + TempCurrenRoom.GameOB.BalootDetailsOB.Projects.Hundred [PlayerID];
		else if (projectBTNGO.transform.GetSiblingIndex () == 3)
			return "400:" + TempCurrenRoom.GameOB.BalootDetailsOB.Projects.FourHundred [PlayerID];
		//Not Reachable
		return "";
	}

	void UpdateRoomPhase9 ()
	{
		StartCoroutine (CallStartAndGameTypeSoundsAfterASec());
		StartCoroutine (GiveTheExposedCard ());
		if (TempCurrenRoom.GameOB.BalootDetailsOB.GameType == 2)
			PlayingWindowsManager6.Instance.SetTrumpAndBidder (TempCurrenRoom.GameOB.BalootDetailsOB.TrumpType);
		BalootManager6.Instance.AssignPlayerCardsAndActivateOthersCards (false);
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDWhoStartRound = TempCurrenRoom.GameOB.BalootDetailsOB.BidsWinnerID;
		BalootManager6.Instance.SetCardsForPlayers (false);
		for (int i = 0; i < 4; i++)
			TempCurrenRoom.GameOB.Cards.PlayersCards [i].PlayerCardsList = BalootManager6.Instance.Cards.PlayersCards [i].PlayerCardsList;
		if (TempCurrenRoom.GameOB.BalootDetailsOB.GameType == 2
		    && (TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel == 2 || TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel == 4))
			TempCurrenRoom.GameOB.BalootDetailsOB.Locked = true;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = TempCurrenRoom.GameOB.BalootDetailsOB.BidsWinnerID;
		TempCurrenRoom.ActionID = 10;
		PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private IEnumerator CallStartAndGameTypeSoundsAfterASec(){
		yield return new WaitForSeconds(1);
		AudioManager.Instance.PlaySound (1);
		PlayingWindowsManager6.Instance.CallBalootSounds (GetGameTypeStringFromGameTypeID(TempCurrenRoom.GameOB.BalootDetailsOB.GameType));
	}

	private string GetGameTypeStringFromGameTypeID(int ThisGameTypeID){
		if (ThisGameTypeID == 1)
			return "Sun";
		else if (ThisGameTypeID == 2)
			return "Hokum";
		else if (ThisGameTypeID == 3)
			return "Ashkal";
		else 
			return "";
	}

	private IEnumerator GiveTheExposedCard ()
	{
		int PlayerWhoTakesExposedCard = TempCurrenRoom.GameOB.BalootDetailsOB.PlayerWhoTakesExposedCard;
		PlayingWindowsManager6.Instance.ExposedCardGO.GetComponent<Animator> ().SetInteger ("Thorugh", -6);
		PlayingWindowsManager6.Instance.ExposedCardGO.GetComponent<Animator> ().SetInteger ("CollectorPlayerID", FromOnlineToLocal (PlayerWhoTakesExposedCard));
		yield return new WaitForSeconds (1f);
		PlayingWindowsManager6.Instance.ExposedCardGO.SetActive (false);
	}

	void UpdateRoomPhase8 ()
	{
		//Here Player Select Trump if could
		Debug.Log ("UpdateRoomPhase8");
		PlayingWindowsManager6.Instance.SetTimer (FromOnlineToLocal (TempCurrenRoom.GameOB.BalootDetailsOB.BidsWinnerID),
			TimerWaitingTime (TempCurrenRoom.GameOB.BalootDetailsOB.BidsWinnerID), 4, false);
		if (Player.Instance.GameCreator && TempCurrenRoom.GameOB.BalootDetailsOB.GameType != 2) {
			TempCurrenRoom.ActionID = 9;
			PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
			return;
		}
		if (Player.Instance.OnlinePlayerID != TempCurrenRoom.GameOB.BalootDetailsOB.BidsWinnerID)
			return;
		//Here is the bidding winner
		if (TempCurrenRoom.GameOB.BalootDetailsOB.GameType == 2)
			PlayingWindowsManager6.Instance.ShowSelectTarneebWindow (TempCurrenRoom.GameOB.BalootDetailsOB.ExposedCard);
	}

	public void TrumSelected (string TrumpType)
	{
		PlayingWindowsManager6.Instance.SelectTarneebTypeTarneebsGO [0].transform.parent.parent.gameObject.SetActive (false);
		TempCurrenRoom.GameOB.BalootDetailsOB.TrumpType = TrumpType;
		TempCurrenRoom.ActionID = 9;
		PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	//Bidding Ends
	private void UpdateRoomPhase7 ()
	{
		Debug.Log ("UpdateRoomPhase7");
		if (TempCurrenRoom.GameOB.BalootDetailsOB.CurrentPopString.Length > 1 &&
		    !TempCurrenRoom.GameOB.BalootDetailsOB.CurrentPopString.Contains ("Hokum"))
			PlayingWindowsManager6.Instance.ShowBidPop (FromOnlineToLocal (TempCurrenRoom.GameOB.BalootDetailsOB.LastBidderID),
				TempCurrenRoom.GameOB.BalootDetailsOB.CurrentPopString);
		if (TempCurrenRoom.GameOB.BalootDetailsOB.GameType == 2 || DifferenceOver100 ())
			ShowDoublingStuff ();
		if (!Player.Instance.GameCreator)
			return;
		if (TempCurrenRoom.GameOB.BalootDetailsOB.GameType == 2 || DifferenceOver100 ())
			return;
		TempCurrenRoom.ActionID = 8;
		PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	bool DifferenceOver100 ()
	{
		int Team1Score = 0;
		int Team2Score = 0;
		Team1Score = TempCurrenRoom.GameOB.PlayersScores [0] + TempCurrenRoom.GameOB.PlayersScores [2];
		Team2Score = TempCurrenRoom.GameOB.PlayersScores [1] + TempCurrenRoom.GameOB.PlayersScores [3];
		if (TempCurrenRoom.GameOB.BalootDetailsOB.BidsWinnerID == 0 || TempCurrenRoom.GameOB.BalootDetailsOB.BidsWinnerID == 2) {
			if (Team1Score > 100 && Team2Score < 100)
				return true;
		} else {
			if (Team2Score > 100 && Team1Score < 100)
				return true;
		}
		return false;
	}

	void ShowDoublingStuff ()
	{
		Debug.Log ("ShowDoublingStuff");
		int PlayerIDBidTurn = TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn;
		PlayingWindowsManager6.Instance.SetTimer (FromOnlineToLocal (PlayerIDBidTurn),
			TimerWaitingTime (PlayerIDBidTurn), 2, false);
		PlayingWindowsManager6.Instance.DoubleTxtGO.GetComponent<Text> ().text = TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel+"";
		if (!IsThisMyBidTurn ())
			return;
		int BidderWinnerID = TempCurrenRoom.GameOB.BalootDetailsOB.BidsWinnerID;
		if (TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel == 0) {
			if (Player.Instance.OnlinePlayerID == OppistePlayersID (BidderWinnerID).Item1 ||
			    Player.Instance.OnlinePlayerID == OppistePlayersID (BidderWinnerID).Item2)
				PlayingWindowsManager6.Instance.ShowDoulbeWindow (TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel == 4);
		} else
			PlayingWindowsManager6.Instance.ShowDoulbeWindow (TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel == 4);
	}

	private Tuple<int,int> OppistePlayersID (int PlayerID)
	{
		if (PlayerID == 0 || PlayerID == 2)
			return Tuple.Create (1, 3);
		else
			return Tuple.Create (0, 2);
	}

	public void DoubleBTN ()
	{
		int BidderID = TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn;
		PlayingWindowsManager6.Instance.DoubleWindowGO.SetActive (false);
		TempCurrenRoom.GameOB.BalootDetailsOB.LastBidderID = TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn;
		TempCurrenRoom.GameOB.BalootDetailsOB.CurrentPopString = "Double";
		if (BidderID != TempCurrenRoom.GameOB.BalootDetailsOB.BidsWinnerID)
			TempCurrenRoom.GameOB.BalootDetailsOB.OppisiteDoublerID = BidderID;
		TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel += 1;
		if (TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel == 5) {
			TempCurrenRoom.GameOB.BalootDetailsOB.Gahwa = true;
			TempCurrenRoom.ActionID = 8;
			PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
			return;
		} else
			TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = NextDoublder ();
		PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	int NextDoublder ()
	{
		if (TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn == TempCurrenRoom.GameOB.BalootDetailsOB.BidsWinnerID)
			return TempCurrenRoom.GameOB.BalootDetailsOB.OppisiteDoublerID;
		else
			return TempCurrenRoom.GameOB.BalootDetailsOB.BidsWinnerID;
	}

	public void PassDoubleBTN ()
	{
		Debug.Log ("PassDoubleBTN");
		PlayingWindowsManager6.Instance.DoubleWindowGO.SetActive (false);
		TempCurrenRoom.GameOB.BalootDetailsOB.LastBidderID = TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn;
		TempCurrenRoom.GameOB.BalootDetailsOB.CurrentPopString = "Pass";
		Debug.Log (TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel + "||" + TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn);
		if (TempCurrenRoom.GameOB.BalootDetailsOB.RoundDoubleLevel == 1
		    && (TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn == 0 || TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn == 1)) {
			TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = MyPartnerID (TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn);
		} else
			TempCurrenRoom.ActionID = 8;
		PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	//Bidding Phase
	void UpdateRoomPhase6 ()
	{
		Debug.Log ("UpdateRoomPhase6");
		int PlayerIDBidTurn = TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn;
		if (TempCurrenRoom.GameOB.BalootDetailsOB.CurrentPopString.Length > 1)
			PlayingWindowsManager6.Instance.ShowBidPop (FromOnlineToLocal (TempCurrenRoom.GameOB.BalootDetailsOB.LastBidderID),
				TempCurrenRoom.GameOB.BalootDetailsOB.CurrentPopString);
		if (Player.Instance.GameCreator) {
			if (NoOfPasses (true) == 4) {//RestartGame
				NewRound ();
				return;
			} else if (TheOnlyPlayerWhoCalledSun () != -1)
				EndBiddingPhase (1, TheOnlyPlayerWhoCalledSun ());
			else if (TheOnlyPlayerWhoCalledAshkal () != -1)
				EndBiddingPhase (3, TheOnlyPlayerWhoCalledAshkal ());
			else if (NoOfPasses (true) == 3 && TheOnlyPlayerWhoCalledHokumConfirmed () != -1)
				EndBiddingPhase (2, TheOnlyPlayerWhoCalledHokum ());
		}
		if (NoMoreWindows2 ())
			return;
		PlayingWindowsManager6.Instance.SetTimer (FromOnlineToLocal (PlayerIDBidTurn),
			TimerWaitingTime (PlayerIDBidTurn), 1, false);
		if (Player.Instance.OnlinePlayerID != PlayerIDBidTurn)
			return;
		else if (NoOfPasses (true) == 3 && HokumCalled ())
			PlayingWindowsManager6.Instance.ShowBidWindow2 (true, false, true, false, false);
		else if (HokumCalled ())
			PlayingWindowsManager6.Instance.ShowBidWindow2 (true, false, false, IsShowAshkal (), true);
		else
			PlayingWindowsManager6.Instance.ShowBidWindow2 (true, true, false, IsShowAshkal (), true);
	}

	//Player Bid Turn
	void UpdateRoomPhase5 ()
	{
		Debug.Log ("UpdateRoomPhase5");
		int PlayerIDBidTurn = TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn;
		if (TempCurrenRoom.GameOB.BalootDetailsOB.CurrentPopString.Length > 1)
			PlayingWindowsManager6.Instance.ShowBidPop (FromOnlineToLocal (TempCurrenRoom.GameOB.BalootDetailsOB.LastBidderID),
				TempCurrenRoom.GameOB.BalootDetailsOB.CurrentPopString);
		if (Player.Instance.GameCreator) {
			if (NoOfPasses (true) == 4) {//Go to Bid Round 2
				TempCurrenRoom.ActionID = 6;
				for (int i = 0; i < 4; i++)
					TempCurrenRoom.GameOB.BalootDetailsOB.Bas [i] = false;
				PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
				return;
			} else if (NoOfPasses (true) == 2 && TheOnlyPlayerWhoCalledSun () != -1)
				EndBiddingPhase (1, TheOnlyPlayerWhoCalledSun ());
			else if (NoOfPasses (true) == 2 && TheOnlyPlayerWhoCalledAshkal () != -1)
				EndBiddingPhase (3, TheOnlyPlayerWhoCalledAshkal ());
			else if (NoOfPasses (true) == 3 && TheOnlyPlayerWhoCalledHokumConfirmed () != -1)
				EndBiddingPhase (2, TheOnlyPlayerWhoCalledHokum ());
		}
		if (NoMoreWindows ())
			return;
		PlayingWindowsManager6.Instance.SetTimer (FromOnlineToLocal (PlayerIDBidTurn),
			TimerWaitingTime (PlayerIDBidTurn), 1, false);
		if (Player.Instance.OnlinePlayerID != PlayerIDBidTurn)
			return;
		if (PlayerCallSunOrAshkalInOtherTeam ())
			PlayingWindowsManager6.Instance.ShowBidWindow1 (true, false, false, IsShowAshkal (), true);
		else if (NoOfPasses (true) == 3 && HokumCalled ())
			PlayingWindowsManager6.Instance.ShowBidWindow1 (true, false, true, IsShowAshkal (), false);
		else if (HokumCalled ())
			PlayingWindowsManager6.Instance.ShowBidWindow1 (true, false, false, IsShowAshkal (), true);
		else
			PlayingWindowsManager6.Instance.ShowBidWindow1 (true, true, false, IsShowAshkal (), true);
	}

	bool IsShowAshkal ()
	{
		int PlayerWhoStartBidding = TempCurrenRoom.GameOB.BalootDetailsOB.PlayerWhoStartBids;
		int AshkalPlayer1 = NextNO (NextNO (PlayerWhoStartBidding));
		int AshkalPlayer2 = NextNO (AshkalPlayer1);
		if (Player.Instance.OnlinePlayerID == AshkalPlayer1 || Player.Instance.OnlinePlayerID == AshkalPlayer2)
			return true;
		else
			return false;
	}

	bool NoMoreWindows ()
	{
		if (NoOfPasses (true) == 4)
			return true;
		if (TempCurrenRoom.GameOB.BalootDetailsOB.BiddingEnds)
			return true;
		if (NoOfPasses (true) == 3 && TheOnlyPlayerWhoCalledHokumConfirmed () != -1)
			return true;
		return false;
	}

	bool NoMoreWindows2 ()
	{
		if (NoOfPasses (true) == 4)
			return true;
		if (TheOnlyPlayerWhoCalledSun () != -1)
			return true;
		if (TheOnlyPlayerWhoCalledAshkal () != -1)
			return true;
		if (TheOnlyPlayerWhoCalledHokumConfirmed () != -1)
			return true;
		return false;
	}

	//Round BTNS
	public void SunBTN (bool FirstRound)
	{
		int BidderID = TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn;
		TempCurrenRoom.GameOB.BalootDetailsOB.LastBidderID = BidderID;
		TempCurrenRoom.GameOB.BalootDetailsOB.SunChoosed [Player.Instance.OnlinePlayerID] = true;
		if (PlayerCallSunOrAshkalInOtherTeam ()) {
			EndBiddingPhase (1, BidderID);
			return;
		} else if (!FirstRound) {
			EndBiddingPhase (1, BidderID);
			return;
		} else {
			if (Player.Instance.OnlinePlayerID == 0 || Player.Instance.OnlinePlayerID == 2) {//Team 1
				if (TempCurrenRoom.GameOB.BalootDetailsOB.Bas [1])
					TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = 1;
				else if (TempCurrenRoom.GameOB.BalootDetailsOB.Bas [3])
					TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = 3;
				else {
					EndBiddingPhase (1, BidderID);
					return;
				}
			} else {
				if (TempCurrenRoom.GameOB.BalootDetailsOB.Bas [0])
					TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = 0;
				else if (TempCurrenRoom.GameOB.BalootDetailsOB.Bas [2])
					TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = 2;
				else {
					EndBiddingPhase (1, BidderID);
					return;
				}
			}
			TempCurrenRoom.GameOB.BalootDetailsOB.CurrentPopString = "Sun";
			PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
		}
	}

	public void AshkalBTN (bool FirstRound)
	{
		int BidderID = TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn;
		TempCurrenRoom.GameOB.BalootDetailsOB.LastBidderID = BidderID;
		TempCurrenRoom.GameOB.BalootDetailsOB.AshkalChoosed [Player.Instance.OnlinePlayerID] = true;
		if (PlayerCallSunOrAshkalInOtherTeam ()) {
			EndBiddingPhase (3, BidderID);
			return;
		} else if (!FirstRound) {
			EndBiddingPhase (3, BidderID);
			return;
		} else {
			if (Player.Instance.OnlinePlayerID == 0 || Player.Instance.OnlinePlayerID == 2) {//Team 1
				if (TempCurrenRoom.GameOB.BalootDetailsOB.Bas [1])
					TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = 1;
				else if (TempCurrenRoom.GameOB.BalootDetailsOB.Bas [3])
					TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = 3;
				else {
					EndBiddingPhase (3, BidderID);		
					return;
				}
			} else {
				if (TempCurrenRoom.GameOB.BalootDetailsOB.Bas [0])
					TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = 0;
				else if (TempCurrenRoom.GameOB.BalootDetailsOB.Bas [2])
					TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = 2;
				else {
					EndBiddingPhase (3, BidderID);	
					return;
				}
			}
			TempCurrenRoom.GameOB.BalootDetailsOB.CurrentPopString = "Ashkal";
			PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
		}
	}

	public void HokumBTN (bool FirstRound)
	{
		Debug.Log ("FirstRound" + FirstRound);
		int BidderID = TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn;
		TempCurrenRoom.GameOB.BalootDetailsOB.LastBidderID = BidderID;
		TempCurrenRoom.GameOB.BalootDetailsOB.CurrentPopString = "Hokum";
		TempCurrenRoom.GameOB.BalootDetailsOB.Hokum [BidderID] = true;
		if (FirstRound) {
			TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = NextNO (BidderID);
			for (int i = 0; i < 4; i++)
				TempCurrenRoom.GameOB.BalootDetailsOB.Bas [i] = false;
		} else {
			if (Player.Instance.OnlinePlayerID == 0 || Player.Instance.OnlinePlayerID == 2) {//Team 1
				if (!TempCurrenRoom.GameOB.BalootDetailsOB.Bas [1])
					TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = 1;
				else if (!TempCurrenRoom.GameOB.BalootDetailsOB.Bas [3])
					TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = 3;
			} else {
				Debug.Log ("3");
				if (!TempCurrenRoom.GameOB.BalootDetailsOB.Bas [0])
					TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = 0;
				else if (!TempCurrenRoom.GameOB.BalootDetailsOB.Bas [2])
					TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = 2;
			}
		}
		PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	public void BasBTN (bool FirstRound)
	{
		int BidderID = TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn;
		PlayingWindowsManager6.Instance.BidWindows1.SetActive (false);
		PlayingWindowsManager6.Instance.BidWindows2.SetActive (false);
		TempCurrenRoom.GameOB.BalootDetailsOB.LastBidderID = BidderID;
		if (FirstRound)
			TempCurrenRoom.GameOB.BalootDetailsOB.CurrentPopString = "Bas";
		else
			TempCurrenRoom.GameOB.BalootDetailsOB.CurrentPopString = "Wala";
		TempCurrenRoom.GameOB.BalootDetailsOB.Bas [BidderID] = true;
		if (PlayerCallSunOrAshkalInOtherTeam ()) {
			if (BidderID == 0 || BidderID == 1)
				TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = MyPartnerID (TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn);
			else {//End Phase, Player who called sun Or Ashkal is the winner
				for (int i = 0; i < 4; i++)
					TempCurrenRoom.GameOB.BalootDetailsOB.Bas [i] = false;
				if (TheOnlyPlayerWhoCalledSun () != -1)
					EndBiddingPhase (1, TheOnlyPlayerWhoCalledSun ());
				else
					EndBiddingPhase (3, TheOnlyPlayerWhoCalledAshkal ());
				return;
			}
			PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
			return;
		} else {
			TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = NextNO (BidderID);
			PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
		}
	}

	public void ConfirmHokumBTN (bool FirstRound)
	{
		PlayingWindowsManager6.Instance.BidWindows1.SetActive (false);
		PlayingWindowsManager6.Instance.BidWindows2.SetActive (false);
		TempCurrenRoom.GameOB.BalootDetailsOB.CurrentPopString = "ConfirmHokumBTN";
		EndBiddingPhase (2, TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn);
	}

	private void EndBiddingPhase (int GameType, int BiddingWinner)
	{
		TempCurrenRoom.GameOB.BalootDetailsOB.BidsWinnerID = BiddingWinner;
		TempCurrenRoom.GameOB.BalootDetailsOB.BiddingEnds = true;
		TempCurrenRoom.GameOB.BalootDetailsOB.PlayerWhoTakesExposedCard = BiddingWinner;
		if (GameType == 3)//Ashkal
			TempCurrenRoom.GameOB.BalootDetailsOB.PlayerWhoTakesExposedCard = MyPartnerID (BiddingWinner);
		TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = OppistePlayersID (BiddingWinner).Item1;
		TempCurrenRoom.GameOB.BalootDetailsOB.GameType = GameType;
		for (int i = 0; i < 4; i++)
			TempCurrenRoom.GameOB.BalootDetailsOB.Bas [i] = false;
		TempCurrenRoom.ActionID = 7;
		PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private bool HokumCalled ()
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.BalootDetailsOB.Hokum [i])
				return true;
		return false;
	}

	int TheOnlyPlayerWhoCalledSun ()
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.BalootDetailsOB.SunChoosed [i])
				return i;
		//No One is hokum
		return -1;
	}

	int TheOnlyPlayerWhoCalledAshkal ()
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.BalootDetailsOB.AshkalChoosed [i])
				return i;
		//No One is hokum
		return -1;
	}

	int TheOnlyPlayerWhoCalledHokum ()
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.BalootDetailsOB.Hokum [i])
				return i;
		//No One is hokum
		return -1;
	}

	int TheOnlyPlayerWhoCalledHokumConfirmed ()
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.GameOB.BalootDetailsOB.HokumConfirmed [i])
				return i;
		//No One is hokum
		return -1;
	}

	private bool PlayerCallSunOrAshkalInOtherTeam ()
	{
		if (TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn == 0 || TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn == 2) {
			if (TempCurrenRoom.GameOB.BalootDetailsOB.SunChoosed [1] || TempCurrenRoom.GameOB.BalootDetailsOB.SunChoosed [3]
			    || TempCurrenRoom.GameOB.BalootDetailsOB.AshkalChoosed [1] || TempCurrenRoom.GameOB.BalootDetailsOB.AshkalChoosed [3])
				return true;
		} else {
			if (TempCurrenRoom.GameOB.BalootDetailsOB.SunChoosed [0] || TempCurrenRoom.GameOB.BalootDetailsOB.SunChoosed [2]
			    || TempCurrenRoom.GameOB.BalootDetailsOB.AshkalChoosed [0] || TempCurrenRoom.GameOB.BalootDetailsOB.AshkalChoosed [2])
				return true;
		}
		return false;
	}

	//EndsBTNSSocendRound
	private int NoOfPasses (bool FirstOrSocends)
	{
		int NoOfPassesCounter = 0;
		for (int i = 0; i < 4; i++) {
			if (FirstOrSocends) {
				if (TempCurrenRoom.GameOB.BalootDetailsOB.Bas [i])
					NoOfPassesCounter++;
			} else {
				if (TempCurrenRoom.GameOB.BalootDetailsOB.Wala [i])
					NoOfPassesCounter++;
			}
		}
		return NoOfPassesCounter;
	}

	bool IsThisMyBidTurn ()
	{
		if (Player.Instance.OnlinePlayerID == TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn)
			return true;
		else
			return false;
	}

	private int PreviosPlayerID (int PlayerID)
	{
		int ThisPlayerID = PlayerID - 1;
		if (ThisPlayerID < 0)
			return 3;
		else
			return ThisPlayerID;
	}

	private bool SceneNotReady;

	void UpdateRoomPhase4 ()
	{
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (PlayerID == -1)
			return;
		SceneNotReady = true;
		BalootManager6.Instance.Cards.PlayersCards [PlayerID].PlayerCardsList =
			TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerID].PlayerCardsList;
		BalootManager6.Instance.SetAndArrangeMyCards ();
		ResetData ();
		ReturnsMyCardsAvailibity ();
		PlayingWindowsManager6.Instance.ShowExposedCard (TempCurrenRoom.GameOB.BalootDetailsOB.ExposedCard);
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.GameOB.BalootDetailsOB.PlayerBidTurn = TempCurrenRoom.GameOB.BalootDetailsOB.PlayerWhoStartBids;
		TempCurrenRoom.ActionID = 5;
		PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void ResetData ()
	{
		//		for (int i = 0; i < 4; i++)
		//			PlayingWindowsManager6.Instance.PlayerEstimationDetails [i].GetComponent<Text> ().text = "";
		//		PlayingWindowsManager6.Instance.ResetWindows ();
	}

	void UpdateRoomPhase3 ()
	{
		if (Player.Instance.OnlinePlayerID == -1)
			return;
		if (SceneNotReady)
			return;
		if (IsAllPlayersReady () && Player.Instance.GameCreator) {
			TempCurrenRoom.ActionID = -1;
			PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
			return;
		}
		UpdateRoomPhase2 ();
		if (TempCurrenRoom.PlayersReady [Player.Instance.OnlinePlayerID])
			return;
		SetPlayersScoresTxts ();
		IfPlayerHoldingCardGetItBack ();
		PlayingWindowsManager6.Instance.TrumpDetailsWindow.SetActive (false);
		PlayingFireBaseManager6.Instance.SetOnDissconnectHandle ();
		BalootManager6.Instance.AssignPlayerCardsAndActivateOthersCards (true);
		PlayingWindowsManager6.Instance.WinsWindow.SetActive (false);
		TempCurrenRoom.PlayersReady [Player.Instance.OnlinePlayerID] = true;
		if (Player.Instance.GameCreator)
			SetReadyForComputer ();
		PlayingFireBaseManager6.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void SetReadyForComputer ()
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.SeatsStatus [i] == 0)
				TempCurrenRoom.PlayersReady [i] = true;
	}

	bool IsAllPlayersReady ()
	{
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++)
			if (!TempCurrenRoom.PlayersReady [i])
				return false;
		return true;
	}

	void SetPlayersScoresTxts ()
	{
		for (int i = 0; i < 4; i++)
			PlayingWindowsManager6.Instance.TeamsScores [FromOnlineToLocal (i)].GetComponent<Text> ().text = TempCurrenRoom.GameOB.PlayersScores [i].ToString ();
	}

	private int NextNO (int NO)
	{
		NO++;
		if (NO >= 4)
			NO = 0;
		return NO;
	}

	#region implemented abstract members of PlayersManagerParent

	public override void OnApplicationPause (bool pauseStatus)
	{
		if (TempCurrenRoom.ActionID < 4)
			return;
		try {
			if (pauseStatus)
				CreatorPauseOrExit (false);
			else
				StartCoroutine (SetandarrangeCor (pauseStatus));
		} catch {
			//Debug.Log ("OnApplicationPause\t" + e.Message);
		}
	}

	#endregion

	private IEnumerator SetandarrangeCor (bool pauseStatus)
	{
		ShowBackBTN ();
		yield return new WaitForSeconds (2.5f);
		try {
			BalootManager6.Instance.SetAndArrangeMyCards ();
		} catch {
		}
	}


	#region implemented abstract members of PlayersManagerParent

	public override void IamBack ()
	{
		TempCurrenRoom.SeatsStatus [Player.Instance.OnlinePlayerID] = 1;
		PlayerIsBack (true);
		PlayingFireBaseFather.ParentInstance.UploudThisJson (TempCurrenRoom, false);
	}

	#endregion

	private void OnDestroy ()
	{
		StopAllCoroutines ();
	}
}