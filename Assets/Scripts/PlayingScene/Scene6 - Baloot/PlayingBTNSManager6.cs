﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayingBTNSManager6 : PlayingBTNSManagerFather {
	
	public static PlayingBTNSManager6 Instance;

	protected override void Awake(){
		base.Awake ();
		Instance = this;
	}

	public override void BTNSFunction(){
		base.BTNSFunction ();Debug.Log ("BTNName");
		if(CurrentBTNGO.transform.parent.name.Contains("BiddingWindow"))
			CurrentBTNGO.transform.parent.gameObject.SetActive (false);
		if (BTNName == ("RestartBTN"))
			PlayersManager6.Instance.RestartGame ();
		else if (BTNName == ("SunBTN"))
			PlayersManager6.Instance.SunBTN (true);
		//BidWindow1
		else if (BTNName == ("HokumBTN"))
			PlayersManager6.Instance.HokumBTN (true);
		else if (BTNName == ("AshkalBTN"))
			PlayersManager6.Instance.AshkalBTN (true);
		else if (BTNName == ("BasBTN"))
			PlayersManager6.Instance.BasBTN (true);
		else if (BTNName == ("HokumConfirmBTN"))
			PlayersManager6.Instance.ConfirmHokumBTN (true);
		//BidWindow2
		else if (BTNName == ("SunBTN2"))
			PlayersManager6.Instance.SunBTN (false);
		else if (BTNName == ("HokumBTN2"))
			PlayersManager6.Instance.HokumBTN (false);
		else if (BTNName == ("AshkalBTN2"))
			PlayersManager6.Instance.AshkalBTN (false);
		else if (BTNName == ("HokumConfirmBTN2"))
			PlayersManager6.Instance.ConfirmHokumBTN (false);
		else if (BTNName == ("BasBTN2"))
			PlayersManager6.Instance.BasBTN (false);
		//DoubleWindow
		else if (BTNName == ("DoubleBTN")) {
			CurrentBTNGO.transform.parent.parent.gameObject.SetActive (false);
			PlayersManager6.Instance.DoubleBTN ();
		} else if (BTNName == ("DoublePassBTN")) {
			CurrentBTNGO.transform.parent.parent.gameObject.SetActive (false);
			PlayersManager6.Instance.PassDoubleBTN ();
		} else if (BTNName.Contains ("Tarneeb")) {
			CurrentBTNGO.transform.parent.parent.gameObject.SetActive (false);
			PlayersManager6.Instance.TrumSelected (GetChar (BTNName, 0));
		} else if (BTNName == ("Sira") || BTNName == ("50") || BTNName == ("100") || BTNName == ("400"))
			PlayersManager6.Instance.ProjectSelected (CurrentBTNGO);
		else if (BTNName == "ProjectsDoneBTN") {
			PlayingWindowsManager6.Instance.ProjectsWindowGO.SetActive (false);
			PlayersManager6.Instance.DoneBTN ();
		}
	}
}