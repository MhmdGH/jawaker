﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public class PlayingWindowsManager6 : PlayingWindowsManagerFather {

	public static PlayingWindowsManager6 Instance;
	public GameObject TrumpDetailsWindow,ExposedCardGO,BidWindows1,BidWindows2,DoubleWindowGO,DoubleTxtGO,
	DoubleBTNTxtGO,SelectTarneebWindowGO,ProjectsWindowGO;
	//BidWindows1BTNS, 0 => Sun, 1=> Hokum, 2=> HokumConfirm, 3=> Ashkal,4=> Bas
	public GameObject[] BidsPops,BidWindows1BTNS,BidWindows2BTNS,
	SelectTarneebTypeTarneebsGO,PlayersProjectsCardsGO1,PlayersProjectsCardsGO2;

	public override void Awake(){
		base.Awake ();
		Instance = this;
	}

	public void SetAndShowProjectCards(BalootProjects balootProjects){

		for (int i = 0; i < 4; i++) {//i is loop for all 4 players
			int AcutalI = PlayersManagerParent.ParentInstance.FromOnlineToLocal (i);
			if (i < balootProjects.PlayersProjectsGroup1.Length) {
				List<string> CardList1 = balootProjects.PlayersProjectsGroup1 [AcutalI].CardList;
				if (CardList1.Count > 1) {
					for (int u = 0; u < CardList1.Count; u++) {
						PlayersProjectsCardsGO1 [AcutalI].transform.GetChild (u).gameObject.SetActive (true);
						PlayersProjectsCardsGO1 [AcutalI].transform.GetChild (u).GetComponent<Card> ().CardID = CardList1 [u];
						PlayersProjectsCardsGO1 [AcutalI].transform.GetChild (u).GetComponent<Card> ().SetCardCoverOrFace ();
					}
				}
			}
			if (i < balootProjects.PlayersProjectsGroup2.Length) {
				List<string> CardList2 = balootProjects.PlayersProjectsGroup2 [AcutalI].CardList;
				if (CardList2.Count > 1) {
					for (int u = 0; u < CardList2.Count; u++) {
						PlayersProjectsCardsGO2 [AcutalI].transform.GetChild (u).gameObject.SetActive (true);
						PlayersProjectsCardsGO2 [AcutalI].transform.GetChild (u).GetComponent<Card> ().CardID = CardList2 [u];
						PlayersProjectsCardsGO2 [AcutalI].transform.GetChild (u).GetComponent<Card> ().SetCardCoverOrFace ();
					}
				}
			}
		}
	}

	public void ShowSelectTarneebWindow(string ExposedCardTarneebType){
		ExposedCardTarneebType = GetChar (ExposedCardTarneebType,0);
		if (ExposedCardTarneebType == "C")
			SelectTarneebTypeTarneebsGO[0].SetActive (false);
		else if (ExposedCardTarneebType == "D")
			SelectTarneebTypeTarneebsGO[1].SetActive (false);
		else if (ExposedCardTarneebType == "S")
			SelectTarneebTypeTarneebsGO[2].SetActive (false);
		else if (ExposedCardTarneebType == "H")
			SelectTarneebTypeTarneebsGO[3].SetActive (false);
		SelectTarneebWindowGO.SetActive (true);
	}

	public void ShowDoulbeWindow(bool LastOne){
		Debug.Log ("LastOne "+LastOne);
		if (LastOne) {
			if(MainLanguageManager.Instance.IsArabic)
				DoubleBTNTxtGO.GetComponent<Text> ().text = "قهوة";
			else
				DoubleBTNTxtGO.GetComponent<Text> ().text = "Gahwa";
		}
		DoubleWindowGO.SetActive (true);
	}

	public void ShowBidPop(int BidPopNO,string BidValue){
		StartCoroutine (ShowBidCor(BidPopNO,BidValue));
	}

	public override void ShowAndSetPartnersPhotos (string[] IMGSUrl)
	{
		OnStartBTNGO.SetActive (false);
		ChoosePartnerGO.SetActive (true);
		for (int i = 0; i < ChooseParternersGOs.Length; i++)
			PlayingFBManager6.Instance.SetFPPhoto (IMGSUrl [i], ChooseParternersGOs [i].GetComponent<Image> ());
	}

	public void SetTrumpAndBidder(string TrumpType){
		Debug.Log ("SetTrumpAndBidder");
		AudioManager.Instance.PlaySound (PlayersManagerParent.ParentInstance.TrumpTypeSound(TrumpType));
		TrumpDetailsWindow.transform.GetChild (2).gameObject.SetActive (false);
		TrumpDetailsWindow.SetActive (true);
		TrumpDetailsWindow.transform.GetChild (1).gameObject.SetActive (true);
		TrumpDetailsWindow.transform.GetChild(1).GetComponent<Image>().sprite = 
			Resources.Load<Sprite> ("Playing/"+TrumpType);
	}

	private IEnumerator ShowBidCor(int BidPopNO,string BidValue){
		CallBalootSounds (BidValue);
		BidsPops [BidPopNO].SetActive (true);
		BidsPops [BidPopNO].transform.GetChild (0).GetComponent<Text> ().text = BidValueTranslated(BidValue);
		yield return new WaitForSeconds (2);
		BidsPops [BidPopNO].SetActive (false);
	}

	public void CallBalootSounds (string bidValue)
	{
		if (bidValue == "Double")
			AudioManager.Instance.PlaySound (27);
		else if (bidValue == "Pass")
			AudioManager.Instance.PlaySound (16);
		else if (bidValue == "Sun")
			AudioManager.Instance.PlaySound (25);
		else if (bidValue == "Ashkal")
			AudioManager.Instance.PlaySound (21);
		else if (bidValue == "Hokum")
			AudioManager.Instance.PlaySound (24);
		else if (bidValue == "Bas")
			AudioManager.Instance.PlaySound (22);
		else if (bidValue == "Wala")
			AudioManager.Instance.PlaySound (26);
	}

	string BidValueTranslated (string bidValue)
	{
		if (MainLanguageManager.Instance.IsArabic) {
			if (bidValue == "Double")
				bidValue = ArabicFixer.Fix("دبل");
			else if (bidValue == "Pass")
				bidValue = ArabicFixer.Fix("باص");
			else if (bidValue == "Sun")
				bidValue = ArabicFixer.Fix("صن");
			else if (bidValue == "Ashkal")
				bidValue = ArabicFixer.Fix("أشكال");
			else if (bidValue == "Hokum")
				bidValue = ArabicFixer.Fix("حكم");
			else if (bidValue == "Bas")
				bidValue = ArabicFixer.Fix("بس");
			else if (bidValue == "Wala")
				bidValue = ArabicFixer.Fix("ولا");
			else if (bidValue == "ConfirmHokumBTN")
				bidValue = ArabicFixer.Fix("تأكيد الحكم");
		}

		return bidValue;
	}

	public override void StopTimer(){
		try{
		base.StopTimer ();
		}catch{
		}
	}

	protected override IEnumerator StartThisTimer(int PlayerNO,float WaitingTime,int FunToDOAfterEnd,bool JustCreatorDoIT){
		PlayersTimer [PlayerNO].SetActive (true);
		WaitingTime = WaitingTime*SliderSmothnessMultiplier;
		float ActualTimerTikValue = TimerTikValue*SliderSmothnessMultiplier;
		PlayersTimer [PlayerNO].GetComponent<Slider> ().maxValue = WaitingTime;
		PlayersTimer [PlayerNO].GetComponent<Slider> ().value = WaitingTime;
		while (WaitingTime > 0) {
			PlayersTimer [PlayerNO].GetComponent<Slider> ().value -= ActualTimerTikValue;
			WaitingTime-=ActualTimerTikValue;
			yield return new WaitForSeconds (TimerTikValue*0.1f);
		}
		PlayersTimer [PlayerNO].SetActive (false);
		if (JustCreatorDoIT && Player.Instance.GameCreator)
			PlayersManager6.Instance.MasterTakeControl (FunToDOAfterEnd);
		else if(!JustCreatorDoIT)
			PlayersManager6.Instance.MasterTakeControl (FunToDOAfterEnd);
	}

	public void ShowExposedCard(string CardID){
		ExposedCardGO.SetActive (true);
		ExposedCardGO.GetComponent<Card> ().CardID = CardID;
		ExposedCardGO.GetComponent<Card> ().SetCardCoverOrFace ();
	}

	public void GiveExposedCardToThisPlayer(int PlayerID){
		StartCoroutine (GiveExposedCardToThisPlayerCor(PlayerID));
	}

	private IEnumerator GiveExposedCardToThisPlayerCor(int PlayerID){
		ExposedCardGO.GetComponent<Animator> ().SetInteger ("CollectorPlayerID",PlayerID);
		yield return new WaitForSeconds (1);
		ExposedCardGO.SetActive (false);
	}

	public void ShowBidWindow1(bool ShowSunBTN,bool ShowHokumBTN,bool ShowHokumConfirmBTN,bool AshkalBTN,bool ShowBasBTN){
		BidWindows1.SetActive (true);
		BidWindows1BTNS[0].SetActive (ShowSunBTN);
		BidWindows1BTNS[1].SetActive (ShowHokumBTN);
		BidWindows1BTNS[2].SetActive (ShowHokumConfirmBTN);
		BidWindows1BTNS[3].SetActive (AshkalBTN);
		BidWindows1BTNS[4].SetActive (ShowBasBTN);
	}

	public void ShowBidWindow2(bool ShowSunBTN,bool ShowHokumBTN,bool ShowHokumConfirmBTN,bool AshkalBTN,bool ShowBasBTN){
		BidWindows2.SetActive (true);
		BidWindows2BTNS[0].SetActive (ShowSunBTN);
		BidWindows2BTNS[1].SetActive (ShowHokumBTN);
		BidWindows2BTNS[2].SetActive (ShowHokumConfirmBTN);
		BidWindows2BTNS[3].SetActive (AshkalBTN);
		BidWindows2BTNS[4].SetActive (ShowBasBTN);
	}

	private void OnDestroy(){
		StopAllCoroutines ();
	}
}