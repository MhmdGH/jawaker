﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Card7 : Card,IPointerDownHandler,IPointerUpHandler{
	
	#region IPointerUpHandler implementation
	public void OnPointerUp (PointerEventData eventData)
	{
		PlayersManager7.Instance.DeActiveAllPlaces ();
		try{StopCoroutine (ChooseCardCor);}catch{
		}
		JackarooManager7.Instance.ArrangeCards ();
	}
	#endregion

	#region IPointerDownHandler implementation
	public void OnPointerDown (PointerEventData eventData)
	{
		if (!CanDrag)
			return;
		ChooseCardCor = CardUp ();
		StartCoroutine (ChooseCardCor);
	}
	#endregion

	#region implemented abstract members of Card

	public override void ThroughIt (int StartPos)
	{
		GetComponent<Animator> ().enabled = true;
		GetComponent<Animator> ().SetInteger ("Thorugh",StartPos);
	}

	#endregion

	#region implemented abstract members of Card

	public override void SendCard ()
	{
		PlayersManager7.Instance.ThrowCard (this.gameObject);
	}

	#endregion

	public override void OnEndDrag(PointerEventData eventData){
		if (!CanDrag)
			return;
		GetComponent<Image> ().color = new Color (1,1,1,1);
		PlayersManagerParent.PlayerHoldingCard = false;
		if (PutCardOnBoard ())
			SendCard ();
		else
			ReturnCardBack ();
	}
}