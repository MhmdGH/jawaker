using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardsInfos7{
	
	public string[] CardTypes,CardNumpers;
	public List<string> HalfOfCards;
	public CardsInfos7(){
		CardTypes = new string[]{ "D","H","S","C"};
		CardNumpers = new string[]{"02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14"};
		HalfOfCards = new List<string> ();
		for (int i = 0; i < CardNumpers.Length; i++)
			for (int u = 0; u < CardTypes.Length; u++)
			HalfOfCards.Add (CardTypes[u]+""+CardNumpers[i]);
	}
}