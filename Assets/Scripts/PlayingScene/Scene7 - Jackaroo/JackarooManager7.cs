﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class JackarooManager7 : CardsManager
{

	public static JackarooManager7 Instance;
	public GameObject[] MyPlayerCardsGO,PlayersCardsContentGO;
	public GameObject CardGO;
	[HideInInspector]
	public PlayersCardsOB1 Cards;
	private List<string> ModifeidCardList;
	private int Margin=65;

	void Awake ()
	{
		Instance = this;
	}

	void Start ()
	{
		ModifeidCardList = new List<string> ();
	}

	public void SetAndArrangeMyCards ()
	{
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (Cards.PlayersCards [PlayerID].PlayerCardsList.Count > MyPlayerCardsGO.Length)
			CreateAndAssignMyCards ();
		for (int i = 0; i < PlayersCardsContentGO [0].transform.childCount; i++)
			PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card7> ().CardID = "0";
		//ForWatcher
		if (PlayerID == -1) {
			for (int i = 0; i < PlayersCardsContentGO [0].transform.childCount; i++)
				PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card7> ().CardID = Cards.PlayersCards [4].PlayerCardsList [i];
			return;
		} else {
			List<string> CardsList = Cards.PlayersCards [PlayerID].PlayerCardsList;
			ModifeidCardList.Clear ();

			for (int i = 0; i < CardsList.Count; i++)
				if (!CardsList [i].StartsWith ("0"))
					ModifeidCardList.Add (CardsList [i]);
			for (int i = 0; i < ModifeidCardList.Count; i++) {
				PlayersCardsContentGO [0].transform.GetChild (i).gameObject.SetActive (true);
				PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card7> ().CardID = ModifeidCardList [i];
			}
			for (int i = 0; i < PlayersCardsContentGO [0].transform.childCount; i++)
				if (PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card7> ().CardID.StartsWith ("0"))
					PlayersCardsContentGO [0].transform.GetChild (i).gameObject.SetActive (false);
		}
		List<string> DCards = new List<string> ();
		List<string> SCards = new List<string> ();
		List<string> HCards = new List<string> ();
		List<string> CCards = new List<string> ();
		for (int i = 0; i < ModifeidCardList.Count; i++) {
			if (ModifeidCardList [i].StartsWith ("D"))
				DCards.Add (ModifeidCardList [i]);
			else if (ModifeidCardList [i].StartsWith ("S"))
				SCards.Add (ModifeidCardList [i]);
			else if (ModifeidCardList [i].StartsWith ("H"))
				HCards.Add (ModifeidCardList [i]);
			else if (ModifeidCardList [i].StartsWith ("C"))
				CCards.Add (ModifeidCardList [i]);
		}
		DCards.Sort ();
		SCards.Sort ();
		HCards.Sort ();
		CCards.Sort ();
		int Counter = 0;
		for (int i = 0; i < DCards.Count; i++) {
			PlayersCardsContentGO [0].transform.GetChild (Counter).GetComponent<Card7> ().CardID = DCards [i];
			Counter++;
		}
		for (int i = 0; i < SCards.Count; i++) {
			PlayersCardsContentGO [0].transform.GetChild (Counter).GetComponent<Card7> ().CardID = SCards [i];
			Counter++;
		}
		for (int i = 0; i < HCards.Count; i++) {
			PlayersCardsContentGO [0].transform.GetChild (Counter).GetComponent<Card7> ().CardID = HCards [i];
			Counter++;
		}
		for (int i = 0; i < CCards.Count; i++) {
			PlayersCardsContentGO [0].transform.GetChild (Counter).GetComponent<Card7> ().CardID = CCards [i];
			Counter++;
		}
		for (int i = 0; i < PlayersCardsContentGO [0].transform.childCount; i++) {
			PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card> ().CanDrag = true;
			PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<Card7> ().SetCardCoverOrFace ();
		}
		ArrangeCards ();
	}

	public void ArrangeCards ()
	{
		int FullMargin = 0, ActiveChilds = 0;
		for (int i = 0; i < PlayersCardsContentGO [0].transform.childCount; i++)
			if (PlayersCardsContentGO [0].transform.GetChild (i).gameObject.activeSelf)
				ActiveChilds++;
		FullMargin = Margin * (ActiveChilds);
		for (int i = 0; i < PlayersCardsContentGO [0].transform.childCount; i++) {
			int TargetedXPos = (-FullMargin / 2) + (Margin * i);
			PlayersCardsContentGO [0].transform.GetChild (i).GetComponent<RectTransform> ().localPosition = new Vector3 (TargetedXPos, 0,0);
		}
	}

	public void RefreshOtherPlayersCards ()
	{
		List<CardList> OnlinePlayersCards = PlayersManager7.Instance.TempCurrenRoom.GameOB.Cards.PlayersCards;
		for (int i = 1; i < OnlinePlayersCards.Count; i++)
			for (int u = 0; u < OnlinePlayersCards [i].PlayerCardsList.Count; u++)
				PlayersCardsContentGO [PlayersManager7.Instance.FromOnlineToLocal (i)].
				transform.GetChild (u).gameObject.SetActive (OnlinePlayersCards [i].PlayerCardsList [u].Length > 2);
	}

	public void AssignPlayerCardsAndActivateOthersCards (bool FirstOrSocend)
	{
		StartCoroutine (AssignPlayerCardsAndActivateOthersCardsCor (FirstOrSocend));
	}

	private IEnumerator AssignPlayerCardsAndActivateOthersCardsCor (bool FourCards)
	{
		int TargetedActiveCardsNO = 5;
		if (FourCards) {
			Cards = new PlayersCardsOB1 ();
			TargetedActiveCardsNO = 4;
		}
		CreateAndAssignMyCards ();
		for (int i = 0; i < TargetedActiveCardsNO; i++)
			for (int u = 1; u < 4; u++)
				PlayersCardsContentGO [u].transform.GetChild (i).gameObject.SetActive (true);
		yield return null;
	}

	private void CreateAndAssignMyCards ()
	{
		for (int i = 0; i < MyPlayerCardsGO.Length; i++)
			MyPlayerCardsGO [i].gameObject.SetActive (false);
		for (int i = 0; i < 5; i++) {
			MyPlayerCardsGO [i].gameObject.SetActive (true);
			GameObject NewCard = MyPlayerCardsGO [i].gameObject;
			NewCard.GetComponent<Card7> ().CanDrag = false;
			NewCard.GetComponent<Card7> ().IsCover = false;
			MyPlayerCardsGO [i] = NewCard;
		}
	}

	CardsInfos7 NewCardsInfo;
	private bool FirstTime;
	public void SetCardsForPlayers (bool FourCards, bool NewCards)
	{
//		Debug.Log ("*****************Starts");
//		Debug.Log ("FourCards:"+FourCards+"||NewCards :"+NewCards);
		for (int i = 0; i < 4; i++)
			Cards.PlayersCards [i].PlayerCardsList.Clear ();
		if (NewCards) 
			NewCardsInfo = new CardsInfos7 ();
//		Debug.Log ("NewCardsInfo\t"+NewCardsInfo.HalfOfCards.Count);
		int AddThisNoOfCards = 5;
		if (FourCards)
			AddThisNoOfCards = 4;
		for (int u = 0; u < 4; u++) {
			for (int i = 0; i < AddThisNoOfCards; i++) {
				int RandomNO = UnityEngine.Random.Range (0, NewCardsInfo.HalfOfCards.Count);
				Cards.PlayersCards [u].PlayerCardsList.Add (NewCardsInfo.HalfOfCards [RandomNO]);
				NewCardsInfo.HalfOfCards.RemoveAt (RandomNO);
			}
		}
//		Debug.Log ("*****************Ends");
	}

	private void OnDestroy ()
	{
		StopAllCoroutines ();
	}
}