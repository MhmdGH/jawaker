using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

[Serializable]
public class Game7:GameOBParent{
	public Game7(){
		PlayersScores = new int[4]{0,0,0,0};
		Cards = new PlayersCardsOB1 ();
		CurrentRound = new Round1 ();
	}
}