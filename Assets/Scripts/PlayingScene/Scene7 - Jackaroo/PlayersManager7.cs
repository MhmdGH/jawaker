﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class PlayersManager7 : PlayersManagerParent
{
	public static PlayersManager7 Instance;
	public GameObject CardGO, TableGO;
	public GameObject[] PlacesGO, Players0Stones, Players1Stones, Players2Stones, Players3Stones,
		SafeZoneParents;
	public List<GameObject[]> PlayersStonesContainter;


	public override void Awake ()
	{
		base.Awake ();
		Instance = this;
	}

	public override void Start ()
	{
		CardsListGO = PlayersCardsGO [0].transform.GetChild (1).gameObject;
		TempCurrenRoom = new JsonRoomO ("Default", new string[4]{ "0", "0", "0", "0" }, new Game7 ());
		base.Start ();
		SceneNotReady = false;
		SetStonesContainterList ();
	}

	void SetStonesContainterList ()
	{
		PlayersStonesContainter = new List<GameObject[]> (4);
		PlayersStonesContainter.Add (Players0Stones);
		PlayersStonesContainter.Add (Players1Stones);
		PlayersStonesContainter.Add (Players2Stones);
		PlayersStonesContainter.Add (Players3Stones);
	}

	void RotateTableAndSetPlacesColors ()
	{
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (PlayerID == -1)
			PlayerID = 0;
		Color TargetedColor = new Color ();
		if (PlayerID == 0)
			TargetedColor = Color.red;
		else if (PlayerID == 1)
			TargetedColor = Color.blue;
		else if (PlayerID == 3)
			TargetedColor = Color.yellow;
		else
			TargetedColor = Color.green;
		TableGO.GetComponent<RectTransform> ().localRotation = Quaternion.Euler (0, 0, PlayerID * 90);
		for (int i = 0; i < PlacesGO.Length; i++) {
			PlacesGO [i].GetComponent<Image> ().color = TargetedColor;
			PlacesGO [i].SetActive (false);
		}
		for (int u = 0; u < 4; u++)
			SafeZoneParents [PlayerID].transform.GetChild (u).transform.GetComponent<Image> ().color = TargetedColor;
	}

	//Main Reciever Fun
	public override void UpdateRoom (JsonRoomO CurrentRoomJSON)
	{
		
		base.UpdateRoom (CurrentRoomJSON);
		
		if (TempCurrenRoom.NoMaster)
			AssignNewMasterOrRemoveRoom ();
		if (TempCurrenRoom.Update == 0) {
			PlayingFireBaseManager7.Instance.SetOnDissconnectHandle ();
			return;
		}
		PlayingWindowsManager7.Instance.StopTimer ();
		switch (TempCurrenRoom.ActionID) {
		case -1:
			UpdateRoomPhaseMinus1 ();
			break;
		case 0:
			UpdateRoomPhase0 ();
			break;
		case 2:
			UpdateRoomPhase2 ();
			break;
		case 3:
			UpdateRoomPhase3 ();
			break;
		case 4:
			UpdateRoomPhase4 ();
			break;
		case 5:
			UpdateRoomPhase5 ();
			break;
		case 6:
			UpdateRoomPhase6 ();
			break;
		case 7:
			UpdateRoomPhase7 ();
			break;
		case 8:
			UpdateRoomPhase8 ();
			break;
		case 9:
			UpdateRoomPhase9 ();
			break;
		case 10:
			UpdateRoomPhase10 ();
			break;
		case 12:
			UpdateRoomPhase12 ();
			break;
		case 13:
			UpdateRoomPhase13 ();
			break;
		case 14:
			UpdateRoomPhase14 ();
			break;
		case 15:
			UpdateRoomPhase15 ();
			break;
		}
	}

	void UpdateRoomPhaseMinus1 ()
	{
		AudioManager.Instance.PlaySound (0);
		if (!Player.Instance.GameCreator)
			return;
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++)
			TempCurrenRoom.PlayersReady [i] = false;
		JackarooManager7.Instance.SetCardsForPlayers (TempCurrenRoom.GameOB.JackarooDetailsOB.RoundNO != 3, TempCurrenRoom.GameOB.JackarooDetailsOB.RoundNO == 1);
		TempCurrenRoom.ActionID = 4;
		TempCurrenRoom.Available = false;
		for (int i = 0; i < 4; i++)
			TempCurrenRoom.GameOB.Cards.PlayersCards [i].PlayerCardsList = JackarooManager7.Instance.Cards.PlayersCards [i].PlayerCardsList;
		PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase0 ()
	{
		if (TempCurrenRoom.ReloadScene)
			ReloadScene ();
	}

	void ReloadScene ()
	{
		TempCurrenRoom.ReloadScene = false;
		TempCurrenRoom.ActionID = 3;
		SceneNotReady = true;
		if (Player.Instance.GameCreator)
			PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);

	}

	public void MasterTakeControl (int funToDOAfterEnd)
	{
		if (funToDOAfterEnd == 1)
			MasterOrPlayerThrowCardNOW ();
		else if (funToDOAfterEnd == 2)
			AutoSelecteStone ();
		else if (funToDOAfterEnd == 3)
			ChooseAce1Or11Now ();
		else if (funToDOAfterEnd == 4)
			AutoSwitchNow ();
		else if (funToDOAfterEnd == 5)
			ChooseAnyStoneToMove5StepsNow ();
	}

	void ChooseAnyStoneToMove5StepsNow ()
	{
		int PlayerID = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		int PlayerSeatStatus = TempCurrenRoom.SeatsStatus [PlayerID];
		if (MyTurnToPlay () && PlayerSeatStatus == 1) {//If Player Is Here
			StoneBTNClick (false, FirstAvailbeStoneCanMove5Steps ().Item1, FirstAvailbeStoneCanMove5Steps ().Item2);
		} else if (Player.Instance.GameCreator && PlayerSeatStatus == 0) { //If player Not Here
			if (ThereIsStonesCanMove5Steps ())
				StoneBTNClick (false, FirstAvailbeStoneCanMove5Steps ().Item1, FirstAvailbeStoneCanMove5Steps ().Item2);
			else {
				TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = NextNO (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay);
				TempCurrenRoom.ActionID = 5;
				PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
			}
		}
	}

	private Tuple<int,int> FirstAvailbeStoneCanMove5Steps ()
	{
		int PlayerIDTurnToPlayer = TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay];
		for (int u = 0; u < 4; u++)
			if (PlayersStonesContainter [PlayerIDTurnToPlayer] [u].GetComponent<Stone> ().CanBePlayedWithThisCard (5) == 3)
				return Tuple.Create (PlayerIDTurnToPlayer, u);
		for (int i = 0; i < 4; i++)
			for (int u = 0; u < 4; u++)
				if (PlayersStonesContainter [i] [u].GetComponent<Stone> ().CanBePlayedWithThisCard (5) == 3)
					return Tuple.Create (i, u);
		throw new NotImplementedException ();
	}

	void AutoSwitchNow ()
	{
		int PlayerID = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		int PlayerSeatStatus = TempCurrenRoom.SeatsStatus [PlayerID];
		if (MyTurnToPlay () && PlayerSeatStatus == 1) {//If Player Is Here
			IfPlayerHoldingCardGetItBack ();
			ShowStonesThatCanBeSwitched (true);
		} else if (Player.Instance.GameCreator && PlayerSeatStatus == 0) {//If player Not Here
			TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay] = 0;
			ShowStonesThatCanBeSwitched (true);
		}
	}

	void ChooseAce1Or11Now ()
	{
		int PlayerID = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		int CurrentStoneNoHolder = TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.StoneNO;
		int PlayerSeatStatus = TempCurrenRoom.SeatsStatus [PlayerID];
		if (MyTurnToPlay () && PlayerSeatStatus == 1) {//If Player Is Here
			IfPlayerHoldingCardGetItBack ();
			if (PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerID]] [CurrentStoneNoHolder].GetComponent<Stone> ().Ace1Or11 (false))
				AceChoose1Or11 (11);
			else
				AceChoose1Or11 (1);
		} else if (Player.Instance.GameCreator && PlayerSeatStatus == 0) {//If player Not Here
			TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay] = 0;
			if (PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerID]] [CurrentStoneNoHolder].GetComponent<Stone> ().Ace1Or11 (false))
				AceChoose1Or11 (11);
			else
				AceChoose1Or11 (1);
		}
	}

	void AutoSelecteStone ()
	{
		int PlayerID = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		int PlayerSeatStatus = TempCurrenRoom.SeatsStatus [PlayerID];
		//If Player Is Here
		if (MyTurnToPlay () && PlayerSeatStatus == 1) {
			for (int i = 0; i < 4; i++) {
				if (PlayingWindowsManager7.Instance.StoneSelections [i].activeSelf) {
					SelectStoneNoToMove (i);
					return;
				}
			}
			//If Player Not Here, Master Do IT
		} else if (Player.Instance.GameCreator && PlayerSeatStatus == 0) {
			TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay] = 0;
			for (int i = 0; i < 4; i++) {
				if (PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerID]] [i].GetComponent<Stone> ().
					CanBePlayedWithThisCard (GetCardNO (TempCurrenRoom.GameOB.JackarooDetailsOB.CurrentCardID)) != 0) {
					SelectStoneNoToMove (i);
					return;
				}
			}
			TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = NextNO (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay);
			TempCurrenRoom.ActionID = 5;
			PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
		}
	}

	void MasterOrPlayerThrowCardNOW ()
	{
		int PlayerID = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		int PlayerSeatStatus = TempCurrenRoom.SeatsStatus [PlayerID];
		if (!HaveCards (PlayerID))
			return;
		//If Player Is Here
		if (MyTurnToPlay () && PlayerSeatStatus == 1) {
			IfPlayerHoldingCardGetItBack ();
			for (int i = 0; i < CardsListGO.transform.childCount; i++) {
				if (!CardsListGO.transform.GetChild (i).gameObject.activeSelf)
					continue;
				if (CardsListGO.transform.GetChild (i).GetComponent<Card> ().CardID== FirstAvailabeCardOnOtherPlayer()) {
					CardsListGO.transform.GetChild (i).GetComponent<Card7> ().SendCard ();
					CheckPlayerKick ();
					return;
				}
			}
			//If Player Not Here, Master Do IT
		} else if (Player.Instance.GameCreator && PlayerSeatStatus == 0) {
			TempCurrenRoom.SeatsStatus [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay] = 0;
			if (NoOfStonesOnBoard (PlayerID) == 0 && !PlayerHaveAceOrSheikh ())
				StartCoroutine (RemoveMyCardsSlowly ());
			else
				ThrowCardNow (FirstAvailabeCardOnOtherPlayer ());
		}
	}

	List<string> ThisPlayerCardsList;

	string FirstAvailabeCardOnOtherPlayer ()
	{
		int PlayerID = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		ThisPlayerCardsList = TempCurrenRoom.GameOB.
			Cards.PlayersCards [PlayerID].PlayerCardsList;
		if (NoOfStonesOnBoard (PlayerID) == 0) {
			for (int i = 0; i < ThisPlayerCardsList.Count; i++) {
				if (ThisPlayerCardsList [i].Length > 2&&GetCardNO (ThisPlayerCardsList [i]) >= 13)
					return ThisPlayerCardsList [i];
			}
		} else {
			for (int i = 0; i < ThisPlayerCardsList.Count; i++) {//Any Availabe Card Availbe To Play in Other Player Hand
				for (int u = 0; u < 4; u++) {
					if (ThisPlayerCardsList [i].Length > 2 &&
					    PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerID]] [u].GetComponent<Stone> ().CanBePlayedWithThisCard (GetCardNO (ThisPlayerCardsList [i])) != 0)
						return ThisPlayerCardsList [i];
				}
			}
			for (int i = 0; i < ThisPlayerCardsList.Count; i++)
				if (ThisPlayerCardsList [i].Length > 2)
					return ThisPlayerCardsList [i];
		}
		return ThisPlayerCardsList [0];
	}

	public void RestartGame ()
	{
		PlayingWindowsManager7.Instance.WinsWindow.SetActive (false);
		Game7 NewGameOB = new Game7 ();
		for (int i = 0; i < NewGameOB.PlayersScores.Length; i++)
			NewGameOB.PlayersScores [i] = 0;
		TempCurrenRoom.GameOB = NewGameOB;
		TempCurrenRoom.ReloadScene = true;
		TempCurrenRoom.ActionID = 0;
		PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase15 ()
	{
		//Game Ends
		AddWinsPointsForPlayers(OneOfTeamsWins());
		if (MainFireBaseManager.Instance.IsACompition)
			InnerSetCompData ();
		PlayingWindowsManager7.Instance.ShowWindWindow (OneOfTeamsWins ());
	}
	private int ThisWinnerNO;
	private void InnerSetCompData ()
	{
		if (Player.Instance.GameCreator) {
			for (int i = 0; i < TempCurrenRoom.DatabasePlayersIDs.Length; i++)
				if (TempCurrenRoom.DatabasePlayersIDs [i].Length < 4)
					return;
			if (ThisWinnerNO == 0) {
				SetCompData (TempCurrenRoom.DatabasePlayersIDs [0], TempCurrenRoom.DatabasePlayersIDs [2],
					TempCurrenRoom.PlayersFBIDs [0], TempCurrenRoom.PlayersFBIDs [2]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [1]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [3]);
			} else {
				SetCompData (TempCurrenRoom.DatabasePlayersIDs [1], TempCurrenRoom.DatabasePlayersIDs [3],
					TempCurrenRoom.PlayersFBIDs [1], TempCurrenRoom.PlayersFBIDs [3]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [0]);
				PlayingFireBaseFather.ParentInstance.RemoveThisPlayerFromComp (TempCurrenRoom.DatabasePlayersIDs [2]);
			}
		}
	}

	private void AddWinsPointsForPlayers(int WinnerNO){
		
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (WinnerNO == 0 && (PlayerID == 0 || PlayerID == 2)) {
			Player.Instance.AddXPsAndCLubPoints (XPsValue);
			Player.Instance.Coins += ScoreValue;
		}else if (WinnerNO == 1 && (PlayerID == 1 || PlayerID == 3)) {
			Player.Instance.AddXPsAndCLubPoints (XPsValue);
			Player.Instance.Coins += ScoreValue;
		}
		ThisWinnerNO = WinnerNO;
	}

	void UpdateRoomPhase14 ()
	{
	}

	void UpdateRoomPhase13 ()
	{
		//Full Round Ends
		if (!Player.Instance.GameCreator)
			return;
		if (FullGameEnds ()) {
			TempCurrenRoom.ActionID = 15;
			PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
		}
	}

	bool FullGameEnds ()
	{
		return false;
	}

	private int TeamScore (bool Team1)
	{
		int Team1Score = 0;
		int Team2Score = 0;
		Team1Score = TempCurrenRoom.GameOB.PlayersScores [0];
		Team2Score = TempCurrenRoom.GameOB.PlayersScores [1];
		if (Team1)
			return Team1Score;
		else
			return Team2Score;
	}

	void UpdateRoomPhase12 ()
	{
		//COllect Cards Here And Throw it to winner Player
	}

	#region implemented abstract members of PlayersManagerParent

	public override void SetLastRoundCards ()
	{
		return;//set it in your fukin ass bitch
	}

	#endregion

	private void UpdateRoomPhase11 ()
	{
		
	}

	//Player Move Stone Here
	int CurrentPlayedPlayerID, CurrentPlayedStoneNO, CurrentPlayedStoneTargetedPlaceNO, DevideMovedValue;

	private void UpdateRoomPhase10 ()
	{
//		Debug.Log ("UpdateRoomPhase10");
		CurrentPlayedPlayerID = TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.MoverID;
		CurrentPlayedStoneNO = TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.StoneNO;
		CurrentPlayedStoneTargetedPlaceNO = TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.TargetPlace;
		PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [CurrentPlayedPlayerID]] [CurrentPlayedStoneNO].GetComponent<Stone> ().Move (CurrentPlayedStoneTargetedPlaceNO);
		if (!Player.Instance.GameCreator)
			return;
		if (OneOfTeamsWins () != -1) {
			TempCurrenRoom.ActionID = 15;
			PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
		} else {
			CheckWinnerAndControllers ();
			if (TempCurrenRoom.GameOB.JackarooDetailsOB.DevideStatus) {
				TempCurrenRoom.GameOB.JackarooDetailsOB.CurrentCardID = "D03";
				if (TempCurrenRoom.GameOB.JackarooDetailsOB.DevideChoosed3Or4)
					TempCurrenRoom.GameOB.JackarooDetailsOB.CurrentCardID = "D04";	
				TempCurrenRoom.ActionID = 9;
				PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
			} else {
				TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = NextNO (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay);
				TempCurrenRoom.ActionID = 5;
				PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
			}
		}
	}

	int OneOfTeamsWins ()
	{
		if (IsThisPlayerFinish (0) && IsThisPlayerFinish (2))
			return 0;
		else if (IsThisPlayerFinish (1) && IsThisPlayerFinish (3))
			return 1;
		else
			return -1;
	}

	void CheckWinnerAndControllers ()
	{
		for (int i = 0; i < 4; i++)
			if (IsThisPlayerFinish (i) && ControllerID (i) != MyPartnerID (i))
				TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [i] = MyPartnerID (i);
	}

	private int ControllerID (int PlayerID)
	{
		return TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerID];
	}

	bool IsThisPlayerFinish (int FinisherID)
	{
		for (int i = 0; i < 4; i++)
			if (!PlayersStonesContainter [FinisherID] [i].GetComponent<Stone> ().InTheSafeZone)
				return false;
		return true;
	}

	bool IHaveThisCardType (string Cardtype)
	{
		for (int i = 0; i < CardsListGO.transform.childCount; i++) {
			if (CardsListGO.transform.GetChild (i).GetComponent<Card7> ().CardID.StartsWith (Cardtype))
				return true;
		}
		return false;
	}

	private int FromOnlineToLocalStonePlace (int OtherOnlinePlayerID, int StonePlaceNO)
	{
		int PlayerIDOnMyDevice = FromOnlineToLocal (OtherOnlinePlayerID);
		return StonePlaceNO + (PlayerIDOnMyDevice * 19);
	}

	public void ThrowCard (GameObject Cardjo)
	{
		if (MyTurnToPlay () && TempCurrenRoom.ActionID == 5&&CanThrowCard)
			StartCoroutine (RemoveOrderly (Cardjo));
		else
			Cardjo.GetComponent<Card7> ().ReturnCardBack ();
	}

	private bool CanThrowCard = true;

	private IEnumerator RemoveOrderly (GameObject Cardjo)
	{
		CanThrowCard = false;
		ThrowCardNow (Cardjo.GetComponent<Card7> ().CardID);
		Cardjo.SetActive (false);
		Cardjo.GetComponent<Card> ().ReturnCardBack ();
		yield return new WaitForSeconds (0.03f);
		JackarooManager7.Instance.SetAndArrangeMyCards ();
		yield return new WaitForSeconds (1);
		CanThrowCard = true;
		yield return null;
	}

	void ThrowCardNow (string CardID)
	{
		string CardIDString = CardID;
		int PlayerTurn = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		TempCurrenRoom.GameOB.JackarooDetailsOB.CurrentCardID = CardID;
		TempCurrenRoom.GameOB.JackarooDetailsOB.CurrentThrower = PlayerTurn;
		TempCurrenRoom.GameOB.CurrentRound.PlayersCards [PlayerTurn] = CardIDString;
		RemoveThisCardFromOnlineCardsList (PlayerTurn, CardIDString);
		TempCurrenRoom.ActionID = 9;
		PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	private void RemoveThisCardFromOnlineCardsList (int PlayerNO, string CardID)
	{
		for (int i = 0; i < TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList.Count; i++) {
			if (TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList [i] == CardID) {
				TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerNO].PlayerCardsList [i] = "0";
				return;
			}
		}
	}


	//Reciev Throwd Card,Show Stones Selection
	void UpdateRoomPhase9 ()
	{
		int PlayerIDHolder = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		int CurrentCardNO = GetCardNO (TempCurrenRoom.GameOB.JackarooDetailsOB.CurrentCardID); 
		bool IsComplex = TempCurrenRoom.GameOB.JackarooDetailsOB.Complex;
		if ((!IsComplex) ||
		    (IsComplex && CurrentCardNO != 10)) {
			if (IsComplex && CurrentCardNO == 5)
				PlayingWindowsManager7.Instance.SetTimer (FromOnlineToLocal (PlayerIDHolder),
					TimerWaitingTime (PlayerIDHolder), 5, false);
			else
				PlayingWindowsManager7.Instance.SetTimer (FromOnlineToLocal (PlayerIDHolder),
					TimerWaitingTime (PlayerIDHolder), 2, false);
		}
		if (!TempCurrenRoom.GameOB.JackarooDetailsOB.DevideStatus) {
			CardsThrowerMangaer.Instance.ThrowThisCard (TempCurrenRoom.GameOB.JackarooDetailsOB.CurrentCardID,
				FromOnlineToLocal (TempCurrenRoom.GameOB.JackarooDetailsOB.CurrentThrower));
		}
		if (Player.Instance.GameCreator)
			TempCurrenRoom.GameOB.JackarooDetailsOB.DevideStatus = false;
		if (IsComplex && CurrentCardNO == 10) {
			if (!Player.Instance.GameCreator)
				return;
			if (HaveCards (PrevPlayer (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay)))
				RemoveCardFromThisPlayer (PrevPlayer (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay));
			TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = NextNO (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay);
			TempCurrenRoom.ActionID = 5;
			PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
		}
		if (IsComplex && CurrentCardNO == 5) {
			if (!MyTurnToPlay ())
				return;
			if (ThereIsStonesCanMove5Steps ())
				ShowStonesThatCanBeMove5Steps ();
			else {
				TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = NextNO (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay);
				TempCurrenRoom.ActionID = 5;
				PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
			}
			return;
		}
		if (!MyTurnToPlay ())
			return;
		ShowStoneSelection ();
	}

	void ShowStonesThatCanBeMove5Steps ()
	{
		for (int i = 0; i < 4; i++)
			for (int u = 0; u < 4; u++)
				if (PlayersStonesContainter [i] [u].GetComponent<Stone> ().CanBePlayedWithThisCard (5) == 3)
					PlayersStonesContainter [i] [u].GetComponent<Stone> ().Move5BTNGO.SetActive (true);
	}

	void RemoveCardFromThisPlayer (int PlayerIDToRemoveCardFrom)
	{
		List<string> CardsList = TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerIDToRemoveCardFrom].PlayerCardsList;
		for (int i = 0; i < CardsList.Count; i++)
			if (CardsList [i].Length > 2) {
				CardsList [i] = "0";
				break;
			}
	}

	int PrevPlayer (int ThisPlayerID)
	{
		ThisPlayerID--;
		if (ThisPlayerID < 0)
			ThisPlayerID = 3;
		return ThisPlayerID;
	}


	int AvailableStonesToMove ()
	{
		int AvailbeCounter = 0;
		int PlayerIDHolder = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		for (int i = 0; i < 4; i++) {
			if (PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerIDHolder]] [i].GetComponent<Stone> ().
				CanBePlayedWithThisCard (GetCardNO (TempCurrenRoom.GameOB.JackarooDetailsOB.CurrentCardID)) != 0) {
				AvailbeCounter++;
			}
		}
		return AvailbeCounter;
	}

	void ShowStoneSelection ()
	{
		PlayingWindowsManager7.Instance.StoneSelections [0].transform.parent.gameObject.SetActive (true);
		int AvailbeCounter = 0, FirstAvailbe = 0;
		for (int i = 0; i < 4; i++) {
			if (PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [Player.Instance.OnlinePlayerID]] [i].GetComponent<Stone> ().
				CanBePlayedWithThisCard (GetCardNO (TempCurrenRoom.GameOB.JackarooDetailsOB.CurrentCardID)) != 0) {
				PlayingWindowsManager7.Instance.StoneSelections [i].SetActive (true);
				FirstAvailbe = i;
				AvailbeCounter++;
			} else
				PlayingWindowsManager7.Instance.StoneSelections [i].SetActive (false);
		}
		if (AvailbeCounter == 1)
			SelectStoneNoToMove (FirstAvailbe);
		else if (AvailbeCounter == 0) {//No Stones Available to move
			TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = NextNO (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay);
			TempCurrenRoom.ActionID = 5;
			PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
		}
	}

	public void SelectStoneNoToMove (int StoneNO)
	{
		int CurrentStoneNoHolder = StoneNO;
		int CurrentCardNO = GetCardNO (TempCurrenRoom.GameOB.JackarooDetailsOB.CurrentCardID);
		PlayingWindowsManager7.Instance.StoneSelections [0].transform.parent.gameObject.SetActive (false);
		int PlayerIDHolder = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		if (CurrentCardNO != 14 && CurrentCardNO != 11 && CurrentCardNO != 7)
			TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersStones [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerIDHolder]].Stones [StoneNO] = PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerIDHolder]] [StoneNO].
			GetComponent<Stone> ().TargetedPlace;
		TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.MoverID = PlayerIDHolder;
		TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.StoneNO = CurrentStoneNoHolder;
		if (GetCardNO (TempCurrenRoom.GameOB.JackarooDetailsOB.CurrentCardID) == 14) {//Choose Ace Condition
			PlayingWindowsManager7.Instance.StopTimer ();
			PlayingWindowsManager7.Instance.SetTimer (FromOnlineToLocal (PlayerIDHolder),
				TimerWaitingTime (PlayerIDHolder), 3, false);
			if (MyTurnToPlay ())
				Show1AndOr11ForThisStone ();
			return;
		} else if (GetCardNO (TempCurrenRoom.GameOB.JackarooDetailsOB.CurrentCardID) == 11) {//Choose Jack To Switch
			PlayingWindowsManager7.Instance.StopTimer ();
			PlayingWindowsManager7.Instance.SetTimer (FromOnlineToLocal (PlayerIDHolder),
				TimerWaitingTime (PlayerIDHolder), 4, false);
			if (MyTurnToPlay ())
				ShowStonesThatCanBeSwitched (false);
			return;
		} else if (GetCardNO (TempCurrenRoom.GameOB.JackarooDetailsOB.CurrentCardID) == 7) {//Choose Seven Devided
			DoTheDevideStuff ();
			return;
		} else {
			TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.TargetPlace = PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerIDHolder]] [StoneNO].
			GetComponent<Stone> ().TargetedPlace;
			TempCurrenRoom.ActionID = 10;
			PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
		}
	}

	void DoTheDevideStuff ()
	{
		int PlayerIDHolder = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		int CurrentStoneNoHolder = TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.StoneNO;
		TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.TargetPlace = PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerIDHolder]] [CurrentStoneNoHolder].
			GetComponent<Stone> ().TargetedPlace;
		TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersStones [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerIDHolder]].Stones [CurrentStoneNoHolder] = TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.TargetPlace;
		TempCurrenRoom.GameOB.JackarooDetailsOB.DevideStatus = true;
		TempCurrenRoom.GameOB.JackarooDetailsOB.DevideChoosed3Or4 = PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerIDHolder]] [CurrentStoneNoHolder].
			GetComponent<Stone> ().DevideChoosed3Or4;
		TempCurrenRoom.ActionID = 10;
		PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void ShowStonesThatCanBeSwitched (bool AutoSwitch)
	{
		bool ThereIsCardsCanBeSwitched = false;
		int PlayerIDHolder = TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.MoverID;
		int CurrentStoneNoHolder = TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.StoneNO;
		for (int i = 0; i < 4; i++) {
			for (int u = 0; u < 4; u++) {
				if (i == PlayerIDHolder && u == CurrentStoneNoHolder)
					continue;
				if (AutoSwitch && PlayersStonesContainter [i] [u].GetComponent<Stone> ().CanSwitch ()) {
					ThereIsCardsCanBeSwitched = true;
					StoneBTNClick (true, i, u);
					return;
				}
				if (PlayersStonesContainter [i] [u].GetComponent<Stone> ().CanSwitch ()) {
					ThereIsCardsCanBeSwitched = true;
					PlayersStonesContainter [i] [u].GetComponent<Stone> ().SwitchBTNGO.SetActive (true);
				}
			}
		}
		if (!ThereIsCardsCanBeSwitched) {
			TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = NextNO (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay);
			TempCurrenRoom.ActionID = 5;
			PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
		}
	}

	public void StoneBTNClick (bool SwitchOrMove5, int OwnerID, int StoneNo)
	{
		for (int i = 0; i < 4; i++)
			for (int u = 0; u < 4; u++) {
				if (SwitchOrMove5)
					PlayersStonesContainter [i] [u].GetComponent<Stone> ().SwitchBTNGO.SetActive (false);
				else
					PlayersStonesContainter [i] [u].GetComponent<Stone> ().Move5BTNGO.SetActive (false);
			}
		if (SwitchOrMove5) {
			TempCurrenRoom.GameOB.JackarooDetailsOB.SwitchStone.Switcher2ID = OwnerID;
			TempCurrenRoom.GameOB.JackarooDetailsOB.SwitchStone.SwitchStone2NO = StoneNo;
			TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.TargetPlace = PlayersStonesContainter [OwnerID] [StoneNo].GetComponent<Stone> ().CurrentPlace;
		} else {
			TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersStones [OwnerID].Stones [StoneNo] = PlayersStonesContainter [OwnerID] [StoneNo].GetComponent<Stone> ().TargetedPlace;
			TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.MoverID = OwnerID;
			TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.StoneNO = StoneNo;
			TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.TargetPlace = PlayersStonesContainter [OwnerID] [StoneNo].GetComponent<Stone> ().TargetedPlace;
		}
		TempCurrenRoom.ActionID = 10;
		PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void Show1AndOr11ForThisStone ()
	{
		int PlayerIDHolder = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		int CurrentStoneNoHolder = TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.StoneNO;
		PlayingWindowsManager7.Instance.ShowAce1Or11GO.SetActive (true);
		PlayingWindowsManager7.Instance.ShowAce1Or11GO.transform.GetChild (0).gameObject.SetActive (false);
		PlayingWindowsManager7.Instance.ShowAce1Or11GO.transform.GetChild (1).gameObject.SetActive (false);
		if (PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerIDHolder]] [CurrentStoneNoHolder].GetComponent<Stone> ().Ace1Or11 (true))
			PlayingWindowsManager7.Instance.ShowAce1Or11GO.transform.GetChild (0).gameObject.SetActive (true);
		if (PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerIDHolder]] [CurrentStoneNoHolder].GetComponent<Stone> ().Ace1Or11 (false))
			PlayingWindowsManager7.Instance.ShowAce1Or11GO.transform.GetChild (1).gameObject.SetActive (true);
	}

	public void AceChoose1Or11 (int OneOr11)
	{
		int PlayerIDHolder = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		int CurrentStoneNoHolder = TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.StoneNO;
		PlayingWindowsManager7.Instance.ShowAce1Or11GO.SetActive (false);
		TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.TargetPlace = GetTargetPlaceForAce (OneOr11);
		TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersStones [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerIDHolder]].Stones [CurrentStoneNoHolder] = TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.TargetPlace;
		TempCurrenRoom.ActionID = 10;
		PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	int GetTargetPlaceForAce (int oneOr11)
	{
		int PlayerIDHolder = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		int CurrentStoneNoHolder = TempCurrenRoom.GameOB.JackarooDetailsOB.MoveStone.StoneNO;
		int ThisStoneCurrentPlace = PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerIDHolder]] [CurrentStoneNoHolder].GetComponent<Stone> ().CurrentPlace;
		if (ThisStoneCurrentPlace == -1)
			return PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerIDHolder]] [CurrentStoneNoHolder].GetComponent<Stone> ().GetThisPlayerBase (PlayerIDHolder);
		else
			return PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerIDHolder]] [CurrentStoneNoHolder].GetComponent<Stone> ().AddTheseSteps (oneOr11);//Target Place
	}

	void UpdateRoomPhase8 ()
	{
	}

	//Bidding Ends
	private void UpdateRoomPhase7 ()
	{
	}

	void UpdateRoomPhase6 ()
	{
	}

	private int CardNo, AvailablePlaceStatus;

	public void ShowAvailablePlaces (string StringCardNO)
	{
		if (!MyTurnToPlay ())
			return;
		int PlayerID = Player.Instance.OnlinePlayerID;
		CardNo = GetCardNO (StringCardNO);
		for (int i = 0; i < 4; i++) {
			AvailablePlaceStatus = PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerID]] [i].GetComponent<Stone> ().CanBePlayedWithThisCard (CardNo);
			if (AvailablePlaceStatus == 6) {
				CardNo = 1;
				AvailablePlaceStatus = 3;
			}
			if (NoOfStonesOnBoard (PlayerID) == 0) {
				if (CardNo >= 13) {
					PlacesGO [PlayerID * 19].SetActive (true);
				}
			} else {
				if (AvailablePlaceStatus != 0) {
					if (AvailablePlaceStatus == 1)
						PlacesGO [PlayerID * 19].SetActive (true);
					else if (AvailablePlaceStatus == 2) {
						int StartPos = PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerID]] [i].GetComponent<Stone> ().CurrentPlace -
						               PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerID]] [i].GetComponent<Stone> ().SafeZoneConstant () - 1;
						int StepsToMoves = 0;
						if (CardNo == 14)//Its a fukin ace
							CardNo = 1;
						if (CardNo == 7)//Its a Devide
							CardNo = 3;
						StepsToMoves = CardNo;
						if (StepsToMoves + StartPos > 4)
							return;
						for (int u = StartPos + 1; u <= StartPos + StepsToMoves; u++)
							SafeZoneParents [PlayerID].transform.GetChild (u).gameObject.SetActive (true);
					} else if (AvailablePlaceStatus == 3) {
						if (TempCurrenRoom.GameOB.JackarooDetailsOB.Complex && (CardNo == 10 || CardNo == 5))
							return;
						int StartPos = PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerID]] [i].GetComponent<Stone> ().CurrentPlace;
						int StepsToMoves = 0, SafeZoneCounter = 0, Actualu = 0;
						int StoneSafePos = PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerID]] [i].GetComponent<Stone> ().SafeZoneStartPos;
						bool InTheSafeZone = false;
						if (CardNo == 14)//Its a fukin ace
							CardNo = 11;
						StepsToMoves = CardNo;
						for (int u = StartPos + 1; u <= StepsToMoves + StartPos; u++) {
							Actualu = ActualPlace (u);
							if (Actualu == StoneSafePos || Actualu - 1 == StoneSafePos) {
								InTheSafeZone = true;
								if (Actualu == StoneSafePos) {
									PlacesGO [Actualu].SetActive (true);
									continue;
								}
							}
							if (InTheSafeZone) {
								SafeZoneParents [PlayerID].transform.GetChild (SafeZoneCounter).gameObject.SetActive (true);
								SafeZoneCounter++;
							} else
								PlacesGO [Actualu].SetActive (true);
						}
					} else if (AvailablePlaceStatus == 7) {
						int StartPos = PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerID]] [i].GetComponent<Stone> ().CurrentPlace;
						int StepsToMoves = 0, SafeZoneCounter = 0;
						bool InTheSafeZone = false;
						StepsToMoves = 4;
						for (int u = StartPos + 1; u <= StepsToMoves + StartPos; u++) {
							if (u == PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerID]] [i].GetComponent<Stone> ().SafeZoneStartPos) {
								InTheSafeZone = true;
								PlacesGO [ActualPlace (u)].SetActive (true);
								continue;
							}
							if (InTheSafeZone) {
								SafeZoneParents [PlayerID].transform.GetChild (SafeZoneCounter).gameObject.SetActive (true);
								SafeZoneCounter++;
							} else
								PlacesGO [ActualPlace (u)].SetActive (true);
						}
					} else if (AvailablePlaceStatus == 5) {
						int StartPos = PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerID]] [i].GetComponent<Stone> ().CurrentPlace;
						for (int u = StartPos - 1; u >= StartPos - 4; u--)
							PlacesGO [ActualPlace (u)].SetActive (true);
					}
				}
			}
		}
	}

	public void DeActiveAllPlaces ()
	{
		for (int i = 0; i < PlacesGO.Length; i++)
			PlacesGO [i].SetActive (false);
		for (int i = 0; i < SafeZoneParents.Length; i++)
			for (int u = 0; u < SafeZoneParents [i].transform.childCount; u++)
				SafeZoneParents [i].transform.GetChild (u).gameObject.SetActive (false);
	}

	int ActualPlace (int targetedPlaceNO)
	{
		if (targetedPlaceNO >= 76 && targetedPlaceNO < 100)
			targetedPlaceNO -= 76;
		else if (targetedPlaceNO < 0)
			targetedPlaceNO += 76;
		return targetedPlaceNO;
	}

	//If Player Dont Have Ace or K to start game,And Show Availbe Cards
	void UpdateRoomPhase5 ()
	{
//		Debug.Log ("UpdateRoomPhase5");

		if (NoOfPlayersWhoHaveCards () == 0) {
			if (Player.Instance.GameCreator)
				SetNewCards ();
			return;
		}
		JackarooManager7.Instance.RefreshOtherPlayersCards ();
		int PlayerIDTurnToPlay = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		PlayingWindowsManager7.Instance.SetTimer (FromOnlineToLocal (PlayerIDTurnToPlay),
			TimerWaitingTime (PlayerIDTurnToPlay), 1, false);
		JackarooManager7.Instance.SetAndArrangeMyCards ();
		if (!MyTurnToPlay ())
			return;
		if (NoOfStonesOnBoard (PlayerIDTurnToPlay) == 0 && !PlayerHaveAceOrSheikh ())
			StartCoroutine (RemoveMyCardsSlowly ());
//		else
//			SetAvailableCards ();//If he want to do it
	}

	private IEnumerator RemoveMyCardsSlowly ()
	{
		yield return new WaitForSeconds (1);
		IfPlayerHoldingCardGetItBack ();
		List<string> CardsList = TempCurrenRoom.GameOB.Cards.PlayersCards [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay].PlayerCardsList;
		for (int i = 0; i < CardsList.Count; i++)
			CardsList [i] = "0";
		TempCurrenRoom.ActionID = 5;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = NextNO (TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay);
		PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void SetAvailableCards ()
	{
		return;
//		int PlayerID = TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
//		//Watcher
//		bool MakeThemAllAvailbae = false;
//		if (Player.Instance.OnlinePlayerID == -1)
//			return;
//		IfPlayerHoldingCardGetItBack ();
//		for (int i = 0; i < CardsListGO.transform.childCount; i++)
//			CardAvailability (CardsListGO.transform.GetChild (i).gameObject, false);
//		for (int i = 0; i < CardsListGO.transform.childCount; i++) {
//			if (!CardsListGO.transform.GetChild (i).gameObject.activeSelf)
//				continue;
//			if (TempCurrenRoom.GameOB.JackarooDetailsOB.Complex && GetCardNO (CardsListGO.transform.GetChild (i).GetComponent<Card> ().CardID) == 10) {
//				CardAvailability (CardsListGO.transform.GetChild (i).gameObject, true);
//				MakeThemAllAvailbae = true;
//				continue;
//			}
//			if (TempCurrenRoom.GameOB.JackarooDetailsOB.Complex && GetCardNO (CardsListGO.transform.GetChild (i).GetComponent<Card> ().CardID) == 5) {
//				if (ThereIsStonesCanMove5Steps ()) {
//					CardAvailability (CardsListGO.transform.GetChild (i).gameObject, true);
//					MakeThemAllAvailbae = true;
//				}
//				continue;
//			}
//			int CardNO = GetCardNO (CardsListGO.transform.GetChild (i).GetComponent<Card> ().CardID);
//			for (int u = 0; u < TempCurrenRoom.GameOB.Cards.PlayersCards [0].PlayerCardsList.Count; u++) {
//				if (!CardsListGO.transform.GetChild (u).gameObject.activeSelf)
//					continue;
//				if (PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerID]] [u].GetComponent<Stone> ().CanBePlayedWithThisCard (CardNO) != 0) {
//					CardAvailability (CardsListGO.transform.GetChild (i).gameObject, true);
//					MakeThemAllAvailbae = true;
//					break;
//				}
//			}
//		}
//		if (!MakeThemAllAvailbae)
//			for (int i = 0; i < CardsListGO.transform.childCount; i++)
//				CardAvailability (CardsListGO.transform.GetChild (i).gameObject, true);
	}

	bool ThereIsStonesCanMove5Steps ()
	{
		for (int i = 0; i < 4; i++)
			for (int u = 0; u < 4; u++)
				if (PlayersStonesContainter [i] [u].GetComponent<Stone> ().CanBePlayedWithThisCard (5) == 3)
					return true;
		return false;
	}

	private int NoOfStonesOnBoard (int PlayerID)
	{
		int StonesCounter = 0;
		for (int i = 0; i < 4; i++)
			if (PlayersStonesContainter [TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers [PlayerID]] [i].GetComponent<Stone> ().CurrentPlace != -1)
				StonesCounter++;
		return StonesCounter;
	}

	bool PlayerHaveAceOrSheikh ()
	{
		List<string> ThisCardsList = TempCurrenRoom.GameOB.Cards.PlayersCards [TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay].PlayerCardsList;
		for (int i = 0; i < ThisCardsList.Count; i++)
			if (ThisCardsList [i].Length > 2 && (GetCardNO (ThisCardsList [i]) == 13 || GetCardNO (ThisCardsList [i]) == 14))
				return true;
		return false;
	}

	private int NextNO (int NextThisNO)
	{
		if (NoOfPlayersWhoHaveCards () <= 1)
			return OnlyPlayerWhoHaveCards ();
		int ThisInt = NextThisNO + 1;
		if (ThisInt > 3)
			ThisInt = 0;
		if (HaveCards (ThisInt))
			return ThisInt;
		else
			return NextNO (ThisInt);
	}

	int OnlyPlayerWhoHaveCards ()
	{
		for (int i = 0; i < 4; i++) {
			List<string> ThisCardsList = TempCurrenRoom.GameOB.Cards.PlayersCards [i].PlayerCardsList;
			for (int u = 0; u < ThisCardsList.Count; u++)
				if (ThisCardsList [u].Length > 2)
					return i;
		}
		return -1;
	}

	private int NoOfPlayersWhoHaveCards ()
	{
		int Counter = 0;
		for (int i = 0; i < 4; i++)
			if (HaveCards (i))
				Counter++;
		return Counter;
	}

	private bool HaveCards (int PlayerNO)
	{
		ThisPlayerCardsList = TempCurrenRoom.GameOB.
			Cards.PlayersCards [PlayerNO].PlayerCardsList;
		for (int i = 0; i < ThisPlayerCardsList.Count; i++)
			if (ThisPlayerCardsList [i].Length > 2)
				return true;
		return false;
	}

	private bool SceneNotReady;

	void UpdateRoomPhase4 ()
	{
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (PlayerID == -1)
			return;
		JackarooManager7.Instance.ArrangeCards ();
		SceneNotReady = true;
		JackarooManager7.Instance.Cards.PlayersCards [PlayerID].PlayerCardsList =
			TempCurrenRoom.GameOB.Cards.PlayersCards [PlayerID].PlayerCardsList;
		JackarooManager7.Instance.SetAndArrangeMyCards ();
		if (!Player.Instance.GameCreator)
			return;
		TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay = TempCurrenRoom.GameOB.JackarooDetailsOB.PlayerWhoStartGame;
		TempCurrenRoom.ActionID = 5;
		PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void SetNewCards ()
	{
		TempCurrenRoom.ActionID = -1;
		TempCurrenRoom.Available = false;
		TempCurrenRoom.GameOB.JackarooDetailsOB.RoundNO++;
		if (TempCurrenRoom.GameOB.JackarooDetailsOB.RoundNO >= 4) {//< to >
			TempCurrenRoom.GameOB.JackarooDetailsOB.RoundNO = 1;
			TempCurrenRoom.ReloadScene = true;
			TempCurrenRoom.ActionID = 0;
		}
		PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void UpdateRoomPhase3 ()
	{
		int PlayerID = Player.Instance.OnlinePlayerID;
		if (PlayerID == -1)
			return;
		if (SceneNotReady)
			return;
		if (IsAllPlayersReady () && Player.Instance.GameCreator) {
			TempCurrenRoom.ActionID = -1;
			PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
			return;
		}
		UpdateRoomPhase2 ();
		if (Player.Instance.OnlinePlayerID!=-1&&TempCurrenRoom.PlayersReady [PlayerID])
			return;
		PlayingWindowsManager7.Instance.BackGroundCardGO.SetActive (true);
		IfPlayerHoldingCardGetItBack ();
		PlayingFireBaseManager7.Instance.SetOnDissconnectHandle ();
		JackarooManager7.Instance.AssignPlayerCardsAndActivateOthersCards (true);
		PlayingWindowsManager7.Instance.WinsWindow.SetActive (false);
		TempCurrenRoom.PlayersReady [PlayerID] = true;
		RotateTableAndSetPlacesColors ();
		SetTheFukinStones ();
		if (Player.Instance.GameCreator)
			SetReadyForComputer ();
		PlayingFireBaseManager7.Instance.UploudThisJson (TempCurrenRoom, true);
	}

	void SetTheFukinStones ()
	{
		for (int i = 0; i < 4; i++)
			for (int u = 0; u < 4; u++) {
				int ThisStonePlace = TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersStones [i].Stones [u];
				if (ThisStonePlace != -1)
					PlayersStonesContainter [i] [u].GetComponent<Stone> ().MoveImediatly (ThisStonePlace);
			}
	}

	void SetReadyForComputer ()
	{
		for (int i = 0; i < 4; i++)
			if (TempCurrenRoom.SeatsStatus [i] == 0)
				TempCurrenRoom.PlayersReady [i] = true;
	}

	bool IsAllPlayersReady ()
	{
		for (int i = 0; i < TempCurrenRoom.GameOB.PlayersNO; i++)
			if (!TempCurrenRoom.PlayersReady [i])
				return false;
		return true;
	}

	#region implemented abstract members of PlayersManagerParent

	public override void OnApplicationPause (bool pauseStatus)
	{
		if (TempCurrenRoom.ActionID < 4)
			return;
		try {
			if (pauseStatus)
				CreatorPauseOrExit (false);
			else
				StartCoroutine (SetandarrangeCor (pauseStatus));
		} catch {
			//Debug.Log ("OnApplicationPause\t" + e.Message);
		}
	}

	#endregion

	private IEnumerator SetandarrangeCor (bool pauseStatus)
	{
		ShowBackBTN ();
		yield return new WaitForSeconds (2.5f);
		try {
			JackarooManager7.Instance.SetAndArrangeMyCards ();
		} catch {
		}
	}


	#region implemented abstract members of PlayersManagerParent

	public override void IamBack ()
	{
		TempCurrenRoom.SeatsStatus [Player.Instance.OnlinePlayerID] = 1;
		PlayerIsBack (true);
		PlayingFireBaseFather.ParentInstance.UploudThisJson (TempCurrenRoom, false);
	}

	#endregion

	private void OnDestroy ()
	{
		StopAllCoroutines ();
	}
}