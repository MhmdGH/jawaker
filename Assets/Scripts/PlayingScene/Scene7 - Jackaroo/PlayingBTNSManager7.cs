﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayingBTNSManager7 : PlayingBTNSManagerFather {
	
	public static PlayingBTNSManager7 Instance;

	protected override void Awake(){
		base.Awake ();
		Instance = this;
	}

	public override void BTNSFunction(){
		base.BTNSFunction ();
		if(CurrentBTNGO.transform.parent.name.Contains("BiddingWindow"))
			CurrentBTNGO.transform.parent.gameObject.SetActive (false);
		if (BTNName == ("RestartBTN"))
			PlayersManager7.Instance.RestartGame ();
		else if(CurrentBTNGO.transform.parent.name=="StonesSelection")//Stone Selection
			PlayersManager7.Instance.SelectStoneNoToMove (CurrentBTNGO.transform.GetSiblingIndex());
		else if(CurrentBTNGO.transform.parent.name=="AceSelection")
			PlayersManager7.Instance.AceChoose1Or11 (int.Parse(GetChar(BTNName,0)+GetChar(BTNName,1)));
	}
}