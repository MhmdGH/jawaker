﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Database;
using Firebase;
using Firebase.Unity.Editor;
using System;

public class PlayingFireBaseManager7 : PlayingFireBaseFather
{

	public static PlayingFireBaseManager7 Instance;

	public override void StartGame ()
	{	
		TempRoomToUpload.GameOB.JackarooDetailsOB.Complex = MainFireBaseManager.GameType.Contains ("Complex");
		base.StartGame ();
	}

	#region implemented abstract members of PlayingFireBaseFather

	protected override void SetInitials ()
	{
		TempRoomToUpload = new JsonRoomO ("", new string[4]{ "1", "2", "3", "4" }, new Game7 ());
		CurrentRoomUpdate = TempRoomToUpload;
	}

	#endregion

	public override void Awake ()
	{
		base.Awake ();
		Instance = this;
	}

	public override void UpdateRoomNow (string UpdatedRoomJson)
	{
		//Default Values
		try{
			JsonUtility.FromJsonOverwrite (UpdatedRoomJson, CurrentRoomUpdate);}catch(Exception e){
			Debug.Log (e.Message);
		}
		PlayersManager7.Instance.UpdateRoom (CurrentRoomUpdate);
	}
}