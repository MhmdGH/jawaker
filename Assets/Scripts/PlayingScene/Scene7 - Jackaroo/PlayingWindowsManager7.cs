﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayingWindowsManager7 : PlayingWindowsManagerFather {

	public static PlayingWindowsManager7 Instance;
	public GameObject[] StoneSelections;
	public GameObject ShowAce1Or11GO,BackGroundCardGO;

	public override void Awake(){
		base.Awake ();
		Instance = this;
	}

	public override void ShowAndSetPartnersPhotos (string[] IMGSUrl)
	{
		OnStartBTNGO.SetActive (false);
		ChoosePartnerGO.SetActive (true);
		for (int i = 0; i < ChooseParternersGOs.Length; i++)
			PlayingFBManager7.Instance.SetFPPhoto (IMGSUrl [i], ChooseParternersGOs [i].GetComponent<Image> ());
	}

	public override void StopTimer(){
		try{
		base.StopTimer ();
		}catch{
		}
	}

	protected override IEnumerator StartThisTimer(int PlayerNO,float WaitingTime,int FunToDOAfterEnd,bool JustCreatorDoIT){
		PlayersTimer [PlayerNO].SetActive (true);
		WaitingTime = WaitingTime*SliderSmothnessMultiplier;
		float ActualTimerTikValue = TimerTikValue*SliderSmothnessMultiplier;
		PlayersTimer [PlayerNO].GetComponent<Slider> ().maxValue = WaitingTime;
		PlayersTimer [PlayerNO].GetComponent<Slider> ().value = WaitingTime;
		while (WaitingTime > 0) {
			PlayersTimer [PlayerNO].GetComponent<Slider> ().value -= ActualTimerTikValue;
			WaitingTime-=ActualTimerTikValue;
			yield return new WaitForSeconds (TimerTikValue*0.1f);
		}
		PlayersTimer [PlayerNO].SetActive (false);
		if (JustCreatorDoIT && Player.Instance.GameCreator)
			PlayersManager7.Instance.MasterTakeControl (FunToDOAfterEnd);
		else if(!JustCreatorDoIT)
			PlayersManager7.Instance.MasterTakeControl (FunToDOAfterEnd);
	}

	private void OnDestroy(){
		StopAllCoroutines ();
	}
}