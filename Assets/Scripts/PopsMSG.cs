﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ArabicSupport;

public class PopsMSG : MonoBehaviour {

	public static PopsMSG Instance;
	[HideInInspector]
	public GameObject ContainerGO;
	private Text MSGGOTxt;

	void Awake(){
		Instance = this;
		AssignGO ();
	}

	void AssignGO ()
	{
		ContainerGO = GetComponent<RectTransform> ().GetChild (0).gameObject;
		MSGGOTxt = ContainerGO.GetComponent<RectTransform>().GetChild(1).GetChild(1).gameObject.GetComponent<Text>();
	}

	public void OkBTN(){
		ContainerGO.SetActive (false);
	}

	public void ShowPopMSG(string MSGBody){
		if (MainLanguageManager.Instance.IsArabic) {
			if (MSGBody == "Purchase Success!")
				MSGBody = ArabicFixer.Fix ("تم الشراء\n بنجاح!");
			else if (MSGBody == "Only Basha Can Create Compititions")
				MSGBody = ArabicFixer.Fix ("فقط الباشا يستطيع \nإنشاء مسابقات");
			else if (MSGBody == "Please Wait!")
				MSGBody = ArabicFixer.Fix ("من فضلك \n انتظر");
			else if (MSGBody == "No Enough Coins")
				MSGBody = ArabicFixer.Fix ("لاتملك نقود كافية");
			else if (MSGBody == "No Enough Coins!")
				MSGBody = ArabicFixer.Fix ("لاتملك نقود كافية");
			else if (MSGBody=="Color Selected!") {
				Debug.Log ("2");
				MSGBody = ArabicFixer.Fix ("تم إختيار\n اللون!");
			}
			else if (MSGBody == "Under Development")
				MSGBody = ArabicFixer.Fix ("تحت التطوير");
			else if (MSGBody == "You've Already Have A Booster!")
				MSGBody = ArabicFixer.Fix ("أنت تملك \nمسرع حاليا!");
			else if(MSGBody =="No Available Rooms!")
				MSGBody = ArabicFixer.Fix ("لا توجد \nألعاب متاحة!");
			else if(MSGBody =="Send Successfully!")
				MSGBody = ArabicFixer.Fix ("تم ارسال \n النقود بنجاح!");
			else if(MSGBody=="Purchase Failed!")
				MSGBody = ArabicFixer.Fix ("فشل الشراء!");
			else if(MSGBody=="You Have No Friends!")
				MSGBody = ArabicFixer.Fix ("لايوجد لديك \n أصدقاء حاليا!");
			else if(MSGBody=="There No Notification!")
				MSGBody = ArabicFixer.Fix ("لا إشعارات \n لديك!");
			else if(MSGBody=="Please Choose Another Game")
				MSGBody = ArabicFixer.Fix ("اختر لعبة اخرى لو سمحت");
			else if(MSGBody=="If You Leave, You Will Lose 500 Coins!")
				MSGBody = ArabicFixer.Fix ("اذا خرجت\n ستخسر 500 ذهبة");
			else if(MSGBody=="Master Kicked You!")
				MSGBody = ArabicFixer.Fix ("لقد تم طردك!");
			else if(MSGBody=="You are already joined a club")
				MSGBody = ArabicFixer.Fix ("انت منضم \n لنادي الان");
			else if(MSGBody=="You are already sent a request")
				MSGBody = ArabicFixer.Fix ("لقد قمت\n بارسال طلب");
			else if(MSGBody=="You are already sent a request Or already Joined a Competition")
				MSGBody = ArabicFixer.Fix ("لقد قمت\n  بارسال طلب  \n او منضم لمسابقة");
			else if(MSGBody=="Competition is full!")
				MSGBody = ArabicFixer.Fix ("المسابقة ممتلئة");
			else if(MSGBody=="Request Sent Successfully")
				MSGBody = ArabicFixer.Fix ("لقد تم \n ارسال الطلب بنجاح");
			else if(MSGBody=="You are already in a competition!")
				MSGBody = ArabicFixer.Fix ("انت منضم \n لمسابقة");
			else if(MSGBody=="Enter Valid Value")
				MSGBody = ArabicFixer.Fix ("ادخل قيمة صحيحة");
			else if(MSGBody=="Player Added Successfully!")
				MSGBody = ArabicFixer.Fix ("تم الارسال");
			
		}
		MSGGOTxt.text = MSGBody;
		ContainerGO.SetActive (true);
	}
}