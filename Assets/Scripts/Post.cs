﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public class Post : MonoBehaviour {

	public Image CreatorIMG;
	public Text CreatorName,LikesNOTxt,CommentsNOTxt,PostMSGBodyTxt;
	public GameObject ReadMoreGO;
	private PostO MyPostO;

	public void SetPostDetails(PostO ThisPostO){
		MyPostO = ThisPostO;
		FBManager.Instance.SetFPPhoto (MyPostO.CreatorFBID,CreatorIMG);
		CreatorName.text = MyPostO.CreatorName;
		if (MainLanguageManager.Instance.IsArabic) {
			LikesNOTxt.text = ArabicFixer.Fix("لايك)"+MyPostO.LikesNO+"(");
			CommentsNOTxt.text = ArabicFixer.Fix("تعليق)"+MyPostO.CommentsNO+"(");
			ReadMoreGO.GetComponent<Text> ().text = ArabicFixer.Fix("المزيد");
		}else{
			LikesNOTxt.text = "Likes("+MyPostO.LikesNO+")";
			CommentsNOTxt.text = "Comment("+MyPostO.CommentsNO+")";
		}
		PostMSGBodyTxt.text = MyPostO.MSGBody;
		ReadMoreGO.SetActive (MyPostO.MSGBody.Length > 20);
	}

	public void OnPostClick(){
		ChatScreenManager.Instance.ShowThisPostDetails (MyPostO);
	}
}