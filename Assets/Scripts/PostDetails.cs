using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using ArabicSupport;
using AdvancedInputFieldPlugin;

public class PostDetails :MonoBehaviour
{
	public static PostDetails Instance;
	public Image CreatorIMG,LikeIMG;
	public Text CreatorNameTxt,PostBTNTxt,CommentBTNTxt,PostMSGBodyTxt;
	public RectTransform CommentsListContent;
	[HideInInspector]
	public PostO MyPostO;
	public GameObject CommentPrefab,PostConteinerGO,CommentsContainerGO,ConteinerGO;
	public AdvancedInputField CommentInputFeild;

	void Awake(){
		Instance=this;
	}

	void Start(){
		PostO Newpost = new PostO ();
	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.Escape))
		if (ConteinerGO.activeSelf)
			ConteinerGO.SetActive (false);
	}

	public void ShowConteiner(int ContainerNO){
		PostConteinerGO.SetActive (ContainerNO==0);
		CommentsContainerGO.SetActive (ContainerNO==1);
		if (ContainerNO == 1)
			RefreshCommentList ();
		if (ContainerNO == 0) {
			PostBTNTxt.color = Color.yellow;
			CommentBTNTxt.color = Color.white;
		} else {
			PostBTNTxt.color = Color.white;
			CommentBTNTxt.color = Color.yellow;
		}
	}

	public void ShowThisPostDetails(PostO ThisPost){
		ConteinerGO.SetActive (true);
		ShowConteiner (0);
		WindowsManager.Instance.ShowLoader (0.5f);
		MyPostO = ThisPost;
		string hola="";
		FBManager.Instance.SetFPPhoto (MyPostO.CreatorFBID,CreatorIMG);
		CreatorNameTxt.text = MyPostO.CreatorName;
		MyPostO.MSGBody = SpliceText (MyPostO.MSGBody,25);
		hola = ArabicFixer.Fix (MyPostO.MSGBody,false,false);
		PostMSGBodyTxt.text = hola;
		MainFireBaseManager.Instance.CheckIfIHitLikeOnThisPost (MyPostO.PostID,true);
	}
			
	public static string SpliceText(string inputText, int lineLength) {

		string[] stringSplit = inputText.Split(' ');
		int charCounter = 0;
		string finalString = "";

		for(int i=0; i < stringSplit.Length; i++){
			finalString += stringSplit[i] + " ";
			charCounter += stringSplit[i].Length;

			if(charCounter > lineLength){
				finalString += "\n";
				charCounter = 0;
			}
		}
		return finalString;
	}

	private string GetChar (string ThisString, int IntIndex)
	{
		return ThisString.ToCharArray ().GetValue (IntIndex).ToString ();
	}

	public void RefreshCommentList(){
		MainFireBaseManager.Instance.RefreshThisPostComments (CommentsListContent,MyPostO.PostID);
	}

	public void AddThisComment(CommentO ThisCommentO){
		GameObject NewCommentPrefab = Instantiate (CommentPrefab,CommentsListContent) as GameObject;
		NewCommentPrefab.GetComponent<ChatComment> ().SetPostDetails (ThisCommentO);
	}

	public void BTNSFunctions(){
		GameObject CurrentBTNGO =EventSystem.current.currentSelectedGameObject;
		string BTNName = CurrentBTNGO.name;
		if (BTNName == "PostBTN")
			ShowConteiner (0);
		else if (BTNName == "CommentsBTN")
			ShowConteiner (1);
		else if (BTNName == "ClosePostDetailsBTN") {
			HideMe ();
			ChatScreenManager.Instance.RefreshChatScreen ();
		} else if (BTNName == "AddCommentBTN")
			AddCommentFun ();
		else if (BTNName == "likeBTN")
			AddLikeToThisPost ();
	}

	void AddLikeToThisPost ()
	{
		if (!ClubsManager.Instance.IsClubMember ()) {
			PopsMSG.Instance.ShowPopMSG ("Members only");
			return;
		}
		MainFireBaseManager.Instance.CheckIfIHitLikeOnThisPost (MyPostO.PostID,false);
	}

	public IEnumerator AddLikeToThisPostAfterChecked(){
		WindowsManager.Instance.ShowLoader (0.5f);
		LikeO NewLike = new LikeO ();
		NewLike.LikeID = RandomIDGenerator.Instance.GetRandomID ();
		NewLike.PlayerID = Player.Instance.DatabasePlayerID;
		MainFireBaseManager.Instance.ToggleNewLikeThisOne (NewLike,MyPostO.PostID,true);
		yield return new WaitForSeconds (0.5f);
		MainFireBaseManager.Instance.CheckIfIHitLikeOnThisPost (MyPostO.PostID,true);
	}

	public void AfterCheckLikeBTN(bool HittedOrNot){
		if (HittedOrNot)
			LikeIMG.color = Color.blue;
		else
			LikeIMG.color = new Color (0,0,1,0.25f);
	}

	void AddCommentFun ()
	{
		if (!ClubsManager.Instance.IsClubMember ()) {
			PopsMSG.Instance.ShowPopMSG ("Members only");
			return;
		}
		if (CommentInputFeild.Text.Length < 1)
			return;
		CommentO NewComment = new CommentO ();
		NewComment.CommentID = RandomIDGenerator.Instance.GetRandomID ();
		NewComment.CommentMSG = CommentInputFeild.Text;
		NewComment.PlayerFBID = Player.Instance.FBID;
		MainFireBaseManager.Instance.CreateNewCommentThisOne (NewComment,MyPostO.PostID);
		CommentInputFeild.Text = "";
		RefreshCommentList ();
	}

	void HideMe(){
		ConteinerGO.SetActive (false);
	}
}