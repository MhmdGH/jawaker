using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]
public class PostO
{
	public string PostID,CreatorFBID,CreatorName,MSGBody;
	public int LikesNO,CommentsNO;
	public LikesO Likes;
	public CommentsO Comments;
}