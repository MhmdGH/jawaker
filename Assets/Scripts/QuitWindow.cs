﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuitWindow : MonoBehaviour {

	public static QuitWindow Instance;
	public GameObject ContainerGO;
	public Button YesBTN,NoBTN;
	public Text QuitMSG;

	void Awake(){
		Instance = this;
	}

	void Start(){
		if (LanguangeManagerFather.ParentInstance.IsArabic)
			QuitMSG.text = ArabicSupport.ArabicFixer.Fix ("الخروج من اللعبة؟");
		YesBTN.onClick.AddListener (YesBTNClicked);
		NoBTN.onClick.AddListener (NoBTNClicked);
	}

	void YesBTNClicked ()
	{
		PlayingBTNSManagerFather.ParentInstance.QuitNow ();
	}


	void NoBTNClicked ()
	{
		ContainerGO.SetActive (false);
	}
}