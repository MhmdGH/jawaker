﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomsFather : MonoBehaviour {

	public static RoomsFather Instance;
	private RectTransform MyRectTransform;

	void Awake(){
		Instance=this;
	}

	void Start(){
		MyRectTransform = GetComponent<RectTransform> ();
	}

	public void JoinFirstAvailbeRoom(){
		Debug.Log ("JoinFirstAvailbeRoom");
		if (MyRectTransform.childCount > 0)
			MyRectTransform.GetChild (0).GetComponent<Room> ().ClickOnRoomBTN ();
		else
			PopsMSG.Instance.ShowPopMSG ("No Available Rooms!");
	}
}