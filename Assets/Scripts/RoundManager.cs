﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundManager : MonoBehaviour {

	public GameObject[] RoundsTables;

	public void SetRoundPlayers(List<OneRound> TheseRounds){
		for (int i = 0; i < RoundsTables.Length; i++)
			RoundsTables [i].GetComponent<CompTableItem> ().SetPlayersPhotos (TheseRounds[i].PlayersFBID);
	}

	public void ActivateOrNot(bool ActiveTables){
		for (int i = 0; i < RoundsTables.Length; i++)
			RoundsTables [i].GetComponent<CompTableItem> ().ActiveOrNot (ActiveTables);
	}
}