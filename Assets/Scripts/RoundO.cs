using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]
public class RoundO
{
	public List<OneRound> Rounds;
	public RoundO(int RoundLevel){
		Rounds = new List<OneRound> (RoundLevel);
		for(int i=0;i<RoundLevel;i++)
			Rounds.Add (new OneRound());
	}
}
