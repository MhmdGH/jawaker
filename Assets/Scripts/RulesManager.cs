﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RulesManager : MonoBehaviour {

	public static RulesManager Instance;
	[HideInInspector]
	public bool IsShowed;
	public GameObject ContainerGO;
	public GameObject[] RulesWindowsGO;
	public string[] RulesNames;
	public Text RulesNameTxt;

	void Awake(){
		Instance=this;
	}

	public void ShowTheseRules(int RulesNO){
		IsShowed = true;
		ContainerGO.SetActive (true);
		for (int i = 0; i < RulesWindowsGO.Length; i++)
			RulesWindowsGO [i].SetActive (false);
		RulesWindowsGO [RulesNO].SetActive (true);
		RulesNameTxt.text = RulesNames [RulesNO];
	}

	public void HideRules(){
		IsShowed = false;
		ContainerGO.SetActive (false);
	}
}