﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SearchResaults : MonoBehaviour {

	public Image ResaultsIMG;
	public Text ResaultsNameTxt;
	[HideInInspector]
	public OnlinePlayerO ThisPlayerResault;

	public void SetResaultsDetails(OnlinePlayerO NewResault){
		ThisPlayerResault = NewResault;
		FBManager.Instance.SetFPPhoto (ThisPlayerResault.FBID,ResaultsIMG);
		ResaultsNameTxt.text = ThisPlayerResault.PlayerName;
		GetComponent<Button> ().onClick.AddListener (ShowPlayerDataOnClick);
	}

	private void ShowPlayerDataOnClick(){
		OnlinePlayerData.Instance.ShowDataForThisPlayer(ThisPlayerResault);
	}
}