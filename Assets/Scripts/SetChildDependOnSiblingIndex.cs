﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetChildDependOnSiblingIndex : MonoBehaviour {

	void Start(){
		name = GetComponent<RectTransform> ().GetSiblingIndex ()+1+"";
		GetComponent<Text> ().text = name;
	}
}