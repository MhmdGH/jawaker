﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderManager : MonoBehaviour {

	public Text MinLevelNoTxt;

	public void OnValueChanged(){
		MinLevelNoTxt.text = GetComponent<Slider> ().value.ToString ("0");
	}

}