﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stone : MonoBehaviour
{
	[HideInInspector]
	public int CurrentPlace, StoneOrder, OwnerID, TargetedPlace,SafeZoneStartPos;
	[HideInInspector]
	public Vector2 StartPos;
	[HideInInspector]
	public RectTransform MyRectTransform;
	[HideInInspector]
	public bool InTheSafeZone,DevideChoosed3Or4;
	[HideInInspector]
	public GameObject SwitchBTNGO,Move5BTNGO;

	void Awake ()
	{
		PlayerPrefs.DeleteAll ();
		PlayerPrefs.Save ();
		MyRectTransform = GetComponent<RectTransform> ();
		AssignStoneBTNSStuff ();
	}

	void AssignStoneBTNSStuff ()
	{
		SwitchBTNGO = transform.GetChild (0).gameObject;
		SwitchBTNGO.GetComponent<Button>().onClick.AddListener(() => StoneBTNClick(true));
		Move5BTNGO = transform.GetChild (1).gameObject;
		Move5BTNGO.GetComponent<Button>().onClick.AddListener(() => StoneBTNClick(false));
	}

	void Start ()
	{
		InTheSafeZone = false;
		TargetedPlace = -1;
		StoneOrder = transform.GetSiblingIndex ();
		StartPos = GetComponent<RectTransform> ().localPosition;
		OwnerID = int.Parse (GetChar (name, 0));
		CurrentPlace = -1;
		SafeZoneStartPos = SetThisStoneBasePos ();
	}

	int SetThisStoneBasePos ()
	{
		if (OwnerID == 0)
			return 74;
		else if (OwnerID == 1)
			return 17;
		else if (OwnerID == 2)
			return 36;
		else
			return 55;
	}

	public void StoneBTNClick(bool SwitchOrMove5){
		if (SwitchOrMove5) {
			PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.SwitchStone.Switcher2ID = OwnerID;
			PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.SwitchStone.SwitchStone2NO = StoneOrder;
		}
		int PlayerID = OwnerID;
		int StoneNO = StoneOrder;
		PlayersManager7.Instance.StoneBTNClick (SwitchOrMove5,PlayerID,StoneNO);
	}

	private string GetChar (string ThisString, int IntIndex)
	{
		return ThisString.ToCharArray ().GetValue (IntIndex).ToString ();
	}

	public int AddTheseSteps(int StepsNo){
		bool ThisIneTheSafeZone = false;
		int StepsInTheSafeZone=-1,Acutali;
		if (CurrentPlace == SafeZoneStartPos)
			ThisIneTheSafeZone = true;
		if (InTheSafeZone)
			return StepsNo + CurrentPlace;
		for (int i = CurrentPlace + 1; i <= CurrentPlace + StepsNo; i++) {
			Acutali = ActualPlace (i);
			if (Acutali == SafeZoneStartPos||ThisIneTheSafeZone) {
				if(Acutali==CurrentPlace + StepsNo&&StepsInTheSafeZone==-1)
					return ActualPlace(CurrentPlace + StepsNo);
				ThisIneTheSafeZone = true;
				StepsInTheSafeZone++;
				continue;
			}
		}
		if (ThisIneTheSafeZone) {
			return ActualPlace (StepsInTheSafeZone + SafeZoneConstant ());
		}
		else
			return ActualPlace(CurrentPlace + StepsNo);			
	}

	public int SafeZoneConstant ()
	{
		return (OwnerID+1) * 100;
	}

	public void Move (int TargetedPlaceNO)
	{
		int PlaydCard = 0;
		if(PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.CurrentCardID.Length>2)
			PlaydCard = PlayersManager7.Instance.GetCardNO (PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.CurrentCardID);
		if (PlaydCard==11) {
			int SwitcherNO2 = PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.SwitchStone.Switcher2ID;
			int SwitchedStoneNo2 = PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.SwitchStone.SwitchStone2NO;
			if (OwnerID != SwitcherNO2 || StoneOrder != SwitchedStoneNo2) {
				PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersStones [SwitcherNO2].Stones [SwitchedStoneNo2] = CurrentPlace;
				PlayersManager7.Instance.PlayersStonesContainter [SwitcherNO2] [SwitchedStoneNo2].GetComponent<Stone> ().Move (CurrentPlace);
			}
		} 
		else if (PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.Complex && PlaydCard == 13&&
			CurrentPlace!=-1) 
			RemoveAllStonesInTheWay (CurrentPlace);
		CurrentPlace = ActualPlace (TargetedPlaceNO);
		if (PlaydCard != 11 && !InTheSafeZone && ThereIsAfukinStoneHere (CurrentPlace)) {
			PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersStones [PlayerNoStoneRemove].Stones [StoneNoToRemove] = -1;
			PlayersManager7.Instance.PlayersStonesContainter [PlayerNoStoneRemove] [StoneNoToRemove].GetComponent<Stone> ().RemoveStone ();
		}
		StartCoroutine (MoveStone (TargetedPlaceNO));
	}
		
	private IEnumerator MoveStone (int TargetedPlaceNO)
	{
		if (TargetedPlaceNO > 100) {
			InTheSafeZone = true;
			TargetedPlaceNO -= SafeZoneConstant ();
		}
		float time = 0f;
		Vector2 StartPosition = MyRectTransform.localPosition;
		Vector2 EndPosition = new Vector2 ();
		if (InTheSafeZone) {
			if (TargetedPlaceNO == 0)
				TargetedPlaceNO = 1;
			EndPosition = PlayersManager7.Instance.SafeZoneParents [OwnerID].transform.GetChild (TargetedPlaceNO - 1).transform.gameObject.GetComponent<RectTransform> ().localPosition;
		}
		else
			EndPosition = PlayersManager7.Instance.PlacesGO [ActualPlace (TargetedPlaceNO)].GetComponent<RectTransform> ().localPosition;	
		float TimeToMove = 0.2f;
		while (time < TimeToMove) {
			MyRectTransform.localPosition = Vector2.Lerp (StartPosition, EndPosition, time / TimeToMove);
			time = time + Time.deltaTime;
			yield return null;
		}
		MyRectTransform.localPosition = EndPosition;
	}

	public void MoveImediatly(int TargetedPlaceNO){
		CurrentPlace = ActualPlace (TargetedPlaceNO);
		if (TargetedPlaceNO > 100) {
			InTheSafeZone = true;
			TargetedPlaceNO -= SafeZoneConstant ();
		}
		Vector2 StartPosition = MyRectTransform.localPosition;
		Vector2 EndPosition = new Vector2 ();
		if (InTheSafeZone)
			EndPosition = PlayersManager7.Instance.SafeZoneParents [OwnerID].transform.GetChild (TargetedPlaceNO - 1).transform.gameObject.GetComponent<RectTransform> ().localPosition;
		else
			EndPosition = PlayersManager7.Instance.PlacesGO [ActualPlace (TargetedPlaceNO)].GetComponent<RectTransform> ().localPosition;	
		MyRectTransform.localPosition = EndPosition;
	}

	void RemoveAllStonesInTheWay (int ThisCurrentPlace)
	{
		bool ThisInSafePos = false;
		int SafeZoneCounter=1,Actuali=0;
		for (int i = ThisCurrentPlace + 1; i <= 13 + ThisCurrentPlace; i++) {
			Actuali = ActualPlace (i);
			if (ThisInSafePos) {
				if (ThereIsAfukinStoneHere (Actuali) && !IsABase (Actuali)) {
					PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersStones [PlayerNoStoneRemove].Stones [StoneNoToRemove] = -1;
					PlayersManager7.Instance.PlayersStonesContainter [PlayerNoStoneRemove] [StoneNoToRemove].GetComponent<Stone> ().RemoveStone ();
				}
				SafeZoneCounter++;
				continue;
			}
			if (Actuali == SafeZoneStartPos)
				ThisInSafePos = true;
			if (ThereIsAfukinStoneHere (Actuali) && !IsABase (Actuali)) {
				PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersStones [PlayerNoStoneRemove].Stones [StoneNoToRemove] = -1;
				PlayersManager7.Instance.PlayersStonesContainter [PlayerNoStoneRemove] [StoneNoToRemove].GetComponent<Stone> ().RemoveStone ();
			}
		}	
	}

	public void RemoveStone(){//just a move, no backend
		StartCoroutine (RemoveStoneCor());
	}

	private IEnumerator RemoveStoneCor ()
	{
		CurrentPlace = -1;
		float time = 0f;
		Vector2 StartPosition = MyRectTransform.localPosition;
		Vector2 EndPosition = new Vector2 ();
		EndPosition = StartPos;
		float TimeToMove = 0.2f;
		while (time < TimeToMove) {
			MyRectTransform.localPosition = Vector2.Lerp (StartPosition, EndPosition, time / TimeToMove);
			time = time + Time.deltaTime;
			yield return null;
		}
		MyRectTransform.localPosition = EndPosition;
	}

	int ActualPlace (int targetedPlaceNO)
	{
		if (targetedPlaceNO >= 76 && targetedPlaceNO < 100)
			targetedPlaceNO -= 76;
		else if (targetedPlaceNO < 0)
			targetedPlaceNO += 76;
		return targetedPlaceNO;
	}

	//1 > Set Stone To STart
	//2 > Stone in safe zone
	//3 > Normal Move
	public int CanBePlayedWithThisCard (int CardNO)
	{
		int PlayerIDToPlayHolder = PlayersManager7.Instance.TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		int ControllerPlayerIDToPlayHolder = PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers[PlayerIDToPlayHolder];
		if (CurrentPlace == -1) {//Home Status
			if ((CardNO == 13 || CardNO == 14) && (!ThereIsAfukinStoneHere (GetThisPlayerBase (ControllerPlayerIDToPlayHolder)))) {
				TargetedPlace = 0 + (PlayerIDToPlayHolder * 19);
				return 1;
			} else
				return 0;
		} else if (InTheSafeZone) {//SafeZone
			if (CardNO < 4 || CardNO == 14||CardNO==7) {
				if (CardNO == 14)
					CardNO = 1;
				else if (CardNO == 7) {
					TargetedPlace = 3 + CurrentPlace;
					DevideChoosed3Or4 = true;
					CardNO = 3;
				}
				if ((CurrentPlace - 1 - SafeZoneConstant ()) + CardNO < 4 && CanMoveTheseSteps (CurrentPlace, CardNO)) {
					TargetedPlace = CardNO + CurrentPlace;
					return 2;
				}
				else
					return 0;
			} else
				return 0;
		} else if (CurrentPlace != -1) { //Not in the save zone,not in home
			return CardNoToStoneMoveAlgorithm (CardNO);
		}
		return 0;
	}

	int CardNoToStoneMoveAlgorithm (int CardNO)
	{
		if (CardNO == 11 && CanSwitch ())//Jack to switch
			return 4;
		else if (CardNO == 11 && !CanSwitch ())//Jack to switch
			return 0;
		else if (CardNO == 4 && CanMoveBackWards ()) {
			TargetedPlace = CurrentPlace-4;
			return 5;
		}
		else if (CardNO == 4 && !CanMoveBackWards ())
			return 0;
		else if (CardNO == 7 && CanMoveTheseSteps (CurrentPlace, 4)) {
			DevideChoosed3Or4 = false;
			TargetedPlace =  AddTheseSteps (4);
			return 7;
		} else if (CardNO == 7 && CanMoveTheseSteps (CurrentPlace, 3)) {
			DevideChoosed3Or4 = true;
			TargetedPlace =  AddTheseSteps (3);
			return 7;
		}
		else if (CardNO == 7 && !CanMoveTheseSteps (CurrentPlace,4))
			return 0;
		else if (CardNO == 7 && !CanMoveTheseSteps (CurrentPlace,3))
			return 0;
		else if (CardNO == 14 && CanMoveTheseSteps (CurrentPlace, 11)) //Ace 11
			return 3;
		else if (CardNO == 14 && CanMoveTheseSteps (CurrentPlace, 1)) //Ace 1
			return 6;
		else if (CardNO == 14)
			return 0;
		else if (CanMoveTheseSteps (CurrentPlace, CardNO)) {
			int FromCardNOToActualSteps = GetTheFukinActualSteps (CardNO);
			TargetedPlace = AddTheseSteps (FromCardNOToActualSteps);
			return 3;
		}
		return 0;
	}

	public bool Ace1Or11 (bool OneOr11)
	{
		if (OneOr11 && CanMoveTheseSteps (CurrentPlace, 1))//Ace 1
			return true;
		if (!OneOr11 && CanMoveTheseSteps (CurrentPlace, 11) && CurrentPlace != -1)//Ace 11
			return true;
		return false;
	}

	bool CanMoveBackWards ()
	{
		for (int i = ActualPlace(CurrentPlace - 1); i >= ActualPlace(CurrentPlace - 4); i--) {
			if (!IsThisPlaceAvailabe (i))
				return false;
		}
		return true;
	}

	public int GetTheFukinActualSteps (int cardNO)
	{
		if (cardNO == 14)
			return 11;
		else
			return cardNO;
	}

	public int GetThisPlayerBase (int PlayerNO)
	{
		int ControllerPlayerID = PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers[PlayerNO];
		if (ControllerPlayerID == 0)
			return 0;
		else if (ControllerPlayerID == 1)
			return 19;
		else if (ControllerPlayerID == 2)
			return 38;
		else
			return 57;
	}

	private bool CanMoveTheseSteps (int ThisCurrentPlace, int NoOfSteps)
	{
		bool ThisInSafePos = false;
		int SafeZoneCounter = 1, SafeZoneIterator = 0, Actuali = 0;
		if (InTheSafeZone && NoOfSteps > 4)
			return false;
		for (int i = ThisCurrentPlace + 1; i <= NoOfSteps + ThisCurrentPlace; i++) {
			Actuali = ActualPlace (i);
			if (Actuali-1 == SafeZoneStartPos)
				ThisInSafePos = true;
			if (ThisInSafePos) {
				if (SafeZoneCounter > 4)
					return false;
				SafeZoneIterator = SafeZoneCounter + SafeZoneConstant ();
				if (!IsThisPlaceAvailabe (SafeZoneIterator)) {
					return false;
				}
				SafeZoneCounter++;
				continue;
			}
			if (!IsThisPlaceAvailabe (Actuali))
				return false;
		}
		return true;
	}

	bool IsThisPlaceAvailabe (int PlaceNO)
	{
		PlaceNO = ActualPlace (PlaceNO);
		PlayersStonesO[] ThisPlayersStonesO = PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersStones;
		//loop for all stones if there a base
		int PlayerIDToPlayHolder = PlayersManager7.Instance.TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		int ControllerPlayerIDToPlayHolder = PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers[PlayerIDToPlayHolder];
		for (int i = 0; i < 4; i++) {
			for (int u = 0; u < 4; u++) {
				if (i == ControllerPlayerIDToPlayHolder) {//My Stones Area
					if (ThisPlayersStonesO [i].Stones [u] == PlaceNO) {
						return false;
					}
				} else {
					if (ThisPlayersStonesO [i].Stones [u] == PlaceNO && IsABase (PlaceNO))
						return false;
				}
			}
		}
		return true;
	}

	private bool CanBeSiwthced (int OwnerID, int StoneNO)
	{
		int StonePlcae = PlayersManager7.Instance.PlayersStonesContainter [OwnerID] [StoneNO].GetComponent<Stone> ().CurrentPlace;
		if (StoneIsInTheSafeZone (OwnerID, StoneNO) || IsABase (StonePlcae))
			return false;
		else
			return true;
	}

	bool StoneIsInTheSafeZone (int ThisownerID, int stoneNO)
	{
		return PlayersManager7.Instance.PlayersStonesContainter[ThisownerID][stoneNO].GetComponent<Stone>().InTheSafeZone;
	}

	private bool IsABase (int PlaceNO)
	{
		if (PlaceNO == 0 || PlaceNO == 19 || PlaceNO == 38 || PlaceNO == 57 ||
			ThereIsAfukinStoneHere (PlaceNO - 1))
			return true;
		else
			return false;
	}

	int PlayerNoStoneRemove,StoneNoToRemove;
	bool ThereIsAfukinStoneHere (int PlaceNO)
	{
		PlayersStonesO[] ThisPlayersStonesO = PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersStones;
		for (int i = 0; i < 4; i++) {
			for (int u = 0; u < 4; u++) {
				if (StoneOrder==u&&OwnerID==i)
					continue;
				if (ThisPlayersStonesO [i].Stones [u] == PlaceNO) {
					StoneNoToRemove = u;
					PlayerNoStoneRemove = i;
					return true;
				}
			}
		}
		return false;
	}

	public bool CanSwitch ()
	{
		if (IsABase (CurrentPlace) || InTheSafeZone || CurrentPlace == -1)
			return false;
		int PlayerIDToPlayHolder = PlayersManager7.Instance.TempCurrenRoom.GameOB.CurrentRound.PlayerIDTurnToPlay;
		int PlayerIDToPlayHolderController = PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersIDStonesControllers[PlayerIDToPlayHolder];
		for (int i = 0; i < 4; i++) {
			for (int u = 0; u < 4; u++) {
				if (i == PlayerIDToPlayHolderController && u == StoneOrder)
					continue;
				int StonePlace = PlayersManager7.Instance.TempCurrenRoom.GameOB.JackarooDetailsOB.PlayersStones [i].Stones [u];
				if (CanBeSiwthced (i, u))
					return true;
			}
		}
		return false;
	}
}