using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

[Serializable]
public class StoreDataO
{
	public int[] SeatsColors,Boosters,VipItems,TableTypes,PurchaseDayOfYear,PurchaseYearOfCentury;
	public int[] ClubItems;
	public int BashaDays;
	/*
	 * PurchaseDayOfYear:
	 * 0	=>	Basha
	 * 1	=>	Boosters
	 * 2	=> 	ColorsDays
	 */

	public StoreDataO(){
		SeatsColors = new int[10]{0,0,0,0,0,0,0,0,0,0};
		Boosters = new int[2]{0,0};
		ClubItems = new int[2]{0,0};
		VipItems = new int[2]{0,0};
		TableTypes = new int[1]{0};
		PurchaseDayOfYear = new int[3]{0,0,0};
		PurchaseYearOfCentury = new int[3]{0,0,0};
	}
}