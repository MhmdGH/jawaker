﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class StoreManager : MonoBehaviour {

	public static StoreManager Instance;
	public GameObject ItemGO,StoreItemsTxtBTN,MyItemsTxtBTN,StoreStuffContainerGO,
	StoreListViewGO,SendItemsViewGO,MyITemsListViewGO,ClubsSection,FriendItemPrefab,SendItemGo;
	public GameObject[] SeatsColorsGO,MyItems;
	public Sprite[] IconsSprites;
	[HideInInspector]
	public StoreDataO StoreData,PurchasedData;
	[HideInInspector]
	public StoreItemsPricesO StoreItemsPrices;
	public RectTransform FriendsRect;
	public string CurrentFriendSendID;

	/*
	 * MyItems
	 * 0	=> Basha
	 * 1	=> Booster
	 * 2	=> UpPatch
	 * 3	=> ButtomPatch
	 * */

	/*
	 * IconsSprites Details:
	 * 0=> LightingBolt
	 * 1=> Crown
	 * 2=> PlayerIcon
	 * 3=> CoinIcon
	 * */

	void Awake(){
		Instance=this;
	}

	void Start(){
		SetStoreData ();
		RefreshStoreData ();
		PlayerDetails.Instance.RefreshPlayerDetailsData ();
	}

	void SetStoreData ()
	{
		StoreData = new StoreDataO ();
		PurchasedData = new StoreDataO ();
		StoreItemsPrices = new StoreItemsPricesO ();
		LoadStoreData ();
		CheckStoreDateCounters ();
	}

	void CheckStoreDateCounters ()
	{
		Player.Instance.BashaDays -= DaysCounted(0);
		Player.Instance.BoosterDays -= DaysCounted(1);
		Player.Instance.ColorDays -= DaysCounted(2);
		if (Player.Instance.BashaDays <= 0) {
			Player.Instance.BashaDays = 0;
			PurchasedData.BashaDays = 0;
		}
		if (Player.Instance.BoosterDays <= 0) {
			StoreData.Boosters [0] = 0;
			StoreData.Boosters [1] = 0;
			Player.Instance.BoosterDays = 0;
			PurchasedData.Boosters[0]= 0;
			PurchasedData.Boosters[1]= 0;
		}
		if (Player.Instance.ColorDays <= 0) {
			Player.Instance.ColorDays = 0;
			PurchasedData.SeatsColors [Player.Instance.PlayerColor] = 0;
			Player.Instance.PlayerColor = 0;
		}
		SaveData ();
		Player.Instance.SavePlayerData ();
	}

	private int DaysCounted(int BashaOrBoostersOrColorDays){
		int YearsCounted = DateTime.UtcNow.Year - StoreData.PurchaseYearOfCentury [BashaOrBoostersOrColorDays];
		if (StoreData.PurchaseYearOfCentury [BashaOrBoostersOrColorDays] == 0)
			YearsCounted = 0;
		int DaysCounted = (DateTime.UtcNow.DayOfYear+(YearsCounted*360)) - StoreData.PurchaseDayOfYear [BashaOrBoostersOrColorDays];
		if (StoreData.PurchaseDayOfYear [BashaOrBoostersOrColorDays] == 0)
			DaysCounted = 0;
		return DaysCounted;
	}

	public void SaveData(){
		string StoreDataJson = JsonUtility.ToJson (StoreData);
		PlayerPrefs.SetString ("StoreDataJson",StoreDataJson);
		string PurchasedItems = JsonUtility.ToJson (PurchasedData);
		PlayerPrefs.SetString ("PurchasedData",PurchasedItems);
	}

	public void LoadStoreData(){
		string StoreDataJson = PlayerPrefs.GetString ("StoreDataJson","");
		JsonUtility.FromJsonOverwrite (StoreDataJson,StoreData);
		string PurchasedStoreData = PlayerPrefs.GetString ("PurchasedData","");
		JsonUtility.FromJsonOverwrite (PurchasedStoreData,PurchasedData);
	}

	void RefreshStoreData ()
	{
		for(int i=0;i<SeatsColorsGO.Length;i++)
			if(StoreData.SeatsColors[i]!=0)
				SeatsColorsGO[i].GetComponent<RectTransform>().GetChild(2).GetComponent<Text>().text ="Purchased!";
		RefreshMyItems ();
		ClubsSection.SetActive (Player.Instance.Level>=4);
	}

	private void RefreshMyItems ()
	{
		for(int i=0;i<MyItems.Length;i++)
			MyItems [i].SetActive (false);
		MyItems [0].SetActive (PurchasedData.BashaDays > 0);
		MyItems [1].SetActive (PurchasedData.Boosters[0]>0||PurchasedData.Boosters[1]>0);
		MyItems [2].SetActive (PurchasedData.VipItems[0]>0);
		MyItems [3].SetActive (PurchasedData.VipItems[1]>0);
		MyItems [4].SetActive (PurchasedData.ClubItems[0]>0);
		MyItems [5].SetActive (PurchasedData.ClubItems[1]>0);
		for(int i=6;i<MyItems.Length;i++)
			MyItems [i].SetActive (PurchasedData.SeatsColors[i-6]>0);
	}

	public void BTNSFunction(){
		GameObject CurrentBTNGO = EventSystem.current.currentSelectedGameObject;
		string BTNName = CurrentBTNGO.name;
		if (BTNName == "StoreItemsBTN") {
			MyITemsListViewGO.SetActive (false);
			StoreListViewGO.SetActive (true);
			SendItemsViewGO.SetActive (false);
			CurrentFriendSendID = "";
			StoreItemsTxtBTN.GetComponent<Text> ().color = Color.yellow;
			MyItemsTxtBTN.GetComponent<Text> ().color = Color.white;
			SendItemGo.GetComponent<Text> ().color = Color.white;
		} else if (BTNName == "MyItemsBTN") {
			ShowMyItem ();
		} else if (BTNName == "SendItemToFriend") {
			CurrentFriendSendID = "";
			MyITemsListViewGO.SetActive (false);
			StoreListViewGO.SetActive (false);
			SendItemsViewGO.SetActive (true);
			WindowsManager.Instance.ShowLoader (1);
			StoreItemsTxtBTN.GetComponent<Text> ().color = Color.white;
			MyItemsTxtBTN.GetComponent<Text> ().color = Color.white;
			SendItemGo.GetComponent<Text> ().color = Color.yellow;
			MainFireBaseManager.Instance.GiveMeMyFriendsListForSendItems();
		}else if (BTNName == "CloseBTN")
			StoreStuffContainerGO.SetActive (false);
		else
			BuyITem (BTNName);
	}

	public void ShowMyItem ()
	{
		MyITemsListViewGO.SetActive (true);
		StoreListViewGO.SetActive (false);
		SendItemsViewGO.SetActive (false);
		WindowsManager.Instance.ShowLoader (1);
		StoreItemsTxtBTN.GetComponent<Text> ().color = Color.white;
		MyItemsTxtBTN.GetComponent<Text> ().color = Color.yellow;
		SendItemGo.GetComponent<Text> ().color = Color.white;
	}

	void BuyITem (string ItemName)
	{
		int ItemNO = GetItemNO (ItemName);
		//Seats Colors
		if (ItemName.Contains ("SeatColors")) {
			ColorSelect.Instance.ShowSeatsColors (ItemNO+1);
		}else if (ItemName.Contains ("Basha")) {//Bashas Days
				if (Player.Instance.Coins >= StoreItemsPrices.Basha [ItemNO]) {
					CheckBashaDateCounter ();
				PopsMSG.Instance.ShowPopMSG ("Purchase Success!");
				Player.Instance.Coins -= StoreItemsPrices.Basha [ItemNO];
				PurchasedData.BashaDays = AddThisBashaNo (ItemNO);
				}else
					PopsMSG.Instance.ShowPopMSG ("No Enough Coins!");
		}else if (ItemName.Contains ("Boosters")) {//Boosters
			if (Player.Instance.Coins >= StoreItemsPrices.Boosters [ItemNO]) {
				if (StoreData.Boosters [0] == 0&&StoreData.Boosters [1]==0) {
					CheckBoosterCounter ();
					PopsMSG.Instance.ShowPopMSG ("Purchase Success!");
					Player.Instance.Coins -= StoreItemsPrices.Boosters [ItemNO];
					if (ItemNO == 0)	
						PurchasedData.Boosters[0] = 1;
					else
						PurchasedData.Boosters[0]= 2;
				}else
					PopsMSG.Instance.ShowPopMSG ("You've Already Have A Booster!");
			}else
				PopsMSG.Instance.ShowPopMSG ("No Enough Coins!");
		}else if (ItemName.Contains ("VIP")) {//VIP
			if (Player.Instance.Coins >= StoreItemsPrices.VipItems [ItemNO]) {
				if (StoreData.VipItems [ItemNO]==0) {
					PopsMSG.Instance.ShowPopMSG ("Purchase Success!");
					Player.Instance.Coins -= StoreItemsPrices.VipItems[ItemNO];
					if (ItemNO == 0)
						PurchasedData.VipItems[0]= 1;
					else
						PurchasedData.VipItems[1]= 1;
				}else 
					PopsMSG.Instance.ShowPopMSG ("You've Already Purchased This Item!");
			}else
				PopsMSG.Instance.ShowPopMSG ("No Enough Coins!");
		}else if (ItemName.Contains ("Coins")) {
//				IAPManager.Instance.BuyCoinsPack (ItemNO);
		}else if (ItemName.Contains ("Clubs")) {
			if (ItemNO == 0) {
				if (Player.Instance.Coins >= StoreItemsPrices.Clubs [ItemNO]) {
					if (Player.Instance.CanEditClubColor == 0) {
						PopsMSG.Instance.ShowPopMSG ("Purchase Success!");
						Player.Instance.Coins -= StoreItemsPrices.Clubs[ItemNO];
						PurchasedData.ClubItems [0] = 1;
					} else
						PopsMSG.Instance.ShowPopMSG ("You've Already Purchased This Item!");
				} else
					PopsMSG.Instance.ShowPopMSG ("No Enough Coins!");
			} else if (ItemNO == 1) {
				if (Player.Instance.Coins >= StoreItemsPrices.Clubs [ItemNO]) {
					if (Player.Instance.CanEditClubVip == 0) {
						PopsMSG.Instance.ShowPopMSG ("Purchase Success!");
						Player.Instance.Coins -= StoreItemsPrices.Clubs[ItemNO];
						PurchasedData.ClubItems [1] = 1;
					} else
						PopsMSG.Instance.ShowPopMSG ("You've Already Purchased This Item!");
				} else
					PopsMSG.Instance.ShowPopMSG ("No Enough Coins!");
			}
		}
		SaveAndRefreshNewPlayerData ();
	}

	public void SelectPurchasedItem(){
		GameObject CurrentBTNGO = EventSystem.current.currentSelectedGameObject;
		string BTNName = CurrentBTNGO.name;
		int ItemNo = CurrentBTNGO.transform.GetSiblingIndex ();
		if (ItemNo == 0) {
			if (CurrentFriendSendID.Length > 2) {
				MainFireBaseManager.Instance.SendItemForFriend (CurrentFriendSendID, 1, PurchasedData.BashaDays);
				PurchasedData.BashaDays = 0;
				CurrentFriendSendID = "";
				PopsMSG.Instance.ShowPopMSG ("Item Sent");
			} else {
				PopsMSG.Instance.ShowPopMSG ("Item activated!");
				if (Player.Instance.BashaDays != 0)
					return;
				Player.Instance.Badges [7] = 1;
				Player.Instance.BashaDays = PurchasedData.BashaDays;
			}
		}else if (ItemNo == 1) {
			if (CurrentFriendSendID.Length > 2) {
				if(PurchasedData.Boosters[0]>0)
					MainFireBaseManager.Instance.SendItemForFriend (CurrentFriendSendID, 2, PurchasedData.Boosters[0]);
				else
					MainFireBaseManager.Instance.SendItemForFriend (CurrentFriendSendID, 2, PurchasedData.Boosters[1]);
				PurchasedData.Boosters[0] = 0;
				PurchasedData.Boosters[1] = 0;
				CurrentFriendSendID = "";
				PopsMSG.Instance.ShowPopMSG ("Item Sent");
			} else {
				PopsMSG.Instance.ShowPopMSG ("Item activated!");
				if (Player.Instance.BoosterDays != 0)
					return;
				Player.Instance.BoosterDays = PurchasedData.Boosters [0];
			}
		}else if (ItemNo == 2) {
			if (CurrentFriendSendID.Length > 2) {
				MainFireBaseManager.Instance.SendItemForFriend (CurrentFriendSendID, 3, PurchasedData.VipItems [0]);
				PurchasedData.VipItems [0] = 0;
				CurrentFriendSendID = "";
				PopsMSG.Instance.ShowPopMSG ("Item Sent");
			} else {
				PopsMSG.Instance.ShowPopMSG ("Item activated!");
				Player.Instance.BashaButtomPatch = PurchasedData.VipItems [0];
			}
		}else if (ItemNo == 3) {
			if (CurrentFriendSendID.Length > 2) {
				MainFireBaseManager.Instance.SendItemForFriend (CurrentFriendSendID, 4, PurchasedData.VipItems [1]);
				PurchasedData.VipItems [1] = 0;
				CurrentFriendSendID = "";
				PopsMSG.Instance.ShowPopMSG ("Item Sent");
			} else {
				PopsMSG.Instance.ShowPopMSG ("Item activated!");
				Player.Instance.BashaUpPatch = PurchasedData.VipItems [1];
			}
		}else if (ItemNo == 4) {
			PopsMSG.Instance.ShowPopMSG ("Item activated!");
			Player.Instance.CanEditClubColor= PurchasedData.ClubItems[0];
		}else if (ItemNo == 5) {
			PopsMSG.Instance.ShowPopMSG ("Item activated!");
			Player.Instance.CanEditClubVip  = PurchasedData.ClubItems[1];
		}
		else if (ItemNo >= 6) {
			if (CurrentFriendSendID.Length > 2) {
				MainFireBaseManager.Instance.SendItemForFriend (CurrentFriendSendID, 0, ItemNo);
				PurchasedData.SeatsColors [ItemNo] = 0;
				CurrentFriendSendID = "";
				PopsMSG.Instance.ShowPopMSG ("Item Sent");
			} else {
				Player.Instance.PlayerColor = ItemNo - 5;
				PopsMSG.Instance.ShowPopMSG ("Color Selected");
			}
		}
		SaveAndRefreshNewPlayerData ();
	}

	public void SaveAndRefreshNewPlayerData ()
	{
		SaveData ();
		Player.Instance.SavePlayerData ();
		MainPlayerDetails.Instance.SetPlayerData ();
		PlayerDetails.Instance.RefreshPlayerDetailsData ();
		RefreshStoreData ();
	}

	public void SaveAndRefreshNewPlayerData (bool WithSavePlayerData)
	{
		SaveData ();
		if(WithSavePlayerData)
			Player.Instance.SavePlayerData ();
		MainPlayerDetails.Instance.SetPlayerData ();
		PlayerDetails.Instance.RefreshPlayerDetailsData ();
		RefreshStoreData ();
	}

	void CheckBashaDateCounter ()
	{
		if (Player.Instance.BashaDays == 0) {
			StoreData.PurchaseDayOfYear [0] = DateTime.UtcNow.DayOfYear;
			StoreData.PurchaseYearOfCentury [0] = DateTime.UtcNow.Year;
		}
	}

	void CheckBoosterCounter ()
	{
		if (Player.Instance.BoosterDays == 0) {
			StoreData.PurchaseDayOfYear [1] = DateTime.UtcNow.DayOfYear;
			StoreData.PurchaseYearOfCentury [1] = DateTime.UtcNow.Year;
		}
	}

	public void CheckColorCounter ()
	{
		if (Player.Instance.ColorDays == 0) {
			StoreData.PurchaseDayOfYear [2] = DateTime.UtcNow.DayOfYear;
			StoreData.PurchaseYearOfCentury [2] = DateTime.UtcNow.Year;
		}
	}

	public void ShowMyFriendsList(List<FriendsO> MyFriendsList){
		bool ThereIsFriends = false;
		for (int i = 0; i < FriendsRect.childCount; i++)
			DestroyObject (FriendsRect.GetChild (i).gameObject);
		foreach(FriendsO FriendItem in MyFriendsList){
			ThereIsFriends = true;
			GameObject NewFriendItem = Instantiate (FriendItemPrefab,FriendsRect);
			NewFriendItem.GetComponent<Friend> ().SetFriendDetails(FriendItem,true);
		}
		if (!ThereIsFriends)
			PopsMSG.Instance.ShowPopMSG ("You Have No Friends!");
	}

	private int AddThisBashaNo (int itemNO)
	{
		if (itemNO == 0)
			return 7;
		else if (itemNO == 1)
			return 30;
		else if (itemNO == 2)
			return 90;
		else if (itemNO == 3)
			return 180;
		else
			return 360;
	}

	private int GetItemNO (string ItemName){
		return int.Parse (GetChar (ItemName, 0));
	}

	protected string GetChar(string ThisString,int IntIndex){
		return ThisString.ToCharArray ().GetValue (IntIndex).ToString ();
	}
}