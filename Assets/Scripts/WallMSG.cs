﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WallMSG : MonoBehaviour {

	public Text MSGTxt;

	public void SetMSGDetails(string ThisMSGTxt){
		MSGTxt.text = ThisMSGTxt;
	}
}