﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WhiteWatchers : MonoBehaviour {

	public static WhiteWatchers Instance;
	public RectTransform ListContentRect;
	public GameObject ItemPrefab,ContainerGO;

	void Awake(){
		Instance = this;
	}

	public void ShowWatchersList(){
		Show ();
		for (int i = 0; i < ListContentRect.childCount; i++)
			Destroy (ListContentRect.GetChild(i).gameObject);
		List<string> WatchersNames = PlayersManagerParent.ParentInstance.TempCurrenRoom.WhiteWatchersList;
		for (int i = 0; i < WatchersNames.Count; i++) {
			GameObject NewWatcherGO = Instantiate (ItemPrefab,ListContentRect);
			NewWatcherGO.GetComponent<Text> ().text = WatchersNames [i];
		}
	}

	public void Show(){
		ContainerGO.SetActive (true);
	}

	public void Hide(){
		ContainerGO.SetActive (false);
	}

}